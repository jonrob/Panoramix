/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// HEPVisUtils :
#include <HEPVis/actions/SoCollectAction.h>
#include <HEPVis/actions/SoGL2PSAction.h>
#include <HEPVis/actions/SoPainterAction.h>
#include <HEPVis/misc/SoStyleCache.h>
#include <HEPVis/misc/SoTools.h>
#include <HEPVis/nodekits/SoCanvas.h>
#include <HEPVis/nodekits/SoDisplayRegion.h>
#include <HEPVis/nodekits/SoImageRegion.h>
#include <HEPVis/nodekits/SoPage.h>
#include <HEPVis/nodekits/SoRegion.h>
#include <HEPVis/nodekits/SoTextRegion.h>
#include <HEPVis/nodes/SoEdgedFaceSet.h>
#include <HEPVis/nodes/SoGrid.h>
#include <HEPVis/nodes/SoHighlightMaterial.h>
#include <HEPVis/nodes/SoImage.h>
#include <HEPVis/nodes/SoImageWriter.h>
#include <HEPVis/nodes/SoMarkerSet.h>
#include <HEPVis/nodes/SoStyle.h>
#include <HEPVis/nodes/SoTextHershey.h>
#include <HEPVis/nodes/SoTextTTF.h>
#include <HEPVis/nodes/SoTextVtk.h>
#include <HEPVis/nodes/SoViewportRegion.h>

// HEPVisGeometry (use HEPVisUtils) :
#include <HEPVis/nodes/So2DArrow.h>
#include <HEPVis/nodes/So3DErrorBar.h>
#include <HEPVis/nodes/SoArrow.h>
#include <HEPVis/nodes/SoBox.h>
#include <HEPVis/nodes/SoCircleArc.h>
#include <HEPVis/nodes/SoCons.h>
#include <HEPVis/nodes/SoCoordinateAxis.h>
#include <HEPVis/nodes/SoDisk.h>
#include <HEPVis/nodes/SoEllipsoid.h>
#include <HEPVis/nodes/SoEllipsoidSegment.h>
#include <HEPVis/nodes/SoEllipticalPrism.h>
#include <HEPVis/nodes/SoPcon.h>
#include <HEPVis/nodes/SoPolyVol.h>
#include <HEPVis/nodes/SoPolygon.h>
#include <HEPVis/nodes/SoPolyhedron.h>
#include <HEPVis/nodes/SoQuad.h>
#include <HEPVis/nodes/SoRing.h>
#include <HEPVis/nodes/SoTorus.h>
#include <HEPVis/nodes/SoTrap.h>
#include <HEPVis/nodes/SoTrd.h>
#include <HEPVis/nodes/SoTriangle.h>
#include <HEPVis/nodes/SoTubs.h>
#include <HEPVis/nodes/SoWedge.h>

// HEPVisDetector (use HEPVisGeometry, HEPVisUtils) :
#ifndef __CINT__
#  include <HEPVis/nodekits/SoIdealBeamTrackKit.h>
#  include <HEPVis/nodekits/SoIdealTrackKit.h>
#  include <HEPVis/nodekits/SoMeterStickKit.h>
#endif
#include <HEPVis/nodekits/SoDblSiStripDetKit.h>
#include <HEPVis/nodekits/SoDblSiStripWedgeDetKit.h>
#include <HEPVis/nodekits/SoDetectorExample.h>
#include <HEPVis/nodekits/SoDetectorTreeKit.h>
#include <HEPVis/nodekits/SoSiStripDetKit.h>
#include <HEPVis/nodekits/SoSiStripWedgeDetKit.h>
#include <HEPVis/nodekits/SoStereoSiStripDetKit.h>
#include <HEPVis/nodes/SoCrystalHit.h>
#include <HEPVis/nodes/SoHadEmCrystalHit.h>
#include <HEPVis/nodes/SoHelicalTrack.h>
#include <HEPVis/nodes/SoJet.h>
#include <HEPVis/nodes/SoReconTrack.h>
#include <HEPVis/nodes/SoSiStrips.h>
#include <HEPVis/nodes/SoSiWedgeStrips.h>
#include <HEPVis/nodes/SoVtxReconTrack.h>

// HEPVisAnalysis (use HEPVisUtils) :
#include <HEPVis/nodekits/SoAxes2D.h>
#include <HEPVis/nodekits/SoAxes3D.h>
#include <HEPVis/nodekits/SoAxis.h>
#include <HEPVis/nodekits/SoPlotter.h>
#include <HEPVis/nodekits/SoPlotterRegion.h>

// HEPVisGUI (use HEPVisUtils) :
#include <HEPVis/nodekits/SoArrowButton.h>
#include <HEPVis/nodekits/SoCommand.h>
#include <HEPVis/nodekits/SoFileSelectionBox.h>
#include <HEPVis/nodekits/SoForm.h>
#include <HEPVis/nodekits/SoList.h>
#include <HEPVis/nodekits/SoPrimitive.h>
#include <HEPVis/nodekits/SoPushButton.h>
#include <HEPVis/nodekits/SoScrolledList.h>
#include <HEPVis/nodekits/SoSelectionBox.h>
#include <HEPVis/nodekits/SoShell.h>
#include <HEPVis/nodekits/SoText.h>
#include <HEPVis/nodes/SoBackPrimitive.h>
