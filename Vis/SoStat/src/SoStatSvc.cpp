/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// this :
#include "SoStatSvc.h"

// Gaudi :
#include <GaudiKernel/MsgStream.h>

DECLARE_COMPONENT( SoStatSvc )

//////////////////////////////////////////////////////////////////////////////
SoStatSvc::SoStatSvc( const std::string& aName, ISvcLocator* aSvcLoc )
    : Service( aName, aSvcLoc )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{}
//////////////////////////////////////////////////////////////////////////////
SoStatSvc::~SoStatSvc()
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{}
//////////////////////////////////////////////////////////////////////////////
StatusCode SoStatSvc::initialize()
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  StatusCode status = Service::initialize();
  if ( status.isFailure() ) return status;
  MsgStream log( msgSvc(), Service::name() );
  log << MSG::DEBUG << " SoStatSvc::initialize " << endmsg;
  setProperties();
  return status;
}
//////////////////////////////////////////////////////////////////////////////
StatusCode SoStatSvc::finalize()
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  MsgStream log( msgSvc(), Service::name() );
  log << MSG::DEBUG << "SoStatSvc finalized successfully" << endmsg;
  return Service::finalize();
}
