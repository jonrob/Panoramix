/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// include files
// Inventor
#include "Inventor/nodes/SoSeparator.h"
#include <Inventor/nodes/SoLightModel.h>
// HEPVis :
#include "HEPVis/nodekits/SoRegion.h"
#include <HEPVis/misc/SoStyleCache.h>
#include <HEPVis/nodes/SoHighlightMaterial.h>
// mandatory for visualization!!!
#include "OnXSvc/Win32.h"
// STD & STL
#include <algorithm>
#include <functional>
#include <string>
#include <vector>
// GaudiKernel
#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/IRegistry.h"
#include "GaudiKernel/SmartDataPtr.h"
#include "GaudiKernel/SmartRef.h"
// OnXSvc
#include "OnXSvc/ClassID.h"
#include "OnXSvc/Helpers.h"
#include "OnXSvc/IUserInterfaceSvc.h"
// Event
#include "Event/CaloCluster.h"
// CaloDet
#include "CaloDet/DeCalorimeter.h"
// Local
#include "CaloClusterRep.h"
#include "SoCaloClusterCnv.h"

#include <Lib/Interfaces/ISession.h>
#include <Lib/smanip.h>

/** @file SoCaloClusterCnv.cpp
 *
 *  implementation of SoCaloClusterCnv class
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   20/10/2001
 */

// ============================================================================
/// mandatory factory business
// ============================================================================

DECLARE_CONVERTER( SoCaloClusterCnv )

// ============================================================================
/// standard constructor
// ============================================================================
SoCaloClusterCnv::SoCaloClusterCnv( ISvcLocator* svcLoc ) : SoCaloBaseCnv( classID(), svcLoc ) {
  setName( "SoCaloClusterCnv" );
  setDetSvcName( "DetectorDataSvc" );
}

// ============================================================================
/// destructor
// ============================================================================
SoCaloClusterCnv::~SoCaloClusterCnv() {}

// ============================================================================
/// initialize
// ============================================================================
StatusCode SoCaloClusterCnv::initialize() {
  StatusCode sc = SoCaloBaseCnv::initialize();
  if ( sc.isFailure() ) { return Error( "initialize: could not initialize base class!" ); }
  return StatusCode::SUCCESS;
}

// ============================================================================
/// finalize
// ============================================================================
StatusCode SoCaloClusterCnv::finalize() { return SoCaloBaseCnv::finalize(); }

long SoCaloClusterCnv::repSvcType() const { return i_repSvcType(); }

/// ============================================================================
/// Class ID for created object == class ID for this specific converter
// ============================================================================
const CLID& SoCaloClusterCnv::classID() { return LHCb::CaloClusters::classID(); }

// ============================================================================
/// storage Type
// ============================================================================
unsigned char SoCaloClusterCnv::storageType() { return So_TechnologyType; }

// ============================================================================
// create the representation
// ============================================================================
StatusCode SoCaloClusterCnv::createRep( DataObject* object, IOpaqueAddress*& /* Address */ ) {
  ///
  typedef LHCb::CaloClusters::const_iterator ClusIt;
  ///
  if ( 0 == object ) { return Error( "createRep: DataObject* points to NULL" ); }
  if ( 0 == uiSvc() ) { return Error( "createRep: IUserInterfaceSvc* points to NULL" ); }
  SoRegion* soRegion = uiSvc()->currentSoRegion();
  if ( soRegion == 0 ) { return Error( "createRep: SoRegion* points to NULL" ); }

  const LHCb::CaloClusters* clusters = dynamic_cast<const LHCb::CaloClusters*>( object );
  if ( 0 == clusters ) {
    return Error( "createRep: wrong cast to KeyedContainer<LHCb::CaloCluster, Containers::HashMap>!" );
  }
  ///

  if ( clusters->size() == 0 ) {
    return Print( "container " + object->registry()->identifier() + " is empty", MSG::INFO, StatusCode::SUCCESS );
  }

  ///
  /// locate the calorimeter (needed for digits)
  const DeCalorimeter* decalo = 0;
  for ( ClusIt it = clusters->begin(); clusters->end() != it; ++it ) {
    const LHCb::CaloCluster* cl = *it;
    if ( 0 == cl ) { continue; }
    std::cout << " cluster " << *cl << std::endl;

    for ( LHCb::CaloCluster::Digits::const_iterator idig = cl->entries().begin(); cl->entries().end() != idig;
          ++idig ) {
      const LHCb::CaloDigit* digit = idig->digit();
      if ( 0 == digit ) { continue; }
      decalo = calorimeter( digit->cellID().calo() );
      break;
    }
    if ( 0 != decalo ) { break; }
  }
  if ( 0 == decalo ) { return Error( "could not locate the calorimeter!" ); }

  // Build the data-accessor name :
  std::string da_name = "none"; // Should be EcalClusters, HcalClusters.
  {
    std::vector<std::string> words;
    Lib::smanip::words( object->registry()->identifier(), "/", words );
    if ( words.size() >= 1 ) da_name = words[words.size() - 1];
  }

  /// create the visualizator
  CaloClusterRep vis( decalo, da_name );

  ///
  /// E or Et ?
  {
    bool etVis = false;
    Lib::smanip::tobool( attribute( "CaloEtVis" ), etVis );
    vis.setEtVis( etVis );
  }
  /// Linear or logarithm ?
  {
    bool logVis = false;
    Lib::smanip::tobool( attribute( "CaloLogVis" ), logVis );
    vis.setLogVis( logVis );
  }
  /// Scale
  {
    double scale = 5;
    Lib::smanip::todouble( attribute( "CaloScale" ), scale );
    scale *= Gaudi::Units::cm / Gaudi::Units::GeV;
    vis.setEScale( scale );
  }
  /// visualize the digits?
  {
    bool visDigits = true;
    Lib::smanip::tobool( attribute( "CaloVisDigits" ), visDigits );
    vis.setVisDigits( visDigits );
  }
  /// visualize shared digits ?
  {
    bool visShared = false;
    Lib::smanip::tobool( attribute( "CaloVisShared" ), visShared );
    vis.setVisShared( visShared );
  }
  /// visualize the structure ?
  {
    bool visStructure = false;
    Lib::smanip::tobool( attribute( "CaloVisStructure" ), visStructure );
    vis.setVisStructure( visStructure );
  }
  /// visualize the covariance ?
  {
    bool visCovariance = false;
    Lib::smanip::tobool( attribute( "CaloVisCovariance" ), visCovariance );
    vis.setVisCovariance( visCovariance );
  }
  /// visualize the cluster spread ?
  {
    bool visSpread = false;
    Lib::smanip::tobool( attribute( "CaloVisSpread" ), visSpread );
    vis.setVisSpread( visSpread );
  }
  /// visualize the spread prism ?
  {
    bool visPrism = false;
    Lib::smanip::tobool( attribute( "CaloVisPrism" ), visPrism );
    vis.setVisPrism( visPrism );
  }

  // Representation attributes :
  double r = 0.5, g = 0.5, b = 0.5;
  double hr = 1.0, hg = 1.0, hb = 1.0;
  Lib::smanip::torgb( attribute( "modeling.color" ), r, g, b );
  Lib::smanip::torgb( attribute( "modeling.highlightColor" ), hr, hg, hb );
  bool modeling = modelingSolid();
  vis.setSolid( modeling );

  SoStyleCache* soStyleCache = soRegion->styleCache();
  SoLightModel* lightModel   = 0;
  if ( modeling ) {
    lightModel = soStyleCache->getLightModelPhong();
  } else { // SoLightModel::BASE_COLOR);
    lightModel = soStyleCache->getLightModelBaseColor();
  }
  // Material :
  SoMaterial* highlightMaterial =
      soStyleCache->getHighlightMaterial( float( r ), float( g ), float( b ), float( hr ), float( hg ), float( hb ) );

  for ( ClusIt it = clusters->begin(); clusters->end() != it; ++it ) {
    const LHCb::CaloCluster* cl = *it;
    if ( 0 == cl ) { continue; }

    SoSeparator* separator = vis( cl );
    if ( separator ) {
      //  Send scene graph to the viewing region
      // (in the "dynamic" sub-scene graph) :

      separator->insertChild( highlightMaterial, 0 );
      separator->insertChild( lightModel, 0 );

      region_addToDynamicScene( *soRegion, separator );
    } else {
      Error( "createRep: SoSeparator* points to NULL! skip it!" );
    }
  }
  ///
  return StatusCode::SUCCESS;
}

// ============================================================================
/// locate the calorimeter //////
// ============================================================================
StatusCode SoCaloClusterCnv::locateCalo( CaloCellCode::CaloIndex calo ) {
  ///
  if ( ( calo < m_calos.size() ) && ( 0 != m_calos[calo] ) ) { return StatusCode::SUCCESS; }
  ///
  if ( !detSvc() ) { return Error( "IDataProviderSvc* points to NULL!" ); }
  ///
  const std::string name( "/dd/Structure/LHCb/DownstreamRegion/" + caloName( calo ) );
  ///
  SmartDataPtr<DeCalorimeter> det( detSvc(), name );
  if ( !det ) { return Error( "could not locate " + name ); }
  ///
  while ( !( calo < m_calos.size() ) ) { m_calos.push_back( nullptr ); }
  ///
  m_calos[calo] = det;
  ///
  return StatusCode::SUCCESS;
  ///
}

// ============================================================================
/// calorimeter
// ============================================================================
const DeCalorimeter* SoCaloClusterCnv::calorimeter( CaloCellCode::CaloIndex calo ) {
  ///
  StatusCode sc = locateCalo( calo );
  if ( sc.isFailure() ) { return 0; }
  ///
  return *( m_calos.begin() + calo );
  ///
}

std::string SoCaloClusterCnv::attribute( const std::string& att ) const {
  if ( 0 == uiSvc() ) return "";
  std::string value;
  uiSvc()->session()->parameterValue( att, value );
  return value;
}
bool SoCaloClusterCnv::modelingSolid() const {
  if ( 0 == uiSvc() ) return true;
  std::string value;
  if ( !uiSvc()->session()->parameterValue( "modeling.modeling", value ) ) return true;
  return ( value == "solid" ? true : false );
}

// ============================================================================
/// the end
// ============================================================================
