/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Inventor
#include "Inventor/nodes/SoSeparator.h"
#include <Inventor/nodes/SoLightModel.h>
// HEPVis :
#include "HEPVis/nodekits/SoRegion.h"
#include <HEPVis/misc/SoStyleCache.h>
#include <HEPVis/nodes/SoHighlightMaterial.h>
// OnXSvc
#include "OnXSvc/Win32.h"
// STD & STL
#include <algorithm>
#include <functional>
#include <string>
#include <vector>
// GaudiKernel
#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/IRegistry.h"
#include "GaudiKernel/SmartDataPtr.h"
// OnXSvc
#include "OnXSvc/ClassID.h"
#include "OnXSvc/Helpers.h"
#include "OnXSvc/IUserInterfaceSvc.h"
// CaloEvent
#include "Event/CaloDigit.h"
#include "Kernel/CaloCellID.h"
// CaloDet
#include "CaloDet/DeCalorimeter.h"
// Local
#include "CaloDigitRep.h"
#include "SoCaloDigitCnv.h"

#include <Lib/Interfaces/ISession.h>
#include <Lib/smanip.h>

/** @file SoCaloDigitCnv.cpp
 *
 *  implementation of SoCaloDigitCnv class
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   18/04/2001
 */

// ============================================================================
/// mandatory factory business
// ============================================================================
DECLARE_CONVERTER( SoCaloDigitCnv )

// ============================================================================
/// standard constructor
// ============================================================================
SoCaloDigitCnv::SoCaloDigitCnv( ISvcLocator* svcLoc ) : SoCaloBaseCnv( classID(), svcLoc ), m_calos() {
  setName( "SoCaloDigitCnv" );
  setDetSvcName( "DetectorDataSvc" );
}

// ============================================================================
/// standard desctructor
// ============================================================================
SoCaloDigitCnv::~SoCaloDigitCnv() {}

// ============================================================================
/// initialize
// ============================================================================
StatusCode SoCaloDigitCnv::initialize() {
  StatusCode sc = SoCaloBaseCnv::initialize();
  if ( sc.isFailure() ) { return Error( "initialize: could not initialize base class!" ); }
  return StatusCode::SUCCESS;
}

// ============================================================================
/// finalize
// ============================================================================
StatusCode SoCaloDigitCnv::finalize() { return SoCaloBaseCnv::finalize(); }

long SoCaloDigitCnv::repSvcType() const { return i_repSvcType(); }

// ============================================================================
/// Class ID for created object == class ID for this specific converter
// ============================================================================
const CLID& SoCaloDigitCnv::classID() { return LHCb::CaloDigits::classID(); }

// ============================================================================
/// storage Type
// ============================================================================
unsigned char SoCaloDigitCnv::storageType() { return So_TechnologyType; }

// ============================================================================
/// the only one essential method
// ============================================================================
StatusCode SoCaloDigitCnv::createRep( DataObject* object, IOpaqueAddress*& /* Address */ ) {
  ///
  typedef LHCb::CaloDigits::const_iterator DigIt;
  ///
  if ( 0 == object ) { return Error( "createRep: DataObject* points to NULL" ); }
  if ( 0 == uiSvc() ) { return Error( "createRep: IUserInterfaceSvc* points to NULL" ); }
  SoRegion* soRegion = uiSvc()->currentSoRegion();
  if ( soRegion == 0 ) { return Error( "createRep: SoRegion* points to NULL" ); }
  const LHCb::CaloDigits* digits = dynamic_cast<const LHCb::CaloDigits*>( object );
  if ( 0 == digits ) {
    return Error( "createRep: wrong cast to KeyedContainer<LHCb::CaloDigit,Containers::HashMap>!" );
  }
  ///
  if ( digits->size() == 0 ) {
    return Print( "container " + object->registry()->identifier() + " is empty", MSG::INFO, StatusCode::SUCCESS );
  }

  /// find corresponding de-calorimeter
  DigIt idig;
  for ( idig = digits->begin(); idig != digits->end(); ++idig ) {
    if ( *idig ) break;
  }
  if ( digits->end() == idig ) {
    return Warning( "container " + object->registry()->identifier() + " contains only NULLs", StatusCode::SUCCESS );
  }

  /// locate calorimeter
  const auto           calo   = ( *idig )->cellID().calo();
  const DeCalorimeter* decalo = calorimeter( calo );
  if ( 0 == decalo ) { return Error( "could not locate calorimeter" ); }
  // Build the data-accessor name :
  std::string da_name = "none"; // Should be Ecal, Hcal, Prs, Spd.

  if ( CaloCellCode::CaloNumFromName( "Ecal" ) == int( calo ) ) {
    // we are in Ecal
    da_name = "Ecal";
  } else if ( CaloCellCode::CaloNumFromName( "Prs" ) == int( calo ) ) {
    // we are in Prs
    da_name = "Prs";
  } else if ( CaloCellCode::CaloNumFromName( "Spd" ) == int( calo ) ) {
    // we are in Spd
    da_name = "Spd";
  } else if ( CaloCellCode::CaloNumFromName( "Hcal" ) == int( calo ) ) {
    // we are in Hcal
    da_name = "Hcal";
  }
  da_name = da_name + "Digits";

  /// create "visualizator"
  CaloDigitRep vis( decalo, da_name );

  /// E or Et ?
  {
    bool etVis = false;
    Lib::smanip::tobool( attribute( "CaloEtVis" ), etVis );
    vis.setEtVis( etVis );
  }
  /// Linear or logarithm ?
  {
    bool logVis = false;
    Lib::smanip::tobool( attribute( "CaloLogVis" ), logVis );
    vis.setLogVis( logVis );
  }
  /// Scale
  {
    double scale = 5;
    Lib::smanip::todouble( attribute( "CaloScale" ), scale );
    scale *= Gaudi::Units::cm / Gaudi::Units::GeV;
    vis.setEScale( scale );
  }

  // Representation attributes :
  double r = 0.5, g = 0.5, b = 0.5;
  double hr = 1.0, hg = 1.0, hb = 1.0;
  Lib::smanip::torgb( attribute( "modeling.color" ), r, g, b );
  Lib::smanip::torgb( attribute( "modeling.highlightColor" ), hr, hg, hb );
  bool modeling = modelingSolid();
  vis.setSolid( modeling );

  SoStyleCache* soStyleCache = soRegion->styleCache();
  SoLightModel* lightModel   = 0;
  if ( modeling ) {
    lightModel = soStyleCache->getLightModelPhong();
  } else { // SoLightModel::BASE_COLOR);
    lightModel = soStyleCache->getLightModelBaseColor();
  }
  // Material :
  SoMaterial* highlightMaterial =
      soStyleCache->getHighlightMaterial( float( r ), float( g ), float( b ), float( hr ), float( hg ), float( hb ) );

  {
    for ( DigIt idig = digits->begin(); idig != digits->end(); ++idig ) {
      const LHCb::CaloDigit* dig = *idig;
      if ( 0 == dig ) { continue; }

      SoSeparator* separator = vis( dig );
      if ( separator ) {

        separator->insertChild( highlightMaterial, 0 );
        separator->insertChild( lightModel, 0 );

        region_addToDynamicScene( *soRegion, separator );
      } else {
        Error( "createRep: SoSeparator* points to NULL!, skip it!" );
      }
    }
  }
  ///
  return StatusCode::SUCCESS;
  ///
}

// ============================================================================
/// locate the calorimeter //////
// ============================================================================
StatusCode SoCaloDigitCnv::locateCalo( CaloCellCode::CaloIndex calo ) {
  ///
  if ( ( calo < m_calos.size() ) && ( 0 != m_calos[calo] ) ) { return StatusCode::SUCCESS; }
  ///
  if ( 0 == detSvc() ) { return Error( "IDataProviderSvc* points to NULL!" ); }
  ///
  const std::string name( "/dd/Structure/LHCb/DownstreamRegion/" + caloName( calo ) );
  ///
  SmartDataPtr<DeCalorimeter> det( detSvc(), name );
  if ( !det ) { return Error( "could not locate \"" + name + "\"" ); }
  ///
  while ( !( calo < m_calos.size() ) ) { m_calos.push_back( 0 ); }
  ///
  m_calos[calo] = det;
  ///
  return StatusCode::SUCCESS;
  ///
}

// ============================================================================
/// calorimeter
// ============================================================================
const DeCalorimeter* SoCaloDigitCnv::calorimeter( CaloCellCode::CaloIndex calo ) {
  ///
  StatusCode sc = locateCalo( calo );
  if ( sc.isFailure() ) { return 0; }
  ///
  return *( m_calos.begin() + calo );
  ///
}
std::string SoCaloDigitCnv::attribute( const std::string& att ) const {
  if ( 0 == uiSvc() ) return "";
  std::string value;
  uiSvc()->session()->parameterValue( att, value );
  return value;
}
bool SoCaloDigitCnv::modelingSolid() const {
  if ( 0 == uiSvc() ) return true;
  std::string value;
  if ( !uiSvc()->session()->parameterValue( "modeling.modeling", value ) ) return true;
  return ( value == "solid" ? true : false );
}

// ============================================================================
/// the end
// ============================================================================
