/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: L0CaloDigitRep.h,v 1.2 2009-06-03 14:33:28 jonrob Exp $
// ============================================================================
// CVS tag $Name: not supported by cvs2svn $
// ============================================================================
// $Log: not supported by cvs2svn $
// Revision 1.1  2008/09/24 15:12:38  truf
// *** empty log message ***
//
//
// ============================================================================
#ifndef SOCALO_L0CALODIGITREP_H
#  define SOCALO_L0CALODIGITREP_H 1
/// STL
#  include <functional>
// GaudiKernel
#  include "Event/L0CaloCandidate.h"
#  include "GaudiKernel/StatusCode.h"
//
class SoSeparator;
class DeCalorimeter;
namespace LHCb {
  class L0CaloCandidate;
}
//

// ============================================================================
/** @class L0CaloDigitRep  L0CaloDigitRep.h
 *
 *  Helper class(functor) which performs the real
 *  visualization of L0CaloDigit* Object
 *
 *  @author Fernando Rodrigues flfr@if.ufrj.br
 *  @date   14/08/2008
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   19/04/2001
 */
// ============================================================================

class L0CaloDigitRep : public std::unary_function<const LHCb::L0CaloCandidate*, SoSeparator*> {

  //
public:
  /** standard constructor
   *  @param DeCalo  pointer to DeCalorimeter object
   *                 (source of geometry  information)
   *  @param Type    "Object Type" for picking
   *  @param EScale  drawing scale
   *  @param EtVis   flag for drawing the transverse energy
   *  @param LogVis  flag for logarithmic drawing
   *  @param TxtVis  flag for drawing the text
   *  @param TxtSize text size
   *  @param TxtPos  text position
   */
  L0CaloDigitRep( const DeCalorimeter* DeCalo, const std::string& Type = "",
                  const double EScale = 5 * Gaudi::Units::cm / Gaudi::Units::GeV, const bool EtVis = false,
                  const bool LogVis = false, const bool TxtVis = false,
                  // const double           TxtSize  = 0.05      ,
                  const double TxtSize = 10, const double TxtPos = 0.02 );

  /** virtual destructor
   */
  virtual ~L0CaloDigitRep();

  /** the only one essential method
   *  @param digit pointer to the digit
   *  @return pointer to corresponsing Inventor Node
   */
  SoSeparator* operator()( const LHCb::L0CaloCandidate* digit, const std::string& pickName = "" ) const;

  /// accessor to type object (for picking)
  inline const std::string& type() const { return m_type; }
  /// accessor  to calorimeter object
  inline const DeCalorimeter* deCalo() const { return m_deCalo; }
  /// get energy scale
  inline double eScale() const { return logVis() ? 1000 * m_eScale : m_eScale; }
  /// get et-visualization flag
  inline bool etVis() const { return m_etVis; }
  /// get logarithm-visualization flag
  inline bool logVis() const { return m_logVis; }
  /// get text visualization flag
  inline bool txtVis() const { return m_txtVis; }
  /// get text size
  inline double txtSize() const { return m_txtSize; }
  /// get text position
  inline double txtPos() const { return m_txtPos; }
  /// get the size of the box
  inline double size() const { return m_size; }
  /// set new value for calorimeter
  inline L0CaloDigitRep& setDeCalo( const DeCalorimeter* newDeCalo ) {
    m_deCalo = newDeCalo;
    return *this;
  }
  inline L0CaloDigitRep& setType( const std::string& newType ) {
    m_type = newType;
    return *this;
  }
  /// set new value for energy scale
  inline L0CaloDigitRep& setEScale( const double newEScale ) {
    m_eScale = newEScale;
    return *this;
  }
  /// set new value for et-visualization flag
  inline L0CaloDigitRep& setEtVis( const bool newEtVis ) {
    m_etVis = newEtVis;
    return *this;
  }
  /// set new value for logarithm visualization flag
  inline L0CaloDigitRep& setLogVis( const bool newLogVis ) {
    m_logVis = newLogVis;
    return *this;
  }
  /// set new value for text visualization flag
  inline L0CaloDigitRep& setTxtVis( const bool newTxtVis ) {
    m_txtVis = newTxtVis;
    return *this;
  }
  /// set new value for tetx size
  inline L0CaloDigitRep& setTxtSize( const double newTxtSize ) {
    m_txtSize = newTxtSize;
    return *this;
  }
  /// set new value for text position
  inline L0CaloDigitRep& setTxtPos( const double newTxtPos ) {
    m_txtPos = newTxtPos;
    return *this;
  }
  inline L0CaloDigitRep& setSolid( bool newSolid ) {
    m_solid = newSolid;
    ;
    return *this;
  }

private:
  ///
  std::string          m_type;
  const DeCalorimeter* m_deCalo;
  double               m_eScale;
  double               m_txtSize;
  double               m_txtPos;
  bool                 m_etVis;
  bool                 m_logVis;
  bool                 m_txtVis;
  bool                 m_solid;
  ///
  mutable double m_size;
};

// ============================================================================
#endif ///  SOCALO_L0CALODIGITVIS_H
// ============================================================================
