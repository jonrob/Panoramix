/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// OnXSvc
#include "OnXSvc/Helpers.h"
#include "OnXSvc/ISoConversionSvc.h"
#include "OnXSvc/IUserInterfaceSvc.h"
#include "OnXSvc/Win32.h"
// this
#include "CaloDigitType.h"
// Inventor
#include <Inventor/nodes/SoLightModel.h>
#include <Inventor/nodes/SoSeparator.h>
// HEPVis :
#include <HEPVis/misc/SoStyleCache.h>
#include <HEPVis/nodes/SoHighlightMaterial.h>
// STD & STL
#include <algorithm>
// GaudiKernel
#include "GaudiKernel/Point3DTypes.h"
// Lib
#include <Lib/Compiler.h>
#include <Lib/Interfaces/IIterator.h>
#include <Lib/Interfaces/IManager.h>
#include <Lib/Interfaces/ISession.h>
#include <Lib/Property.h>
#include <Lib/Variable.h>
#include <Lib/smanip.h>
// Gaudi
#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/SmartDataPtr.h"
#include <GaudiKernel/ISvcLocator.h>
// Event
#include "Event/CaloClusterEntry.h"
#include "Event/CaloDataFunctor.h"
#include "Event/CaloDigit.h"
#include <Event/MCParticle.h>
#include <Linker/LinkedTo.h>
// CaloDet
#include "CaloDet/DeCalorimeter.h"
// local
#include "CaloDigitRep.h"

// ============================================================================
/** @file CaloDigitType.cpp
 *
 * Implementation file for class CaloDigitType
 *
 * @date 10/10/2001
 * @author  Vanya Belyaev Ivan.Belyaev@itep.ru
 */
// ============================================================================

// ============================================================================
/** Standard Constructor
 *  @param Type    "So"-type name
 *  @param SvcLoc  pointer to Service Locator
 */
// ============================================================================
CaloDigitType::CaloDigitType( const std::string& Type, ISvcLocator* SvcLoc, IPrinter& Printer )
    : OnX::BaseType( Printer ), CaloBaseType( Type, SvcLoc ) {
  initialize();

  addProperty( "id", Lib::Property::POINTER );
  addProperty( "e", Lib::Property::DOUBLE );
  /// CaloCellID.index()
  addProperty( "index", Lib::Property::INTEGER );
  /// calorimeter
  addProperty( "calo", Lib::Property::INTEGER );
  /// area in teh calorimeter
  addProperty( "area", Lib::Property::INTEGER );
  /// column number
  addProperty( "col", Lib::Property::INTEGER );
  /// row number
  addProperty( "row", Lib::Property::INTEGER );
  /// calorimeter
  addProperty( "cal", Lib::Property::STRING );
  /// x-position of center of the cell
  addProperty( "x", Lib::Property::DOUBLE );
  /// y-position of center of the cell
  addProperty( "y", Lib::Property::DOUBLE );
  /// transverse  energy
  addProperty( "et", Lib::Property::DOUBLE );
  /// cell size
  addProperty( "cellsize", Lib::Property::DOUBLE );
}

// ============================================================================
/** Destructor
 */
// ============================================================================
CaloDigitType::~CaloDigitType() { finalize(); }

// ===========================================================================
/** get name(type)
 */
// ============================================================================
std::string CaloDigitType::name() const { return CaloBaseType::name(); }

// ===========================================================================
// ============================================================================
Lib::IIterator* CaloDigitType::iterator() {
  class Iterator : public Lib::IIterator {
  public: // Lib::IIterator
    virtual Lib::Identifier object() {
      if ( !fVector ) return 0;
      while ( true ) {
        if ( fIterator == fVector->end() ) return 0;
        if ( *fIterator ) return *fIterator;
        ++fIterator;
      }
      return 0;
    }
    virtual void next() {
      if ( fVector ) ++fIterator;
    }
    virtual void* tag() { return 0; }

  public:
    Iterator( LHCb::CaloDigits* aVector = 0 ) : fVector( aVector ) {
      if ( fVector ) fIterator = fVector->begin();
    }

  private:
    LHCb::CaloDigits*          fVector;
    LHCb::CaloDigits::iterator fIterator;
  };

  if ( 0 == evtSvc() ) {
    Error( "iterator(): evtSvc() points to NULL!" );
    return 0;
  }
  /// get address of objects
  setData( attribute( name() ) );
  Print( "Data Container '" + data() + "' is used", MSG::DEBUG );
  if ( data().empty() ) {
    Warning( "iterator(): '" + name() + "' attribute is not given!" );
    return 0;
  }
  /// retrieve data container
  SmartDataPtr<LHCb::CaloDigits> digits( evtSvc(), data() );
  if ( !digits ) {
    Error( "iterator():: could not retrieve '" + data() + "'" );
    return 0;
  }
  if ( digits->size() == 0 ) {
    Warning( "iterator(): '" + name() + "' collection is empty." );
    return new Iterator();
  }

  return new Iterator( digits ); // deleted by the caller.
}

// ===========================================================================
// ============================================================================
Lib::Variable CaloDigitType::value( Lib::Identifier identifier, const std::string& tag, void* ) {
  if ( tag.empty() ) {
    Error( "getProperty() - wrong name!" );
    return Lib::Variable( printer() );
  }
  /// bad style :-(((
  const LHCb::CaloDigit* digit = (const LHCb::CaloDigit*)identifier;
  if ( 0 == digit ) {
    Error( "getProperty('" + tag + "' LHCb::CaloDigit* points to NULL!" );
    return Lib::Variable( printer() );
  }
  ///
  if ( "id" == tag ) {
    return Lib::Variable( printer(), (void*)digit );
  } else if ( "e" == tag ) {
    return Lib::Variable( printer(), digit->e() );
  }
  ///
  const LHCb::CaloCellID cellID( digit->cellID() );
  if ( "index" == tag ) {
    return Lib::Variable( printer(), (int)cellID.index() );
  } else if ( "calo" == tag ) {
    return Lib::Variable( printer(), (int)cellID.calo() );
  } else if ( "area" == tag ) {
    return Lib::Variable( printer(), (int)cellID.area() );
  } else if ( "col" == tag ) {
    return Lib::Variable( printer(), (int)cellID.col() );
  } else if ( "row" == tag ) {
    return Lib::Variable( printer(), (int)cellID.row() );
  }
  ///
  const auto Calo = cellID.calo();
  if ( "cal" == tag ) { return Lib::Variable( printer(), caloName( Calo ) ); }
  ///
  const DeCalorimeter* decalo = calo( Calo );
  if ( 0 == decalo ) {
    Error( "getProperty(" + tag + ") - could not locate the calorimeter!" );
    return Lib::Variable( printer() );
  }
  if ( "x" == tag ) {
    return Lib::Variable( printer(), decalo->cellX( cellID ) );
  } else if ( "y" == tag ) {
    return Lib::Variable( printer(), decalo->cellY( cellID ) );
  } else if ( "cellsize" == tag ) {
    return Lib::Variable( printer(), decalo->cellSize( cellID ) );
  }
  ///
  LHCb::CaloDataFunctor::EnergyTransverse<const DeCalorimeter*> Et( decalo );
  if ( "et" == tag ) { return Lib::Variable( printer(), Et( digit ) ); }
  ///
  Error( "getProperty(): unknown name='" + tag + "'" );
  ///
  return Lib::Variable( printer() );
}

// ===========================================================================
// ============================================================================
void CaloDigitType::beginVisualize() { m_parentNode = parent( DYNAMIC_SCENE ); }
void CaloDigitType::endVisualize() { m_parentNode = 0; }

void CaloDigitType::visualize( Lib::Identifier id, void* ) {
  ///
  if ( 0 == id ) {
    Error( "visualize(): Lib::Identifier equals to NULL!" );
    return;
  }
  ///
  const LHCb::CaloDigit* digit = (const LHCb::CaloDigit*)id;
  std::string            value;
  uiSvc()->session()->parameterValue( "modeling.what", value );
  if ( value == "MCParticle" ) {
    LHCb::CaloDigit* object = (LHCb::CaloDigit*)id;
    visualizeMCParticle( *object );
    return;
  }

  SoSeparator* node = m_parentNode;
  if ( node == 0 ) {
    Error( "visualize(): parent SoSeparator* points to NULL! " );
    return;
  }

  /// create and configure visualizator
  const DeCalorimeter* Calo = calo( digit->cellID().calo() );
  if ( 0 == Calo ) {
    Error( "visualize(): DeCalorimeter* points to NULL! " );
    return;
  }
  CaloDigitRep vis( Calo, myType() );
  /// E or Et ?
  {
    bool etVis = false;
    Lib::smanip::tobool( attribute( "CaloEtVis" ), etVis );
    vis.setEtVis( etVis );
  }
  /// Linear or logarithm ?
  {
    bool logVis = false;
    Lib::smanip::tobool( attribute( "CaloLogVis" ), logVis );
    vis.setLogVis( logVis );
  }
  /// Scale
  {
    double scale = 5;
    Lib::smanip::todouble( attribute( "CaloScale" ), scale );
    scale *= Gaudi::Units::cm / Gaudi::Units::GeV;
    vis.setEScale( scale );
  }

  // Representation attributes :
  double r = 0.5, g = 0.5, b = 0.5;
  double hr = 1.0, hg = 1.0, hb = 1.0;
  Lib::smanip::torgb( attribute( "modeling.color" ), r, g, b );
  Lib::smanip::torgb( attribute( "modeling.highlightColor" ), hr, hg, hb );
  bool modeling = modelingSolid();
  vis.setSolid( modeling );

  /// create the representation
  SoSeparator* separator = vis( digit );
  if ( 0 == separator ) {
    Error( "visualize(): could not create SoSeparator!" );
    return;
  }

  SoMaterial* highlightMaterial =
      styleCache()->getHighlightMaterial( float( r ), float( g ), float( b ), float( hr ), float( hg ), float( hb ) );

  separator->insertChild( highlightMaterial, 0 );
  if ( modeling ) {
    separator->insertChild( styleCache()->getLightModelPhong(), 0 );
  } else { // SoLightModel::BASE_COLOR);
    separator->insertChild( styleCache()->getLightModelBaseColor(), 0 );
  }

  node->addChild( separator );
}
//////////////////////////////////////////////////////////////////////////////
void CaloDigitType::visualizeMCParticle( LHCb::CaloDigit& aCaloDigit )
//////////////////////////////////////////////////////////////////////////////

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  LinkedTo<LHCb::MCParticle, LHCb::CaloDigit> CaloDigitLink( evtSvc(), 0, LHCb::CaloDigitLocation::Ecal );

  LHCb::MCParticle* part = CaloDigitLink.first( &aCaloDigit );
  const std::string tmp2( "SoConversionSvc" );
  ISoConversionSvc* soSvc;
  StatusCode        sc = svcLoc()->service( tmp2, soSvc );

  while ( NULL != part ) {
    LHCb::MCParticles* objs = new LHCb::MCParticles;
    objs->add( part );
    // Convert it :
    IOpaqueAddress* addr = 0;
    StatusCode      sc   = soSvc->createRep( objs, addr );
    if ( sc.isSuccess() ) sc = soSvc->fillRepRefs( addr, objs );
    objs->remove( part );
    delete objs;
    part = CaloDigitLink.next();
  }
}
