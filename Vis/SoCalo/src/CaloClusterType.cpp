/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// CVS tag $Name: not supported by cvs2svn $
// ============================================================================
// $Log: not supported by cvs2svn $
// Revision 1.27  2008/07/28 08:13:33  truf
// just to please cmt/cvs, no changes
//
// Revision 1.26  2008/02/05 13:56:58  gybarran
// G.Barrand : rm OSC_VERSION_16_0 logic
//
// Revision 1.25  2007/03/19 15:19:21  ranjard
// v8r1 - fix for LHCb v22r2
//
// Revision 1.24  2007/02/02 16:52:21  truf
// *** empty log message ***
//
// Revision 1.23  2006/12/07 15:09:01  gybarran
// G.Barrand : use SoStyleCache
//
// Revision 1.22  2006/12/07 10:01:07  gybarran
// G.Barrand : handle modeling solid, wire_frame
//
// Revision 1.21  2006/12/07 09:00:29  gybarran
// G.Barrand : optimize iterator and getting of the currentRegion
//
// Revision 1.20  2006/10/24 15:30:43  gybarran
// *** empty log message ***
//
// Revision 1.19  2006/10/24 12:51:09  gybarran
// G.Barrand : prepare OSC-16
//
// Revision 1.18  2006/05/24 07:02:03  ranjard
// v1r2 - adapt to LHCb v21r0
//
// Revision 1.17  2006/03/30 06:51:06  gybarran
// *** empty log message ***
//
// Revision 1.16  2006/03/30 06:33:41  gybarran
// *** empty log message ***
//
// Revision 1.15  2006/03/29 20:01:13  gybarran
// *** empty log message ***
//
// Revision 1.14  2006/03/09 16:30:26  odescham
// v6r2 - migration to LHCb v20r0
//
// Revision 1.13  2003/06/24 14:08:24  gybarran
// *** empty log message ***
//
// Revision 1.12  2003/06/24 14:05:40  barrand
// *** empty log message ***
//
// Revision 1.11  2002/10/17 10:15:25  barrand
// *** empty log message ***
//
// Revision 1.10  2002/10/16 20:10:18  barrand
// G.Barrand : CERN_WIN32 porting
//
// Revision 1.9  2002/10/15 14:57:30  barrand
// G.Barrand : migrate to Lib/v5 and HEPVis/v6r1
//
// Revision 1.8  2002/09/11 16:57:15  barrand
// G.Barrand : have things compiling...
//
// Revision 1.7  2002/09/11 16:50:21  barrand
// G.Barrand : error in case of cluster with no digits
//
// Revision 1.6  2002/09/11 08:30:44  barrand
// *** empty log message ***
//
// Revision 1.5  2002/09/11 08:28:51  barrand
// *** empty log message ***
//
// Revision 1.4  2002/09/11 07:00:57  barrand
// G.Barrand : update according new event model and OnX/v11
//
// Revision 1.3  2001/12/02 15:52:15  ibelyaev
//  fix z-positions of the clusters
//
// Revision 1.2  2001/11/26 10:11:13  ibelyaev
//  major redesign and update
//
// Revision 1.1  2001/10/21 16:26:47  ibelyaev
// Visualization for CaloClusters is added
//
// ============================================================================
// incldue files
// OnXSvc
#include "OnXSvc/Helpers.h"
#include "OnXSvc/ISoConversionSvc.h"
#include "OnXSvc/IUserInterfaceSvc.h"
#include "OnXSvc/Win32.h"
// OnX
#include <OnX/Interfaces/IUI.h>
// this
#include "CaloClusterType.h"
// Inventor
#include <Inventor/nodes/SoLightModel.h>
#include <Inventor/nodes/SoSeparator.h>
// HEPVis :
#include <HEPVis/misc/SoStyleCache.h>
#include <HEPVis/nodes/SoHighlightMaterial.h>
// STD & STL
#include <algorithm>
// GaudiKernel
#include "GaudiKernel/Point3DTypes.h"
// Lib
#include <Lib/Compiler.h>
#include <Lib/Interfaces/IIterator.h>
#include <Lib/Interfaces/IManager.h>
#include <Lib/Interfaces/ISession.h>
#include <Lib/Property.h>
#include <Lib/Variable.h>
#include <Lib/smanip.h>
// Gaudi
#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/SmartDataPtr.h"
#include "GaudiKernel/SmartRef.h"
#include <GaudiKernel/ISvcLocator.h>
// DetDesc
#include "DetDesc/IGeometryInfo.h"
// CaloEvent
#include "Event/CaloDataFunctor.h"
// CaloDet
#include "CaloDet/DeCalorimeter.h"
// local
#include "CaloClusterRep.h"

#include <Linker/LinkedTo.h>
// Event model :
#include <Event/MCParticle.h>
//

// ============================================================================
/** @file CaloClusterType.cpp
 *
 * Implementation file for class CaloClusterType
 *
 * @date 10/10/2001
 * @author  Vanya Belyaev Ivan.Belyaev@itep.ru
 */
// ============================================================================

// ============================================================================
/** Standard Constructor
 *  @param Type    "So"-type name
 *  @param SvcLoc  pointer to Service Locator
 */
// ============================================================================
CaloClusterType::CaloClusterType( const std::string& Type, ISvcLocator* SvcLoc, IPrinter& Printer )
    : OnX::BaseType( Printer ), CaloBaseType( Type, SvcLoc ) {
  initialize();

  addProperty( "id", Lib::Property::POINTER );
  addProperty( "e", Lib::Property::DOUBLE );
  addProperty( "x", Lib::Property::DOUBLE );
  addProperty( "y", Lib::Property::DOUBLE );
  addProperty( "digs", Lib::Property::INTEGER );
  addProperty( "status", Lib::Property::INTEGER );
  /// transverse energy
  addProperty( "et", Lib::Property::DOUBLE );
  /// at boundary ?
  addProperty( "boundary", Lib::Property::BOOLEAN );
  /// seed cell cellID
  addProperty( "cellID", Lib::Property::INTEGER );
  /// seed cell cellsize
  addProperty( "cellsize", Lib::Property::DOUBLE );
}

// ============================================================================
/** Destructor
 */
// ============================================================================
CaloClusterType::~CaloClusterType() { finalize(); }

// ===========================================================================
/** get name(type)
 */
// ============================================================================
std::string CaloClusterType::name() const { return CaloBaseType::name(); }

// ===========================================================================
// get Lib identifiers
// ============================================================================
Lib::IIterator* CaloClusterType::iterator() {
  class Iterator : public Lib::IIterator {
  public: // Lib::IIterator
    virtual Lib::Identifier object() {
      if ( !fVector ) return 0;
      while ( true ) {
        if ( fIterator == fVector->end() ) return 0;
        if ( *fIterator ) return *fIterator;
        ++fIterator;
      }
      return 0;
    }
    virtual void next() {
      if ( fVector ) ++fIterator;
    }
    virtual void* tag() { return 0; }

  public:
    Iterator( LHCb::CaloClusters* aVector = 0 ) : fVector( aVector ) {
      if ( fVector ) fIterator = fVector->begin();
    }

  private:
    LHCb::CaloClusters*          fVector;
    LHCb::CaloClusters::iterator fIterator;
  };

  if ( 0 == evtSvc() ) {
    Error( "iterator(): evtSvc() points to NULL!" );
    return 0;
  }
  /// get address of objects
  setData( attribute( name() ) );
  Print( "Data Container '" + data() + "' is used", MSG::DEBUG );
  if ( data().empty() ) {
    Warning( "iterator(): '" + name() + "' attribute is not given!" );
    return 0;
  }
  /// retrieve data container
  SmartDataPtr<LHCb::CaloClusters> clusters( evtSvc(), data() );
  if ( !clusters ) {
    Error( "iterator():: could not retrieve '" + data() + "'" );
    return 0;
  }
  if ( clusters->size() == 0 ) {
    Warning( "iterator(): '" + name() + "' collection is empty." );
    return new Iterator();
  }

  return new Iterator( clusters ); // deleted by the caller.
}

// ===========================================================================
/// get the specific property
// ============================================================================
Lib::Variable CaloClusterType::value( Lib::Identifier identifier, const std::string& tag, void* ) {
  /// local type
  typedef LHCb::CaloCluster::Digits::const_iterator CIT;
  ///
  if ( tag.empty() ) {
    Error( "getProperty() - wrong name!" );
    return Lib::Variable( printer() );
  }
  /// bad style :-(((
  const LHCb::CaloCluster* cluster = (const LHCb::CaloCluster*)identifier;
  if ( 0 == cluster ) {
    Error( "getProperty('" + tag + "' LHCb::CaloCluster* points to NULL!" );
    return Lib::Variable( printer() );
  }
  ///
  if ( "id" == tag ) {
    return Lib::Variable( printer(), (void*)cluster );
  } else if ( "e" == tag ) {
    return Lib::Variable( printer(), (double)cluster->e() );
  } else if ( "x" == tag ) {
    return Lib::Variable( printer(), (double)cluster->position().x() );
  } else if ( "y" == tag ) {
    return Lib::Variable( printer(), (double)cluster->position().y() );
  } else if ( "digs" == tag ) {
    return Lib::Variable( printer(), (int)cluster->entries().size() );
  } else if ( "status" == tag ) {
    return Lib::Variable( printer(), (int)cluster->type() );
  }

  const DeCalorimeter* decalo = calo( CaloCellCode::CaloNumFromName( myType() ) );

  if ( "et" == tag ) {
    if ( 0 == decalo || 0 == decalo->geometry() ) { return Lib::Variable( printer(), (double)-1.0 ); }
    const double et =
        cluster->e() * sin( Gaudi::XYZPoint( cluster->position().x(), cluster->position().y(),
                                             decalo->geometry()->toGlobal( Gaudi::XYZPoint( 0, 0, 0 ) ).z() )
                                .theta() );
    return Lib::Variable( printer(), et );
  } else if ( "boundary" == tag ) {
    for ( CIT it1 = cluster->entries().begin(); cluster->entries().end() != it1; ++it1 ) {
      const LHCb::CaloDigit* dig1 = it1->digit();
      if ( 0 == dig1 ) { continue; }
      for ( CIT it2 = it1 + 1; cluster->entries().end() != it2; ++it2 ) {
        const LHCb::CaloDigit* dig2 = it2->digit();
        if ( 0 == dig2 ) { continue; }
        if ( dig1->cellID().area() != dig2->cellID().area() ) { return Lib::Variable( printer(), true ); }
      }
    }
    return Lib::Variable( printer(), false );
  }
  /// look for seeding digit
  const LHCb::CaloDigit* seed = 0;
  for ( CIT it = cluster->entries().begin(); cluster->entries().end() != it; ++it ) {
    const LHCb::CaloDigit* dig = it->digit();
    if ( 0 == dig ) { continue; }
    if ( LHCb::CaloDigitStatus::SeedCell & it->status() ) {
      seed = dig;
      break;
    }
  }
  ///
  if ( "cellID" == tag ) {
    int cellID = 0 != seed ? (int)seed->cellID().all() : 0;
    return Lib::Variable( printer(), cellID );
  } else if ( "cellsize" == tag ) {
    const DeCalorimeter* decalo   = 0 != seed ? calo( seed->cellID().calo() ) : 0;
    const double         cellsize = ( ( 0 != decalo ) && ( 0 != seed ) ) ? decalo->cellSize( seed->cellID() ) : -1;
    return Lib::Variable( printer(), cellsize );
  }
  ///
  Error( "getProperty(): unknown name='" + tag + "'" );
  ///
  return Lib::Variable( printer() );
}

// ===========================================================================
// ============================================================================
void CaloClusterType::beginVisualize() { m_parentNode = parent( DYNAMIC_SCENE ); }
void CaloClusterType::endVisualize() { m_parentNode = 0; }
void CaloClusterType::visualize( Lib::Identifier id, void* ) {
  const LHCb::CaloCluster* cluster = (const LHCb::CaloCluster*)id;
  if ( 0 == cluster ) {
    Error( "visualize():  LHCb::CaloCluster* points to NULL!" );
    return;
  }
  ///
  std::string value;
  uiSvc()->session()->parameterValue( "modeling.what", value );
  if ( value == "MCParticle" ) {
    LHCb::CaloCluster* object = (LHCb::CaloCluster*)id;
    visualizeMCParticle( *object );
    return;
  }
  SoSeparator* node = m_parentNode;
  if ( node == 0 ) {
    Error( "visualize(): parent SoSeparator* points to NULL! " );
    return;
  }
  /// create and configure visualizator
  const DeCalorimeter* Calo = calo( CaloCellCode::CaloNumFromName( myType() ) );
  if ( 0 == Calo ) {
    Error( "visualize(): DeCalorimeter* points to NULL! " );
    return;
  }
  ///
  CaloClusterRep vis( Calo, myType() );
  ///
  /// E or Et ?
  {
    bool etVis = false;
    Lib::smanip::tobool( attribute( "CaloEtVis" ), etVis );
    vis.setEtVis( etVis );
  }
  /// Linear or logarithm ?
  {
    bool logVis = false;
    Lib::smanip::tobool( attribute( "CaloLogVis" ), logVis );
    vis.setLogVis( logVis );
  }
  /// Scale
  {
    double scale = 5;
    Lib::smanip::todouble( attribute( "CaloScale" ), scale );
    scale *= Gaudi::Units::cm / Gaudi::Units::GeV;
    vis.setEScale( scale );
  }
  /// visualize the digits?
  {
    bool visDigits = true;
    Lib::smanip::tobool( attribute( "CaloVisDigits" ), visDigits );
    vis.setVisDigits( visDigits );
  }
  /// visualize shared digits ?
  {
    bool visShared = false;
    Lib::smanip::tobool( attribute( "CaloVisShared" ), visShared );
    vis.setVisShared( visShared );
  }
  /// visualize the structure ?
  {
    bool visStructure = false;
    Lib::smanip::tobool( attribute( "CaloVisStructure" ), visStructure );
    vis.setVisStructure( visStructure );
  }
  /// visualize the covariance ?
  {
    bool visCovariance = false;
    Lib::smanip::tobool( attribute( "CaloVisCovariance" ), visCovariance );
    vis.setVisCovariance( visCovariance );
  }
  /// visualize the cluster spread ?
  {
    bool visSpread = false;
    Lib::smanip::tobool( attribute( "CaloVisSpread" ), visSpread );
    vis.setVisSpread( visSpread );
  }
  /// visualize the spread prism ?
  {
    bool visPrism = false;
    Lib::smanip::tobool( attribute( "CaloVisPrism" ), visPrism );
    vis.setVisPrism( visPrism );
  }

  bool found = false;
  for ( LHCb::CaloCluster::Entries::const_iterator it = cluster->entries().begin(); cluster->entries().end() != it;
        ++it ) {
    const LHCb::CaloDigit* digit = it->digit();
    if ( digit ) {
      found = true;
      break;
    }
  }
  if ( !found ) {
    Error( "visualize(): cluster with no valid digits ! " );
    return;
  }

  // Representation attributes :
  double r = 0.5, g = 0.5, b = 0.5;
  double hr = 1.0, hg = 1.0, hb = 1.0;
  Lib::smanip::torgb( attribute( "modeling.color" ), r, g, b );
  Lib::smanip::torgb( attribute( "modeling.highlightColor" ), hr, hg, hb );
  bool modeling = modelingSolid();
  vis.setSolid( modeling );

  /// create the representation
  SoSeparator* separator = vis( cluster );
  if ( 0 == separator ) {
    Error( "visualize(): could not create SoSeparator!" );
    return;
  }

  SoMaterial* highlightMaterial =
      styleCache()->getHighlightMaterial( float( r ), float( g ), float( b ), float( hr ), float( hg ), float( hb ) );
  separator->insertChild( highlightMaterial, 0 );
  if ( modeling ) {
    separator->insertChild( styleCache()->getLightModelPhong(), 0 );
  } else { // SoLightModel::BASE_COLOR);
    separator->insertChild( styleCache()->getLightModelBaseColor(), 0 );
  }

  node->addChild( separator );
}
//////////////////////////////////////////////////////////////////////////////
void CaloClusterType::visualizeMCParticle( LHCb::CaloCluster& aCaloCluster )
//////////////////////////////////////////////////////////////////////////////

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  LinkedTo<LHCb::MCParticle, LHCb::CaloCluster> CaloClusterLink( evtSvc(), 0, LHCb::CaloClusterLocation::Ecal );
  LHCb::MCParticle*                             part = CaloClusterLink.first( &aCaloCluster );
  const std::string                             tmp( "SoConversionSvc" );
  ISoConversionSvc*                             soSvc;
  StatusCode                                    sc = svcLoc()->service( tmp, soSvc );

  while ( NULL != part ) {
    LHCb::MCParticles* objs = new LHCb::MCParticles;
    objs->add( part );
    // Convert it :
    IOpaqueAddress* addr = 0;
    StatusCode      sc   = soSvc->createRep( objs, addr );
    if ( sc.isSuccess() ) sc = soSvc->fillRepRefs( addr, objs );
    objs->remove( part );
    delete objs;
    part = CaloClusterLink.next();
  }
}

// ============================================================================
// the end
// ============================================================================
