/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//
// ============================================================================
// Inventor
#include <Inventor/nodes/SoSeparator.h>
// HEPVis :
#include <HEPVis/misc/SoStyleCache.h>
#include <HEPVis/nodekits/SoDisplayRegion.h>
// Include files
#include "OnXSvc/Win32.h"
// GaudiKernel
#include "GaudiKernel/Bootstrap.h"
#include "GaudiKernel/IChronoStatSvc.h"
#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/IMessageSvc.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/SmartDataPtr.h"
#include "GaudiKernel/System.h"
// GaudiKernel
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/Stat.h"
// Lib
#include <Lib/Interfaces/IManager.h>
#include <Lib/Interfaces/ISession.h>
// OnX
#include <OnX/Interfaces/IUI.h>
// OnXSvc
#include "OnXSvc/Helpers.h"
#include "OnXSvc/IUserInterfaceSvc.h"
// CaloKernel
#include "CaloKernel/CaloException.h"
// CaloDet
#include "CaloDet/DeCalorimeter.h"
// local
#include "CaloBaseType.h"

// ============================================================================
/** @file CaloBaseType.cpp
 *
 *  Implementation file for class : CaloBaseType
 *
 * @date 09/10/2001
 * @author Vanya Belyaev Ivan.Belyaev@itep.ru
 */
// ============================================================================

// ============================================================================
/** Standard constructor
 *  @param TypeName name of type
 *  @param SvcLoc pointer to Service locator
 */
// ============================================================================
CaloBaseType::CaloBaseType( const std::string& TypeName, ISvcLocator* SvcLoc )
    : m_myType( TypeName )
    , m_svcLoc( SvcLoc )
    //
    , m_detSvc( 0 )
    , m_evtSvc( 0 )
    , m_msgSvc( 0 )
    , m_chronoSvc( 0 )
    , m_uiSvc( 0 )
    //
    , m_detSvcName( "DetectorDataSvc" )
    , m_evtSvcName( "EventDataSvc" )
    , m_msgSvcName( "MessageSvc" )
    , m_chronoSvcName( "ChronoStatSvc" )
    , m_uiSvcName( "OnXSvc" )
    //
    , m_data( "" )
    //
    , m_calos()
    //
    , m_init( StatusCode::FAILURE )
    , m_soStyleCache( 0 )
    , m_parentNode( 0 ) {
  if ( 0 == svcLoc() ) { m_svcLoc = Gaudi::svcLocator(); }
}

// ============================================================================
/** destructor
 */
// ============================================================================
CaloBaseType::~CaloBaseType() {
  if ( m_init.isSuccess() ) { m_init = finalize(); }
  if ( m_soStyleCache ) m_soStyleCache->unref();
}

// ============================================================================
/** initialize the base type
 *  @return status code
 */
// ============================================================================
StatusCode CaloBaseType::initialize() {
  ///
  if ( 0 == svcLoc() ) { m_svcLoc = Gaudi::svcLocator(); }
  if ( 0 == svcLoc() ) { return StatusCode::FAILURE; } ///< RETURN !!!
  ///
  if ( 0 != m_msgSvc ) {
    m_msgSvc->release();
    m_msgSvc = 0;
  }
  if ( !m_msgSvcName.empty() ) {
    StatusCode sc = svcLoc()->service( m_msgSvcName, m_msgSvc, true );
    if ( sc.isFailure() || 0 == m_msgSvc ) { return StatusCode::FAILURE; }
    m_msgSvc->addRef();
  }
  ///
  if ( 0 != m_chronoSvc ) {
    m_chronoSvc->release();
    m_chronoSvc = 0;
  }
  if ( !m_chronoSvcName.empty() ) {
    StatusCode sc = svcLoc()->service( m_chronoSvcName, m_chronoSvc, true );
    if ( sc.isFailure() || 0 == m_chronoSvc ) {
      return Error( "Could not locate ChronoSvc='" + m_chronoSvcName + "'" );
    }
    m_chronoSvc->addRef();
  }
  ///
  if ( 0 != m_detSvc ) {
    m_detSvc->release();
    m_detSvc = 0;
  }
  if ( !m_detSvcName.empty() ) {
    StatusCode sc = svcLoc()->service( m_detSvcName, m_detSvc, true );
    if ( sc.isFailure() || 0 == m_detSvc ) { return Error( "Could not locate DetSvc='" + m_detSvcName + "'" ); }
    m_detSvc->addRef();
  }
  ///
  if ( 0 != m_uiSvc ) {
    m_uiSvc->release();
    m_uiSvc = 0;
  }
  if ( !m_uiSvcName.empty() ) {
    StatusCode sc = svcLoc()->service( m_uiSvcName, m_uiSvc, true );
    if ( sc.isFailure() || 0 == m_uiSvc ) { return Error( "Could not locate uiSvc='" + m_uiSvcName + "'" ); }
    m_uiSvc->addRef();
  }
  ///
  if ( 0 != m_evtSvc ) {
    m_evtSvc->release();
    m_evtSvc = 0;
  }
  if ( !m_evtSvcName.empty() ) {
    StatusCode sc = svcLoc()->service( m_evtSvcName, m_evtSvc );
    if ( sc.isFailure() || 0 == m_evtSvc ) { return Error( "Could not locate EvtSvc='" + m_evtSvcName + "'" ); }
    m_evtSvc->addRef();
  }
  ///
  m_init = StatusCode::SUCCESS;
  ///
  return m_init;
}

// ============================================================================
/** finalize the base type
 *  @return status code
 */
// ============================================================================
StatusCode CaloBaseType::finalize() {
  m_init = StatusCode::FAILURE;
  ///
  if ( 0 != m_uiSvc ) {
    m_uiSvc->release();
    m_uiSvc = 0;
  }
  if ( 0 != m_evtSvc ) {
    m_evtSvc->release();
    m_evtSvc = 0;
  }
  if ( 0 != m_detSvc ) {
    m_detSvc->release();
    m_detSvc = 0;
  }
  if ( 0 != m_chronoSvc ) {
    m_chronoSvc->release();
    m_chronoSvc = 0;
  }
  if ( 0 != m_msgSvc ) {
    m_msgSvc->release();
    m_msgSvc = 0;
  }
  ///
  return StatusCode::SUCCESS;
}

// ============================================================================
/** return name/type of the object
 *  @return name/type of object
 */
// ============================================================================
std::string CaloBaseType::name() const { return myType(); }

// ============================================================================
/** get (string) value of attribute
 *  @param att attribute name
 *  @return (string) value of attribute
 */
// ============================================================================
std::string CaloBaseType::attribute( const std::string& att ) const {
  if ( 0 == uiSvc() ) { Exception( "CaloBaseType: '" + name() + "'; IUserInterafceSvc* point to NULL!" ); }
  std::string value;
  uiSvc()->session()->parameterValue( att, value );
  return value;
}
bool CaloBaseType::modelingSolid() const {
  if ( 0 == uiSvc() ) return true;
  std::string value;
  if ( !uiSvc()->session()->parameterValue( "modeling.modeling", value ) ) return true;
  return ( value == "solid" ? true : false );
}

// ============================================================================
/** print and count error message
 *  @param msg error message to be printed
 *  @param sc   status code
 *  @return status code
 */
// ============================================================================
StatusCode CaloBaseType::Error( const std::string& msg, const StatusCode& sc ) const {
  Stat stat( chronoSvc(), name() + ":Error" );
  return Print( msg, MSG::ERROR, sc );
}

// ============================================================================
/** print and count warning  message
 *  @param msg warning message to be printed
 *  @param sc   status code
 *  @return status code
 */
// ============================================================================
StatusCode CaloBaseType::Warning( const std::string& msg, const StatusCode& sc ) const {
  Stat stat( chronoSvc(), name() + ":Warning" );
  return Print( msg, MSG::WARNING, sc );
}

// ============================================================================
/** print the message
 *  @param msg   message to be printed
 *  @param lvl  print level
 *  @param sc   status code
 *  @return status code
 */
// ============================================================================
StatusCode CaloBaseType::Print( const std::string& msg, const MSG::Level& lvl, const StatusCode& sc ) const {
  MsgStream log( msgSvc(), name() );
  log << lvl << System::typeinfoName( typeid( *this ) ) << " " << msg;
  ///
  if ( !sc.isSuccess() ) { log << " \tStatusCode=" << sc; }
  ///
  log << endmsg;
  return sc;
}

// ============================================================================
/** exception print and count
 *  @param msg  message to be printed
 *  @param exc  previous exception
 *  @param lvl  print level
 *  @param sc   status code
 *  @return status code
 */
// ============================================================================
StatusCode CaloBaseType::Exception( const std::string& msg, const GaudiException& exc, const MSG::Level&,
                                    const StatusCode& sc ) const {
  Error( msg, sc );
  throw CaloException( msg, exc, sc );
  return sc;
}

// ============================================================================
/** exception print and count
 *  @param msg  message to be printed
 *  @param exc  previous exception
 *  @param lvl  print level
 *  @param sc   status code
 *  @return status code
 */
// ============================================================================
StatusCode CaloBaseType::Exception( const std::string& msg, const std::exception& exc, const MSG::Level&,
                                    const StatusCode& sc ) const {
  Error( msg, sc );
  throw CaloException( msg + "(" + exc.what() + ")", sc );
  return sc;
}

// ============================================================================
/** exception print and count
 *  @param msg  message to be printed
 *  @param lvl  print level
 *  @param sc   status code
 *  @return status code
 */
// ============================================================================
StatusCode CaloBaseType::Exception( const std::string& msg, const MSG::Level&, const StatusCode& sc ) const {
  Error( msg, sc );
  throw CaloException( msg, sc );
  return sc;
}

// ============================================================================
/** accessor to calorimeter detector
 *  @param Calo calorimeter index
 *  @return pointer to calorimeter device
 */
// ============================================================================
const DeCalorimeter* CaloBaseType::calo( CaloCellCode::CaloIndex Calo ) const {
  ///
  StatusCode sc = locateCalo( Calo );
  if ( sc.isFailure() ) { return nullptr; }
  ///
  return *( m_calos.begin() + Calo );
  ///
}

// ============================================================================
/** locate the calorimeter detector
 *  @param Calo index of calorimeter detector
 *  @return status code
 */
// ============================================================================
StatusCode CaloBaseType::locateCalo( CaloCellCode::CaloIndex Calo ) const {
  ///
  if ( ( Calo < m_calos.size() ) && ( 0 != m_calos[Calo] ) ) { return StatusCode::SUCCESS; }
  ///
  if ( 0 == detSvc() ) { return Error( "IDataProviderSvc* points to NULL!" ); }
  ///
  const std::string name( "/dd/Structure/LHCb/DownstreamRegion/" + caloName( Calo ) );
  ///
  SmartDataPtr<const DeCalorimeter> det( detSvc(), name );
  if ( !det ) { return Error( "could not locate " + name ); }
  ///
  while ( !( Calo < m_calos.size() ) ) { m_calos.push_back( 0 ); }
  ///
  m_calos[Calo] = det;
  ///
  return StatusCode::SUCCESS;
  ///
}

// ============================================================================
/** get current page
 *  @return pointer to current page
 */
// ============================================================================
SoRegion* CaloBaseType::region() const {
  if ( 0 == uiSvc() ) {
    Error( "region(): IUserInterfaceSvc* points to NULL!" );
    return 0;
  }
  SoRegion* soRegion = uiSvc()->currentSoRegion();
  if ( 0 == soRegion ) {
    Error( "region(): SoRegion* points to NULL!" );
    return 0;
  }
  ///
  return soRegion;
}

// ============================================================================
/** get parent node/separator
 *  @param  Node  node/separator region name
 *  @param  Page  name of ui manager
 *  @return parent node/separator
 */
// ============================================================================
SoSeparator* CaloBaseType::parent( const std::string& Node ) const {
  SoRegion* soRegion = region();
  if ( 0 == soRegion ) {
    Error( "parent(): SoRegion* points to NULL!" );
    return 0;
  }

  if ( !soRegion->isOfType( SoDisplayRegion::getClassTypeId() ) ) {
    Error( "parent(): SoRegion* not a SoDisplayRegion!" );
    return 0;
  }

  SoDisplayRegion* displayRegion = (SoDisplayRegion*)soRegion;
  if ( Node == DYNAMIC_SCENE ) {
    return displayRegion->getDynamicScene();
  } else if ( Node == STATIC_SCENE ) {
    return displayRegion->getStaticScene();
  } else {
    Error( "parent(): unknown parent kind." );
    return 0;
  }
}

SoStyleCache* CaloBaseType::styleCache() {
  if ( !m_soStyleCache ) m_soStyleCache = new SoStyleCache();
  return m_soStyleCache;
}

// ============================================================================
