/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// CVS tag $Name: not supported by cvs2svn $
// ============================================================================
// $Log: not supported by cvs2svn $
// Revision 1.1  2008/09/24 15:12:38  truf
// *** empty log message ***
//
//
// ============================================================================
// Include files
// OnXSvc
#include "OnXSvc/Helpers.h"
#include "OnXSvc/ISoConversionSvc.h"
#include "OnXSvc/IUserInterfaceSvc.h"
#include "OnXSvc/Win32.h"
// Inventor
#include <Inventor/nodes/SoLightModel.h>
#include <Inventor/nodes/SoSeparator.h>
// HEPVis :
#include <HEPVis/misc/SoStyleCache.h>
#include <HEPVis/nodes/SoHighlightMaterial.h>
// STD & STL
#include <algorithm>
// GaudiKernel
#include "GaudiKernel/Point3DTypes.h"
// Lib
#include <Lib/Compiler.h>
#include <Lib/Interfaces/IIterator.h>
#include <Lib/Interfaces/IManager.h>
#include <Lib/Interfaces/ISession.h>
#include <Lib/Property.h>
#include <Lib/Variable.h>
#include <Lib/smanip.h>
// Gaudi
#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/SmartDataPtr.h"
#include <GaudiKernel/ISvcLocator.h>
// Event
#include "Event/CaloClusterEntry.h"
#include "Event/L0CaloCandidate.h"
#include <Event/MCParticle.h>
#include <Linker/LinkedTo.h>
// CaloDet
#include "CaloDet/DeCalorimeter.h"
// this
#include "L0CaloDigitType.h"
// local
#include "L0CaloDigitRep.h"
// ============================================================================
/** @file L0CaloDigitType.cpp
 *
 *  Implementation file for class L0CaloDigitType
 *
 *  @author Fernando Rodrigues flfr@if.ufrj.br
 *  @date   13/08/2008
 *
 *  Based on CaloDigitType.cpp
 *  @author  Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date 10/10/2001
 */
// ============================================================================

// ============================================================================
/**
 * Files used for L0 Calo Visualization:
 *    Event/SoCaloCandidate.h
 * Created:
 *    SoCaloSvc.h(.cpp)
 *    L0CaloDigitType.h(.cpp)
 *    SoL0CaloDigitCnv.h(.cpp)
 *    L0CaloDigitRep.h(.cpp)
 *    L0CaloDataFunctor.h
 */
// ============================================================================

// ============================================================================
/** L0CaloCandidate Summary:
 * vi $LHCBRELEASES/LHCB/LHCB_v25r0/InstallArea/include/Event/L0CaloCandidate.h
 *
 * CLID = 6001
 * Location: "Trig/L0/Calo" , "Trig/L0/FullCalo"
 * Constructor: L0CaloCandidate(
 *              int type, cosnt CaloCellID& ID, int etCode, double et,
 *              const Gaudi::XYZPoint& cellCenter, double cellSize)
 * Variables names:
 * int type() const;               << Type of trigger, from L0DUBase::Type
 *                                    namespace Type {
 *                                        enum Scale{Digit=0, MuonPt, CaloEt};};
 * const LHCb::CaloCellID& id() const; << Calo Cell ID. To access MC information...
 * int etCode() const;             << Integer version of Et.
 * double et() const;              << Transverse energy
 * const Gaudi::XYZPoint& position() const; << Position
 * double posTol() const;          << Tolerance on x/y position = 1/2 cell size
 ***
 * Definition of vector container type for L0CaloCandidate:
 * typedef ObjectVector<L0CaloCandidate> L0CaloCandidates;
 */
// ============================================================================

// ============================================================================
/*
 * $LHCBRELEASES/LHCB/LHCB_v25r0/InstallArea/include/Kernel/CaloCellID.h
 * // Explicit constructor from Calo, Area, Row and Column
 * loCellID(unsigned int Calo,
 *          unsigned int Area,
 *          unsigned int Row,
 *          unsigned int Column);
 *
 * Probably Calo values are: 0=SPD; 1=PS; 2=ECAL; 3=HCAL <<<< Based on Det design
 */
// ============================================================================

// ============================================================================
/** Standard Constructor
 *  @param Type    "So"-type name
 *  @param SvcLoc  pointer to Service Locator
 */
// ============================================================================

L0CaloDigitType::L0CaloDigitType( const std::string& Type, ISvcLocator* SvcLoc, IPrinter& Printer )
    : OnX::BaseType( Printer ), CaloBaseType( Type, SvcLoc ) {
  initialize();

  addProperty( "calo", Lib::Property::INTEGER );
  addProperty( "area", Lib::Property::INTEGER );
  addProperty( "row", Lib::Property::INTEGER );
  addProperty( "column", Lib::Property::INTEGER );
  addProperty( "type", Lib::Property::INTEGER );
  addProperty( "etCode", Lib::Property::INTEGER );
  addProperty( "et", Lib::Property::DOUBLE );
  addProperty( "x", Lib::Property::DOUBLE );
  addProperty( "y", Lib::Property::DOUBLE );
  addProperty( "z", Lib::Property::DOUBLE );
  addProperty( "posTol", Lib::Property::DOUBLE );
}

// ============================================================================
/** Destructor
 */
// ============================================================================
L0CaloDigitType::~L0CaloDigitType() { finalize(); }

// ===========================================================================
/** get name(type)
 */
// ============================================================================
std::string L0CaloDigitType::name() const { return CaloBaseType::name(); }

// ===========================================================================
// ============================================================================
Lib::IIterator* L0CaloDigitType::iterator() {
  class Iterator : public Lib::IIterator {
  public: // Lib::IIterator
    virtual Lib::Identifier object() {
      if ( !fVector ) return 0;
      while ( true ) {
        if ( fIterator == fVector->end() ) return 0;
        if ( *fIterator ) return *fIterator;
        ++fIterator;
      }
      return 0;
    }
    virtual void next() {
      if ( fVector ) ++fIterator;
    }
    virtual void* tag() { return 0; }

  public:
    Iterator( LHCb::L0CaloCandidates* aVector = 0 ) : fVector( aVector ) {
      if ( fVector ) fIterator = fVector->begin();
    }

  private:
    LHCb::L0CaloCandidates*          fVector;
    LHCb::L0CaloCandidates::iterator fIterator;
  };

  if ( 0 == evtSvc() ) {
    Error( "iterator(): evtSvc() points to NULL!" );
    return 0;
  }
  /// get address of objects
  setData( attribute( name() ) );
  Print( "Data Container '" + data() + "' is used", MSG::DEBUG );
  if ( data().empty() ) {
    Warning( "iterator(): '" + name() + "' attribute is not given!" );
    return 0;
  }
  /// retrieve data container
  SmartDataPtr<LHCb::L0CaloCandidates> digits( evtSvc(), data() );
  if ( !digits ) {
    Error( "iterator():: could not retrieve '" + data() + "'" );
    return 0;
  }
  if ( digits->size() == 0 ) {
    Warning( "iterator(): '" + name() + "' collection is empty." );
    return new Iterator();
  }

  return new Iterator( digits ); // deleted by the caller.
}

// ===========================================================================
// ============================================================================
Lib::Variable L0CaloDigitType::value( Lib::Identifier identifier, const std::string& tag, void* ) {
  if ( tag.empty() ) {
    Error( "getProperty() - wrong name!" );
    return Lib::Variable( printer() );
  }

  LHCb::L0CaloCandidate* obj = (LHCb::L0CaloCandidate*)identifier;
  if ( 0 == obj ) {
    Error( "getProperty('" + tag + "' LHCb::L0CaloCandidate* points to NULL!" );
    return Lib::Variable( printer() );
  }
  if ( tag == "type" ) {
    return Lib::Variable( printer(), (int)obj->type() );
  } else if ( tag == "etCode" ) {
    return Lib::Variable( printer(), (int)obj->etCode() );
  } else if ( tag == "et" ) {
    return Lib::Variable( printer(), (double)obj->et() );
  } else if ( tag == "posTol" ) {
    return Lib::Variable( printer(), (double)obj->posTol() );
  } else if ( tag == "x" ) {
    Gaudi::XYZPoint point = obj->position();
    return Lib::Variable( printer(), (double)point.X() );
  } else if ( tag == "y" ) {
    Gaudi::XYZPoint point = obj->position();
    return Lib::Variable( printer(), (double)point.Y() );
  } else if ( tag == "z" ) {
    Gaudi::XYZPoint point = obj->position();
    return Lib::Variable( printer(), (double)point.Z() );
  } else if ( tag == "calo" ) {
    LHCb::CaloCellID ID = obj->id();
    return Lib::Variable( printer(), (int)ID.calo() );
  } else if ( tag == "area" ) {
    LHCb::CaloCellID ID = obj->id();
    return Lib::Variable( printer(), (int)ID.area() );
  } else if ( tag == "row" ) {
    LHCb::CaloCellID ID = obj->id();
    return Lib::Variable( printer(), (int)ID.row() );
  } else if ( tag == "column" ) {
    LHCb::CaloCellID ID = obj->id();
    return Lib::Variable( printer(), (int)ID.col() );
  }

  Error( "getProperty(): unknown name='" + tag + "'" );
  ///
  return Lib::Variable( printer() );
}

// ===========================================================================
// ============================================================================
void L0CaloDigitType::beginVisualize() { m_parentNode = parent( DYNAMIC_SCENE ); }
void L0CaloDigitType::endVisualize() { m_parentNode = 0; }

void L0CaloDigitType::visualize( Lib::Identifier id, void* ) {
  ///
  if ( 0 == id ) {
    Error( "visualize(): Lib::Identifier equals to NULL!" );
    return;
  }
  ///
  const LHCb::L0CaloCandidate* digit = (const LHCb::L0CaloCandidate*)id;
  std::string                  value;
  uiSvc()->session()->parameterValue( "modeling.what", value );

  SoSeparator* node = m_parentNode;
  if ( node == 0 ) {
    Error( "visualize(): parent SoSeparator* points to NULL! " );
    return;
  }

  /// create and configure visualizator
  const DeCalorimeter* Calo = calo( digit->id().calo() );
  if ( 0 == Calo ) {
    Error( "visualize(): DeCalorimeter* points to NULL! " );
    return;
  }
  L0CaloDigitRep vis( Calo, myType() );
  /// Linear or logarithm ?
  {
    bool logVis = false;
    Lib::smanip::tobool( attribute( "CaloLogVis" ), logVis );
    vis.setLogVis( logVis );
  }
  /// Scale
  {
    double scale = 5;
    Lib::smanip::todouble( attribute( "CaloScale" ), scale );
    scale *= Gaudi::Units::cm / Gaudi::Units::GeV;
    vis.setEScale( scale );
  }

  // Representation attributes :
  double r = 0.5, g = 0.5, b = 0.5;
  double hr = 1.0, hg = 1.0, hb = 1.0;
  Lib::smanip::torgb( attribute( "modeling.color" ), r, g, b );
  Lib::smanip::torgb( attribute( "modeling.highlightColor" ), hr, hg, hb );
  bool modeling = modelingSolid();
  vis.setSolid( modeling );

  /// create the representation
  SoSeparator* separator = vis( digit );
  if ( 0 == separator ) {
    Error( "visualize(): could not create SoSeparator!" );
    return;
  }

  SoMaterial* highlightMaterial =
      styleCache()->getHighlightMaterial( float( r ), float( g ), float( b ), float( hr ), float( hg ), float( hb ) );

  separator->insertChild( highlightMaterial, 0 );
  if ( modeling ) {
    separator->insertChild( styleCache()->getLightModelPhong(), 0 );
  } else { // SoLightModel::BASE_COLOR);
    separator->insertChild( styleCache()->getLightModelBaseColor(), 0 );
  }

  node->addChild( separator );
}
