/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// CVS tag $Name: not supported by cvs2svn $
// ============================================================================
// $Log: not supported by cvs2svn $
//
// ============================================================================
#ifndef SOCALO_L0CALODIGITTYPE_H
#  define SOCALO_L0CALODIGITTYPE_H 1
// STD & STL
#  include <string>
// Lib & OnX
#  include <Lib/Compiler.h>
#  include <Lib/Interfaces/IIterator.h>
#  include <Lib/Interfaces/IPrinter.h>
#  include <OnX/Core/BaseType.h>
// SoCalo
#  include "CaloBaseType.h"
#  include "Event/L0CaloCandidate.h"

/** @class L0CaloDigitType L0CaloDigitType.h
 *
 *  "So"-representation of L0CaloDigit object
 *
 *  @author Fernando Rodrigues flfr@if.ufrj.br
 *  @date   13/08/2008
 *
 *  Based on CaloDigitType.h
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   10/10/2001
 */
class L0CaloDigitType : public OnX::BaseType, protected CaloBaseType {
public:
  /** standard constructor
   *  @param Type    "So"-type name
   *  @param SvcLoc  pointer to Service Locator
   *  @param Printer reference to a printer.
   */
  L0CaloDigitType( const std::string& Type, ISvcLocator* SvcLoc, IPrinter& Printer );
  virtual ~L0CaloDigitType(); ///< Destructor

public: // Lib::IType
  virtual std::string     name() const;
  virtual Lib::IIterator* iterator();
  virtual Lib::Variable   value( Lib::Identifier, const std::string&, void* );

public: // OnX::IType
  using BaseType::beginVisualize;
  virtual void beginVisualize();
  virtual void visualize( Lib::Identifier, void* );
  virtual void endVisualize();

private:
  void visualizeMCParticle( LHCb::L0CaloCandidate& );
};

// ============================================================================
#endif ///< SOCALO_L0CALODIGITTYPE_H
// ============================================================================
