/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef SOCALO_SOL0CALODIGITCNV_H
#  define SOCALO_SOL0CALODIGITCNV_H 1
// STD and STL
#  include <string>
#  include <vector>
// Calo
#  include "SoCaloBaseCnv.h"

// forward decalarations
class DeCalorimeter;

/**  @class SoL0CaloDigitCnv  SoL0CaloDigitCnv.h
 *
 *   Converter for visualization of
 *   container of L0CaloDigit objects
 *
 *   @author  Fernando Rodrigues flfr@if.ufrj.br
 *   @date    13/08/2008
 *
 *   @class based on SoCaloDigitCnv
 *   @author  Vanya Belyaev
 *   @date    18/04/2001
 */

class SoL0CaloDigitCnv : public SoCaloBaseCnv {
public:
  ///
  /// standard initialization method
  virtual StatusCode initialize();
  /// standard finalization  method
  virtual StatusCode finalize();

  virtual long repSvcType() const;

  /// the only one essential method
  virtual StatusCode createRep( DataObject* /* Object */, IOpaqueAddress*& /* Address */ );
  /// Class ID for created object == class ID for this specific converter
  static const CLID& classID();
  /// storage Type
  static unsigned char storageType();

  /// standard constructor
  SoL0CaloDigitCnv( ISvcLocator* svcLoc );
  /// virtual destructor
  virtual ~SoL0CaloDigitCnv();
  /// helpful methos to save typing
  StatusCode locateCalo( CaloCellCode::CaloIndex calo );
  /// accessor to calorimeter
  const DeCalorimeter* calorimeter( CaloCellCode::CaloIndex calo );
  ///
private:
  ///
  /// default constructor is disabled
  SoL0CaloDigitCnv();
  /// copy constructor is disabled
  SoL0CaloDigitCnv( const SoL0CaloDigitCnv& );
  /// assignment is disabled
  SoL0CaloDigitCnv& operator=( const SoL0CaloDigitCnv& );
  ///
  std::string attribute( const std::string& att ) const;
  bool        modelingSolid() const;

private:
  ///
  /// located calorimeter objects
  std::vector<const DeCalorimeter*> m_calos;
  ///
};

// ============================================================================
#endif //  SOCALO_SOL0CALODIGITCNV_H
// ============================================================================
