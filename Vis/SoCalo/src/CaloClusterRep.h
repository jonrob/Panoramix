/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: CaloClusterRep.h,v 1.8 2009-06-03 14:33:28 jonrob Exp $
// =============================================================================
// CVS tag $Name: not supported by cvs2svn $
// =============================================================================
// $Log: not supported by cvs2svn $
// Revision 1.7  2008/07/28 08:13:33  truf
// just to please cmt/cvs, no changes
//
// Revision 1.6  2006/12/07 10:01:07  gybarran
// G.Barrand : handle modeling solid, wire_frame
//
// Revision 1.5  2006/05/24 07:02:03  ranjard
// v1r2 - adapt to LHCb v21r0
//
// Revision 1.4  2006/03/09 16:30:26  odescham
// v6r2 - migration to LHCb v20r0
//
// Revision 1.3  2002/09/11 07:00:57  barrand
// G.Barrand : update according new event model and OnX/v11
//
// Revision 1.2  2001/11/26 10:11:13  ibelyaev
//  major redesign and update
//
// Revision 1.1  2001/10/21 16:26:47  ibelyaev
// Visualization for CaloClusters is added
//
// =============================================================================
#ifndef SOCALO_CALOCLUSTERREP_H
#  define SOCALO_CALOCLUSTERREP_H 1
// Include files
/// STL
#  include <functional>
/// GaudiKernel
#  include "GaudiKernel/StatusCode.h"
//
#  include "CaloDigitRep.h"
//
class SoSeparator;
class DeCalorimeter;
class CaloCluster;

/** @class CaloClusterRep CaloClusterRep.h
 *
 *  Helper class(functor) which performs the real
 *  visualization of CaloCluster* Object
 *
 *  @author Vanya Belyaev Ivan.Belyaev@cern.ch
 *  @date   20/10/2001
 */

class CaloClusterRep : public std::unary_function<const CaloCluster*, SoSeparator*> {
public:
  /** standard constructor
   *  @param DeCalo         pointer to DeCalorimeter object
   *                        (source of geometry  information)
   *  @param Type           Own "Object Type" for picking
   *  @param TypeD          Digit "Object Type" for picking
   *  @param EScale         drawing scale
   *  @param EtVis          flag for drawing the transverse energy
   *  @param LogVis         flag for logarithmic drawing
   *  @param TxtVis         flag for drawing the text
   *  @param TxtSize        text size
   *  @param TxtPos         text position
   *  @param VisDigits      flag for drawing of digits
   *  @param VisShared      flag for special processing of shared digits
   *  @param VisStructure   flag for visualization of cluster structure
   *  @param VisCovariance  flag for visualization of covarinace matrix
   *  @param VisSpread      flag for visualization of cluster spread
   *  @param VisPrism       flag for visualization of spread prism
   */
  CaloClusterRep( const DeCalorimeter* DeCalo, const std::string& Type = "", const std::string& TypeD = "",
                  const double EScale = 5 * Gaudi::Units::cm / Gaudi::Units::GeV, const bool EtVis = false,
                  const bool LogVis = false, const bool TxtVis = false, const double TxtSize = 0.05,
                  const double TxtPos = 0.02, const bool VisDigits = true, const bool VisShared = false,
                  const bool VisStructure = false, const bool VisCovariance = false, const bool VisSpread = true,
                  const bool VisPrism = false );

  /**  virtual destructor
   */
  virtual ~CaloClusterRep();

  /** the only one essential method
   *  @param  cluster pointer to CaloCluster to be visualizaed
   *  @return pointer to Inventor node
   */
  SoSeparator* operator()( const LHCb::CaloCluster* cluster ) const;

  /** accessor to type object (for picking)
   *  @return own type
   */
  inline const std::string& type() const { return m_type; }

  /** accessor to digit visualization flag
   *  @return digits visualization flag
   */
  inline bool visDigits() const { return m_visDigits; }

  /** accessor to shared digits  visualization flag
   *  @return shared visualization flag
   */
  inline bool visShared() const { return m_visShared; }

  /** accessor to cluster structure  visualization flag
   *  @return cluster structure  visualization flag
   */
  inline bool visStructure() const { return m_visStructure; }

  /** accessor to covariance matrix visualization flag
   *  @return covariance matrix visualization flag
   */
  inline bool visCovariance() const { return m_visCovariance; }

  /** accessor to cluster spread    visualization flag
   *  @return cluster spread    visualization flag
   */
  inline bool visSpread() const { return m_visSpread; }

  /** accessor to spread prism      visualization flag
   *  @return spread prism      visualization flag
   */
  inline bool visPrism() const { return m_visPrism; }

  /** accessor  to calorimeter object
   *  @return pointer to calorimeetr detector
   */
  inline const DeCalorimeter* deCalo() const { return digRep().deCalo(); }

  /** accessor to digit type object (for picking)
   *  @return digit type
   */
  inline const std::string& typeDig() const { return digRep().type(); }

  /** get energy scale
   *  @return scale for energy
   */
  inline double eScale() const { return digRep().eScale(); }

  /** get et-visualization flag
   *  @return et-visualization flag
   */
  inline bool etVis() const { return digRep().etVis(); }

  /** get logarithm-visualization flag
   *  @return logarithm-visualization flag
   */
  inline bool logVis() const { return digRep().logVis(); }

  /** get text visualization flag
   *  @return text visualization flag
   */
  inline bool txtVis() const { return digRep().txtVis(); }

  /** get text size
   *  @return text size
   */
  inline double txtSize() const { return digRep().txtSize(); }

  /** get text position
   *  @return text position
   */
  inline double txtPos() const { return digRep().txtPos(); }

  /** get digit representation
   *  @return digit representation
   */
  inline const CaloDigitRep& digRep() const { return m_digRep; }

  /** set new value for digits visualization flag
   *  @param newVal new value for digits visualization flag
   *  @return self-reference
   */
  inline CaloClusterRep& setVisDigits( const bool newVal ) {
    m_visDigits = newVal;
    return *this;
  }

  /** set new value for shared digits visualization flag
   *  @param newVal new value for sahred digits visualization flag
   *  @return self-reference
   */
  inline CaloClusterRep& setVisShared( const bool newVal ) {
    m_visShared = newVal;
    if ( newVal ) { setVisDigits( true ); }
    return *this;
  }

  /** set new value for cluster structure  visualization flag
   *  @param newVal new value for sahred digits visualization flag
   *  @return self-reference
   */
  inline CaloClusterRep& setVisStructure( const bool newVal ) {
    m_visStructure = newVal;
    return *this;
  }

  /** set new value for covarinace matrix visualization flag
   *  @param newVal new value for covarinace matrix visualization flag
   *  @return self-reference
   */
  inline CaloClusterRep& setVisCovariance( const bool newVal ) {
    m_visCovariance = newVal;
    return *this;
  }

  /** set new value for cluster spread    visualization flag
   *  @param newVal new value for cluster spread    visualization flag
   *  @return self-reference
   */
  inline CaloClusterRep& setVisSpread( const bool newVal ) {
    m_visSpread = newVal;
    return *this;
  }

  /** set new value for spread prism  visualization flag
   *  @param newVal new value for spread prism   visualization flag
   *  @return self-reference
   */
  inline CaloClusterRep& setVisPrism( const bool newVal ) {
    m_visPrism = newVal;
    if ( newVal ) { setVisSpread( newVal ); }
    return *this;
  }

  /** set new value for "own type"
   *  @param newType new own type (quite useless)
   *  @return self-reference
   */
  inline CaloClusterRep& setType( const std::string& newType ) {
    m_type = newType;
    return *this;
  }

  /** set new value for "digit type"
   *  @param newType new digit type (quite useless)
   *  @return self-reference
   */
  inline CaloClusterRep& setDigType( const std::string& newType ) {
    m_digRep.setType( newType );
    return *this;
  }

  /** set new value for calorimeter
   *  @param newdeCalo new calorimeter detector
   *  @return self-reference
   */
  inline CaloClusterRep& setDeCalo( const DeCalorimeter* newDeCalo ) {
    m_digRep.setDeCalo( newDeCalo );
    return *this;
  }

  /** set new value for energy scale
   *  @param newEscale new value for energy scale
   *  @return self-reference
   */
  inline CaloClusterRep& setEScale( const double newEScale ) {
    m_digRep.setEScale( newEScale );
    return *this;
  }

  /** set new value for et-visualization flag
   *  @param newEtVis new value for et-visualization flag
   *  @return self-reference
   */
  inline CaloClusterRep& setEtVis( const bool newEtVis ) {
    m_digRep.setEtVis( newEtVis );
    return *this;
  }

  /** set new value for logarithm visualization flag
   *  @param newLogVis new value for logarithm visualization flag
   *  @return self-reference
   */
  inline CaloClusterRep& setLogVis( const bool newLogVis ) {
    m_digRep.setLogVis( newLogVis );
    return *this;
  }

  /** set new value for text visualization flag
   *  @param  newTxtVis new value for text visualization flag
   *  @return self-reference
   */
  inline CaloClusterRep& setTxtVis( const bool newTxtVis ) {
    m_digRep.setTxtVis( newTxtVis );
    return *this;
  }

  /** set new value for text size
   *  @param newTxtSize new value for text size
   *  @return self-reference
   */
  inline CaloClusterRep& setTxtSize( const double newTxtSize ) {
    m_digRep.setTxtSize( newTxtSize );
    return *this;
  }

  /** set new value for text position
   *  @param newTxtPos  new value for text position
   *  @return self-reference
   */
  inline CaloClusterRep& setTxtPos( const double newTxtPos ) {
    m_digRep.setTxtPos( newTxtPos );
    return *this;
  }

  inline CaloClusterRep& setSolid( bool newSolid ) {
    m_digRep.setSolid( newSolid );
    return *this;
  }

private:
  ///
  std::string m_type;
  ///
  bool m_visDigits;
  bool m_visShared;
  bool m_visStructure;
  bool m_visCovariance;
  bool m_visSpread;
  bool m_visPrism;
  ///
  CaloDigitRep m_digRep;
  ///
};

// ============================================================================
#endif ///< SOCALO_CALOCLUSTERREP_H
// ============================================================================
