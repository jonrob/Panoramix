/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: CaloDigitRep.h,v 1.12 2009-06-03 14:33:28 jonrob Exp $
// ============================================================================
// CVS tag $Name: not supported by cvs2svn $
// ============================================================================
// $Log: not supported by cvs2svn $
// Revision 1.11  2008/07/28 08:13:33  truf
// just to please cmt/cvs, no changes
//
// Revision 1.10  2006/12/07 10:01:07  gybarran
// G.Barrand : handle modeling solid, wire_frame
//
// Revision 1.9  2006/05/24 07:02:03  ranjard
// v1r2 - adapt to LHCb v21r0
//
// Revision 1.8  2006/03/29 09:07:36  gybarran
// *** empty log message ***
//
// Revision 1.7  2006/03/09 16:30:27  odescham
// v6r2 - migration to LHCb v20r0
//
// Revision 1.6  2002/09/13 06:27:24  barrand
// G.Barrand : changes in order to be able to have picking info of digits reps of a cluster
//
// Revision 1.5  2002/09/11 07:00:58  barrand
// G.Barrand : update according new event model and OnX/v11
//
// Revision 1.4  2001/11/26 10:11:13  ibelyaev
//  major redesign and update
//
// Revision 1.3  2001/10/21 16:26:48  ibelyaev
// Visualization for CaloClusters is added
//
// Revision 1.2  2001/10/19 07:27:47  barrand
// Deposit an id on the objects rep in order to have feedback picking
//
// Revision 1.1.1.1  2001/10/17 18:26:06  ibelyaev
// New Package: Visualization of Calorimeter objects
//
// ============================================================================
#ifndef SOCALO_CALODIGITREP_H
#  define SOCALO_CALODIGITREP_H 1
/// STL
#  include <functional>
// GaudiKernel
#  include "GaudiKernel/StatusCode.h"
//
class SoSeparator;
class DeCalorimeter;
class CaloDigit;
//

// ============================================================================
/** @class CaloDigitRep  CaloDigitRep.h
 *
 *  Helper class(functor) which performs the real
 *  visualization of CaloDigit* Object
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   19/04/2001
 */
// ============================================================================

class CaloDigitRep : public std::unary_function<const LHCb::CaloDigit*, SoSeparator*> {
  ///
public:
  /** standard constructor
   *  @param DeCalo  pointer to DeCalorimeter object
   *                 (source of geometry  information)
   *  @param Type    "Object Type" for picking
   *  @param EScale  drawing scale
   *  @param EtVis   flag for drawing the transverse energy
   *  @param LogVis  flag for logarithmic drawing
   *  @param TxtVis  flag for drawing the text
   *  @param TxtSize text size
   *  @param TxtPos  text position
   */
  CaloDigitRep( const DeCalorimeter* DeCalo, const std::string& Type = "",
                const double EScale = 5 * Gaudi::Units::cm / Gaudi::Units::GeV, const bool EtVis = false,
                const bool LogVis = false, const bool TxtVis = false,
                // const double           TxtSize  = 0.05      ,
                const double TxtSize = 10, const double TxtPos = 0.02 );

  /** virtual destructor
   */
  virtual ~CaloDigitRep();

  /** the only one essential method
   *  @param digit pointer to the digit
   *  @return pointer to corresponsing Inventor Node
   */
  SoSeparator* operator()( const LHCb::CaloDigit* digit, const std::string& pickName = "" ) const;

  /// accessor to type object (for picking)
  inline const std::string& type() const { return m_type; }
  /// accessor  to calorimeter object
  inline const DeCalorimeter* deCalo() const { return m_deCalo; }
  /// get energy scale
  inline double eScale() const { return logVis() ? 1000 * m_eScale : m_eScale; }
  /// get et-visualization flag
  inline bool etVis() const { return m_etVis; }
  /// get logarithm-visualization flag
  inline bool logVis() const { return m_logVis; }
  /// get text visualization flag
  inline bool txtVis() const { return m_txtVis; }
  /// get text size
  inline double txtSize() const { return m_txtSize; }
  /// get text position
  inline double txtPos() const { return m_txtPos; }
  /// get the size of the box
  inline double size() const { return m_size; }
  /// set new value for calorimeter
  inline CaloDigitRep& setDeCalo( const DeCalorimeter* newDeCalo ) {
    m_deCalo = newDeCalo;
    return *this;
  }
  inline CaloDigitRep& setType( const std::string& newType ) {
    m_type = newType;
    return *this;
  }
  /// set new value for energy scale
  inline CaloDigitRep& setEScale( const double newEScale ) {
    m_eScale = newEScale;
    return *this;
  }
  /// set new value for et-visualization flag
  inline CaloDigitRep& setEtVis( const bool newEtVis ) {
    m_etVis = newEtVis;
    return *this;
  }
  /// set new value for logarithm visualization flag
  inline CaloDigitRep& setLogVis( const bool newLogVis ) {
    m_logVis = newLogVis;
    return *this;
  }
  /// set new value for text visualization flag
  inline CaloDigitRep& setTxtVis( const bool newTxtVis ) {
    m_txtVis = newTxtVis;
    return *this;
  }
  /// set new value for tetx size
  inline CaloDigitRep& setTxtSize( const double newTxtSize ) {
    m_txtSize = newTxtSize;
    return *this;
  }
  /// set new value for text position
  inline CaloDigitRep& setTxtPos( const double newTxtPos ) {
    m_txtPos = newTxtPos;
    return *this;
  }
  inline CaloDigitRep& setSolid( bool newSolid ) {
    m_solid = newSolid;
    ;
    return *this;
  }

private:
  ///
  std::string          m_type;
  const DeCalorimeter* m_deCalo;
  double               m_eScale;
  double               m_txtSize;
  double               m_txtPos;
  bool                 m_etVis;
  bool                 m_logVis;
  bool                 m_txtVis;
  bool                 m_solid;
  ///
  mutable double m_size;
};

// ============================================================================
#endif ///  SOCALO_CALODIGITVIS_H
// ============================================================================
