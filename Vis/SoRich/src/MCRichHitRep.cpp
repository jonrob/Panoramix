/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/** @file MCRichHitRep.cpp
 *
 *  Implementation file for visual representation object for class : MCRichHitRep
 *
 *  @author  Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date    09/05/2004
 */

#include <OnXSvc/Win32.h>

// This :
#include "MCRichHitRep.h"

#include <cmath>

// geometry
#include "GaudiKernel/Point3DTypes.h"

// Inventor
#include <Inventor/nodes/SoCoordinate3.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoLightModel.h>
#include <Inventor/nodes/SoSeparator.h>

#include <HEPVis/misc/SoStyleCache.h>
#include <HEPVis/nodes/SoHighlightMaterial.h>
#include <HEPVis/nodes/SoSceneGraph.h>

// RichEvent
#include "Event/MCRichHit.h"

// namespaces
using namespace LHCb;

// ============================================================================
// standard constructor
// ============================================================================
MCRichHitRep::MCRichHitRep( const std::string& Type, const Qualities& qualities, SoStyleCache* styleCache )
    : m_type( Type ), m_qualities( qualities ), m_styleCache( styleCache ) {}

// ============================================================================
// destructor
// ============================================================================
MCRichHitRep::~MCRichHitRep() {}

// ============================================================================
// ============================================================================
SoSeparator* MCRichHitRep::begin() const {
  SoSeparator* separator = new SoSeparator();
  separator->addChild( m_styleCache->getLightModelBaseColor() );
  return separator;
}

// ============================================================================
// ============================================================================
void MCRichHitRep::represent( const MCRichHit* hit, SoSeparator* aSeparator ) const {
  // if(!hit) return;

  // Build picking string id :
  char sid[64];
  ::sprintf( sid, "MCRichHit/0x%lx", (unsigned long)hit );

  SoSceneGraph* separator = new SoSceneGraph();
  separator->setString( sid );

  aSeparator->addChild( separator );

  separator->addChild( m_styleCache->getHighlightMaterial( qualities().color(), qualities().hcolor() ) );

  if ( markerStyle() == "marker" ) {

    // Hit position
    const Gaudi::XYZPoint& gPos = hit->entry();

    SbVec3f points[1];
    points[0].setValue( gPos.x(), gPos.y(), gPos.z() );

    // Add one So per particle
    SoCoordinate3* coordinate3 = new SoCoordinate3;
    coordinate3->point.setValues( 0, 1, points );
    separator->addChild( coordinate3 );

    // So marker type
    SoMarkerSet* markerSet = new SoMarkerSet;
    markerSet->numPoints   = 1;
    markerSet->markerIndex = qualities().markerType();
    separator->addChild( markerSet );
  }
}

// ============================================================================
