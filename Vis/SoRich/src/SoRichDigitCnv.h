/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/** @file SoRichDigitCnv.h
 *
 *  Header file for RICH "So" visualisation converter : SoRichDigitCnv
 *
 *  @author  Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date    09/05/2004
 */

#ifndef SORICH_SORICHDIGITCNV_H
#  define SORICH_SORICHDIGITCNV_H 1

// SoRich
#  include "SoRichBaseCnv.h"

// Rich SmartIDTool
#  include "RichInterfaces/IRichSmartIDTool.h"

/**  @class SoRichDigitCnv  SoRichDigitCnv.h
 *
 *   Converter for visualization of RichDigit objects
 *
 *   @author  Chris Jones   Christopher.Rob.Jones@cern.ch
 *   @date    09/05/2004
 */

class SoRichDigitCnv : public SoRichBaseCnv {
public:
  /// standard initialization method
  virtual StatusCode initialize();

  /// standard finalization  method
  virtual StatusCode finalize();

  /// Returns the representation type
  virtual long repSvcType() const;

  // the only one essential method
  virtual StatusCode createRep( DataObject* /* Object */, IOpaqueAddress*& /* Address */ );

  /// Class ID for created object == class ID for this specific converter
  static const CLID& classID();

  /// storage Type
  static unsigned char storageType();

  // protected:

  /// standard constructor
  SoRichDigitCnv( ISvcLocator* svcLoc );

  /// virtual destructor
  virtual ~SoRichDigitCnv();

private:
  /// default constructor is disabled
  SoRichDigitCnv();

  /// copy constructor is disabled
  SoRichDigitCnv( const SoRichDigitCnv& );

  /// assignment is disabled
  SoRichDigitCnv& operator=( const SoRichDigitCnv& );

private:
  /// Pointer to RichSmartID tool
  const Rich::ISmartIDTool* m_smartIDTool;
};

// ============================================================================
#endif //  SORICH_SORICHDIGITCNV_H
// ============================================================================
