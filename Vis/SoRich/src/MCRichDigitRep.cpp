/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/** @file MCRichDigitRep.cpp
 *
 *  Implementation file for visual representation object for class : MCRichDigitRep
 *
 *  @author  Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date    09/05/2004
 */

#define SORICH_MCRICHDIGITREP_CPP 1

#include <cmath>

// geometry
#include "GaudiKernel/Point3DTypes.h"

// Inventor
#include <Inventor/nodes/SoCoordinate3.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoLightModel.h>
#include <Inventor/nodes/SoRotation.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoSwitch.h>
#include <Inventor/nodes/SoTransform.h>
#include <Inventor/nodes/SoTranslation.h>

#include <HEPVis/misc/SoStyleCache.h>
#include <HEPVis/nodes/SoSceneGraph.h>

#include <OnXSvc/Win32.h>

// RichEvent
#include "Event/MCRichDigit.h"

// local
#include "MCRichDigitRep.h"

// namespaces
using namespace LHCb;

// ============================================================================
// standard constructor
// ============================================================================
MCRichDigitRep::MCRichDigitRep( const std::string& Type, const Qualities& qualities,
                                const Rich::ISmartIDTool* smartTool, SoStyleCache* styleCache )
    : m_type( Type ), m_qualities( qualities ), m_smartIDTool( smartTool ), m_styleCache( styleCache ) {}

// ============================================================================
// destructor
// ============================================================================
MCRichDigitRep::~MCRichDigitRep() {}

// ============================================================================
// ============================================================================
SoSeparator* MCRichDigitRep::operator()( const MCRichDigit* digit ) const {

  if ( 0 == digit || 0 == smartIDTool() ) return nullptr;

  // RichSmartID for this digit
  const RichSmartID id = digit->key();

  // digit position from smart ID
  Gaudi::XYZPoint gPos;
  if ( !smartIDTool()->globalPosition( id, gPos ) ) return nullptr;
  // Use this for old releases with the old interface
  // Gaudi::XYZPoint gPos = smartIDTool()->globalPosition( id );

  // Build picking string id :
  // char sid[64];
  //::sprintf(sid,"MCRichDigit/0x%lx",(unsigned long)digit);

  // new SoSeparator
  SoSceneGraph* separator = new SoSceneGraph();
  // separator->setString(sid);

  // Material
  separator->addChild( m_styleCache->getHighlightMaterial( qualities().color(), qualities().hcolor() ) );

  // light model
  separator->addChild( m_styleCache->getLightModelBaseColor() );

  // draw style
  separator->addChild( m_styleCache->getLineStyle() );

  if ( markerStyle() == "marker" ) {

    SbVec3f points[1];
    points[0].setValue( gPos.x(), gPos.y(), gPos.z() );

    // Add one So per particle
    SoCoordinate3* coordinate3 = new SoCoordinate3;
    coordinate3->point.setValues( 0, 1, points );
    separator->addChild( coordinate3 );

    // So marker type
    SoMarkerSet* markerSet = new SoMarkerSet;
    markerSet->numPoints   = 1;
    markerSet->markerIndex = qualities().markerType();
    separator->addChild( markerSet );
  }

  return separator;
}

// ============================================================================
