/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// this :
#include "SoRichSvc.h"

// Gaudi :
#include <GaudiKernel/IToolSvc.h>
#include <GaudiKernel/MsgStream.h>

// OnXSvc :
#include <OnXSvc/IUserInterfaceSvc.h>

// local
#include "MCRichHitType.h"
#include "MCRichOpticalPhotonType.h"
#include "MCRichSegmentType.h"

DECLARE_COMPONENT( SoRichSvc )

SoRichSvc::SoRichSvc( const std::string& name, ISvcLocator* pSvcLocator )
    : Service( name, pSvcLocator )
    , m_uiSvc( 0 )
    , m_soConSvc( 0 )
    , m_evtDataSvc( 0 )
    , m_toolSvc( 0 )
    , m_msgStream( 0 ) {}

SoRichSvc::~SoRichSvc() { delete m_msgStream; }

StatusCode SoRichSvc::initialize() {
  StatusCode sc = Service::initialize();
  if ( sc.isFailure() ) return sc;

  debug() << "SoRichSvc::initialize " << endmsg;

  setProperties();

  if ( !( getService( "OnXSvc", m_uiSvc ) && getService( "SoConversionSvc", m_soConSvc ) &&
          getService( "EventDataSvc", m_evtDataSvc ) && getService( "ToolSvc", m_toolSvc ) ) )
    return StatusCode::FAILURE;

  m_uiSvc->addType( new MCRichHitType( m_uiSvc, m_soConSvc, m_evtDataSvc ) );
  m_uiSvc->addType( new MCRichOpticalPhotonType( m_uiSvc, m_soConSvc, m_evtDataSvc ) );
  m_uiSvc->addType( new MCRichSegmentType( m_uiSvc, m_soConSvc, m_evtDataSvc ) );

  return sc;
}

StatusCode SoRichSvc::finalize() {
  releaseSvc( m_uiSvc );
  releaseSvc( m_soConSvc );
  releaseSvc( m_evtDataSvc );
  releaseSvc( m_toolSvc );
  debug() << "Finalized successfully" << endmsg;
  return StatusCode::SUCCESS;
}
