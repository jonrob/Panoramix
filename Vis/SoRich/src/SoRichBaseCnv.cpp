/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/** @file SoRichBaseCnv.cpp
 *
 *  Implementation file for RICH "So" visualisation converter base class : SoRichBaseCnv
 *
 *  @author  Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date    09/05/2004
 */

// STD
#include <sstream>

#include "OnXSvc/Win32.h"

#include "GaudiKernel/Stat.h"
#include "GaudiKernel/System.h"

#include "OnXSvc/ClassID.h"
#include "OnXSvc/ISoConversionSvc.h"
#include "OnXSvc/IUserInterfaceSvc.h"

#include "SoRichBaseCnv.h"

// standard constructor
SoRichBaseCnv::SoRichBaseCnv( const CLID& clid, ISvcLocator* svcLoc )
    : Rich::Converter( So_TechnologyType, clid, svcLoc ), m_soSvc( NULL ), m_uiSvc( NULL ) {
  setName( "SoRichBaseCnv" );
}

// standard (virtual) destructor
SoRichBaseCnv::~SoRichBaseCnv() {}

// finalize
StatusCode SoRichBaseCnv::finalize() {
  // release used services
  if ( 0 != m_soSvc ) {
    m_soSvc->release();
    m_soSvc = 0;
  }
  if ( 0 != m_uiSvc ) {
    m_uiSvc->release();
    m_uiSvc = 0;
  }
  // try to finalize the base class
  return Rich::Converter::finalize();
}

// main method
StatusCode SoRichBaseCnv::createRep( DataObject* Object, IOpaqueAddress*& /* Address*/ ) {
  if ( 0 == Object ) {
    return Error( "createRep(): DataObject* points to NULL!" );
  } else if ( 0 == soSvc() ) {
    return Error( "createRep(): ISoConversionSvc* points to NULL!" );
  }
  return StatusCode::SUCCESS;
}

ISoConversionSvc* SoRichBaseCnv::soSvc() const {
  if ( !m_soSvc ) { m_soSvc = this->svc<ISoConversionSvc>( "SoConversionSvc" ); }
  return m_soSvc;
}

IUserInterfaceSvc* SoRichBaseCnv::uiSvc() const {
  if ( !m_uiSvc ) { m_uiSvc = this->svc<IUserInterfaceSvc>( "OnXSvc" ); }
  return m_uiSvc;
}
