/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef SoConversionSvc_h
#define SoConversionSvc_h

// Inheritance :
#include <GaudiKernel/ConversionSvc.h>
#include <OnXSvc/ISoConversionSvc.h>

class SoConversionSvc : public ConversionSvc, public ISoConversionSvc {
public: // IInterface
  StatusCode queryInterface( const InterfaceID&, void** );

public: // IService
  virtual StatusCode initialize();

public:
  /** Convert the transient object to the requested representation.
   *  e.g. conversion to persistent objects.
   *  @return    Status code indicating success or failure
   *  @param     Object     Pointer to location of the object
   *  @param     Address Reference to location of pointer with the
   *  object address.
   */
  virtual StatusCode createRep( DataObject* Object, IOpaqueAddress*& Address );

  virtual StatusCode fillRepRefs( IOpaqueAddress* /* Address */, DataObject* /* Object  */ ) {
    return StatusCode::SUCCESS;
  }

  virtual StatusCode UpdateRepRefs( IOpaqueAddress* /* Address */, DataObject* /* Object  */ ) {
    return StatusCode::SUCCESS;
  }

  virtual StatusCode updateRepRefs( IOpaqueAddress* /* pAddress */, DataObject* /* pObject  */ ) {
    return StatusCode::SUCCESS;
  }

  SoConversionSvc( const std::string&, ISvcLocator* );
  virtual ~SoConversionSvc();
};

#endif
