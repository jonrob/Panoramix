/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef OnXSvc_OnXSvc_h
#define OnXSvc_OnXSvc_h

// Gaudi :
#include <GaudiKernel/DataObject.h>
#include <GaudiKernel/IDataManagerSvc.h>
#include <GaudiKernel/IRunable.h>
#include <GaudiKernel/MsgStream.h>
#include <GaudiKernel/Service.h>
#include <GaudiKernel/SmartDataPtr.h>
#include <GaudiKernel/SmartIF.h>

// OnXSvc :
#include <OnXSvc/IUserInterfaceSvc.h>

template <typename T>
class SvcFactory;
class IAppMgrUI;
class IDataProviderSvc;
class IDataSelector;
class ISoConversionSvc;
class IHistogramSvc;
class MsgStream;

#ifdef OSC_VERSION_LT_16_10
namespace OnX {
  class Main;
}
#endif

namespace Slash {
  namespace UI {
    class IUI;
  }
} // namespace Slash

class OnXSvc : public Service, virtual public IRunable, virtual public IUserInterfaceSvc {
public: // IInterface
  virtual StatusCode queryInterface( const InterfaceID&, void** );

public: // IService
  virtual StatusCode initialize();
  virtual StatusCode finalize();

public: // IRunable
  virtual StatusCode run();

public: // IUserInterfaceSvc
  virtual StatusCode                    visualize( const std::string& );
  virtual StatusCode                    visualize( const DataObject& );
  virtual StatusCode                    visualize( const AIDA::IHistogram& );
  virtual StatusCode                    visualize( const Gaudi::XYZPoint& );
  virtual StatusCode                    visualize( const std::vector<Gaudi::XYZPoint>&, RepType = LINES );
  virtual std::vector<std::string>      dataChildren( const std::string& );
  virtual void                          ls( const std::string&, int depth = -1 );
  virtual bool                          writeToString( const std::string&, std::string& );
  virtual void                          nextEvent();
  virtual IService*                     getService( const std::string& );
  virtual void                          clearDetectorStore();
  virtual StatusCode                    changeGeometry( const std::string& );
  virtual StatusCode                    changeColors( const std::string& );
  virtual void                          openEventFile( const std::string& );
  virtual std::string                   torgb( const std::string& );
  virtual void*                         topointer( const std::string& ) const;
  virtual IAppMgrUI*                    appMgr() const;
  virtual SoPage*                       currentSoPage();
  virtual SoRegion*                     currentSoRegion();
  virtual const std::string&            cuts() const;
  virtual Slash::UI::IWidget*           currentWidget();
  virtual void                          setSession( Slash::Core::ISession* );
  virtual Slash::Core::ISession*        session();
  virtual Slash::Core::IWriter&         printer();
  virtual void                          addType( Slash::Data::IAccessor* );
  virtual Slash::Data::IProcessor*      typeManager();
  virtual Slash::Data::IAccessor*       metaType();
  virtual std::vector<std::string>      getHighlightedSoShapeNames();
  virtual std::vector<ContainedObject*> getHighlightedContainedObject();

  OnXSvc( const std::string&, ISvcLocator* );
  virtual ~OnXSvc();

private:
  virtual void      eventInfo();
  bool              visitToXML( IDataProviderSvc*, SmartIF<IDataManagerSvc>&, SmartDataPtr<DataObject>&, std::string& );
  IDataProviderSvc* dataProvider( const std::string& );
  bool            ls( IDataProviderSvc*, SmartIF<IDataManagerSvc>&, SmartDataPtr<DataObject>&, MsgStream&, int, int& );
  Slash::UI::IUI* findUI();

private:
  IAppMgrUI*         fAppMgrUI;
  ISoConversionSvc*  fSoCnvSvc;
  IDataProviderSvc*  fEventDataSvc;
  IDataProviderSvc*  fDetectorDataSvc;
  IHistogramSvc*     fHistogramSvc;
  bool               fThreaded;
  std::string        fToolkit;
  std::string        fFile;
  bool               fOutputToTerminal;
  std::string        fCuts;
  mutable MsgStream* fLog;
#ifdef OSC_VERSION_LT_16_10
  OnX::Main* fOnXMain;
#else
  bool fOwnSession;
#endif
  Slash::Core::ISession*               fSession;
  Slash::Data::IProcessor*             fTypeManager; // FIXME fAccessorManager
  std::vector<Slash::Data::IAccessor*> fTypes;
  Slash::Data::IAccessor*              fMetaType;
  Slash::Core::IWriter*                fLibPrinter;
  Slash::Core::IWriter*                fGaudiPrinter;

private:
  /// On-demand access to MsgStream object
  inline MsgStream& msgStream() const {
    if ( !fLog ) { fLog = new MsgStream( msgSvc(), Service::name() ); }
    return *fLog;
  }
};

#endif
