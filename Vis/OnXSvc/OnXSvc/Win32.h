/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef OnXSvc_Win32_h
#define OnXSvc_Win32_h

// The below are used both in Gaudi and Windows includes !
// ( Who will have the final word : Pere or Bill ? )

#ifdef WIN32
#  define IID IIID
#  define MSG MESS
#  undef ERROR
#  undef CONST
#  undef PLANES
#endif

#endif
