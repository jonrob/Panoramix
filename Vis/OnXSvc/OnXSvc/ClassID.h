/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef SoLHCb_ClassID_h
#define SoLHCb_ClassID_h

#include <GaudiKernel/ClassID.h>

unsigned const char So_TechnologyType = 74;

/// unique CLID
static const CLID CLID_Histogram = 10000;

#endif
