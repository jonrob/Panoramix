/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <GaudiKernel/IRunable.h>
#include <OnXSvc/ISoConversionSvc.h>
#include <OnXSvc/IUserInterfaceSvc.h>

template <class T>
struct OnXSvc_Interface {
  OnXSvc_Interface() {}
  static T* cast( IInterface* in ) {
    void* out = 0;
    if ( in->queryInterface( T::interfaceID(), &out ).isSuccess() ) {
      return (T*)out;
    } else
      return 0;
  }
};

//--- Templace instantiations
struct OnXSvc_dict_Instantiations {
  std::vector<Gaudi::XYZPoint>    m_std_vector_Gaudi_XYZPoint;
  std::allocator<Gaudi::XYZPoint> m_std_allocator_Gaudi_XYZPoint;

  OnXSvc_Interface<IRunable>          m_OnXSvc_Interface_IRunable;
  OnXSvc_Interface<IUserInterfaceSvc> m_OnXSvc_Interface_IUserInterfaceSvc;
  OnXSvc_Interface<ISoConversionSvc>  m_OnXSvc_Interface_ISoConversionSvc;
};
