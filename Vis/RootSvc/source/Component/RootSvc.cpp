/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "RootSvc.h"

#include <GaudiKernel/MsgStream.h>
#include <GaudiKernel/System.h>

DECLARE_COMPONENT( RootSvc )

RootSvc::RootSvc( const std::string& aName, ISvcLocator* aSvcLoc ) : Service( aName, aSvcLoc ) {}

RootSvc::~RootSvc() {}

StatusCode RootSvc::queryInterface( const InterfaceID& aRIID, void** aPPVIF ) {
  if ( aRIID == IID_IRootSvc ) {
    *aPPVIF = static_cast<IRootSvc*>( this );
    addRef();
    return StatusCode::SUCCESS;
  } else {
    return Service::queryInterface( aRIID, aPPVIF );
  }
}

StatusCode RootSvc::initialize() {
  StatusCode status = Service::initialize();
  if ( status.isFailure() ) return status;

  MsgStream log( msgSvc(), Service::name() );
  log << MSG::DEBUG << "initialize" << endmsg;

  setProperties();
  return StatusCode::SUCCESS;
}

StatusCode RootSvc::finalize() {
  MsgStream log( msgSvc(), Service::name() );
  log << MSG::DEBUG << "RootSvc finalized successfully" << endmsg;
  return Service::finalize();
}

#include <AIDA/IHistogram.h>
#include <GaudiKernel/HistogramBase.h>
#include <TH1D.h>
#include <TVirtualPad.h>

StatusCode RootSvc::visualize( const AIDA::IHistogram& aHistogram ) {
  MsgStream                   log( msgSvc(), Service::name() );
  const Gaudi::HistogramBase* pih = dynamic_cast<const Gaudi::HistogramBase*>( &aHistogram );
  if ( pih ) {
    TH1* th = dynamic_cast<TH1*>( pih->representation() );
    if ( th ) {
      th->Draw();
      if ( gPad ) gPad->Update();
      return StatusCode::SUCCESS;
    }
  }
  log << MSG::ERROR << " histogram is not a GaudiPI one." << endmsg;
  return StatusCode::FAILURE;
}
