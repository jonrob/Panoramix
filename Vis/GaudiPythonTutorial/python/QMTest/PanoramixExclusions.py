###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiTest import LineSkipper, RegexpReplacer
from GaudiConf.QMTest.LHCbExclusions import preprocessor as LHCbPreprocessor

preprocessor = LHCbPreprocessor + \
  LineSkipper(["TimingAuditor.T...   INFO * UnpackTrack"]) + \
  LineSkipper(["OnXSvc               INFO OnX GUI file"]) + \
  LineSkipper(["VisualizationSvc     INFO Loading visualization attributes file"]) + \
  LineSkipper(["TimingAuditor.T...   INFO EVENT LOOP"])
