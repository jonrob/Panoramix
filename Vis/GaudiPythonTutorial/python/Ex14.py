###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
f_2009 = True
rerunL0DU = False

from ROOT import TH1F, TBrowser
# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "2010"
lhcbApp.Simulation = True

appConf = ApplicationMgr(OutputLevel=INFO, AppName='Ex14')

from Configurables import GaudiSequencer

MessageSvc().Format = '% F%40W%S%7W%R%T %0W%M'
if rerunL0DU:
    l0seq = GaudiSequencer("seqL0")
    appConf.TopAlg += [l0seq]
    L0Conf().setProp("L0Sequencer", l0seq)
    L0Conf().setProp("ReplaceL0BanksWithEmulated", True)

# new way to run Hlt
appConf.ExtSvc += ["LoKiSvc"]
from HltConf.Configuration import *
hltconf = HltConf()
hltconf.L0TCK = '0xDC09'
# Default options to rerun L0
# hltconf.replaceL0BanksWithEmulated = True
appConf.TopAlg += [GaudiSequencer('Hlt')]

# for 2d vertex report
from Configurables import HltVertexReportsMaker
HltVertexReportsMaker().VertexSelections += ["PV2D"]

# get timing information
appConf.ExtSvc += ['ToolSvc', 'AuditorSvc']
appConf.AuditAlgorithms = True
AuditorSvc().Auditors += ['TimingAuditor']
SequencerTimerTool().OutputLevel = 4

import GaudiPython
import gaudigadgets

appMgr = GaudiPython.AppMgr()

sel = appMgr.evtsel()
sel.open([
    'PFN:/castor/cern.ch/grid/lhcb/MC/MC09/DST/00004879/0000/00004879_00000001_1.dst'
])

evt = appMgr.evtsvc()
hist = appMgr.histSvc()
aida2root = GaudiPython.gbl.Gaudi.Utils.Aida2ROOT.aida2root
#needs to be done before ! run
import hltexamples

appMgr.run(1)
# nice printouts
hltexamples.l0()
_ = raw_input('l0_values press enter to continue...')
hltexamples.l0_values()
_ = raw_input('l0_candidates press enter to continue...')
hltexamples.l0_candidates()
_ = raw_input('Hlt decisions press enter to continue...')
hltexamples.hlt_decisions()
_ = raw_input('Hlt selection reports press enter to continue...')
hltexamples.hlt_selection_report()

#print " *** configuration  *** "
#hltexamples.hlt_selection_configuration("Hlt1HadronSingleDecision")
#hltexamples.hlt_selection_configuration("Hlt1HadronDiDecision")

_ = raw_input('press enter to continue...')
appMgr.run(200)
gaudigadgets.histo_dump(hist)
rh1 = aida2root(hist['/stat/Hlt1SingleHadronTFGuidedForward/PT'])
rh1.Draw()
