###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os, getpass
files = []

host = os.environ['HOST']
local = host.find('pctommy') > -1
if local:
    path = '/media/Work'
else:
    path = '/castor/cern.ch/user/t/truf/2009'

files = [
    path + '00005879_00000001_1.xdst', path + '00005879_00000002_1.xdst',
    path + '00005879_00000003_1.xdst', path + '00005879_00000004_1.xdst',
    path + '00005879_00000005_1.xdst', path + '00005879_00000006_1.xdst',
    path + '00005879_00000007_1.xdst', path + '00005879_00000009_1.xdst',
    path + '00005879_00000010_1.xdst', path + '00005879_00000011_1.xdst',
    path + '00005879_00000012_1.xdst', path + '00005879_00000013_1.xdst',
    path + '00005879_00000014_1.xdst', path + '00005879_00000015_1.xdst',
    path + '00005879_00000016_1.xdst', path + '00005879_00000017_1.xdst',
    path + '00005879_00000018_1.xdst', path + '00005879_00000019_1.xdst'
]

#manuel files:
files = []
temp = '/castor/cern.ch/user/m/mschille/171/SUBJOBID/outputdata/Brunel.dst'
for n in range(311):
    files.append(temp.replace('SUBJOBID', str(n)))

n = 0
for f in files:
    #nw = f.replace('.xdst','.dst')
    nw = 'Brunel' + str(n) + '.dst'
    n += 1
    os.system('python stripOffMChits.py ' + f + ' ' + nw)
    os.system('rfcp ' + nw + ' /castor/cern.ch/user/t/truf/MC2009')
