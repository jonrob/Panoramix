###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "2010"
lhcbApp.Simulation = True
lhcbApp.DDDBtag = "head-20101206"
lhcbApp.CondDBtag = "sim-20101210-vc-md100"

appConf = ApplicationMgr(OutputLevel=INFO, AppName='Ex13b')

rawwriter = OutputStream(
    'RawWriter',
    Preload=False,
    ItemList=[
        "/Event#1", "/Event/DAQ#1", "/Event/DAQ/RawEvent#1",
        "/Event/DAQ/ODIN#1"
    ],
    Output="DATAFILE='PFN:L0yes.raw' TYP='POOL_ROOTTREE' OPT='REC' ")
appConf.TopAlg = ['EventNodeKiller/EventNodeKiller']
appConf.OutStream = [rawwriter]

from Configurables import EventNodeKiller
EventNodeKiller().Nodes = [
    "Rec", "Trig", "MC", "Raw", "Gen", "Link", "pSim", "Prev", "PrevPrev",
    "Next"
]
EventSelector().PrintFreq = 50

import GaudiPython
import gaudigadgets

appMgr = GaudiPython.AppMgr()

sel = appMgr.evtsel()
sel.open(['$PANORAMIXDATA/MC2010_BsJpsiPhi__00008919_00000068_1.dst'])
evt = appMgr.evtsvc()

appMgr.algorithm(
    'RawWriter').Enable = False  # stop automatic execution of RawWriter
appMgr.algorithm(
    'EventNodeKiller').Enable = False  # stop automatic execution of Nodekiller

first = True
while 1 > 0:
    appMgr.run(1)
    # check L0
    L0dir = evt['Trig/L0/L0DUReport']
    if not L0dir: break  # probably end of input
    if first:
        first = False
        temp = evt['Rec/Header']
        if temp.condDBTags()[0].second != lhcbApp.DDDBtag or temp.condDBTags(
        )[1].second != lhcbApp.CondDBtag:
            print 'wrong tags', temp
            break
    if L0dir.decision() > 0:
        rc = appMgr.algorithm('EventNodeKiller').execute()  # output event
        rc = appMgr.algorithm('RawWriter').execute()  # output event
