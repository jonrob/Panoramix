###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
##############################################################################
# File for running Boole with default 2008 geometry, as defined in
#                                              $DDDBROOT/options/lhcb-2008.py
# Syntax is:
#   gaudirun.py Boole-2008.py <someDataFiles>.py
##############################################################################

from Boole.Configuration import *
from Configurables import UpdateManagerSvc, DDDBConf, CondDB
LHCbApp().DDDBtag = "head-20101206"
LHCbApp().CondDBtag = "sim-20101210-vc-md100"
DDDBConf(DataType="2010")
CondDB().UseLatestTags = ["2010"]
LHCbApp().EvtMax = 1000

# settings for intermediate energy 1.35 TeV early 2011
UpdateManagerSvc().ConditionsOverride += [
    "Conditions/Online/Velo/MotionSystem := double ResolPosRC =-5.0 ; double ResolPosLA = 5.0 ;"
]

Boole().DataType = "2010"
Boole().UseSpillover = False

#-- Main ('signal') event input
import os
if os.environ.has_key('myProdFile'):
    datasetName = os.environ['myProdFile']
else:
    datasetName = 'gauss'

#
if len(os.sys.argv) > 1:
    cycle = int(os.sys.argv[1])
    inputfiles = []
    for c in range(cycle):
        inputfiles.append("DATAFILE='PFN:" + datasetName + "_" + str(c) +
                          ".sim' TYP='POOL_ROOTTREE' OPT='READ'")
else:
    inputfiles = [
        "DATAFILE='PFN:" + datasetName + ".sim' TYP='POOL_ROOTTREE' OPT='READ'"
    ]

# Set the property used to build other file names
Boole().setProp("DatasetName", datasetName)

EventSelector().Input = inputfiles

#-- File catalogs
FileCatalog().Catalogs = ["xmlcatalog_file:" + datasetName + ".xml"]

#-- Save the monitoring histograms
HistogramPersistencySvc().OutputFile = datasetName + '_Boole.root'

#-- Possible output streams. Enabled by setting the corresponding Boole() property
outputName = Boole().outputName()
# writeDigi = true: Standard .digi in POOL format. If extendedDigi = true includes also MCHits
OutputStream(
    "DigiWriter"
).Output = "DATAFILE='PFN:" + outputName + ".digi' TYP='POOL_ROOTTREE' OPT='REC'"

import GaudiPython
appMgr = GaudiPython.AppMgr()
evt = appMgr.evtsvc()

while 1 > 0:
    appMgr.run(1)
    if evt['MC/Header'] == None: break
