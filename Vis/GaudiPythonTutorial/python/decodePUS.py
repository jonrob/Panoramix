###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# get the basic configuration from here
from LHCbConfig import *

lhcbApp.DataType = "MC09"

appConf = ApplicationMgr(OutputLevel=INFO, AppName='DecodePUS')

import GaudiPython
from gaudigadgets import *

appMgr = GaudiPython.AppMgr()

sel = appMgr.evtsel()
sel.open(['$PANORAMIXDATA/MC2010_BsJpsiPhi__00008919_00000068_1.dst'])
evt = appMgr.evtsvc()

VeloChannelID = GaudiPython.gbl.LHCb.VeloChannelID
VeloLiteCluster = GaudiPython.gbl.LHCb.VeloLiteCluster
VeloCluster = GaudiPython.gbl.LHCb.VeloCluster
pair = GaudiPython.makeClass('pair<int,unsigned int>')
vector = GaudiPython.makeClass('vector<pair<int,unsigned int> >')
newcont = GaudiPython.makeClass(
    'KeyedContainer<LHCb::VeloCluster,Containers::KeyedObjectManager<Containers::hashmap> >'
)


def bits(w, n, m):
    temp = w >> n
    mask = (1 << (m - n + 1)) - 1
    result = temp & mask
    return result


def tobin(x, count=32):
    """
         Integer to binary
         Count is number of bits
         """
    return "".join(map(lambda y: str((x >> y) & 1), range(count - 1, -1, -1)))


def decode_l0pus():
    fixit = False
    boole = evt['MC/DigiHeader'].applicationVersion()
    if boole == '"v12r5"': fixit = True
    rb = evt['DAQ/RawEvent']
    Enums = getEnumNames('LHCb::RawBank')
    BankType = Enums['BankType']
    PuHits = [[], [], [], []]
    for k in range(len(BankType)):
        if BankType[k] == 'L0PU':
            size = int((rb.banks(k)[0].size()) / 4 + 0.5)
            for l in range(size):
                hit1 = 0, 0
                hit2 = 0, 0
                word = rb.banks(k)[0].data()[l]
                w = bits(word, 0, 15)
                if w != 0:
                    sensor = w >> 14
                    strip = bits(w, 0, 13)
                    hit1 = sensor, strip
                w = bits(word, 16, 31)
                if w != 0:
                    sensor = w >> 14
                    strip = bits(w, 0, 13)
                    hit2 = sensor, strip
                if not fixit:
                    PuHits[hit1[0]].append(hit1[1])
                    if w != 0: PuHits[hit2[0]].append(hit2[1])
                else:
                    # try to correct the bugs in Boole v12r5
                    strip = int(hit1[1] / 4) * 4 + 2
                    if len(PuHits[hit1[0]]) < 1:
                        PuHits[hit1[0]].append(strip)
                    elif PuHits[hit1[0]][len(PuHits[hit1[0]]) - 1] != strip:
                        PuHits[hit1[0]].append(strip)
                    strip = int(hit2[1] / 4) * 4 + 2
                    if len(PuHits[hit2[0]]) < 1:
                        PuHits[hit2[0]].append(strip)
                    elif PuHits[hit2[0]][len(PuHits[hit2[0]]) - 1] != strip:
                        PuHits[hit2[0]].append(strip)
            break
    return PuHits


def decode_l0analog():
    PuHits = [[], [], [], []]
    ab = evt['Raw/Velo/Clusters'].containedObjects()
    for cl in ab:
        if cl.isPileUp():
            strip = cl.channelID().strip()
            sensor = cl.channelID().sensor() - 128
            # convert to quater precision
            strip = int(strip / 4) * 4 + 2
            PuHits[sensor].append(strip)
    return PuHits


def create_L0PusClusters():
    xb = decode_l0pus()
    #create veloclusters from strip nr and sensor:
    L0Puscont = newcont()
    GaudiPython.setOwnership(L0Puscont, False)
    for sensor in range(len(xb)):
        for strip in xb[sensor]:
            ####chid = VeloChannelID(sensor+128,strip,pustype)
            chid = VeloChannelID(sensor + 128, strip)
            vlcl = VeloLiteCluster(chid, 1., 1, False)
            npair = pair(strip, 99)
            adcValues = vector()
            adcValues.push_back(npair)
            vcl = VeloCluster(vlcl, adcValues)
            vcl.key().setChannelID(chid.channelID())
            GaudiPython.setOwnership(vcl, False)
            L0Puscont.add(vcl)
    evt.registerObject('/Event/Raw/L0Pus/BinaryHits', L0Puscont)
    return


# main sequence
appMgr.run(1)
create_L0PusClusters()
for x in evt['Raw/L0Pus/BinaryHits']:
    print x.strip(0)
