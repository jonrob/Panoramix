###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
MCdata = True
debug = False
f_2008 = True
L0_Accept = True
Elastic = False
No_Spill_over = False
Hlt = False

# get the basic configuration from here
from LHCbConfig import *
if not f_2008:
    lhcbApp.DataType = "DC06"
else:
    lhcbApp.DataType = "2009"
    if MCdata: lhcbApp.Simulation = True

if not MCdata:
    EventSelector(
        ## no OT no Rich
        #    Input = ["DATA='castor:/castor/cern.ch/grid/lhcb/data/2008/RAW/LHCb/PHYSICS_COSMICS/30434/030434_0000075487.raw' SVC='LHCb::MDFSelector'"]
        ## no Velo no Rich
        #    Input = ["DATA='castor:/castor/cern.ch/grid/lhcb/data/2008/RAW/LHCb/PHYSICS_COSMICS/30639/030639_0000076290.raw' SVC='LHCb::MDFSelector'"]
        ## with Rich
        Input=[
            "DATA='castor:/castor/cern.ch/grid/lhcb/data/2008/RAW/LHCb/PHYSICS/30618/030618_0000076246.raw' SVC='LHCb::MDFSelector'"
        ])
if MCdata and f_2008:
    #files = []
    #file  = "DATA='/castor/cern.ch/grid/lhcb/MC/2008/DST/00003991/0000/00003991_00000XXX_5.dst'  TYP='POOL_ROOTTREE' OPT='READ'"
    #for n in range(1,200) :
    # ff = file.replace('XXX','%(X)03d'%{'X':n})
    # files.append(ff)
    files = [
        "DATA='/castor/cern.ch/grid/lhcb/MC/MC09/DST/00004758/0000/00004758_00000001_1.dst'  TYP='POOL_ROOTTREE' OPT='READ'"
    ]
    EventSelector(Input=files)

if Hlt:
    files = ["DATA='Phys_noHlt.dst'  TYP='POOL_ROOTTREE' OPT='READ'"]
    EventSelector(Input=files)

EventSelector().PrintFreq = 1000

appConf = ApplicationMgr(OutputLevel=INFO, AppName='RawBufferSizes')

import GaudiPython, sys, math, os
from ROOT import TH1F, TCanvas, TFile
TH1F.AddDirectory(
    False)  # disable the automatic association of  histograms to directories
from gaudigadgets import *

bb = getEnumNames('LHCb::RawBank')
BankType = bb['BankType']

VeloTell1 = []
for k in range(42):
    VeloTell1.append(k)
for k in range(64, 106):
    VeloTell1.append(k)
for k in range(128, 132):
    VeloTell1.append(k)

hlist = {}
h_all = TH1F('h_all', 'raw eventsize', 500, -0.5, 100000.)
lumi2 = False
lumi5 = False

appMgr = GaudiPython.AppMgr()
evt = appMgr.evtsvc()

nevent = 10000
first_event = 0


def empty():
    flag = False
    if evt['Gen/Collisions'].size() == 1:
        col = evt['Gen/Collisions'][0]
        if col.processType() == 91: flag = True
    return flag


def nospillover():
    flag = False
    next = evt['Next/MC/Header']
    prev = evt['Prev/MC/Header']
    preprev = evt['PrevPrev/MC/Header']
    if not next and not prev and not preprev: flag = True
    return flag


while nevent > 0:
    appMgr.run(1)
    rb = evt['DAQ/RawEvent']
    if not rb:
        print 'end of input files reached:', first_event
        break

# look for L0 decision:
    if L0_Accept and evt['Trig/L0/L0DUReport'].decision() == 0: continue
    if No_Spill_over and not nospillover(): continue
    if Elastic and not empty(): continue

    n_raw = 0
    nevent -= 1
    for k in BankType:
        nTell1 = rb.banks(k).size()
        name = BankType[k]
        if first_event == 0 and nTell1 > 0:
            print k, name, nTell1
        ntot = 0
        for t in range(nTell1):
            nword = rb.banks(k).at(t).totalSize()
            ntot += nword
            hname = name + '_T_' + str(t)
            if not hlist.has_key(hname):
                hlist[hname] = TH1F(hname, hname, 200, -0.5, 1000.)
            result = hlist[hname].Fill(nword)
        if not hlist.has_key(name):
            hlist[name] = TH1F(name, 'banktype ' + name, 1000, -0.5, 100000.)
        if nTell1 > 0:
            result = hlist[name].Fill(ntot)
        n_raw += ntot

    h_all.Fill(n_raw)
    first_event += 1

nTell1s = {}
f = TFile('rawbuffer.root', 'recreate')
for k in BankType:
    name = BankType[k]
    if hlist.has_key(name):
        print '%20s' % (name), 'mean size = ', '%10.2f' % (
            hlist[name].GetMean())
        result = hlist[name].Write()
        for h in hlist:
            if h.find(name + '_T_') > -1:
                if nTell1s.has_key(name):
                    nTell1s[name] += 1
                else:
                    nTell1s[name] = 1
                print '%25s' % (h), '%10.2f' % (hlist[h].GetMean())

print "+++++++++++ compact summary +++++++++++++"
for k in BankType:
    name = BankType[k]
    if not nTell1s.has_key(name): nTell1s[name] = 0
    if hlist.has_key(name):
        print '%20s %4i' % (name, nTell1s[name]), 'mean size = ', '%10.2f' % (
            hlist[name].GetMean())
        result = hlist[name].Write()

result = h_all.Write()
for h in hlist:
    hlist[h].Write()
f.Close()
print 'total event size in memory:', '%10.2f' % (h_all.GetMean())
print 'number of events read', '%10.2f' % (first_event)
#
from ROOT import TH1F, TCanvas, TFile
f = TFile('rawbuffer.root')
