###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "2009"
#lhcbApp.DDDBtag    =  "head-20091120"
#lhcbApp.CondDBtag  =  "sim-20091211-vc15mm-md100"
lhcbApp.DDDBtag = "head-20100119"
lhcbApp.CondDBtag = "sim-20100119-vc15mm-md100"

lhcbApp.Simulation = True

from Configurables import L0Conf
L0Conf().TCK = "0x1309"
from Configurables import L0MuonAlg
l0muon = L0MuonAlg("L0Muon")
l0muon.FoiXSize = [6, 5, 0, 12, 12]
l0muon.FoiYSize = [0, 0, 0, 1, 1]
from Configurables import L0CaloAlg
l0calo = L0CaloAlg('L0Calo')
l0calo.AddECALToHCAL = False
l0seq = GaudiSequencer("seqL0")
L0Conf().setProp("L0Sequencer", l0seq)
L0Conf().setProp("ReplaceL0BanksWithEmulated", True)
L0Conf().setProp("DataType", "2009")
from Configurables import EventNodeKiller
evtkiller = EventNodeKiller('L0')
evtkiller.Nodes = ["Trig", "Raw"]
ApplicationMgr().TopAlg += [l0seq, evtkiller]
EventSelector().PrintFreq = 10000

output = OutputStream("myDstWriter")
output.Output = "DATAFILE='PFN:00005817_00000001_2.xdst' TYP='POOL_ROOTTREE' OPT='REC'"
output.ItemList = [
    "/Event/pRec#999", "/Event/Rec#999", "/Event/DAQ/RawEvent#999",
    "/Event/DAQ/ODIN#999", "/Event/pSim#999", "/Event/MC#999",
    "/Event/Gen#999", "/Event/Link#999"
]
appConf.OutStream.append("myDstWriter")

import GaudiPython
from GaudiPython import gbl

appMgr = GaudiPython.AppMgr()
evt = appMgr.evtsvc()
sel = appMgr.evtsel()
#prefix = 'PFN:/media/Work/'
prefix = 'PFN:/castor/cern.ch/grid/lhcb/MC/2009/XDST/00005817/0000/'

sel.open([
    prefix + '00005817_00000001_1.xdst', prefix + '00005817_00000002_1.xdst',
    prefix + '00005817_00000003_1.xdst', prefix + '00005817_00000004_1.xdst'
])

appMgr.algorithm('myDstWriter').Enable = False

while 1 > 0:
    appMgr.run(1)
    if not evt['Rec/Header']: break  # probably end of input
    appMgr.algorithm('myDstWriter').execute()
