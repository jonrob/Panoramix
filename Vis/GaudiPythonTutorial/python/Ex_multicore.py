###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from processing import Process, Queue, Pool

from random import *
from ROOT import *


#------The function to run in each 'process'
def g(n):
    print 'doing the next iteration to calc something [%i]' % n
    i = 0
    while i < 10000000:
        r = random()
        s = TMath.ASin(r)
        i = i + 1
    print 'woke up again '
    return s


#------Use up to 4 processes
p = Pool(4)

workitems = range(20)
result = p.map_async(g, workitems)

print result.get(timeout=1000)
