###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import httplib, urllib, urlparse, os, sys


def httpExists(url):
    host, path = urlparse.urlsplit(url)[1:3]
    connection = httplib.HTTPConnection(host)  ## Make HTTPConnection Object
    connection.request("HEAD", path)
    response = connection.getresponse()  ## Grab HTTPResponse Object
    if response.status == 200: return True
    return False


debug = False

fn = 'http://lhcb-logs.cern.ch/storage/lhcb/MC/2008/LOG/0000prodid/0000/0000xxxx/jobDescription.xml'

description = "Simulation description"
value = '![CDATA['

for prodid in range(3066, 3090):

    temp = fn.replace('prodid', '%04d' % prodid)

    for k in range(0, 100):
        #try to find log file in the first 100
        url = temp.replace('xxxx', '%04d' % k)
        if debug: print 'try now:', url, httpExists(url)
        result = ''
        if httpExists(url):
            print 'open url', url
            xx = urllib.urlopen(url)
            all = xx.readlines()
            p1 = -1
            p2 = -1
            for line in all:
                if p1 < 0: p1 = line.find(description)
                if p1 > -1:
                    if debug: print 'line found', p1
                    p2 = line.find(value)
                    if p2 > -1:
                        s = len(value)
                        ss = line[p2 + s:p2 + 100].find(']')
                        result = line[p2 + s:p2 + s + ss]
                        if debug: print 'line2 found', p2, result
                        break

        if result != '': break

    print prodid, result
