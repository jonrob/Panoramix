###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os, getpass
files = []

host = os.environ['HOST']
local = host.find('pctommy') > -1
if local:
    path = '/media/Work'
else:
    path = '/castor/cern.ch/user/t/truf/2009'

prodids = [5731, 5730, 5727]

if local:
    allfiles = os.listdir(path)
    for f in allfiles:
        for prod in prodids:
            if f.find('_2.dst') > -1 and f.find('0000' + str(prod)) > -1:
                files.append(path + '/' + f)

for f in files:
    nw = f.replace('_2.dst', '_2_nolumi.dst')
    os.system('python stripOffLumi.py ' + f + ' ' + nw)
