###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# setenv PYTHONPATH /afs/cern.ch/sw/lcg/external/processing/0.51/${CMTCONFIG}/lib/python2.5/site-packages:$PYTHONPATH
from ROOT import TFile, TH1F, TH2F, TCanvas, TBrowser, gStyle, TText, TMath, TF1, gROOT, MakeNullPointer, gSystem
f_2008 = False
debug = False
nbeg = 2001
nmax = 999

# get the basic configuration from here
from LHCbConfig import *
if not f_2008:
    lhcbApp.DataType = "DC06"
else:
    lhcbApp.DataType = "2008"
    lhcbApp.Simulation = True

appConf = ApplicationMgr(OutputLevel=INFO, AppName='ExMuonEff2')
EventSelector().PrintFreq = 1000

import GaudiPython
import GaudiKernel.SystemOfUnits as units

gbl = GaudiPython.gbl
ParticleID = gbl.LHCb.ParticleID
MCTrackInfo = gbl.MCTrackInfo
MCParticle = gbl.LHCb.MCParticle
Track = gbl.LHCb.Track
part = MakeNullPointer(MCParticle)


def muoncheck(t, linkedTo):
    pt = None
    track2mc = linkedTo(MCParticle, Track, 'Rec/Track/Best')
    for mcp in track2mc.range(t):
        if mcp.particleID().abspid() == 13:
            # J/Psi vertex
            jpsi = mcp.mother()
            if jpsi:
                bee = jpsi.mother()
                if bee:
                    ovx = bee.originVertex()
                    if ovx.position().z() > -100. and ovx.position().z() < 50.:
                        pt = mcp.pt()
    return pt


def myIsmuon(key, evt):
    flag = False
    for m in evt['Rec/Track/Muon']:
        if key == m.key():
            flag = True
            break
    return flag


def lifetime(p):
    tau = -1
    bee = p.mother()
    if bee:
        ov = bee.originVertex()
        ev = bee.endVertices()[0].target()
        tau = (ev.time() - ov.time()) / bee.gamma()
    return tau


h_muonpt_rec = TH1F('h_muonpt_rec', 'true muon pt of reconstructed muons', 100,
                    0., 10000.)
h_muonpt_true = TH1F('h_muonpt_true', 'true muon pt', 100, 0., 10000.)

h_muonz_mc = TH1F('h_muonz_mc', 'z of prod vertex for true muons ', 100, -250.,
                  250.)
h_muonz_rv = TH1F('h_muonz_rv',
                  'z of prod vertex for reconstructible in Velo ', 100, -250.,
                  250.)
h_muonz_rl = TH1F('h_muonz_rl', 'z of prod vertex for reconstructible muons ',
                  100, -250., 250.)
h_muonz_rw = TH1F('h_muonz_rw',
                  'z of prod vertex for reconstructed muons in Velo ', 100,
                  -250., 250.)
h_muonz_re = TH1F('h_muonz_re', 'z of prod vertex for reconstructed muons ',
                  100, -250., 250.)
h_muonz_id = TH1F('h_muonz_id',
                  'z of prod vertex for reconstructed and pid muons ', 100,
                  -250., 250.)

h_muonz_mc2 = TH1F('h_muonz_mc2',
                   'z of prod vertex for true muons, theta>0.02 ', 100, -250.,
                   250.)
h_muonz_rv2 = TH1F(
    'h_muonz_rv2', 'z of prod vertex for reconstructible in Velo, theta>0.02 ',
    100, -250., 250.)
h_muonz_rl2 = TH1F('h_muonz_rl2',
                   'z of prod vertex for reconstructible muons, theta>0.02 ',
                   100, -250., 250.)
h_muonz_rw2 = TH1F(
    'h_muonz_rw2',
    'z of prod vertex for reconstructed muons in Velo, theta>0.02  ', 100,
    -250., 250.)
h_muonz_re2 = TH1F('h_muonz_re2',
                   'z of prod vertex for reconstructed muons, theta>0.02  ',
                   100, -250., 250.)
h_muonz_id2 = TH1F(
    'h_muonz_id2',
    'z of prod vertex for reconstructed and pid muons, theta>0.02 ', 100,
    -250., 250.)

h_tau_mc = TH1F('h_tau_mc', 'tau Bs for true muons ', 100, 0., 10.)
h_tau_rv = TH1F('h_tau_rv', 'tau Bs for reconstructible in Velo ', 100, 0.,
                10.)
h_tau_rl = TH1F('h_tau_rl', 'tau Bs for reconstructible muons ', 100, 0., 10.)
h_tau_re = TH1F('h_tau_re', 'tau Bs for reconstructed muons ', 100, 0., 10.)
h_tau_rw = TH1F('h_tau_rw', 'tau Bs for reconstructed muons in Velo ', 100, 0.,
                10.)
h_tau_id = TH1F('h_tau_id', 'tau Bs for reconstructed and pid muons ', 100, 0.,
                10.)
h_tau_mc2 = TH1F('h_tau_mc2', 'tau Bs for true muons, theta>0.02  ', 100, 0.,
                 10.)
h_tau_rv2 = TH1F('h_tau_rv2',
                 'tau Bs for reconstructible in Velo, theta>0.02  ', 100, 0.,
                 10.)
h_tau_rl2 = TH1F('h_tau_rl2', 'tau Bs for reconstructible muons, theta>0.02  ',
                 100, 0., 10.)
h_tau_rw2 = TH1F('h_tau_rw2',
                 'tau Bs for reconstructed muons in Velo, theta>0.02  ', 100,
                 0., 10.)
h_tau_re2 = TH1F('h_tau_re2', 'tau Bs for reconstructed muons, theta>0.02  ',
                 100, 0., 10.)
h_tau_id2 = TH1F('h_tau_id2',
                 'tau Bs for reconstructed and pid muons, theta>0.02  ', 100,
                 0., 10.)

myHistos = {}
for h in gROOT.GetList():
    myHistos[h.GetName()] = h


def processFile(file):
    print '+++++ Start with file ', file
    htemp = {}
    for h in myHistos.keys():
        htemp[h] = myHistos[h].Clone()

    appMgr = GaudiPython.AppMgr()
    sel = appMgr.evtsel()
    sel.open(file)
    evt = appMgr.evtsvc()
    msg = appMgr.service('MessageSvc', 'IMessageSvc')
    dps = appMgr.service('EventDataSvc', 'IDataProviderSvc')
    from LinkerInstances.eventassoc import linkedFrom, linkedTo

    MCDecayFinder = appMgr.toolsvc().create(
        'MCDecayFinder', interface='IMCDecayFinder')
    MCDecayFinder.setDecay('J/psi(1S)')

    jpsi = 443
    ppSvc = appMgr.ppSvc()
    print ppSvc.find(ParticleID(jpsi))
    bs_tau = ppSvc.find(ParticleID(531)).lifetime()

    while 0 < 1:
        appMgr.run(1)
        # check if there are still valid events
        if not evt['Rec/Header']: break
        mc = evt['MC/Particles']
        trackinfo = MCTrackInfo(dps, msg)
        mc2track = linkedFrom(Track, MCParticle, 'Rec/Track/Best')

        cont = evt['Rec/Track/Best']
        for t in cont:
            # check if linked to muon
            pt = muoncheck(t, linkedTo)
            # if debug: print t.key(),pt
            if not pt: continue
            rc = htemp['h_muonpt_true'].Fill(pt)
            if myIsmuon(t.key(), evt): htemp['h_muonpt_rec'].Fill(pt)
# search for muons in MCParticle container from B decays

        if not MCDecayFinder.hasDecay(mc): continue
        decaylist = []
        while MCDecayFinder.findDecay(mc, part) > 0:
            pclone = part.clone()
            GaudiPython.setOwnership(pclone, True)
            decaylist.append(pclone)
        for decay in decaylist:
            bee = decay.mother()
            if not bee: continue
            ovx = bee.originVertex()
            if ovx.position().z() < -100. or ovx.position().z() > 50.: continue

            pid = decay.particleID().pid()
            tau = lifetime(decay) / bs_tau
            daughters = GaudiPython.gbl.std.vector('const LHCb::MCParticle*')()
            MCDecayFinder.descendants(decay, daughters)
            # find the two muons
            muons = []
            for dpart in daughters:
                if dpart.particleID().abspid() != 13: continue
                mother = dpart.mother()
                if mother.particleID().pid() != jpsi: continue
                muons.append(dpart)
            if len(muons) != 2:
                ## print 'big problem',muons,len(daughters)
                continue


#
# check both muons above 20mrad
            thetacut = True
            for m in muons:
                mom = m.momentum()
                theta = mom.pt() / mom.pz()
                thetax = abs(mom.px() / mom.pz())
                thetay = abs(mom.py() / mom.pz())
                if theta < 0.02 or thetax > 0.30 or thetay > 0.25:
                    thetacut = False
            oVx = muons[0].originVertex().position()
            rho = oVx.rho()
            z = oVx.z()
            htemp['h_muonz_mc'].Fill(z)
            htemp['h_tau_mc'].Fill(tau)
            if thetacut:
                htemp['h_muonz_mc2'].Fill(z)
                htemp['h_tau_mc2'].Fill(tau)
            if not trackinfo.hasVelo(muons[0]) or not trackinfo.hasVelo(
                    muons[1]):
                continue
            htemp['h_muonz_rv'].Fill(z)
            htemp['h_tau_rv'].Fill(tau)
            if thetacut:
                htemp['h_muonz_rv2'].Fill(z)
                htemp['h_tau_rv2'].Fill(tau)
            if not trackinfo.hasVeloAndT(
                    muons[0]) or not trackinfo.hasVeloAndT(muons[1]):
                continue
            htemp['h_muonz_rl'].Fill(z)
            htemp['h_tau_rl'].Fill(tau)
            if thetacut:
                htemp['h_muonz_rl2'].Fill(z)
                htemp['h_tau_rl2'].Fill(tau)
            reco1 = False
            for t in mc2track.range(muons[0]):
                if t.type() != t.Long and t.type() != t.Velo and t.type(
                ) != t.Downstream:
                    continue
                reco1 = t.key()
                break
            reco2 = False
            for t in mc2track.range(muons[1]):
                if t.type() != t.Long and t.type() != t.Velo and t.type(
                ) != t.Downstream:
                    continue
                reco2 = t.key()
                break
            if not reco1 or not reco2: continue
            htemp['h_muonz_rw'].Fill(z)
            htemp['h_tau_rw'].Fill(tau)
            if thetacut:
                htemp['h_muonz_rw2'].Fill(z)
                htemp['h_tau_rw2'].Fill(tau)
            reco1 = False
            for t in mc2track.range(muons[0]):
                if t.type() != t.Long: continue
                reco1 = t.key()
                break
            reco2 = False
            for t in mc2track.range(muons[1]):
                if t.type() != t.Long: continue
                reco2 = t.key()
                break
            if not reco1 or not reco2: continue
            htemp['h_muonz_re'].Fill(z)
            htemp['h_tau_re'].Fill(tau)
            if thetacut:
                htemp['h_muonz_re2'].Fill(z)
                htemp['h_tau_re2'].Fill(tau)
            if myIsmuon(reco1, evt) and myIsmuon(reco2, evt):
                htemp['h_muonz_id'].Fill(z)
                htemp['h_tau_id'].Fill(tau)
                if thetacut:
                    htemp['h_muonz_id2'].Fill(z)
                    htemp['h_tau_id2'].Fill(tau)
    print 'finished ', file, 'entries ', h_muonz_mc.GetEntries(
    ), h_muonz_id.GetEntries()
    appMgr.stop()
    return htemp

if __name__ == '__main__':
    files = []
    file = 'PFN:/castor/cern.ch/grid/lhcb/production/DC06/phys-v4-lumi2/00002146/DST/0000/00002146_0000XXXX_5.dst'
    if debug: nmax = 10
    for n in range(nbeg, nbeg + nmax):
        ff = file.replace('XXXX', '%(X)04d' % {'X': n})
        x = os.system('nsls ' + ff.replace('PFN:', ''))
        if x == 0: files.append(ff)
        #------Use up to 8 processes

    if not debug:
        from processing import Pool
        n = 8
        pool = Pool(n)
        result = pool.map_async(processFile, files)
        for hlist in result.get(timeout=10000):
            for h in hlist:
                if myHistos[h]: myHistos[h].Add(hlist[h])
                else: myHistos[h] = hlist[h]
    else:
        myHistos = processFile(files[0])

myHistos['h_muonpt_true'].Draw()
myHistos['h_muonpt_rec'].Draw('same')

f = TFile('muoneff.root', 'recreate')
for h in myHistos:
    sc = myHistos[h].Write()
f.Close()


def eff_calc(h1, h2):
    h3 = h1.Clone()
    h3.Sumw2()
    h3.Divide(h1, h2, 1., 1., 'B')
    return h3


myHistos['h_eff_rv'] = eff_calc(myHistos['h_muonz_rv'], myHistos['h_muonz_mc'])
myHistos['h_eff_rl'] = eff_calc(myHistos['h_muonz_rl'], myHistos['h_muonz_rv'])
myHistos['h_eff_rw'] = eff_calc(myHistos['h_muonz_rw'], myHistos['h_muonz_rl'])
myHistos['h_eff_re'] = eff_calc(myHistos['h_muonz_re'], myHistos['h_muonz_rw'])
myHistos['h_eff_id'] = eff_calc(myHistos['h_muonz_id'], myHistos['h_muonz_re'])

myHistos['h_eff_rv2'] = eff_calc(myHistos['h_muonz_rv2'],
                                 myHistos['h_muonz_mc2'])
myHistos['h_eff_rl2'] = eff_calc(myHistos['h_muonz_rl2'],
                                 myHistos['h_muonz_rv2'])
myHistos['h_eff_rw2'] = eff_calc(myHistos['h_muonz_rw2'],
                                 myHistos['h_muonz_rl2'])
myHistos['h_eff_re2'] = eff_calc(myHistos['h_muonz_re2'],
                                 myHistos['h_muonz_rw2'])
myHistos['h_eff_id2'] = eff_calc(myHistos['h_muonz_id2'],
                                 myHistos['h_muonz_re2'])

myHistos['h_eff_tau_rv'] = eff_calc(myHistos['h_tau_rv'], myHistos['h_tau_mc'])
myHistos['h_eff_tau_rl'] = eff_calc(myHistos['h_tau_rl'], myHistos['h_tau_rv'])
myHistos['h_eff_tau_rw'] = eff_calc(myHistos['h_tau_rw'], myHistos['h_tau_rl'])
myHistos['h_eff_tau_re'] = eff_calc(myHistos['h_tau_re'], myHistos['h_tau_rw'])
myHistos['h_eff_tau_id'] = eff_calc(myHistos['h_tau_id'], myHistos['h_tau_re'])

myHistos['h_eff_tau_rv2'] = eff_calc(myHistos['h_tau_rv2'],
                                     myHistos['h_tau_mc2'])
myHistos['h_eff_tau_rl2'] = eff_calc(myHistos['h_tau_rl2'],
                                     myHistos['h_tau_rv2'])
myHistos['h_eff_tau_rw2'] = eff_calc(myHistos['h_tau_rw2'],
                                     myHistos['h_tau_rl2'])
myHistos['h_eff_tau_re2'] = eff_calc(myHistos['h_tau_re2'],
                                     myHistos['h_tau_rw2'])
myHistos['h_eff_tau_id2'] = eff_calc(myHistos['h_tau_id2'],
                                     myHistos['h_tau_re2'])

c1 = TCanvas('c1', '', 600, 400)
for x in myHistos:
    if x.find('h_eff') > -1:
        if x.find('h_eff_tau') == -1:
            myHistos[x].SetMinimum(0.9)
            lowmin = x.find('id') > -1 or x.find('rl') > -1 or x.find(
                'rv') > -1
            if lowmin: myHistos[x].SetMinimum(0.7)
            myHistos[x].Fit('pol0', "", "", -150., 150.)
            print 'Probability = ', x, myHistos[x].GetFunction(
                'pol0').GetProb()
            myHistos[x].Fit('pol1', "", "", -150., 150.)
            print 'Probability = ', x, myHistos[x].GetFunction(
                'pol1').GetProb()
            print 'Slope = ', x, myHistos[x].GetFunction('pol1').GetParameter(
                1)
        else:
            myHistos[x].SetMinimum(0.9)
            lowmin = x.find('id') > -1
            if lowmin: myHistos[x].SetMinimum(0.5)
            myHistos[x].Fit('pol0', "", "", 0., 10.)
            print 'Probability = ', x, myHistos[x].GetFunction(
                'pol0').GetProb()
            myHistos[x].Fit('pol1', "", "", 0., 10.)
            print 'Probability = ', x, myHistos[x].GetFunction(
                'pol1').GetProb()
            print 'Slope = ', x, myHistos[x].GetFunction('pol1').GetParameter(
                1)
        myHistos[x].Draw()
        c1.Print(x + '.jpg')

myHistos['h_eff_tau_rv'].Draw()
c1.Print('h_eff_tau_rv.jpg')
myHistos['h_eff_tau_rl'].Draw()
c1.Print('h_eff_tau_rl.jpg')
myHistos['h_eff_tau_rw'].Draw()
c1.Print('h_eff_tau_rw.jpg')
myHistos['h_eff_tau_re'].Draw()
c1.Print('h_eff_tau_re.jpg')
myHistos['h_eff_tau_id'].Draw()
c1.Print('h_eff_tau_id.jpg')

alleff = eff_calc(myHistos['h_tau_id'], myHistos['h_tau_mc'])
alleff.SetMinimum(0.5)
alleff.SetMaximum(0.7)
alleff.Fit('pol0', "", "", 0., 10.)
print 'Probability = ', alleff.GetFunction('pol0').GetProb()
alleff.Fit('pol1', "", "", 0., 10.)
print 'Probability = ', alleff.GetFunction('pol1').GetProb()
print 'Slope = ', alleff.GetFunction('pol1').GetParameter(1)
alleff.Draw()

c1.Print('h_eff_tau_all.jpg')

alleff = eff_calc(myHistos['h_tau_id2'], myHistos['h_tau_mc'])
alleff.SetMinimum(0.5)
alleff.SetMaximum(0.7)
alleff.Fit('pol0', "", "", 0., 10.)
print 'Probability = ', alleff.GetFunction('pol0').GetProb()
alleff.Fit('pol1', "", "", 0., 10.)
print 'Probability = ', alleff.GetFunction('pol1').GetProb()
print 'Slope = ', alleff.GetFunction('pol1').GetParameter(1)
alleff.Draw()
c1.Print('h_eff_tau_all2.jpg')


def convert(f):
    myHistos = {}
    f.ReadAll()
    for h in f.GetList():
        myHistos[h.GetName()] = h
    return myHistos
