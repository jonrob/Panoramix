###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# get the basic configuration from here
from LHCbConfig import *
appConf = ApplicationMgr(OutputLevel=INFO, AppName='Ex13')

lhcbApp.DataType = "2010"
lhcbApp.Simulation = True
# container name
candidates = 'AllStreams/Phys/Bs2JpsiPhiDetachedLine/Particles'

InputCopyStream(
).Output = "DATAFILE='PFN:myEvents.dst' TYP='POOL_ROOTTREE' OPT='REC' "
appConf.OutStream = [InputCopyStream()]
EventSelector().PrintFreq = 100

import GaudiPython
from gaudigadgets import *

appMgr = GaudiPython.AppMgr()

sel = appMgr.evtsel()
sel.open(['$PANORAMIXDATA/MC2010_BsJpsiPhi__00008919_00000068_1.dst'])
evt = appMgr.evtsvc()

appMgr.algorithm('InputCopyStream').Enable = False

for n in range(500):
    appMgr.run(1)
    if not evt['Rec/Header']: break
    cont = evt[candidates]
    if cont:
        out = False
        for b in cont:
            if b.pt() > 5000.: out = True
        if out:
            print 'write event', evt['Rec/Header']
            appMgr.algorithm('InputCopyStream').execute()
