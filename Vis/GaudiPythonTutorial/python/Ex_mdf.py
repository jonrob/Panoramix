###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "2008"

appConf = ApplicationMgr()
appConf.OutputLevel = INFO
appConf.AppName = 'Ex_mdf'

EventSelector(
    ## castor  Input = ["DATA='castor:/castor/cern.ch/grid/lhcb/data/2008/RAW/LHCb/PHYSICS_COSMICS/30933/030933_0000077704.raw' SVC='LHCb::MDFSelector'"]
    ## file
    Input=[
        "DATA='/afs/cern.ch/user/t/truf/033062_0000082835.raw' SVC='LHCb::MDFSelector'"
    ])
import GaudiPython

appMgr = GaudiPython.AppMgr()
sel = appMgr.evtsel()
print 'configuration of evtselector:', EventSelector().Input
print 'actual setting:              ', sel.properties()['Input'].value()

evt = appMgr.evtsvc()
appMgr.run(1)
evt.dump()
