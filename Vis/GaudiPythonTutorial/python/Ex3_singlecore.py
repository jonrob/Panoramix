###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import time
start = time.time()
# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "DC06"

importOptions('$PANORAMIXROOT/options/PanoramixCleanUp.py')
importOptions('$PANORAMIXROOT/options/PanoramixFullRec.py')

appConf = ApplicationMgr(OutputLevel=3, AppName='Ex3_singlecore')

EventSelector().PrintFreq = 100

#--- Inport needed modules to be reused in all processes---------------
import GaudiPython
from ROOT import TH1F, TFile, gROOT

h_chi2 = TH1F('h_chi2', 'fit chi2', 100, 0., 10000.)


def processFile(file):
    appMgr = GaudiPython.AppMgr()
    sel = appMgr.evtsel()
    sel.open(file)
    evt = appMgr.evtsvc()
    while 0 < 1:
        appMgr.run(1)
        # check if there are still valid events
        if not evt['Rec/Header']: break
        cont = evt['Rec/Track/Best']
        if cont:
            for t in cont:
                success = h_chi2.Fill(t.chi2())
    return 'success'


if __name__ == '__main__':
    files = [
        'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000001_5.dst'
    ]

    result = processFile(files)

    h_chi2.Draw()
    end = time.time()
    print 'total time', end - start
    print h_chi2.GetEntries()
