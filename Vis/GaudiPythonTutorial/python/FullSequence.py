###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
OnlyTracking = True
# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "DC06"

import GaudiKernel.SystemOfUnits as units
appConf = ApplicationMgr(OutputLevel=WARNING, AppName='FullSequence')

from RecConf.Configuration import *
RecSysConf()

from Configurables import (ProcessPhase, GaudiSequencer)
# Set main sequence
brunelSeq = GaudiSequencer("BrunelSequencer")
appConf.TopAlg = [brunelSeq]
brunelSeq.Members += [
    "ProcessPhase/Init", "ProcessPhase/Reco", "ProcessPhase/MCLinks",
    "ProcessPhase/Check"
]
ProcessPhase("Init").DetectorList += ["Brunel", "Calo"]
# Convert Calo 'packed' banks to 'short' banks if needed
GaudiSequencer("InitCaloSeq").Members += ["GaudiSequencer/CaloBanksHandler"]
importOptions("$CALODAQROOT/options/CaloBankHandler.opts")
# MC truth for tracks
ProcessPhase("MCLinks").DetectorList += ["Tr"]
GaudiSequencer("MCLinksTrSeq").Members += ["TrackAssociator"]

from TrackSys.Configuration import *
TrackSys().ExpertTracking += ["simplifiedGeometry"]

from Configurables import TrackResChecker
ProcessPhase("Check").DetectorList += ["Tr"]
importOptions("$TRACKSYSROOT/options/TrackChecking.opts")
CheckLongTrack = TrackResChecker('CheckLongTrack')
CheckLongTrack.FullDetail = False
CheckLongTrack.SplitByAlgorithm = True
CheckLongTrack.PlotsByMeasType = False
CheckLongTrack.CheckAmbiguity = False

if not OnlyTracking:
    # General sequence for ProtoParticle recalibration in DaVinci
    protoPRecalibration = GaudiSequencer('ProtoPRecalibration')
    appConf.TopAlg += [GaudiSequencer('ProtoPRecalibration')]
    protoPRecalibration.MeasureTime = True
    # Fills the combined DLL values in the charged Proto Particles
    protoPRecalibration.Members += ["ChargedProtoCombineDLLsAlg"]

#=================================================================
AnalysisConf().DataType = lhcbApp.DataType
PhysConf().DataType = lhcbApp.DataType
appConf.TopAlg += [PhysConf().initSequence(), AnalysisConf().initSequence()]
importOptions("$DAVINCIASSOCIATORSROOT/options/DaVinciAssociators.opts")

HistogramPersistencySvc().OutputFile = "FullSequence.root"  # Monitoring histos

EventSelector().PrintFreq = 100

import GaudiPython

appMgr = GaudiPython.AppMgr()

if OnlyTracking:
    # to avoid crash in Calo init, until understood
    appMgr.algorithm('InitCaloSeq').Enable = False
    appMgr.algorithm('RecoCALOSeq').Enable = False
    appMgr.algorithm('RecoRICHSeq').Enable = False
    appMgr.algorithm('RecoPROTOSeq').Enable = False
    appMgr.algorithm('RecoMUONSeq').Enable = False

sel = appMgr.evtsel()
sel.open(
    ['PFN:/afs/cern.ch/user/t/truf/vol1/Bsjpsiphi_00001620_00000004_5.dst'])
evt = appMgr.evtsvc()

appMgr.run(20)
