###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from ROOT import gROOT, TFile, TCanvas, TText, TH1F, TH2F
# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "DC06"

appConf = ApplicationMgr(OutputLevel=INFO, AppName='Ex17')
appConf.ExtSvc += ['TagCollectionSvc/EvtTupleSvc']

import GaudiPython
import gaudigadgets

file = 'PFN:castor:/castor/cern.ch/user/l/lshchuts/stripping/SETC_newDST_v31/1.root'

EventSelector(Input=[
    "COLLECTION=\'TagCreator/1\' DATAFILE='%s' TYP='POOL_ROOT' SEL='(PreselHeavyDimuon>0)'"
    % file
])
FileCatalog().Catalogs = ["xmlcatalog_file:newDST_v31.xml"]

appMgr = GaudiPython.AppMgr()

# some private functions
import math


def calc_canvas_size(l):
    nx = int(math.sqrt(l))
    ny = nx
    while nx * ny < l:
        ny += 1
    return nx, ny


evt = appMgr.evtsvc()
etc = appMgr.evtcolsvc()
etc.dump()
tupl = etc['EventSelector.DataStreamTool_1/TagCreator/1']
print dir(tupl)
print 'Dead end, go to ROOT directly'
_ = raw_input('press enter to continue...')

f = TFile().Open(file.replace('PFN:castor:', 'rfio://'))
tupl = gROOT.FindObjectAny('<local>_TagCreator_1')
lvs = tupl.GetListOfLeaves()
presel = []
for n in lvs:
    print n.GetName()
    if n.GetName().find('Presel') > -1: presel.append(n.GetName())

tupl.Draw('PreselBs2MuMu')

tc = TCanvas('tc', 'presel vs nPrim', 2048, 1536)
nx, ny = calc_canvas_size(len(presel))
tc.Divide(nx, ny)

n = 1
for s in presel:
    sc = tc.cd(n)
    h = TH2F('h_' + s, s + ':nPrim', 10, -0.5, 9.5, 2, -0.5, 1.5)
    h.SetMarkerSize(4.)
    h.SetStats(0)
    sc = tupl.Project('h_' + s, s + ':nPrim')
    h.DrawCopy('text90')
    txt = TText(0.4, 0.5, s)
    txt.DrawClone()
    n += 1

_ = raw_input('press enter to continue...')
# read filtered DST
appMgr.run(1)
print evt['Rec/Header']
