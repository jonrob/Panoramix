###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from ROOT import TH1F, TH2F, TCanvas
# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "2008"
lhcbApp.Simulation = True

appConf = ApplicationMgr(OutputLevel=INFO, AppName='HltVeloPos')

appConf.ExtSvc += ["LoKiSvc"]
from HltConf.Configuration import *
hltconf = HltConf()
hltconf.oldStyle = False
hltconf.hltType = 'PA+LU+VE+MU+HA+PH+EL'

# get timing information
appConf.ExtSvc += ['ToolSvc', 'AuditorSvc']
appConf.AuditAlgorithms = True
AuditorSvc().Auditors += ['TimingAuditor']
SequencerTimerTool().OutputLevel = 4
EventSelector().PrintFreq = 1000

import GaudiPython
import gaudigadgets

appMgr = GaudiPython.AppMgr()
sel = appMgr.evtsel()
sel.open(['PFN:/castor/cern.ch/user/t/truf/MC2008/00003402_121314_v17.dst'])

evt = appMgr.evtsvc()

hxyA = TH2F('hxyA', 'xy position of A-side vertices', 50, -3., 3., 50, -3., 3.)
hxyC = TH2F('hxyC', 'xy position of C-side vertices', 50, -3., 3., 50, -3., 3.)
hzA = TH1F('hzA', 'z position of A-side vertices', 50, -200., 200.)
hzC = TH1F('hzC', 'z position of C-side vertices', 50, -200., 200.)
hxydel = TH2F('hxydel', 'del x vs del y position of A/C-side vertices', 50,
              -1., 1., 50, -1., 1.)
hzdel = TH1F('hzdel', 'del z position of A/C-side vertices', 50, -2., 2.)

while 1 > 0:
    appMgr.run(1)
    if not evt['DAQ/ODIN']: break
    pva = evt['Hlt/VertexReports'].vertexReport('Hlt1VeloASideDecision')
    pvc = evt['Hlt/VertexReports'].vertexReport('Hlt1VeloCSideDecision')
    if pva:
        for pv in evt['Hlt/VertexReports'].vertexReport(
                'Hlt1VeloASideDecision'):
            posA = pv.target().position()
            hxyA.Fill(posA.x(), posA.y())
            hzA.Fill(posA.z())
    if pvc:
        for pv in evt['Hlt/VertexReports'].vertexReport(
                'Hlt1VeloCSideDecision'):
            posC = pv.target().position()
            hxyC.Fill(posC.x(), posC.y())
            hzC.Fill(posC.z())
            if pva:
                for pv2 in evt['Hlt/VertexReports'].vertexReport(
                        'Hlt1VeloASideDecision'):
                    posA = pv2.target().position()
                    hxydel.Fill(posA.x() - posC.x(), posA.y() - posC.y())
                    hzdel.Fill(posA.z() - posC.z())

c1 = TCanvas('c1', 'primary vertex position', 1200, 800)
c1.Divide(2, 3)
c1.cd(1)
hxyA.Draw()
c1.cd(2)
hzA.Draw()
c1.cd(3)
hxyC.Draw()
c1.cd(4)
hzC.Draw()
c1.cd(5)
hxydel.Draw()
c1.cd(6)
hzdel.Draw()
