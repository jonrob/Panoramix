###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import time
start = time.time()
# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "DC06"

analysis = AnalysisConf()
analysis.DataType = "DC06"
from StrippingSelections.StrippingBs2JpsiPhi import sequence as seqBs2JpsiPhi
appConf.TopAlg += [seqBs2JpsiPhi.sequence()]

appConf = ApplicationMgr(OutputLevel=3, AppName='Ex2_singlecore')

EventSelector().PrintFreq = 100

#--- Inport needed modules to be reused in all processes---------------
import GaudiPython
from ROOT import TH1F, TFile, gROOT

h_bmass = TH1F('h_bmass', 'Mass of B candidate', 100, 5200., 5500.)


def processFile(file):
    appMgr = GaudiPython.AppMgr()
    sel = appMgr.evtsel()
    sel.open(file)
    evt = appMgr.evtsvc()
    while 0 < 1:
        appMgr.run(1)
        # check if there are still valid events
        if evt['Rec/Header'] == None: break
        cont = evt['Phys/Bs2JpsiPhi/Particles']
        if cont != None:
            for b in cont:
                success = h_bmass.Fill(b.momentum().mass())
    return 'success'


if __name__ == '__main__':
    files = [
        'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000001_5.dst',
        'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000002_5.dst',
        'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000003_5.dst',
        'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000004_5.dst',
        'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000005_5.dst',
        'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000007_5.dst',
        'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000008_5.dst',
        'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000009_5.dst',
        'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000010_5.dst',
        'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000012_5.dst',
        'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000013_5.dst',
        'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000014_5.dst',
        'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000015_5.dst',
        'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000016_5.dst',
        'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000017_5.dst',
        'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000018_5.dst'
    ]

    result = processFile(files)

    h_bmass.Draw()
    end = time.time()
    print 'total time', end - start
    print h_bmass.GetEntries()
