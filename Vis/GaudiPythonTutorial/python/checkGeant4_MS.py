###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from ROOT import *
# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "2010"
lhcbApp.Simulation = True
lhcbApp.DDDBtag = "head-20101206"
lhcbApp.CondDBtag = "sim-20101210-vc-md100"

from Configurables import TrackMasterExtrapolator, StateSimpleBetheBlochEnergyCorrectionTool
tconf = TrackMasterExtrapolator('TrackMasterExtrapolator')
tconf.ApplyEnergyLossCorr = False
# new, 22/01/2009 added by Wouter to TrackTools
# StateSimpleBetheBlochEnergyCorrectionTool.UseEnergyLossError =  True

appConf = ApplicationMgr(OutputLevel=3, AppName='CheckGeant4 MS')
import GaudiPython, math, sys

from LinkerInstances.eventassoc import *
# reading SIM files, no ODIN bank
EventClockSvc().EventTimeDecoder = 'FakeEventTime'

appMgr = GaudiPython.AppMgr()
sel = appMgr.evtsel()
evt = appMgr.evtsvc()
State = GaudiPython.gbl.LHCb.State
extrap = appMgr.toolsvc().create(
    'TrackMasterExtrapolator', interface='ITrackExtrapolator')
#extrap   = appMgr.toolsvc().create('TrackLinearExtrapolator', interface='ITrackExtrapolator')

if len(sys.argv) == 1:
    fn = '$PANORAMIXDATA/Sel_00006198_00000001_1.xdst'
else:
    fn = sys.argv[1]

l = fn.find('.')
a = fn.rfind('/') + 1
outfn = fn[a:l] + '_MS_plots.root'
outplots = fn[a:l] + '_MS'

# old study
#fn = 'JpsiKs_00001463_00000005_1.sim'

# files with new Geant 4
#fn = 'EM71-LHEP-v25r9.Apr19.sim'
#fn = 'EMFast-LHEP.Apr17.sim'
#fn = 'Apr04.EMFast-QGSP.sim'
#fn = 'EMG4Def-LHEP.Apr18.sim'
#fn = 'Apr03.EMG4Def-QGSP.sim'

sel.open(['PFN:' + fn])

h_pull_x = TH1F('h_pull_x', '  pull at measured point x', 100, -9., 9.)
h_pull_xp = TH1F('h_pull_xp',
                 '  pull at measured point x for positive particles', 100, -9.,
                 9.)
h_pull_y = TH1F('h_pull_y', '  pull at measured point y', 100, -9., 9.)
h_pull_tx = TH1F('h_pull_tx', ' pull at measured point tx', 100, -9., 9.)
h_pull_ty = TH1F('h_pull_ty', ' pull at measured point ty', 100, -9., 9.)
h_pull_qp = TH1F('h_pull_qp', ' pull at measured point q/p', 100, -9., 9.)

h2_err_x = TH2F('h2_err_x', '  diff vs err', 100, -0.5, 0.5, 100, 0., 0.5)
h2_err_y = TH2F('h2_err_y', '  diff vs err', 100, -0.5, 0.5, 100, 0., 0.5)
h2_err_tx = TH2F('h2_err_tx', '  diff vs err', 100, -0.005, 0.005, 100, 0.,
                 0.005)
h2_err_ty = TH2F('h2_err_ty', '  diff vs err', 100, -0.005, 0.005, 100, 0.,
                 0.005)

h2_diff_x_pt = TH2F('h2_diff_x', '  diff x vs pt', 100, 0., 5., 100, -0.1, 0.1)
h2_diff_y_pt = TH2F('h2_diff_y', '  diff y vs pt', 100, 0., 5., 100, -0.1, 0.1)
h2_diff_xp_pt = TH2F('h2_diff_xp', '  diff x vs pt pos particles', 100, 0., 5.,
                     100, -0.1, 0.1)
h2_diff_yp_pt = TH2F('h2_diff_yp', '  diff y vs pt pos particles', 100, 0., 5.,
                     100, -0.1, 0.1)

#/////////////////////////////////////////////////////////////////////////////
#  Do the execution
#/////////////////////////////////////////////////////////////////////////////

for k in range(1000):
    appMgr.run(1)
    if not evt['MC/Header']: break
    mcpold = -1
    for hit in evt['MC/Velo/Hits']:
        mcp = hit.mcParticle()
        # assume first entry point after RF shield is the first in the list
        if mcp.key() == mcpold: continue
        mcpold = mcp.key()
        id = mcp.particleID()
        p = mcp.p()

        # don't take electrons
        if id.abspid() == 11: continue
        # ask for some momentum
        if p < 2000. or hit.p() < 2000.: continue

        # create state at origin:
        vx = mcp.originVertex().position()
        charge = id.threeCharge() / 3.
        pmom = mcp.momentum()
        px = pmom.px()
        py = pmom.py()
        pz = pmom.pz()
        pt = mcp.pt() / 1000.

        # ask for inside RF foil
        if vx.rho() > 1. or abs(vx.z()) > 200.: continue
        # ask for reasonable angles
        if abs(px / pz) > 0.3 or abs(py / pz) > 0.3: continue

        s_origin = State()
        s_origin.setX(vx.x())
        s_origin.setY(vx.y())
        s_origin.setZ(vx.z())
        s_origin.setTx(px / pz)
        s_origin.setTy(py / pz)
        s_origin.setQOverP(charge / p)

        # create state at MCHit:

        vxe = hit.entry()
        pmom = hit.displacement()
        px = pmom.x()
        py = pmom.y()
        pz = pmom.z()
        p = hit.p()

        # ask for inside Velo and forward
        if vxe.z() > 800. or vxe.z() - vx.z() < 0.: continue

        s_hit = State()
        s_hit.setX(vxe.x())
        s_hit.setY(vxe.y())
        s_hit.setZ(vxe.z())
        s_hit.setTx(px / pz)
        s_hit.setTy(py / pz)
        s_hit.setQOverP(charge / p)

        s_extrap = s_origin.clone()
        result = extrap.propagate(s_extrap, vxe.z())

        # make pulls
        sigx = math.sqrt(s_extrap.covariance()(0, 0))
        sigy = math.sqrt(s_extrap.covariance()(1, 1))
        sigtx = math.sqrt(s_extrap.covariance()(2, 2))
        sigty = math.sqrt(s_extrap.covariance()(3, 3))
        sigqp = math.sqrt(s_extrap.covariance()(4, 4))

        if sigx == 0 or sigy == 0 or sigtx == 0 or sigty == 0:
            try:
                print result, s_extrap.x(), s_extrap.y(), s_extrap.z()
                print mcp, vx.x(), vx.y(), vx.z(), vxe.x(), vxe.y(), vxe.z(
                ), sigx, sigy, sigtx, sigty
            except:
                print result, s_extrap.x(), s_extrap.y(), s_extrap.z()
                print mcp, vx.x(), vx.y(), vx.z(), vxe.x(), vxe.y(), vxe.z(
                ), sigx, sigy, sigtx, sigty
            continue

        if sigqp == 0: sigqp = 1E-12
        if sigx == 0: sigx = 1E-12
        if sigy == 0: sigy = 1E-12
        if sigtx == 0: sigtx = 1E-12
        if sigty == 0: sigty = 1E-12

        dx = (s_extrap.x() - s_hit.x())
        dy = (s_extrap.y() - s_hit.y())
        dtx = (s_extrap.tx() - s_hit.tx())
        dty = (s_extrap.ty() - s_hit.ty())
        dqp = (s_extrap.qOverP() - s_hit.qOverP())

        result = h_pull_x.Fill(dx / sigx)
        if charge > 0:
            result = h_pull_xp.Fill(dx / sigx)
        result = h_pull_y.Fill(dy / sigy)
        result = h_pull_tx.Fill(dtx / sigtx)
        result = h_pull_ty.Fill(dty / sigty)
        result = h_pull_qp.Fill(dqp / sigqp)

        result = h2_err_x.Fill(dx, sigx)
        result = h2_err_y.Fill(dy, sigy)
        result = h2_err_tx.Fill(dtx, sigtx)
        result = h2_err_ty.Fill(dty, sigty)

        result = h2_diff_x_pt.Fill(pt, dx)
        result = h2_diff_y_pt.Fill(pt, dy)
        if charge > 0:
            result = h2_diff_xp_pt.Fill(pt, dx)
            result = h2_diff_yp_pt.Fill(pt, dy)

f = TFile(outfn, 'recreate')
for h in gROOT.GetList():
    h.Write()
f.Close()

gStyle.SetOptFit(1111)
#gStyle.SetStatW(0.4)
#gStyle.SetStatFontSize(11)

cs = TCanvas('cs', 'results', 600, 400)
h_pull_x.Fit('gaus')
h_pull_y.Fit('gaus')
h_pull_tx.Fit('gaus')
h_pull_ty.Fit('gaus')
h_pull_qp.Fit('gaus')

csall = TCanvas('csall', 'results', 900, 600)
csall.Divide(2, 2)
csall.cd(1)
h_pull_x.Draw()
csall.cd(2)
h_pull_y.Draw()
csall.cd(3)
h_pull_tx.Draw()
csall.cd(4)
h_pull_ty.Draw()
csall.Print(outplots + 'pulls.gif')

cs2d = TCanvas('cs2d', 'results', 900, 600)
cs2d.Divide(2, 2)
cs2d.cd(1)
h2_err_x.Draw()
cs2d.cd(2)
h2_err_y.Draw()
cs2d.cd(3)
h2_err_tx.Draw()
cs2d.cd(4)
h2_err_ty.Draw()
cs2d.Print(outplots + 'errors.gif')

g = TF1('g', 'gaus')

h2_diff_x_pt.FitSlicesY(g)
h2_diff_y_pt.FitSlicesY(g)

cs2 = TCanvas('cs2', 'results', 600, 400)
h2_diff_x_2 = gROOT.FindObjectAny('h2_diff_x_2')
h2_diff_y_2 = gROOT.FindObjectAny('h2_diff_y_2')
h2_diff_x_2.Draw()
h2_diff_y_2.Draw('same')
cs2.Print(outplots + 'diff_x_y.gif')
