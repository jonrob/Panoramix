###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from ROOT import TH1F, TBrowser, TCanvas
from LHCbConfig import *
lhcbApp.DataType = "2010"
lhcbApp.Simulation = True

appConf = ApplicationMgr(OutputLevel=INFO, AppName='Ex3e')
appConf.HistogramPersistency = "ROOT"
HistogramPersistencySvc.OutputFile = "Ex3e.root"

import GaudiPython

appMgr = GaudiPython.AppMgr()
sel = appMgr.evtsel()
sel.open(['$PANORAMIXDATA/MC2010_BsJpsiPhi__00008919_00000068_1.dst'])
evt = appMgr.evtsvc()


class MyAlg(GaudiPython.PyAlgorithm):
    def execute(self):
        evh = evt['Rec/Header']
        mcps = evt['MC/Particles']
        print 'event # = ', evh.evtNumber()
        rc = h1.fill(mcps.size())
        return True


hist = appMgr.histsvc()
# book AIDA histogram
h1 = hist.book('h1', '# of MCParticles', 40, 0, 5000)
# add python alg to Gaudi
appMgr.addAlgorithm(MyAlg())
appMgr.run(10)

_ = raw_input('press enter to display histograms ...')

hist.dump()
print hist['h1']
aida2root = GaudiPython.gbl.Gaudi.Utils.Aida2ROOT.aida2root
rh1 = aida2root(hist['h1'])
rc = rh1.Draw()
rc = rh1.Fit('gaus')
