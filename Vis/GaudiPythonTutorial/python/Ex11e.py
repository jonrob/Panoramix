###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# get the basic configuration from here
from LHCbConfig import *
################################################

lhcbApp.DataType = "2010"
lhcbApp.Simulation = True
lhcbApp.DDDBtag = "head-20101206"
lhcbApp.CondDBtag = "sim-20101210-vc-md100"

# container name
candidates = 'AllStreams/Phys/Bs2JpsiPhiDetachedLine/Particles'

appConf.OutputLevel = INFO
appConf.AppName = 'Ex11e'

EventSelector().PrintFreq = 100

output = OutputStream("myDstWriter")
output.Output = "DATAFILE='PFN:Bs2JpsiPhi.dst' SVC='Gaudi::RootCnvSvc' OPT='REC'"
output.ItemList = [
    "/Event/MC#999", "/Event/pSim#999", "/Event/pRec#999",
    "/Event/AllStreams#999", "/Event/Link#999", "/Event/Gen#999",
    "/Event/DAQ/RawEvent#1", "/Event/DAQ/ODIN#1", "/Event/Rec/Header#1",
    "/Event/Rec/Status#1"
]
#output.TakeOptionalFromTES = True
#output.Preload         = False
#output.PreloadOptItems = False

appConf.OutStream.append("myDstWriter")

import GaudiPython
import gaudigadgets
appMgr = GaudiPython.AppMgr()
appMgr.service('IODataManager').UseGFAL = False

sel = appMgr.evtsel()
sel.open(['$PANORAMIXDATA/MC2010_BsJpsiPhi__00008919_00000068_1.dst'])

evt = appMgr.evtsvc()
appMgr.algorithm("myDstWriter").Enable = False
appMgr.algorithm("EventNodeKiller").Enable = False

nsignals = 10
while 0 < 1:
    print 'read event'
    appMgr.run(1)
    # check if there are still valid events
    if not evt['Rec/Header']: break
    cont = evt[candidates]
    if cont:
        if cont.size() > 0:
            gaudigadgets.nodes(evt, True)
            nsignals -= 1
            print evt['Rec/Header']
            rc = appMgr.algorithm("myDstWriter").execute()
        else:
            rc = appMgr.algorithm("myDstWriter").execute()
    if nsignals == 0: break
