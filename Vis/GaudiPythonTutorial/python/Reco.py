###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# my private high level configuration
nevents = 1000
#evt_type = 'minbias_450'
#evt_type = 'minbias_7000'
evt_type = 'Beam12_H'
#evt_type = 'Beam12_O'
#evt_type = 'Beam12_C'
#bfield = 'off'
bfield = 'on'
#velopos = 'open'
velopos = 'closed'
dst = False
#dst = True
f_2010 = True

# defining list of histograms
from ROOT import TH1F, TCanvas
hlist = {}
tlist = {}
nmax = 99.5
if evt_type.find('minbias') > -1: nmax = 249.5
hlist['long'] = TH1F('long', 'nr of long tracks', 100, -0.5, nmax)
hlist['velo'] = TH1F('velo', 'nr of velo tracks', 100, -0.5, nmax)
hlist['seed'] = TH1F('seed', 'nr of T tracks', 100, -0.5, nmax)
hlist['long_true'] = TH1F('long_true', 'nr of long tracks true', 100, -0.5,
                          nmax)
hlist['velo_true'] = TH1F('velo_true', 'nr of velo tracks true', 100, -0.5,
                          nmax)
hlist['seed_true'] = TH1F('seed_true', 'nr of T tracks true', 100, -0.5, nmax)

tlist['spd'] = TH1F('spd ', 'spd mulitplicity ', 100, -0.5, 500.)
tlist['hcal'] = TH1F('hcal', 'hcal mulitplicity', 100, -0.5, 500.)

# reconstruction configuration
# get the basic configuration from here
from LHCbConfig import *
if not f_2010:
    lhcbApp.DataType = "DC06"
else:
    lhcbApp.DataType = "2010"
    lhcbApp.Simulation = True
    lhcbApp.DDDBtag = "head-20101206"
    lhcbApp.CondDBtag = "sim-20101210-vc-md100"

appConf = ApplicationMgr(OutputLevel=INFO, AppName='Reco')

if dst:
    #FIXES to be included if running over reconstructed data
    from Configurables import TESCheck, EventNodeKiller
    appConf.TopAlg = ['TESCheck/EvtCheck', 'EventNodeKiller/EventNodeKiller']
    EvtCheck = TESCheck('EvtCheck')
    EvtCheck.Inputs = ["Link/Rec/Track/Best"]
    ENKiller = EventNodeKiller('EventNodeKiller')
    ENKiller.Nodes = ["Rec", "Link/Rec"]

from Configurables import ProcessPhase

# Reconstruction
appConf.TopAlg += ["ProcessPhase/Init", "ProcessPhase/Reco"]
# Initialisation phase
Init = ProcessPhase('Init')
Init.DetectorList = ["Etc", "Brunel"]

from RecConf.Configuration import *
RecSysConf()
# overwrite Default reco sequence
RecSysConf().RecoSequence = ["VELO", "TT", "IT", "OT", "Tr", "Vertex"]

if bfield == 'off':
    MagneticFieldSvc().UseConstantField = True
    MagneticFieldSvc().UseConditions = False
    TrackSys().setSpecialDataOption("fieldOff", True)

    HistogramPersistencySvc.OutputFile = "FieldOff.root"
else:
    HistogramPersistencySvc.OutputFile = "FieldOn.root"
    TrackSys().setSpecialDataOption("fieldOff", False)

if velopos == 'open':
    TrackSys().setSpecialDataOption('veloOpen', True)
if evt_type.find('Beam12') > -1:
    importOptions('$TRACKSYSROOT/options/VeloBeamGas.opts')

from Configurables import TrackAssociator
ta_velo = TrackAssociator('Velo')
ta_seed = TrackAssociator('Seed')
ta_best = TrackAssociator('Best')
ta_velo.TracksInContainer = 'Rec/Track/Velo'
ta_seed.TracksInContainer = 'Rec/Track/Seed'
ta_best.TracksInContainer = 'Rec/Track/Best'

appConf.TopAlg += [ta_velo, ta_seed, ta_best]

import GaudiPython
from gaudigadgets import *

appMgr = GaudiPython.AppMgr()
sel = appMgr.evtsel()
if evt_type == 'minbias_450' and velopos == 'closed':
    sel.open([
        '/castor/cern.ch/lhcb/background/Pilot_2007/30000000.5k.B_OFF.VeloClose.digi'
    ])
if evt_type == 'minbias_450' and velopos == 'open':
    sel.open([
        '/castor/cern.ch/lhcb/background/Pilot_2007/30000000.5k.B_OFF.VeloOpen.digi'
    ])
if evt_type == 'minbias_7000' and velopos == 'closed':
    sel.open([
        '/castor/cern.ch/user/s/sblusk/Brunel/7TeV_FieldOff_MinBias/dst/Brunel_7TeV_Boff_MinBias_1000_0.dst'
    ])

#beam gas
if evt_type == 'Beam12_O':
    sel.open([
        '/castor/cern.ch/user/s/sblusk/7TeV_Boff_Beam12_O/MisAlCh1/Brunel/dst/Brunel_7TeV_Boff_Beam12_O_MisAlCh1_1000_0.dst',
        '/castor/cern.ch/user/s/sblusk/7TeV_Boff_Beam12_O/MisAlCh1/Brunel/dst/Brunel_7TeV_Boff_Beam12_O_MisAlCh1_1000_1.dst',
        '/castor/cern.ch/user/s/sblusk/7TeV_Boff_Beam12_O/MisAlCh1/Brunel/dst/Brunel_7TeV_Boff_Beam12_O_MisAlCh1_1000_2.dst'
    ])
if evt_type == 'Beam12_C':
    sel.open([
        '/castor/cern.ch/user/s/sblusk/7TeV_Boff_Beam12_C/MisAlCh1/Brunel/dst/Brunel_7TeV_Boff_Beam12_C_MisAlCh1_1000_0.dst',
        '/castor/cern.ch/user/s/sblusk/7TeV_Boff_Beam12_C/MisAlCh1/Brunel/dst/Brunel_7TeV_Boff_Beam12_C_MisAlCh1_1000_1.dst',
        '/castor/cern.ch/user/s/sblusk/7TeV_Boff_Beam12_C/MisAlCh1/Brunel/dst/Brunel_7TeV_Boff_Beam12_C_MisAlCh1_1000_2.dst'
    ])
if evt_type == 'Beam12_H':
    sel.open([
        '/castor/cern.ch/user/s/sblusk/7TeV_Boff_Beam12_H/MisAlCh1/Brunel/dst/Brunel_7TeV_Boff_Beam12_H_MisAlCh1_1000_0.dst',
        '/castor/cern.ch/user/s/sblusk/7TeV_Boff_Beam12_H/MisAlCh1/Brunel/dst/Brunel_7TeV_Boff_Beam12_H_MisAlCh1_1000_1.dst',
        '/castor/cern.ch/user/s/sblusk/7TeV_Boff_Beam12_H/MisAlCh1/Brunel/dst/Brunel_7TeV_Boff_Beam12_H_MisAlCh1_1000_2.dst'
    ])

if f_2010:
    sel.open(['$PANORAMIXDATA/Sel_00006198_00000001_1.xdst'])

evt = appMgr.evtsvc()
from LinkerInstances.eventassoc import *
MCParticle = GaudiPython.gbl.LHCb.MCParticle
Track = GaudiPython.gbl.LHCb.Track
longTrack = Track().Long


def n_true(cont, type=None):
    n = 0
    ltrack2part = linkedTo(MCParticle, Track, cont)
    for t in evt[cont]:
        if type and t.type(): continue
        if ltrack2part.range(t).size() > 0: n += 1
    return float(n)


for n in range(nevents):
    appMgr.run(1)
    if not evt['DAQ/RawEvent']: break
    nlong = 0
    cont = 'Rec/Track/Best'
    for t in evt[cont]:
        if t.type() == t.Long: nlong += 1
    sc = hlist['seed'].Fill(evt['Rec/Track/Seed'].size())
    sc = hlist['velo'].Fill(evt['Rec/Track/Velo'].size())
    sc = hlist['long'].Fill(float(nlong))
    nlong_true = n_true('Rec/Track/Best', longTrack)
    nvelo_true = n_true('Rec/Track/Velo')
    nt_true = n_true('Rec/Track/Seed')
    sc = hlist['seed_true'].Fill(nt_true)
    sc = hlist['velo_true'].Fill(nvelo_true)
    sc = hlist['long_true'].Fill(nlong_true)
    # trigger studies
    sc = tlist['spd'].Fill(evt['Raw/Spd/Digits'].size())
    sc = tlist['hcal'].Fill(evt['Raw/Hcal/Digits'].size())

tc = TCanvas('tc', evt_type + ' ' + bfield + ' ' + velopos, 1024, 768)
tc.Divide(2, 4)
i = 1
hlist_sort = hlist.keys()
hlist_sort.sort()
for h in hlist_sort:
    tc.cd(i)
    hlist[h].Draw()
    i += 1
tc.cd(7)
tlist['spd'].Draw()
tc.cd(8)
tlist['hcal'].Draw()
