###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
########################################################################
from Gaudi.Configuration import *
########################################################################
#
# Standard configuration
#
from Configurables import DaVinci
DaVinci().DataType = "MC09"
DaVinci().Simulation = True
DaVinci().HistogramFile = "DVHistos_1.root"  # Histogram file
DaVinci().TupleFile = "DVNtuples.root"  # Ntuple
# DaVinci().MainOptions  = "" # None
########################################################################
# some input files
EventSelector().Input = [
    "DATAFILE='castor:/castor/cern.ch/grid/lhcb/MC/MC09/DST/00004879/0000/00004879_00000001_1.dst' TYP='POOL_ROOTTREE' OPT='READ'"
]

from GaudiPython import AppMgr
appMgr = AppMgr()
evt = appMgr.evtsvc()
#
appMgr.run(1)
evt.dump()
