###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from ROOT import TH1F, TBrowser, TCanvas
# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "2011"
lhcbApp.Simulation = False

appConf = ApplicationMgr(OutputLevel=INFO)

import GaudiPython

appMgr = GaudiPython.AppMgr()
sel = appMgr.evtsel()
sel.open(['$PANORAMIXDATA/Sel_Bsmumu_2highest.dst'])
evt = appMgr.evtsvc()

h = TH1F('h', ' nr. of tracks', 100, 0., 500.)
hp = TH1F('hp', ' momentum of tracks', 100, 0., 50000.)

for n in range(40):
    appMgr.run(1)
    if not evt['Rec/Track/Best']: break  # probably end of file
    tc = evt['Rec/Track/Best']
    rc = h.Fill(tc.size())
    for t in tc:
        rc = hp.Fill(t.p())

h.Draw()
hp.Draw()
