###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from ROOT import TH1F, TCanvas, TLine, TArc, gStyle
import math

# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "2008"
lhcbApp.DDDBtag = "head-20090330"
lhcbApp.CondDBtag = "head-20090401"
lhcbApp.Simulation = False

ApplicationMgr(OutputLevel=INFO, AppName='VeloDet')

import GaudiPython
gbl = GaudiPython.gbl
appMgr = GaudiPython.AppMgr()
appMgr.initialize()  # otherwise ROOT.Math does not exist, very strange !

TH1F.AddDirectory(
    False)  # disable the automatic association of  histograms to directories

XYZPoint = LHCbMath.XYZPoint
XYZVector = LHCbMath.XYZVector
Rotation3D = gbl.Math.Rotation3D
EulerAngles = gbl.Math.EulerAngles
AxisAngle = gbl.Math.AxisAngle

# lets define some useful modules


#/////////////////////////////////////////////////////////////////////////////
# Print info about DeVelo class
# in addition to dir(velo) gives also info about expect arguments
#/////////////////////////////////////////////////////////////////////////////
def DeVelo_print():
    print 'Methods of DeVelo:'
    c = gbl.DeVelo
    #  help(gbl.DeVelo)
    for n in dir(c):
        if n[0] != '_': print getattr(c, n).__doc__


def DeVelo_print2():
    GaudiPython.loaddict('ReflexRflx')
    root = GaudiPython.PyLCGDict.makeNamespace('ROOT')
    ty = root.Reflex.Type.ByName('DeVelo')
    print 'Methods of DeVelo:'
    for i in range(ty.FunctionMemberSize()):
        fm = ty.FunctionMemberAt(i)
        ft = fm.TypeOf()
        print fm.Name() + ' (',
        for j in range(fm.FunctionParameterSize()):
            if j:
                print ',',
                print ft.FunctionParameterAt(
                    j).Name() + ' ' + fm.FunctionParameterNameAt(j),
                default = fm.FunctionParameterDefaultAt(j)
                if default:
                    print ' = ', default
        print ')'


#/////////////////////////////////////////////////////////////////////////////
# Print info about sensors and fill some histograms
#/////////////////////////////////////////////////////////////////////////////
def Velo_Sensors():
    print 'R-sensors: ', len(vectR), 'Phi-sensors: ', len(
        vectPhi), 'PileUp-sensors: ', len(vectPU)

    print 'sensorNumber(), type(), name(), innerRadius(), outerRadius(), z(), siliconThickness(), isR(), isPhi(),isPileUp()'

    for sen in vectS:
        ii = sen.name().find('Velo')
        location = sen.name()[ii + 5:]
        print '%3d' % (sen.sensorNumber()), '%6s' % (sen.type()), '%45s' % (
            location), sen.innerRadius(), sen.outerRadius(), '%8.2f' % (sen.z(
            )), sen.siliconThickness(), sen.isR(), sen.isPhi(), sen.isPileUp()
        xSide = 10.
        if sen.isLeft(): xSide = -10.
        success = hVelo.Fill(sen.z(), xSide)

        pzero = XYZPoint(0., 0., 0.)
        glob = sen.localToGlobal(pzero)
        if sen.isRight():
            newnr = sen.sensorNumber() / 2. + 1
        else:
            newnr = -(sen.sensorNumber() + 1) / 2.
        success = hx_Velo.Fill(newnr, glob.x())
        success = hy_Velo.Fill(newnr, glob.y())
        success = hz_Velo.Fill(sen.sensorNumber(), sen.z())


def Plot_Sensors():
    #/////////////////////////////////////////////////////////////////////////////
    #  Plot R and Phi sensor strip geometry
    #/////////////////////////////////////////////////////////////////////////////
    p1 = XYZPoint(0., 0., 0.)
    p2 = XYZPoint(0., 0., 0.)
    offset = 100.
    hstrip.SetMinimum(-55. + offset)
    hstrip.SetMaximum(55. + offset)
    # gROOT.SetStyle("Plain")

    success = cr.cd(1)
    hstrip.DrawCopy()
    i = int()

    arc = TArc()
    arc.SetNoEdges()
    arc.SetFillStyle(0)
    arc.SetFillColor(0)
    arc.SetLineColor(2)

    for k in range(vectR[0].numberOfStrips()):
        i = k / 5
        if i * 5 == k:
            radius = vectR[0].rOfStrip(k)
            phimin = (vectR[0].phiMinStrip(k)) / math.pi * 180.
            phimax = (vectR[0].phiMaxStrip(k)) / math.pi * 180.
            if phimin > phimax:
                temp = phimin
                phimin = phimax
                phimax = temp
            arc.DrawArc(0, 0 + offset, radius, phimin, phimax, 'only')
            print radius, phimin, phimax

    l = TLine()
    l.SetLineColor(3)
    for k in range(vectPhi[0].numberOfStrips()):
        i = k / 5
        if i * 5 == k:
            lid = VeloChannelID(vectPhi[0].sensorNumber(), k,
                                VeloChannelID.PhiType)
            trajp = velo.trajectory(LHCbID(lid), 0.)
            traj = trajp.get()
            beg = traj.beginPoint()
            end = traj.endPoint()
            success = l.DrawLine(-beg.x() - 5., -beg.y() + offset,
                                 -end.x() - 5., -end.y() + offset)

    xcut = [
        -6692, -11273, -17210, -17522, -41895, -43157, -43916, -41110, -32026,
        -18037, -1285, 15662, 30212, 40134, 43987, 43388, 17237, 17032, 10790,
        6692
    ]
    ycut = [
        -2085, 1320, -92, -111, 2540, 2540, 1603, -15467, -30060, -40049,
        -43905, -41036, -31883, -17846, -735, 5, 2540, 2540, 961, -2085
    ]
    xcutx = [
        -6693, -11166, -44595, -45269, -41705, -32193, -17914, 0., 17914,
        32193, 41705, 45269, 44595, 11166, 6693
    ]
    ycutx = [
        -2083, 2000, 2000, 1167, -17009, -31499, -41324, -45244, -41324,
        -31499, -17009, 1167, 2000, 2000, -2083
    ]
    l.SetLineWidth(2)
    l.SetLineColor(4)
    arc.SetLineWidth(2)
    arc.SetLineColor(4)
    for k in range(19):
        success = l.DrawLine(ycut[k] / 1000. - 5., -xcut[k] / 1000. + offset,
                             ycut[k + 1] / 1000. - 5.,
                             -xcut[k + 1] / 1000. + offset)
    for k in range(14):
        success = l.DrawLine(-ycutx[k] / 1000., xcutx[k] / 1000. + offset,
                             -ycutx[k + 1] / 1000.,
                             xcutx[k + 1] / 1000. + offset)

    radius = 7010 / 1000.
    phimin = -73.
    phimax = 73.
    arc.DrawArc(0, 0 + offset, radius, phimin, phimax, 'only')
    arc.DrawArc(-5., 0 + offset, radius, phimin + 180., phimax + 180., 'only')


def Velo_Sensors_fillHistos():
    #/////////////////////////////////////////////////////////////////////////////
    #  fill histograms with geometry information
    #/////////////////////////////////////////////////////////////////////////////

    for k in range(vectR[0].numberOfStrips()):
        success = hradius_R.Fill(k, vectR[0].rOfStrip(k))
        success = hpitch_R.Fill(k, vectR[0].rPitch(k))

    for k in range(vectPhi[0].numberOfStrips()):
        zone = vectPhi[0].zoneOfStrip(k)
        testRadius = vectPhi[0].rMin(zone)
        success = hradius_Phi.Fill(k, testRadius)
        success = hpitch_Phi.Fill(k, vectPhi[0].phiPitch(testRadius))

    success = c1.cd(1)
    hradius_R.DrawCopy()
    success = c1.cd(2)
    hpitch_R.DrawCopy()
    success = c1.cd(3)
    hradius_Phi.DrawCopy()
    success = c1.cd(4)
    hpitch_Phi.DrawCopy()
    success = c1.cd(5)
    hz_Velo.DrawCopy()
    success = c1.cd(6)
    hVelo.DrawCopy()


def Velo_Alignment_info():

    local = XYZPoint()
    globl = XYZPoint()

    print 'alignment const.  phi,psi,theta, x,  y,  z'
    rot = Rotation3D()
    tra = XYZVector()
    for k in range(1, len(vectS)):
        vectS[k].geometry().toGlobalMatrix().GetDecomposition(rot, tra)
        e = EulerAngles() * rot
        phi = e.Phi() / math.pi * 180
        psi = e.Psi() / math.pi * 180
        theta = e.Theta() / math.pi * 180
        globl = vectS[k].geometry().toGlobal(local)

        print '%3d' % (vectS[k].sensorNumber()), phi, psi, theta, tra.x(
        ), tra.y(), tra.z(), globl.z()


def Velo_Channel_ID():
    #Velo channel ID:
    vc = VeloChannelID(0, 1)


#
#/////////////////////////////////////////////////////////////////////////////
#  Do the execution
#/////////////////////////////////////////////////////////////////////////////

# helpful if you want to reload without re-executing all this
if __name__ == '__main__':
    appMgr = GaudiPython.AppMgr()
    det = appMgr.detSvc()
    velo = det['/dd/Structure/LHCb/BeforeMagnetRegion/Velo']

    VeloChannelID = gbl.LHCb.VeloChannelID
    LHCbID = gbl.LHCb.LHCbID

    vectPhi = []
    vectR = []
    vectPU = []
    vectS = []

    for k in range(42):
        vectR.append(velo.rSensor(k))
        vectS.append(velo.sensor(k))
    for k in range(64, 106):
        vectPhi.append(velo.phiSensor(k))
        vectS.append(velo.sensor(k))
    for k in range(128, 132):
        vectPU.append(velo.sensor(k))
        vectS.append(velo.sensor(k))

    hradius_R = TH1F('hradius_R', 'radius', vectR[0].numberOfStrips(), 0.,
                     vectR[0].numberOfStrips())
    hpitch_R = TH1F('hpitch_R', 'pitch', vectR[0].numberOfStrips(), 0.,
                    vectR[0].numberOfStrips())
    hradius_Phi = TH1F('hradius_Phi', 'radius', vectPhi[0].numberOfStrips(),
                       0., vectPhi[0].numberOfStrips())
    hpitch_Phi = TH1F('hpitch_Phi', 'pitch', vectPhi[0].numberOfStrips(), 0.,
                      vectPhi[0].numberOfStrips())

    hVelo = TH1F('hVelo', 'z position of sensors', 1150, -350., 800.)
    hstrip = TH1F('hstrip', 'Velo Sensors Strip configuration', 10, -60., 60.)

    #
    hx_Velo = TH1F('hx_Velo', 'sensor nr vs. x position of sensors', 200,
                   -100., 100.)
    hy_Velo = TH1F('hy_Velo', 'sensor nr vs. y position of sensors', 200,
                   -100., 100.)
    hz_Velo = TH1F('hz_Velo', 'sensor nr vs. z position of sensors', 200,
                   -100., 100.)

    gStyle.SetOptStat(10000001)
    # gROOT.SetStyle("Plain")
    cr = TCanvas('cr', 'Velo Phi and R sensor', 1000, 1000)
    c1 = TCanvas('c1', 'Velo sensors', 1400, 1100)
    cr.Divide(1, 1)
    c1.Divide(3, 2)

    #DeVelo_print()
    #Velo_Sensors()
    #Velo_Sensors_fillHistos()
    #Velo_Alignment_info()
    Plot_Sensors()

    c1.Update()
    cr.Update()

    VeloChannelID = gbl.LHCb.VeloChannelID
    help(VeloChannelID)

# in case you want to change single procedure:
# import VeloDet
# reload(VeloDet)
