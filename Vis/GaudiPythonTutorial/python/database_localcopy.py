###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# requires: SetupProject LHCb
from PyCoolCopy import copy
from os import environ

#db_name = "LHCBCOND"
db_name = "DDDB"

source_file = environ["SQLITEDBPATH"] + "/" + db_name + ".db"
source = "sqlite_file:%s/%s" % (source_file, db_name)
dest_file = environ["HOME"] + "/my" + db_name + ".db"
dest = "sqlite_file:%s/%s" % (dest_file, db_name)
copy(source, dest, tags=['head-20080729', 'DC06'])
# convert to XML
# set tag = "head-20101003"
# dump_db_to_files.py -c sqlite_file:$SQLITEDBPATH/DDDB.db/DDDB -T $tag -t `date +"%s000000000"` -d /tmp/truf/
