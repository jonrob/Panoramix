###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import ROOT, sys
xx = ROOT.TH1F('xx', 'xx', 100, 1, 0)
xx.Draw()

MCdata = False
debug = False
L0_Accept = False
Elastic = False
No_Spill_over = False
Hlt = False
filter_collisions = False
filter_beamgas = False

if not len(sys.argv) > 1:
    print "need to know input file"
    1 / 0
fin = sys.argv[1]
if not fin.find('eos') < 0 and fin.find('eoslhcb') < 0:
    fin = "root://eoslhcb.cern.ch/" + fin

# filter out beam gas
# run 063567:
#bunchId= 1654.0   : 1661.0
#bunchId= 2545.0   : 4860.0
#bunchId= 2991.0   : 4790.0
#bunchId= 3436.0   : 1956.0

collisions = (2545, 2991)
beamgas = (1654, 3436)

# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "2015"
if MCdata: lhcbApp.Simulation = True

# example for adding different conditions
#from Configurables import ( CondDB, CondDBAccessSvc )
#otCalib = CondDBAccessSvc( 'OTCalib' )
#otCalib.ConnectionString = 'sqlite_file:/afs/cern.ch/user/w/wouter/public/AlignDB/OTCalibration2009.11.22.db/LHCBCOND'
#CondDB().addLayer( otCalib )

EventSelector().PrintFreq = 1000

appConf = ApplicationMgr(OutputLevel=INFO, AppName='RawBufferSizes')

import GaudiPython, sys, math, os

ROOT.TH1F.AddDirectory(
    False)  # disable the automatic association of  histograms to directories
from gaudigadgets import *

bb = getEnumNames('LHCb::RawBank')
BankType = bb['BankType']

VeloTell1 = []
for k in range(42):
    VeloTell1.append(k)
for k in range(64, 106):
    VeloTell1.append(k)
for k in range(128, 132):
    VeloTell1.append(k)

h = {}
stats = {}
stats['all'] = 0
h['all'] = ROOT.TH1F('h_all', 'raw eventsize', 5000, -0.5, 500000.)
lumi2 = False
lumi5 = False

appMgr = GaudiPython.AppMgr()
evt = appMgr.evtsvc()
sel = appMgr.evtsel()

sel.open(fin)

nevent = 25000
first_event = 0


def empty():
    flag = False
    if evt['Gen/Collisions'].size() == 1:
        col = evt['Gen/Collisions'][0]
        if col.processType() == 91: flag = True
    return flag


def nospillover():
    flag = False
    next = evt['Next/MC/Header']
    prev = evt['Prev/MC/Header']
    preprev = evt['PrevPrev/MC/Header']
    if not next and not prev and not preprev: flag = True
    return flag


while nevent > 0:
    appMgr.run(1)
    rb = evt['DAQ/RawEvent']
    if not rb:
        print 'end of input files reached:', first_event
        break

    nr = evt['DAQ/ODIN'].bunchId()
    if filter_collisions and nr not in collisions: continue
    if filter_beamgas and nr not in beamgas: continue
    # look for L0 decision:
    if L0_Accept and evt['Trig/L0/L0DUReport'].decision() == 0: continue
    if No_Spill_over and not nospillover(): continue
    if Elastic and not empty(): continue

    n_raw = 0
    nevent -= 1
    for k in BankType:
        nTell1 = rb.banks(k).size()
        name = BankType[k]
        if first_event == 0 and nTell1 > 0:
            print k, name, nTell1
        ntot = 0
        for t in range(nTell1):
            nword = rb.banks(k).at(t).totalSize()
            ntot += nword
            hname = name + '_T_' + str(t)
            if not h.has_key(hname):
                h[hname] = ROOT.TH1F(hname, hname, 200, -0.5, 1000.)
            result = h[hname].Fill(nword)
        if not h.has_key(name):
            h[name] = ROOT.TH1F(name, 'banktype ' + name, 5000, -0.5, 500000.)
            stats[name] = 0
        if nTell1 > 0:
            stats[name] += ntot
            result = h[name].Fill(ntot)
        n_raw += ntot

    h['all'].Fill(n_raw)
    stats['all'] += n_raw
    first_event += 1

nTell1s = {}
f = ROOT.TFile('rawbuffer.root', 'recreate')
for k in BankType:
    name = BankType[k]
    if h.has_key(name):
        if h[name].GetEntries() > 0:
            print '%20s' % (name), 'mean size = ', '%10.2f' % (
                h[name].GetMean())
        result = h[name].Write()
        for x in h:
            if x.find(name + '_T_') > -1:
                if nTell1s.has_key(name):
                    nTell1s[name] += 1
                else:
                    nTell1s[name] = 1
                print '%25s' % (x), '%10.2f' % (h[x].GetMean())

print "+++++++++++ compact summary +++++++++++++"
for k in BankType:
    name = BankType[k]
    if not nTell1s.has_key(name): nTell1s[name] = 0
    if h.has_key(name):
        if h[name].GetEntries() > 0:
            print '%20s %4i' % (name,
                                nTell1s[name]), 'mean size = ', '%10.2f' % (
                                    h[name].GetMean())
        result = h[name].Write()

result = h['all'].Write()
for x in h:
    h[x].Write()
f.Close()
print 'total event size in memory:', '%10.2f' % (h['all'].GetMean())
print 'number of events read', '%10.2f' % (first_event)
#
for b in stats:
    if stats[b] > 0: print b, stats[b] / float(first_event)
f = ROOT.TFile('rawbuffer.root')
