###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Print material properties for a given point in space
# somehow, does only work within Panoramix, to be understood

# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "2009"

appConf = ApplicationMgr(AppName='Ex4c', OutputLevel=INFO)

import GaudiPython

appMgr = GaudiPython.AppMgr()
det = appMgr.detSvc()
lhcb = det['/dd/Structure/LHCb']
# read one event to get right event time
sel = appMgr.evtsel()
sel.open(['$PANORAMIXDATA/Sel_Bsmumu_2highest.dst'])
evt = appMgr.evtsvc()
# read one event
appMgr.run(1)
# get std namespace
std = GaudiPython.gbl.std

PVPath = std.vector('const IPVolume*')
pvpath = PVPath()
iGeo = GaudiPython.Bindings.makeNullPointer('IGeometryInfo')

lhcb = det['/dd/Structure/LHCb']
XYZPoint = LHCbMath.XYZPoint
point = XYZPoint(0, 0, 0)

level = 10000

geom = lhcb.geometry()
geom.fullGeoInfoForPoint(point, level, iGeo, pvpath)

#get the last element in pvpath

pvlast = pvpath[pvpath.size() - 1]
lvlast = pvlast.lvolume()
matlast = lvlast.material()
print matlast
