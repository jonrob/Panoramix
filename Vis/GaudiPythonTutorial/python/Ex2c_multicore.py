###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import time, os
os.system(
    'setenv PYTHONPATH /afs/cern.ch/user/m/mato/public/processing-0.51/build/lib.linux-x86_64-2.5:${PYTHONPATH}'
)

start = time.time()
# get the basic configuration from here
from LHCbConfig import *
appConf = ApplicationMgr(OutputLevel=999, AppName='Ex2_multicore')
lhcbApp.DataType = "DC06"
AnalysisConf().DataType = lhcbApp.DataType
PhysConf().DataType = lhcbApp.DataType
appConf.TopAlg += [PhysConf().initSequence(), AnalysisConf().initSequence()]
from Configurables import CombineParticles, FilterDesktop
importOptions('$STRIPPINGSELECTIONSROOT/options/StrippingBs2JpsiPhi.py')
appConf.TopAlg += [
    FilterDesktop("StripStdUnbiasedPhi2KK"),
    CombineParticles("StripBs2JpsiPhi")
]

EventSelector().PrintFreq = 100

#--- Inport needed modules to be reused in all processes---------------
import GaudiPython
from pickleROOT import *
from processing import Pool
from ROOT import TH1F, TFile, gROOT


def processFile(file):
    h_bmass = TH1F('h_bmass', 'Mass of B candidate', 100, 5200., 5500.)
    appMgr = GaudiPython.AppMgr()
    sel = appMgr.evtsel()
    sel.open([file])
    evt = appMgr.evtsvc()
    while 0 < 1:
        appMgr.run(1)
        # check if there are still valid events
        if not evt['Rec/Header']: break
        cont = evt['Phys/StripBs2JpsiPhi/Particles']
        if cont:
            for b in cont:
                success = h_bmass.Fill(b.momentum().mass())
    print 'Finishing.... ', file, ' ', h_bmass.GetMean(), h_bmass.GetEntries()
    return h_bmass


#--- In the master process only....

if __name__ == '__main__':
    files = [
        'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000001_5.dst',
        'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000002_5.dst',
        'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000003_5.dst',
        'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000004_5.dst',
        'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000005_5.dst',
        'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000007_5.dst',
        'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000008_5.dst',
        'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000009_5.dst',
        'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000010_5.dst',
        'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000012_5.dst',
        'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000013_5.dst'
    ]

    #------Use up to 8 processes
    n = 4
    pool = Pool(n)

    result = pool.map_async(processFile, files[:n])

    h_bmass = None
    for h in result.get(timeout=10000):
        if h_bmass: h_bmass.Add(h)
        else: h_bmass = h

    h_bmass.Draw()
    end = time.time()
    print 'total time', end - start
    print h_bmass.GetEntries()
