###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from ROOT import TH1F, TFile, gROOT
from Gaudi.Configuration import *
from GaudiPython.Parallel import Task, WorkManager
from GaudiPython import AppMgr


class MyTask(Task):
    def initializeLocal(self):
        self.output = {
            'h_bmass': TH1F('h_bmass', 'Mass of B candidate', 100, 5200.,
                            5500.)
        }

    def initializeRemote(self):
        #--- Get the basic configuration for each remote worker
        from LHCbConfig import lhcbApp
        lhcbApp.DataType = "DC06"
        analysis = AnalysisConf()
        analysis.DataType = "DC06"
        from StrippingSelections.StrippingBs2JpsiPhi import sequence as seqBs2JpsiPhi
        appConf.TopAlg += [seqBs2JpsiPhi.sequence()]

        appConf = ApplicationMgr(OutputLevel=ERROR, AppName='Ex2_parallel')
        EventSelector().PrintFreq = 100

    def process(self, file):
        #--- Process a single file in each worker node
        appMgr = AppMgr()
        sel = appMgr.evtsel()
        sel.open([file])
        evt = appMgr.evtsvc()
        while 0 < 1:
            appMgr.run(1)
            # check if there are still valid events
            if not evt['Rec/Header']: break
            cont = evt['Phys/Bs2JpsiPhi/Particles']
            if cont:
                for b in cont:
                    success = self.output['h_bmass'].Fill(b.momentum().mass())
        appMgr.stop()
        print 'Finishing.... ', file, ' ', self.output['h_bmass'].GetMean(
        ), self.output['h_bmass'].GetEntries()

    def finalize(self):
        self.output['h_bmass'].Draw()
        print 'Total number is: ', self.output['h_bmass'].GetEntries()


#--- In the master process only....
if __name__ == '__main__':
    #--- build the list of files. Perhaps is better to import a module with the files
    #    for example: 'from datasets import Bs2JpsiPhi_files as files'
    files = []
    file = 'PFN:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000XXX_5.dst'
    for n in range(1, 50):
        ff = file.replace('XXX', '%(X)03d' % {'X': n})
        x = os.system('nsls ' + ff.replace('PFN:', ''))
        if x == 0: files.append(ff)

    task = MyTask()
    wmgr = WorkManager(ncpus=4)
    wmgr.process(task, files[:32])
