###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from ROOT import TH1F, TBrowser, TCanvas
# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "2010"
lhcbApp.Simulation = True
lhcbApp.DDDBtag = "head-20101206"
lhcbApp.CondDBtag = "sim-20101210-vc-md100"

appConf = ApplicationMgr(OutputLevel=INFO, AppName='Ex9')

import GaudiPython
from gaudigadgets import getEnumNames
import webbrowser, os


def tobin(x, count=32):
    """
         Integer to binary
         Count is number of bits
         """
    return "".join(map(lambda y: str((x >> y) & 1), range(count - 1, -1, -1)))


appMgr = GaudiPython.AppMgr()
sel = appMgr.evtsel()
lhcbApp.CondDBtag = "sim-20101210-vc-md100"
sel.open(['$PANORAMIXDATA/MC2010_BsJpsiPhi__00008919_00000068_1.dst'])
evt = appMgr.evtsvc()

f = open('raw_buffer.txt', 'w')
for n in range(20):
    appMgr.run(1)
    if evt['Hlt/HltDecReports']:
        f.write('event: ' + str(evt['Rec/Header'].evtNumber()) +
                'Hlt decision: ' + str(evt['Hlt/HltDecReports'].decision()) +
                ' \n')
    else:
        f.write('event: ' + str(evt['Rec/Header'].evtNumber()) + ' \n')
    Enums = getEnumNames('LHCb::RawBank')
    BankType = Enums['BankType']
    rb = evt['DAQ/RawEvent']
    f.write('  banktype sourceID word     hex    binary   \n')
    i = 0
    for k in BankType.keys():
        b = BankType[k]
        nTell1 = rb.banks(k).size()
        for m in range(nTell1):
            size = int((rb.banks(k)[m].size()) / 4 + 0.5)
            for l in range(size):
                word = rb.banks(k)[m].data()[l]
                t2 = (word >> 16) & 0x0000ffff
                t1 = word & 0x0000ffff
                if word < 0:
                    word = t2 * 2**16 + t1
                f.write('%15s %6d  %.8x %32s ' % (b + ' ' + str(
                    rb.banks(k)[m].sourceID()), i, word, tobin(word)))
                f.write('\n')
                i += 1

f.close()

webbrowser.open(os.getcwd() + '/raw_buffer.txt')
