###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from ROOT import TFile, TCanvas, TH1F, TH2F, TH3F
debug = False

from Gaudi.Configuration import *
from GaudiPython.Parallel import Task, WorkManager
from GaudiPython import AppMgr
from LHCbConfig import *


class MyTask(Task):
    def initializeLocal(self):
        self.output = {
            'h_beam':
            TH1F('h_beam', 'beam energie - 5TeV ', 100, -1., 1.),
            'h_decay':
            TH1F('h_decay', 'decay z of proton from elastic collissions ',
                 1000, -5000., 15000.),
            'h_energy':
            TH2F('h_energy',
                 'sum of energies of decay products - 7TeV vs baryon nr ', 100,
                 -10000., 20000., 10, -0.5, 9.5),
            'h_xy':
            TH2F('h_xy', 'x/y ', 100, -1., 1., 100, -1., 1.),
            'h_zxy':
            TH3F('h_zxy', 'z/x/y ', 1000, -3000., 3000., 100, -1., 1., 100,
                 -1., 1.)
        }

    def initializeRemote(self):
        #--- Get the basic configuration for each remote worker
        lhcbApp.DataType = "2009"
        InputCopyStream(
        ).Output = "DATAFILE='PFN:beamgas.dst' TYP='POOL_ROOTTREE' OPT='REC' "
        appConf.OutputLevel = INFO
        appConf.AppName = 'protonDecay'
        appConf.OutStream = [InputCopyStream()]
        EventSelector().PrintFreq = 1000

    def process(self, file):
        appMgr = AppMgr()
        appMgr.algorithm('InputCopyStream').Enable = False
        sel = appMgr.evtsel()
        print 'Open .... ', file
        sel.open([file])
        evt = appMgr.evtsvc()
        while 0 < 1:
            appMgr.run(1)
            # check if there are still valid events
            if not evt['MC/Particles']: break
            if evt['Gen/Collisions'].size() != 1: continue
            col = evt['Gen/Collisions'][0]
            proc = col.processType()
            if proc == 91:
                for p in evt['MC/Particles']:
                    if p.particleID().pid() != 2212: continue
                    vx = p.originVertex()
                    if vx.type() != vx.ppCollision: continue
                    self.output['h_beam'].Fill(p.momentum().E() - 5000000.)
                    for e in p.endVertices():
                        pos = e.position()
                        z = pos.z()
                        sc = self.output['h_decay'].Fill(z)
                        if z > -4000. and z < 10000.:
                            xx = pos.x()
                            yy = pos.y()
                            self.output['h_xy'].Fill(xx, yy)
                            self.output['h_zxy'].Fill(z, xx, yy)
                            if z > 1200. and z < 10000.:
                                print 'proton decay ?', evt[
                                    'DAQ/ODIN'].runNumber(
                                    ), evt['DAQ/ODIN'].eventNumber(
                                    ), xx, yy, z, p.momentum().E()
                            # appMgr.algorithm('InputCopyStream').execute()
                            dlist = []
                            for x in evt['MC/Particles']:
                                if x.mother():
                                    if x.mother().key() == p.key():
                                        dlist.append(x)
                            totE = 0
                            b = 0
                            for d in dlist:
                                totE += d.momentum().E()
                                if d.particleID().isBaryon():
                                    b += float(
                                        d.particleID().threeCharge()) / 3.
                            if z > 0.:
                                sc = self.output['h_energy'].Fill(
                                    totE - 5000000., b)

        appMgr.stop()
        print 'Finishing.... ', file

    def finalize(self):
        f = TFile('DarkMatter.root', 'recreate')
        for h in self.output.keys():
            histo = self.output[h]
            histo.Write()
            print histo.GetTitle(), ':', histo.GetEntries()
        f.Close()


if __name__ == '__main__':
    files = []
    file = 'PFN:castor:/castor/cern.ch/grid/lhcb/MC/MC09/DST/00004827/0000/00004827_00000XXX_1.dst'
    for n in range(1, 200):
        ff = file.replace('XXX', '%(X)03d' % {'X': n})
        x = os.system('nsls ' + ff.replace('PFN:castor:', ''))
        if x == 0: files.append(ff)

    task = MyTask()
    if not debug:
        wmgr = WorkManager(ncpus=8)
        wmgr.process(task, files)
    else:
        task.initializeLocal()
        task.initializeRemote()
        task.process(files[0])
        task.finalize()
    test = task.output
