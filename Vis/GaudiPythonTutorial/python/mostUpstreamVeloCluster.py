###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
velo = det['/dd/Structure/LHCb/BeforeMagnetRegion/Velo']

velorclusters = {}
velophiclusters = {}
vc = evt['Raw/Velo/Clusters']

# t is your long track
for l in t.lhcbIDs():
    if not l.isVelo(): continue
    lv = l.veloID()
    cl = vc.containedObject(lv.channelID())
    if cl.isRType(): velorclusters[velo.sensor(cl.channelID()).z()] = cl
    else: velophiclusters[velo.sensor(cl.channelID()).z()] = cl
sortedr = velorclusters.keys()
sortedr.reverse()
sortedphi = velophiclusters.keys()
sortedphi.reverse()

mostupstreamr = velorclusters[sortedr[0]]
mostupstreamphi = velophiclusters[sortedphi[0]]
print 'cluster size', mostupstreamr.size(
), 'total charge', mostupstreamr.totalCharge()
for strip in mostupstreamr.stripValues():
    print 'strip number', strip[0], 'ADC count', strip[1]
