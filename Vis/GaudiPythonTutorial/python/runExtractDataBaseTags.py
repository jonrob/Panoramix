###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import subprocess, os, sys
if len(sys.argv) > 1:
    fn = sys.argv[1]
else:
    print 'missing file name, stop'
    sys.exit()

f = open('test.log', 'w')
p = subprocess.Popen(
    ['ex', os.environ['GAUDIPYTHONTUTORIAL'] + '/ExtractDataBaseTags.py', fn],
    executable='python',
    env=os.environ,
    stdout=f,
    stderr=f)
p.communicate()
f.close()
keys = {
    'first': '_Event_Rec_Header.LHCb::ProcessHeader.m_condDBTags.first',
    'second': '_Event_Rec_Header.LHCb::ProcessHeader.m_condDBTags.second'
}
ts = {}
f = open('test.log')
for l in f.readlines():
    for k in keys:
        if not l.find(keys[k]) < 0: ts[k] = l.split('=')[1].split(',')
os.system('rm test.log')
tags = {}
n = 0
for t in ts['first']:
    x = ts['second'][n]
    y = t.replace(' ', '').replace('\n', '')
    v = x.replace(' ', '').replace('\n', '')
    print y, v
    tags[y] = v
    n += 1
print tags
