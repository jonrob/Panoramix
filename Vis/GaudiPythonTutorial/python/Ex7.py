###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from ROOT import TH1F, TBrowser, TCanvas
# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "2011"

appConf = ApplicationMgr(AppName='Ex7', OutputLevel=INFO)
appConf.ExtSvc += ['TransportSvc']

import GaudiPython

appMgr = GaudiPython.AppMgr()
sel = appMgr.evtsel()
det = appMgr.detSvc()
lhcb = det['/dd/Structure/LHCb']
sel.open(['$PANORAMIXDATA/Sel_Bsmumu_2highest.dst'])
appMgr.run(1)

tranSvc = appMgr.service('TransportSvc', 'ITransportSvc')
gbl = GaudiPython.gbl
XYZPoint = LHCbMath.XYZPoint
XYZVector = LHCbMath.XYZVector

a = XYZPoint(0, 0, 0)
b = XYZPoint(500., 350., 2000.)
radlength = tranSvc.distanceInRadUnits(a, b)
print 'radiation length between point a and b = %3.4F' % (radlength)
