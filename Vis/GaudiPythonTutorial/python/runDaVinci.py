###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
########################################################################
from Gaudi.Configuration import *
from Configurables import GaudiSequencer
########################################################################
importOptions("$STDOPTS/PreloadUnits.opts")
importOptions("$DAVINCISYSROOT/tests/options/Do09selBu2LLK.py")
preselSeq = GaudiSequencer("Sel09Bu2LLKFilter")
########################################################################
#
# Some Monitoring stuff
#
from Configurables import GaudiSequencer, PrintDecayTree
exampleSeq = GaudiSequencer("ExampleSeq")
tree = PrintDecayTree("PrintBu2LLK")
exampleSeq.Members += [tree]
tree.InputLocations = ["Presel09Bu2LLK"]
#
# Flavour tagging. Don't ask why you'd be tagging a B+...
#
from Configurables import BTagging
tag = BTagging("BTagging")
tag.InputLocations = ["Presel09Bu2LLK"]

########################################################################
#
# Standard configuration
#
from Configurables import DaVinci
DaVinci().EvtMax = 100  # Number of events
DaVinci().SkipEvents = 0  # Events to skip
DaVinci().DataType = "MC09"  # Default is "DC06"
DaVinci().Simulation = True
DaVinci().HistogramFile = "DVHistos_1.root"  # Histogram file
DaVinci().TupleFile = "DVNtuples.root"  # Ntuple
DaVinci().UserAlgorithms = [preselSeq, tag]  # The algorithms
DaVinci().MoniSequence = [exampleSeq]  # Monitoring
# DaVinci().MainOptions  = "" # None
########################################################################
# HLT
# DaVinci().ReplaceL0BanksWithEmulated = True ## enable if you want to rerun L0
DaVinci(
).Hlt2Requires = 'L0+Hlt1'  ## change if you want Hlt2 irrespective of Hlt1
