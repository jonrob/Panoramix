###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os, sys
# requires SetupProject Dirac

if len(sys.argv) < 1:
    print 'enter prodid, try again '
    exit

if not os.getenv('DIRACROOT'):
    print 'Dirac environment required. Use SetupProject Dirac'
    exit

# check for proxy
rc = os.system('lhcb-proxy-info > /dev/null')
if rc != 0: os.system('lhcb-proxy-init')

prodid = str(sys.argv[1])
os.system('dirac-bookkeeping-production-informations ' + prodid)
