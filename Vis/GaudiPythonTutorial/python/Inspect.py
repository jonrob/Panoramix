###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# count number of events on tape
import ROOT, os, sys, subprocess
if len(sys.argv) == 1:
    print 'give input file(s)'
    sys.exit()
fn = []
for t in sys.argv[1].split(','):
    if not t.find('eoslhcb') < 0 and t.find('root') < 0:
        fn.append('root:' + t)
    else:
        fn.append(t)
from LHCbConfig import *
# configure Gaudi with database tags from first event under Rec/Header
addDBTags(fn[0])

#from Configurables import CondDB
#CondDB().LatestGlobalTagByDataType = "2012"

appConf = ApplicationMgr(OutputLevel=INFO)
EventSelector().PrintFreq = 10000

import GaudiPython
# load some additional gadgets
import gaudigadgets

appMgr = GaudiPython.AppMgr()
det = appMgr.detsvc()
stupid = det['/dd/Structure/LHCb']
sel = appMgr.evtsel()
evt = appMgr.evtsvc()

sel.open(fn)

appMgr.run(1)
print evt['Rec/Header']
