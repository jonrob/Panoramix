###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from ROOT import TH1F, TBrowser, TCanvas, MakeNullPointer
import math

#dataset = 'K*mumu'
#dataset = 'pi+pi-'
#dataset  = 'bincl'
dataset = 'BsJpsiPhi'

newDST = True
f_2008 = False
veloFIX = False

# get the basic configuration from here
from LHCbConfig import *
if not f_2008:
    lhcbApp.DataType = "DC06"
    analysis.DataType = "DC06"
else:
    lhcbApp.DataType = "2008"
    analysis.DataType = "2008"
    lhcbApp.Simulation = True
    analysis.Simulation = True

if not f_2008:
    # fix wrong linker tables
    from Configurables import GaudiSequencer, TESCheck, EventNodeKiller, TrackAssociator
    daVinciFix = GaudiSequencer("DaVinciFix")
    evtCheck = TESCheck("EvtCheck")
    eventNodeKiller = EventNodeKiller("DaVinciEvtNodeKiller")
    daVinciFix.Members += [evtCheck, eventNodeKiller, TrackAssociator()]
    evtCheck.Inputs = ["Link/Rec/Track/Best"]
    evtCheck.Stop = False
    eventNodeKiller.Nodes = ["Link/Rec"]

if veloFIX:
    #
    # Trial to fix first MC 2008 production
    #
    from Configurables import UpdateManagerSvc
    txt = "Conditions/Alignment/Velo/ModuleXX := double_v dPosXYZ = 0. 0. 0. ; double_v dRotXYZ = 0. 0. 0. ;"

    for i in range(42):
        nr = str(i)
        if len(nr) == 1: nr = '0' + str(i)
        UpdateManagerSvc().ConditionsOverride.append(txt.replace('XX', nr))

appConf = ApplicationMgr(OutputLevel=INFO, AppName='fakeBreco')

if not newDST and not f_2008: appConf.TopAlg += [daVinciFix]

EventSelector().PrintFreq = 100
FileCatalog().Catalogs = ["xmlcatalog_file:dst_v31.xml"]

import GaudiPython
import gaudigadgets
import GaudiKernel.SystemOfUnits as units

from LinkerInstances.eventassoc import *
gbl = GaudiPython.gbl
ParticleID = gbl.LHCb.ParticleID
mysqrt = gbl.Math.sqrt

appMgr = GaudiPython.AppMgr()

sel = appMgr.evtsel()
# this file gives strange MC associations, K*mumu
if dataset == 'K*mumu':
    sel.open([
        'PFN:/castor/cern.ch/grid/lhcb/production/DC06/v1r0/00002051/DST/0000/00002051_00000005_2.dst'
    ])
if dataset == 'pi+pi-':
    sel.open([
        'PFN:/castor/cern.ch/grid/lhcb/production/DC06/v1r0/00002037/DST/0000/00002037_00000001_2.dst'
    ])
# stripped bbar inclusice background events
if dataset == 'bincl':
    badfiles = [3, 7]
    list = []
    for n in range(7, 100):
        bad = False
        for k in badfiles:
            if k == n: bad = True
        if not bad:
            list.append(
                'PFN:/castor/cern.ch/user/l/lshchuts/stripping/newDST_v31/' +
                str(n) + '.dst')
    sel.open(list)
if dataset == 'BsJpsiPhi':
    if f_2008:
        files = []
        file = '/castor/cern.ch/grid/lhcb/MC/2008/DST/00003783/0000/00003783_00000XXX_5.dst'
        for n in range(1, 10):
            ff = file.replace('XXX', '%(X)03d' % {'X': n})
            x = os.system('nsls ' + ff)
            if x == 0: files.append(ff)
        sel.open(files)
    else:
        if not newDST:
            sel.open([
                'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000001_5.dst',
                'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000002_5.dst',
                'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000003_5.dst'
            ])
        else:
            fn = 'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v4-lumi2/00002146/DST/0000/00002146_00000XXX_5.dst'
            flist = []
            for n in range(200):
                strn = str(n)
                if len(strn) == 1: strn = '00' + str(n)
                if len(strn) == 2: strn = '0' + str(n)
                flist.append(fn.replace('XXX', strn))
            sel.open(flist)

evt = appMgr.evtsvc()

part = MakeNullPointer(gbl.LHCb.MCParticle)
MCDecayFinder = appMgr.toolsvc().create(
    'MCDecayFinder', interface='IMCDecayFinder')
MCDecayFinder.setDecay('[B0,B+,B_s0,B_c+,Lambda_b0]cc')
MCDebugTool = appMgr.toolsvc().create(
    'PrintMCDecayTreeTool', interface='IPrintMCDecayTreeTool')
PrintDecayTreeTool = appMgr.toolsvc().create(
    'PrintDecayTreeTool', interface='IPrintDecayTreeTool')
MCTrackInfo = gbl.MCTrackInfo
MCParticle = gbl.LHCb.MCParticle
Track = gbl.LHCb.Track
LorentzVector = gbl.Math.LorentzVector('ROOT::Math::PxPyPzE4D<double>')
ContainedObject = GaudiPython.gbl.ContainedObject


def investigateClusters2MCParticles(tr):
    VeloCluster = gbl.LHCb.VeloCluster
    STCluster = gbl.LHCb.STCluster
    OTTime = gbl.LHCb.OTTime
    tdet = {
        'velo': 'Raw/Velo/Clusters',
        'tt': 'Raw/TT/Clusters',
        'it': 'Raw/IT/Clusters',
        'ot': 'Raw/OT/Times'
    }
    velotomc = linkedTo(MCParticle, VeloCluster, tdet['velo'])
    tttomc = linkedTo(MCParticle, STCluster, tdet['tt'])
    ittomc = linkedTo(MCParticle, STCluster, tdet['tt'])
    ottomc = linkedTo(MCParticle, ContainedObject, tdet['ot'])
    for l in tr.lhcbIDs():
        key = l.channelID()
        if l.isVelo():
            MCPvec = velotomc.range(evt[tdet['velo']].containedObject(key))
        if l.isTT():
            MCPvec = tttomc.range(evt[tdet['tt']].containedObject(key))
        if l.isIT():
            MCPvec = ittomc.range(evt[tdet['it']].containedObject(key))
        if l.isOT():
            MCPvec = ottomc.range(evt[tdet['ot']].containedObject(key))
        print '----', l.detectorType()
        for m in MCPvec:
            print m.key()


partsvc = appMgr.ppSvc()


def printTable():
    for i in range(9999):
        if not partsvc.find(ParticleID(i)): continue
        print i, partsvc.find(ParticleID(i)).name()


stableParticles = [11, 13, 211, 321, 2212]
beeParticles = {
    -511: -1,
    -521: -2,
    -531: -3,
    -541: -4,
    -5122: -5,
    511: 1,
    521: 2,
    531: 3,
    541: 4,
    5122: 5
}

name2pid = {}
for pid in beeParticles:
    name2pid['mass_' + partsvc.find(ParticleID(pid)).name()] = pid
#evtlist = [324586, 362187, 371479]
evtlist = []


def checkEvtList(enr):
    found = False
    for n in evtlist:
        if n == enr:
            found = True
            break
    return found


histos = {}
for b in beeParticles:
    name = 'mass_' + partsvc.find(ParticleID(b)).name()
    title = name.replace('mass_', 'mass of ')
    histos[name] = TH1F(name, title, 400, 4.5, 6.5)
    name = name + '_eff'
    histos[name] = TH1F(name, title, 400, 4.5, 6.5)

histos['h_gen'] = TH1F('h_gen', 'pid of generated bees', 20, -10., 10.)

nevents = 0
debug = False
if len(evtlist) > 0: appMgr.algorithm('DaVinciFix').Enable = False

while 1 > 0:
    appMgr.run(1)
    if not evt['Rec/Header']: break
    if len(evtlist) > 0: debug = checkEvtList(evt['Rec/Header'].evtNumber())
    if not debug and len(evtlist) > 0: continue
    if len(evtlist) > 0: appMgr.algorithm('DaVinciFix').execute()
    nevents += 1
    if debug and nevents > 10: break
    # look at MC truth
    mc = evt['MC/Particles']
    if not MCDecayFinder.hasDecay(mc): continue
    lfrommc = linkedFrom(Track, MCParticle, 'Rec/Track/Best')
    decaylist = []
    while MCDecayFinder.findDecay(mc, part) > 0:
        pclone = part.clone()
        GaudiPython.setOwnership(pclone, True)
        decaylist.append(pclone)
    for decay in decaylist:
        pid = decay.particleID().pid()
        histos['h_gen'].Fill(beeParticles[pid])
        name = 'mass_' + partsvc.find(ParticleID(pid)).name()
        daughters = GaudiPython.gbl.std.vector('const LHCb::MCParticle*')()
        MCDecayFinder.descendants(decay, daughters)
        recoparticle = []
        if debug: MCDebugTool.printTree(decay, 5)
        for dpart in daughters:
            # check if e, muon, pion, kaon, proton
            stable = False
            for s in stableParticles:
                if dpart.particleID().abspid() == s: stable = True
            if not stable: continue
            # check if origin is close to beam line and inside Velo
            oVx = dpart.originVertex().position()
            if oVx.rho() > 10.: continue
            if abs(oVx.z()) > 40.: continue
            # another check don't accept MCParticles with endVertices before T
            if dpart.endVertices().size() > 0:
                eVx = dpart.endVertices()[0].position()
                if eVx.z() < 7000.: continue
# check if reconstructed
            tcand = []
            for tr in lfrommc.range(dpart):
                # check if long track
                if tr.type() == tr.Long:
                    # additional problem, decays in flight: also MC decay products are matched with reco track
                    # avoid adding twice the same track
                    exist = False
                    trkey = tr.key()
                    for t in tcand:
                        if t.key() == trkey:
                            exist = True
                            break
                    if not exist:
                        tcand.append(tr)
            if len(tcand) == 0: continue
            # if more than one candidate, choose closest to MC truth
            if len(tcand) > 1:
                pmom = dpart.momentum().r()
                diff = [999999., -1]
                for tr in tcand:
                    delta = abs(pmom - tr.p())
                    if delta < diff[0]:
                        diff[0] = delta
                        diff[1] = tr
                if diff[1] == -1: print 'this is impossible, BIG ERROR!'
                tcand[0] = diff[1]
# fake PID:
            pid = dpart.particleID().abspid()
            m = partsvc.find(ParticleID(pid)).mass()
            p = tcand[0].momentum()
            rsq = m * m + p.Mag2()
            E = mysqrt(rsq)
            if debug:
                print 'track added', dpart.key(), dpart.particleID().pid(
                ), p.r(), p.x(), p.y(), p.z(), E, len(tcand)
                dm = dpart.momentum()
                print ' MC truth  ', dpart.key(), dpart.particleID().pid(
                ), dm.r(), dm.x(), dm.y(), dm.z(), dm.E()
            lv = LorentzVector(p.x(), p.y(), p.z(), E)
            fourmomenta = [lv, dpart.momentum()]
            recoparticle.append(fourmomenta)
        if len(recoparticle) < 2: continue
        # ignore B field in Velo close to IP, add up 4 momenta
        bmom = recoparticle[0][0]
        bmommc = recoparticle[0][1]
        if debug:
            print 'reco        ', bmom.x(), bmom.y(), bmom.z(), bmom.E(
            ), bmom.mass(), len(recoparticle)
        for t in range(1, len(recoparticle)):
            bmom += recoparticle[t][0]
            bmommc += recoparticle[t][1]
            if debug:
                print 'sum        ', bmom.x(), bmom.y(), bmom.z(), bmom.E(
                ), bmom.mass()
            if debug:
                print 'sum  MC    ', bmommc.x(), bmommc.y(), bmommc.z(
                ), bmommc.E(), bmommc.mass()
# fill histogram with B mass
        histos[name].Fill(bmom.mass() / units.GeV)
        if debug: print 'reconstructed mass', bmom.mass() / units.GeV
        if bmom.mass() / units.GeV > 1.1 * decay.momentum().mass() / units.GeV:
            print name, bmom.mass() / units.GeV, evt['Rec/Header']
        delmass = abs(bmommc.mass() - decay.momentum().mass())
        if delmass < 50. * units.MeV:
            histos[name + '_eff'].Fill(bmom.mass() / units.GeV)

from ROOT import TText
tc = TCanvas('mass', 'masses', 1600, 1200)
tc.Divide(4, 3)
itc = 1
for h in histos:
    if h.find('_eff') == -1:
        tc.cd(itc)
        itc += 1
        histos[h].Draw()

tceff = TCanvas('eff', 'masses with missing m < 50 MeV', 1600, 1200)
tceff.Divide(4, 3)
itc = 1
for h in histos:
    if h.find('_eff') > -1:
        tceff.cd(itc)
        itc += 1
        histos[h].Draw()
        nentries = histos[h].GetEntries()
        if nentries > 25: histos[h].Fit('gaus')
        # generated bees
        bin = beeParticles[name2pid[h.replace('_eff', '')]]
        genbees = histos['h_gen'].GetBinContent(histos['h_gen'].FindBin(bin))
        eff = 100. * float(nentries) / float(genbees)
        txt = 'Reconstructed bees = %3.2E %s' % (eff, '%')
        y = 0.5 * histos[h].GetMaximum()
        x = 4.7
        tx = TText(x, y, txt)
        tx.DrawText(x, y, txt)
