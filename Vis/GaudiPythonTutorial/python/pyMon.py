###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import GaudiPython
print 'import pyMon'

appMgr = GaudiPython.AppMgr()


class MyAlg(GaudiPython.PyAlgorithm):
    def initialize(self):
        ## book AIDA histogram
        print 'execute initialize'
        return True

    def execute(self):
        print 'execute event loop'
        print GaudiPython.AppMgr().evtsvc()['Rec/Header']
        return True

    def finalize(self):
        print 'execute finalize'
        return True
