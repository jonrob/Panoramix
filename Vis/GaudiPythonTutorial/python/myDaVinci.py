###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import ROOT
from ROOT import TH1F

from runDaVinci import *
importOptions("$DAVINCIROOT/options/MC09-Bs2JpsiPhiDst.py")

from GaudiPython import AppMgr
appMgr = AppMgr()
maxEvents = 10
for n in range(maxEvents):
    appMgr.run(1)

appMgr.algorithms()
muidalg = appMgr.algorithm('MuonIDAlg')
prop = muidalg.properties()
for p in prop:
    print p, ' : ', prop[p].value()

evt = appMgr.evtSvc()
evt.dump()
evt['Rec/Track/Best'].size()
atrack = evt['Rec/Track/Best'][0]
dir(atrack)

hist = appMgr.histSvc()
hist.dump()
hist['DaVinciInit.DaVinciMemory/Virtual mem, all entries'].Draw()

det = appMgr.detsvc()
velo = det['/dd/Structure/LHCb/BeforeMagnetRegion/Velo']
rSensor_10 = velo.rSensor(10)
print rSensor_10.rMax(0)
print rSensor_10.rMin(0)
print rSensor_10.rOfStrip(145)
#
ac = velo.geometry().alignmentCondition()
print ac.printParams()
