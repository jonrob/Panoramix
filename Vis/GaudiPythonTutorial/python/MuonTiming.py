###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from ROOT import TH2F, TCanvas
from GaudiPython import AppMgr
appMgr = AppMgr()
evt = appMgr.evtsvc()

myHisto = {}
tae = [
    "/Event/Prev2/", "/Event/Prev1/", "/Event/", "/Event/Next1/",
    "/Event/Next2/"
]

calo = ['Spd', 'Prs', 'Ecal', 'Hcal']

myHisto['ot'] = TH2F('ot', 'Outer Tracker', 5, -0.5, 4.5, 100, 0., 3000.)
myHisto['tr'] = TH2F('tr', 'Tracks', 5, -0.5, 4.5, 100, 0., 100.)

for c in calo:
    myHisto[c] = TH2F(c, c.upper(), 5, -0.5, 4.5, 100, 0., 1000.)

for n in range(5):
    myHisto[n] = TH2F(
        str(n), 'station ' + str(n + 1), 5, -0.5, 4.5, 100, 0., 100.)

for h in myHisto:
    xax = myHisto[h].GetXaxis()
    for b in tae:
        i = tae.index(b)
        txt = b.replace('/Event/', '')
        xax.SetBinLabel(i + 1, txt.replace('/', ''))

tc = TCanvas('tc', 'timing', 1600, 1200)
tc.Divide(6, 2)


def run():
    count = [0, 0, 0, 0, 0]
    for n in range(1000):
        appMgr.run(1)
        if not evt['DAQ/ODIN']: break
        for b in tae:
            for n in range(5):
                count[n] = 0
            cont = evt[b + 'Raw/Muon/Coords']
            for c in cont:
                tile = c.key()
                station = tile.station()
                count[station] += 1
            for n in range(5):
                rc = myHisto[n].Fill(tae.index(b), count[n])
#
            for c in calo:
                cont = evt[b + 'Raw/' + c + '/Digits']
                rc = myHisto[c].Fill(tae.index(b), cont.size())


#
            cont = evt[b + 'Raw/OT/Times']
            rc = myHisto['ot'].Fill(tae.index(b), cont.size())
            cont = evt[b + 'Rec/Track/Best']
            if cont:
                rc = myHisto['tr'].Fill(tae.index(b), cont.size())

    for n in range(5):
        tc.cd(n + 1)
        myHisto[n].Draw('box')

    n = 6
    tc.cd(n)
    myHisto['tr'].Draw('box')

    for x in myHisto:
        if type(x) != type(1) and x != 'tr':
            n += 1
            tc.cd(n)
            myHisto[x].Draw('box')
    return
