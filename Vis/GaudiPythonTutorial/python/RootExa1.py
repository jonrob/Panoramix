###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from ROOT import *

h = TH1F('h', ' nr. of tracks', 100, 0., 500.)
print 'Help on histogram: dir(h)'
print dir(h)
_ = raw_input('press enter to continue...')

h.Fill(10)
h.Draw()
b = TBrowser()

_ = raw_input('press enter to continue...')
