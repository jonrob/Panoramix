###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import time

from ROOT import TFile, TCanvas, TH1F, TH2F, TH3F, gROOT
debug = False

from Gaudi.Configuration import *
from GaudiPython.Parallel import Task, WorkManager
from GaudiPython import AppMgr
from LHCbConfig import *
#lhcbApp.DataType = "DC06"
lhcbApp.DataType = "MC09"
lhcbApp.Simulation = "True"


class MyTask(Task):
    def initializeLocal(self):
        self.output = {
            'h_bmass': TH1F('h_bmass', 'Mass of B candidate', 100, 5200.,
                            5500.)
        }

    def initializeRemote(self):
        #--- Get the basic configuration for each remote worker
        appConf.AppName = 'Ex2_multicore'
        EventSelector().PrintFreq = 1000
        AnalysisConf().DataType = lhcbApp.DataType
        PhysConf().DataType = lhcbApp.DataType
        from StrippingSelections.StrippingBs2JpsiPhi import sequence as seqBs2JpsiPhi
        appConf.TopAlg += [seqBs2JpsiPhi.sequence()]

    def process(self, file):
        H = self.output
        appMgr = AppMgr()
        sel = appMgr.evtsel()
        sel.open([file])
        evt = appMgr.evtsvc()
        while 0 < 1:
            appMgr.run(1)
            # check if there are still valid events
            if not evt['Rec/Header']: break
            cont = evt['Phys/Bs2JpsiPhi/Particles']
            if cont:
                for b in cont:
                    success = H['h_bmass'].Fill(b.momentum().mass())
        print 'Finishing.... ', file, ' ', H['h_bmass'].GetMean(
        ), H['h_bmass'].GetEntries()
        appMgr.stop()


#--- In the master process only....

if __name__ == '__main__':
    files = []
    if lhcbApp.DataType == "DC06":
        file = 'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001868/DST/0000/00001868_00000XXX_5.dst'
        nmax = 100
    else:
        file = 'PFN:castor:/castor/cern.ch/grid/lhcb/MC/MC09/DST/00004879/0000/00004879_00000XXX_1.dst'
        nmax = 15
    if not debug:
        for n in range(1, nmax):
            ff = file.replace('XXX', '%(X)03d' % {'X': n})
            x = os.system('nsls ' + ff.replace('PFN:castor:', ''))
            if x == 0: files.append(ff)
    else:
        files = [file.replace('XXX', '%(X)03d' % {'X': 12})]

    start = time.time()
    task = MyTask()
    if not debug:
        wmgr = WorkManager(ncpus=2)
        wmgr.process(task, files[:8])
    else:
        task.initializeLocal()
        task.initializeRemote()
        task.process(files[0])
    H = task.output

    H['h_bmass'].Draw()
    print H['h_bmass'].GetEntries()
    end = time.time()
    print 'total time', end - start
