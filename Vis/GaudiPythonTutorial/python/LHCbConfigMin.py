###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# wants to be the ROOT of everything
import ROOT, os, sys
#---Enable Tab completion-----------------------------------------
try:
    import rlcompleter, readline
    readline.parse_and_bind('tab: complete')
    readline.parse_and_bind('set show-all-if-ambiguous On')
except:
    pass
#
from LHCbConfig import addDBTags as addDBTags

# basic LHCb configurations for python users
from Gaudi.Configuration import *
# for backward compatibility with units in opts files
import os.path, GaudiKernel.ProcessJobOptions
GaudiKernel.ProcessJobOptions._parser._parse_units(
    os.path.expandvars("$STDOPTS/units.opts"))
#
import GaudiKernel.SystemOfUnits as units
#
from Configurables import LoKiSvc, LHCbApp, DstConf
LoKiSvc().Welcome = False
lhcbApp = LHCbApp()
# import PanoramixSys.Fixes_Gaudi
# very slow, therefore comment
# import PanoramixSys.Fixes_LoKi

appConf = ApplicationMgr()
appConf.ExtSvc += ['DataOnDemandSvc', 'LHCb::ParticlePropertySvc']
# for particle property service
import PartProp.Service, PartProp.decorators
DstConf().EnableUnpack = ["Reconstruction", "Stripping"]
# provide support for ondemand creation of standard particles
import CommonParticles.StandardBasic
import CommonParticles.StandardIntermediate

# other useful decorators
import gaudigadgets
from GaudiPython import gbl
import LHCbMath.Types
LHCbMath = LHCbMath.Types.Gaudi

# interactive users will probably never run with files on the GRID
# disabling avoids error message about not able to load GFale
from Configurables import Gaudi__IODataManager
iodatamanager = Gaudi__IODataManager()
iodatamanager.setProp('UseGFAL', False)

# LoKi wants to be last
appConf.ExtSvc += ['LoKiSvc']
