###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from ROOT import TH1F, TH2F, TCanvas, gDirectory, gSystem
# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "2010"
lhcbApp.Simulation = True
lhcbApp.DDDBtag = "head-20101206"
lhcbApp.CondDBtag = "sim-20101210-vc-md100"

appConf = ApplicationMgr(OutputLevel=INFO)

import GaudiPython, sys, math, os
from ROOT import *

appMgr = GaudiPython.AppMgr()
sel = appMgr.evtsel()
sel.open(['$PANORAMIXDATA/Sel_00006198_00000001_1.xdst'])
evt = appMgr.evtsvc()

hlist = {}
hlist['MC/IT/Hits'] = TH2F('horigin_IT', 'r vs z origin of MCHits IT', 1000,
                           -100., 13000., 100, 0., 2000.)
hlist['MC/TT/Hits'] = TH2F('horigin_TT', 'r vs z origin of MCHits TT', 500,
                           -100., 4000., 100, 0., 2000.)
hlist['MC/OT/Hits'] = TH2F('horigin_OT', 'r vs z origin of MCHits OT', 1000,
                           -100., 13000., 100, 0., 4000.)
hlist['MC/Velo/Hits'] = TH2F('horigin_Velo', 'r vs z origin of MCHits Velo',
                             500, -500., 2000., 100, 0., 300.)
hlist['MC/Rich/Hits'] = TH2F('horigin_Rich', 'r vs z origin of MCHits Rich',
                             1000, -100., 13000., 100, 0., 5000.)
hlist['MC/Muon/Hits'] = TH2F('horigin_Muon', 'r vs z origin of MCHits Muon',
                             1000, -100., 20000., 100, 0., 5500.)

for n in range(100):
    appMgr.run(1)
    if not evt['Rec/Header']: break
    for h in hlist:
        mch = evt[h]
        for ahit in mch:
            pos = ahit.mcParticle().originVertex().position()
            r = pos.rho()
            z = pos.z()
            if r > 3.:
                result = hlist[h].Fill(z, r)

from ROOT import TFile
f = TFile('hits.root', 'recreate')

for h in hlist:
    hlist[h].Write()
f.Close()

gStyle.SetOptStat('')
gStyle.SetPalette(1)
f = TFile('hits.root')

result = f.ReadAll()
gDirectory.GetListOfKeys()
hlist = []
clist = []
rlist = []
for hkey in gDirectory.GetListOfKeys():
    try:
        h = gROOT.FindObject(hkey.GetName())
        p = h.ProjectionX()
        hlist.append(h)
        clist.append(h.GetName())
        rlist.append(p)
    except:
        p = 0
cs = TCanvas('cs', 'results', 900, 600)
cs.Divide(1, 2)
for c in range(len(hlist)):
    hlist[c].SetXTitle('z [mm]')
    hlist[c].SetYTitle('r [mm]')
    hlist[c].SetMaximum(1000)
    cs.cd(1)
    result = hlist[c].DrawCopy('colz')
    cs.cd(2)
    rlist[c].SetName(hlist[c].GetName().replace('r vs ', ''))
    rlist[c].SetXTitle('z [mm]')
    result = rlist[c].DrawCopy()
    cs.Update()
    cs.Print(clist[c].replace('/', '-') + '.jpg')
