###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
myDEBUG = -1
beamAperture = False
SIMPLIFIED = False
f_2008 = True

tag = ''
if SIMPLIFIED: tag = '_simplified'
#CONDITIONS_PATH='/afs/cern.ch/user/v/vfave/cmtuser/Panoramix_v15r8/WZDet_140108/CondDB/Conditions/MainCatalog.xml'

# get the basic configuration from here
from LHCbConfig import *
if not f_2008:
    lhcbApp.DataType = "DC06"
else:
    lhcbApp.DataType = "2008"

from Configurables import TransportSvc

# overwrite location of geometry
from Configurables import DDDBConf
#lhcbApp.DDDBtag   = "head-20081002"
#lhcbApp.CondDBtag = "head-20081002"
#DDDBConf().DbRoot = "/afs/cern.ch/lhcb/group/panoramix/vol1/myDDDB/lhcb.xml"

#DetectorDataSvc().DetDbLocation = "$XMLVISROOT/DDDB/lhcb-infrastructure.xml";

if SIMPLIFIED:
    TransportSvc.StandardGeometryTop = "/dd/TrackfitGeometry/Structure/LHCb"
    DDDB = allConfigurables("DDDB")
    LHCBCOND = allConfigurables("LHCBCOND")
    DDDB.ConnectionString = "sqlite_file:D:/lhcb_data/myDDDB.db/DDDB"
    DDDB.LazyConnect = False
    LHCBCOND.LazyConnect = False
    DDDB.DefaultTAG = ""

ApplicationMgr(OutputLevel=INFO, AppName='rad_map06', ExtSvc=['TransportSvc'])

## overwrite conditions path
#ApplicationMgr().Environment = {"CONDITIONS_PATH": CONDITIONS_PATH}
# for next LHCb version:
#ApplicationMgr().Environment["CONDITIONS_PATH"] = CONDITIONS_PATH

from ROOT import TH2F, TH1F, TMath, TFile, TCanvas, gStyle, gSystem, gROOT
import GaudiPython, os
gbl = GaudiPython.gbl
std = gbl.std

appMgr = GaudiPython.AppMgr()

appMgr.initialize()
tranSvc = appMgr.service('TransportSvc', 'ITransportSvc')

XYZPoint = LHCbMath.XYZPoint
XYZVector = LHCbMath.XYZVector

Interval = std.pair('double', 'double')
Intersection = std.pair(Interval, 'const Material*')
Intersections = std.vector(Intersection)
intersept = Intersections()

zCenter = 0
tx = 0
ty = 0

z = [835., 978., 2250., 2750., 7670., 8350., 9040., 9420., 11920., 12280., 0.]
hrad_eta = []
for i in range(len(z) - 1):
    name = ' rad length eta vs. phi ' + str(z[i])
    if i > 0:
        name = 'rad length eta vs. phi from ' + str(z[i - 1]) + ' to ' + str(
            z[i])
    key = 'hrad_eta' + str(i)
    hrad_eta.append(TH2F(key, name, 100, -180., 180., 100, 1.5, 5.))

hrad_eta.append(
    TH2F('hrad_eta_rf', 'rad length eta vs. phi before first measured point',
         100, -180., 180., 100, 1.5, 5.))

hrad_xy_bp = TH2F('hrad_xy_bp', 'aperture beam pipe, xy', 100, -5., 5., 100,
                  -5., 5.)
hrad_z_bp = TH1F('hrad_z_bp', 'aperture beam pipe, z', 130, 0., 13000.)

# helpful if you want to reload without re-executing all this
if __name__ == '__main__':

    for k in range(100):
        eta = 1.5 + (float(k) + 0.5) * 3.5 / 100.
        theta = 2. * TMath.ATan(TMath.Exp(-eta))
        for j in range(100):
            phi = -180. + (float(j) + 0.5) * 360. / 100.
            phirad = phi / 180. * TMath.Pi()
            a = XYZPoint(0., 0., 0.)
            # look first for before first measured point
            rz = 8.05
            zz = rz / TMath.Tan(theta)
            if zz < 900.:
                x = rz * TMath.Cos(phirad)
                y = rz * TMath.Sin(phirad)
                b = XYZPoint(x, y, zz)
                radlength = tranSvc.distanceInRadUnits(a, b)
                result = hrad_eta[len(z) - 1].Fill(phi, eta, radlength)
                if myDEBUG > 0:
                    vect = XYZVector(b.x() - a.x(),
                                     b.y() - a.y(),
                                     b.z() - a.z())
                    nWall = tranSvc.intersections(a, vect, 0., 1., intersept,
                                                  0.0001)
                    print 'test ', x, y, zz, phi, eta, radlength
                    for w in range(nWall):
                        print intersept[w].second.name()

            for i in range(len(z) - 1):
                r = z[i] / TMath.Cos(theta)
                x = r * TMath.Cos(phirad) * TMath.Sin(theta)
                y = r * TMath.Sin(phirad) * TMath.Sin(theta)
                b = XYZPoint(x, y, z[i])
                radlength = tranSvc.distanceInRadUnits(a, b)
                result = hrad_eta[i].Fill(phi, eta, radlength)
                tx = x / z[i]
                ty = y / z[i]

                a = b

# check beam pipe aperture:
    if beamAperture:
        for k in range(100):
            x = -5. + (float(k) + 0.5) * 10.0 / 100.
            for j in range(100):
                y = -5. + (float(j) + 0.5) * 10.0 / 100.
                r = TMath.Sqrt(x * x + y * y)
                a = XYZPoint(x, y, 0.)
                b = XYZPoint(x, y, 13000.)
                radlength = tranSvc.distanceInRadUnits(a, b)
                result = hrad_xy_bp.Fill(x, y, radlength)

        for l in range(130):
            z = (float(l) + 0.5) * 13000.0 / 130.
            a = XYZPoint(0, 0, 0.)
            b = XYZPoint(0, 0, z)
            radlength = tranSvc.distanceInRadUnits(a, b)
            result = hrad_z_bp.Fill(z, radlength)

    f = TFile('radlength' + tag + '.root', 'recreate')
    for h in gROOT.GetList():
        h.Write()
    f.Close()


def aftermath(input):
    from ROOT import TFile, TH2F, TH1F, TCanvas
    z = [
        835., 978., 2250., 2750., 7670., 8350., 9040., 9420., 11920., 12280.,
        0.
    ]
    f = TFile(input)
    tag = ''
    if input.find('simpl') > 0: tag = '_simplified'
    result = f.ReadAll()
    hlist = f.GetList()
    hrad_eta = []
    for h in hlist:
        hrad_eta.append(h)
    gStyle.SetOptStat('')
    gStyle.SetPalette(1)
    cc = TCanvas('cc', 'results', 1200, 800)
    cs = TCanvas('cs', 'results', 600, 400)
    cs.Divide(1)
    cc.Divide(3, 4)
    for i in range(len(z)):
        pp = cc.cd(i + 1)
        hrad_eta[i].SetMaximum(0.5)
        if i == len(z) - 1:
            hrad_eta[i].SetMaximum(0.07)
        if i > 3 and i < 8:
            hrad_eta[i].SetMaximum(0.2)
        if i > 1 and i < 4:
            hrad_eta[i].SetMaximum(0.2)
        hrad_eta[i].Draw('colz')
        #get average:
        sum = 0
        nbins = 0
        for ix in range(hrad_eta[i].GetNbinsX() + 1):
            for iy in range(hrad_eta[i].GetNbinsY() + 1):
                biny = hrad_eta[i].GetYaxis().GetBinLowEdge(iy)
                if biny < 4.5 and biny > 1.9:
                    sum += hrad_eta[i].GetCellContent(ix, iy)
                    nbins += 1
        average = '%4.1f' % (100. * sum / float(nbins))
        title = hrad_eta[i].GetTitle()
        if title.find('average') < 0:
            title = title + '  average = ' + average
        hrad_eta[i].SetTitle(title)
        hrad_eta[i].SetYTitle('eta')
        hrad_eta[i].SetXTitle('phi')
        cs.cd(1)
        hrad_eta[i].DrawCopy('colz')
        fn = hrad_eta[i].GetName() + tag + '.gif'
        cs.Print(fn)
        hy = hrad_eta[i].ProjectionY()
        norm = hrad_eta[i].GetNbinsX()
        for k in range(1, hy.GetNbinsX() + 1):
            hy.SetBinContent(k, hy.GetBinContent(k) / norm)
        fn = hrad_eta[i].GetName() + tag + '_eta.gif'
        hy.SetMaximum(0.5)
        hy.SetMinimum(0.0)
        if i == len(z) - 1:
            hy.SetMaximum(0.07)
        if i > 3 and i < 8:
            hy.SetMaximum(0.2)
        if i > 1 and i < 4:
            hy.SetMaximum(0.2)
        hy.SetXTitle('eta')
        hy.SetYTitle('rad.len.')
        hy.DrawCopy()
        cs.Print(fn)
    f.Close()
    return hrad_eta


# input = 'radlength.root'
input = 'radlength' + tag + '.root'
hrad_proj = aftermath(input)
tag = ''


def compare():
    g = TFile('temp_simp.root')
    f = TFile('temp.root')
    cs = TCanvas('cs', 'comparison', 600, 400)
    hrad_eta = []
    hrad_eta_s = []
    for i in range(10):
        hy = f.FindObjectAny('hrad_eta' + str(i) + '_py')
        hrad_eta.append(hy)
        hy = g.FindObjectAny('hrad_eta' + str(i) + '_py')
        hrad_eta_s.append(hy)
    hy = f.FindObjectAny('hrad_eta_rf_py')
    hrad_eta.append(hy)
    hy = g.FindObjectAny('hrad_eta_rf_py')
    hrad_eta_s.append(hy)
    for i in range(len(hrad_eta)):
        hy = hrad_eta[i]
        hys = hrad_eta_s[i]
        if i == len(hrad_eta) - 1:
            hy.SetMaximum(0.07)
        if i > 3 and i < 8:
            hy.SetMaximum(0.2)
        if i > 1 and i < 4:
            hy.SetMaximum(0.2)
        hy.SetXTitle('eta')
        hy.SetYTitle('rad.len.')
        hy.DrawCopy()
        hys.SetLineColor(3)
        hys.Draw('same')
        fn = hy.GetName() + '_comp_eta.gif'
        cs.Print(fn)
