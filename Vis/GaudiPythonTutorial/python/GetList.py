###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from ROOT import TH1F, gROOT, gDirectory, TFile
from LHCbConfig import *
lhcbApp.DataType = "2008"
lhcbApp.Simulation = True

appConf = ApplicationMgr(OutputLevel=4, AppName='test')
EventClockSvc().EventTimeDecoder = 'FakeEventTime'

import GaudiPython
appMgr = GaudiPython.AppMgr()

htest = TH1F('test', '1d histo', 100, 0., 99.)

print 'GetList A', gROOT.GetList().GetSize(), gDirectory.GetList().GetSize()

sel = appMgr.evtsel()
print 'GetList B', gROOT.GetList().GetSize(), gDirectory.GetList().GetSize()
evt = appMgr.evtsvc()

fn = '/castor/cern.ch/user/t/truf/MC2008/mbias_7TeV_2008.sim'
sel.open(['PFN:' + fn])

appMgr.run(1)
htest.Fill(evt['MC/Velo/Hits'].size())

print 'GetList C', gROOT.GetList().GetSize(), gDirectory.GetList().GetSize()

f = TFile('test.root', 'recreate')
for h in gROOT.GetList():
    h.Write()
    print h.GetName()
f.Close()

print 'GetList D', gROOT.GetList().GetSize(), gDirectory.GetList().GetSize()
