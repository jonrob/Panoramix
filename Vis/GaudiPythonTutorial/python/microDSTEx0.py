###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# create a default micro DST

max_Events = 500000

# get the basic configuration from here
from LHCbConfig import *
appConf = ApplicationMgr(OutputLevel=INFO, AppName='microDSTEx0')

####MicroDSTCommon.opts##

lhcbApp.DataType = "2011"
lhcbApp.Simulation = False
EventSelector().PrintFreq = 10000

# container name
stream = 'Semileptonic'
selection_name = 'b2D0MuXpipiB2DMuNuXLine'
candidates = stream + '/Phys/' + selection_name + '/Particles'
output_name = selection_name + '.mdst'

from Configurables import MicroDSTWriter
from Configurables import CombineParticles
# the following is very stupid
from PhysSelPython.Wrappers import SelectionSequence
from PhysSelPython.Wrappers import Selection
stupid1 = Selection(
    selection_name,
    Algorithm=CombineParticles('xxx', DecayDescriptor='Z0->mu+mu-'),
    RequiredSelections=[])
selSequence = SelectionSequence('Seq' + selection_name, TopSelection=stupid1)
micro = MicroDSTWriter('MyDSTWriter')
#micro.MicroDSTFile         = output_name
micro.OutputPrefix = 'MicroDST'
micro.OutputFileSuffix = 'micro'
micro.SelectionSequences = [selSequence]
micro.ExtraItems = ['Trigger/RawEvent#1']
micro.CopyODIN = False
micro.CopyL0DUReport = False
micro.CopyHltDecReports = False
micro.CopyMCTruth = False
micro.CopyBTags = False
# ?micro.CopyPVRelations      = True
micro.CopyPVs = True
micro.CopyParticles = True
micro.CopyProtoParticles = True
micro.CopyRecSummary = True
micro.CopyRecHeader = True
micro.WriteFSR = True
micro.RootInTES = '/Event/Semileptonic/'
microDSTSeq = micro.sequence()
appConf.TopAlg += [microDSTSeq]

import GaudiPython
from gaudigadgets import *

appMgr = GaudiPython.AppMgr()
appMgr.algorithm('Seq' + selection_name).Enable = False
appMgr.initialize()

sel = appMgr.evtsel()
fn = ['/media/Data2/SEMILEPTONIC.DST/UP/00012569_00000004_1.semileptonic.dst']
sel.open(fn)
mdstwriter = 'MyDSTWriterMainSeq'
appMgr.algorithm(mdstwriter).Enable = False

evt = appMgr.evtsvc()

Nevents = 0
while Nevents < max_Events:
    rc = appMgr.run(1)
    if not evt['Rec/Header']: break
    if evt[candidates]:
        if evt[candidates].size() > 0:
            # copy selected event to output file
            xx = nodes(evt, True, '/Event/Semileptonic')
            xx = evt['/Event/Trigger/RawEvent']
            appMgr.algorithm(mdstwriter).execute()
            Nevents += 1

print "Number of selected events:", Nevents
