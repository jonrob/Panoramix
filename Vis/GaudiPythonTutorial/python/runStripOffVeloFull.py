###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os, getpass
files = []

host = os.environ['HOST']
local = host.find('pctommy') > -1
if local:
    path = '/media/Work'
else:
    path = '/castor/cern.ch/grid/lhcb/data/2009/DST/0000xxxx/0000/'

prodids = [5799, 5800, 5801]

if local:
    allfiles = os.listdir(path)
    for f in allfiles:
        for prod in prodids:
            if f.find('_1.dst') > -1 and f.find('0000' + str(prod)) > -1:
                files.append(path + '/' + f)
else:
    for prod in prodids:
        fn = path.replace('xxxx', str(prod))
        for n in range(1, 10):
            ff = '0000' + str(prod) + '_000000' + '%(X)02d' % {
                'X': n
            } + '_1.dst'
            x = os.system('nsls ' + fn + ff)
            if x == 0: files.append(fn + ff)

for f in files:
    nw = f.replace('_1.dst', '_2.dst')
    if not local:
        tmp = nw.split('/')
        nw = '/tmp/' + getpass.getuser() + '/' + tmp[len(tmp) - 1]
    os.system('python $GAUDIPYTHONTUTORIAL/stripOffVeloFull.py ' + f + ' ' +
              nw)
