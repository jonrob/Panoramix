###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from ROOT import TH1F, TBrowser, TCanvas
# get the basic configuration from here
from LHCbConfig import *
appConf = ApplicationMgr(OutputLevel=INFO, AppName='Ex11')
EventSelector().PrintFreq = 100
lhcbApp.DataType = "2011"
# container name
candidates = 'Dimuon/Phys/BetaSBu2JpsiKPrescaledLine/Particles'

import GaudiPython
LorentzVector = GaudiPython.gbl.Math.LorentzVector(
    'ROOT::Math::PxPyPzE4D<double>')

appMgr = GaudiPython.AppMgr()
sel = appMgr.evtsel()
sel.open(['$PANORAMIXDATA/Bu2JpsiK_00011917_00000048_1.dst'])

evt = appMgr.evtsvc()

h_bmass = TH1F('h_bmass', 'Mass of B candidate', 100, 5200., 5500.)

for n in range(40):
    appMgr.run(1)
    # check if there are still valid events
    if not evt['Rec/Header']: break
    cont = evt[candidates]
    if cont:
        for b in cont:
            success = h_bmass.Fill(b.momentum().mass())
h_bmass.Draw()
if h_bmass.GetEntries() < 1:
    print 'ERROR ! No b candidates found.'
