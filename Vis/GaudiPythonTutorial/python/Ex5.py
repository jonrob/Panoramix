###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import GaudiPython
LHCbMath = GaudiPython.gbl.Math
XYZPoint = LHCbMath.XYZPoint
XYZVector = LHCbMath.XYZVector
# Instantiation of the objects:
aPoint = XYZPoint(0., 0., 10.)
aVector = XYZVector(1., 1., 5.)

print aVector.x(), aVector.y(), aVector.z()
