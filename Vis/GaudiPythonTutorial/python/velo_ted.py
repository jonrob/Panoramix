###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *
from Configurables import LHCbApp, UpdateManagerSvc
lhcbApp = LHCbApp()

lhcbApp.Simulation = False
#The position of the Velo was from 6/5/2009  1:14:29 AM  to 6/8/2009 12:39:01 AM

#UpdateManagerSvc().ConditionsOverride += ["Conditions/Alignment/Velo/VeloRight := double_v dPosXYZ = 0 0 0;",  "Conditions/Alignment/Velo/VeloLeft  :=  double_v dPosXYZ = 0 0 0;"]
#UpdateManagerSvc().ConditionsOverride += ["Conditions/Online/Velo/MotionSystem := double ResolPosRC =-1. ; double ResolPosLA = 1. ;"]

# And from 6/8/2009  12:39:01 AM to 6/8/2009  8:53:04 AM (from run 50482)
#UpdateManagerSvc().ConditionsOverride += ["Conditions/Alignment/Velo/VeloRight := double_v dPosXYZ = 0 0 0;",  "Conditions/Alignment/Velo/VeloLeft  :=  double_v dPosXYZ = 0 0 0;"]
#UpdateManagerSvc().ConditionsOverride += ["Conditions/Online/Velo/MotionSystem := double ResolPosRC =-1.45 ; double ResolPosLA = 1. ;"]

#ideal position
UpdateManagerSvc().ConditionsOverride += [
    "Conditions/Alignment/Velo/VeloRight := double_v dPosXYZ = 0 0 0;",
    "Conditions/Alignment/Velo/VeloLeft  :=  double_v dPosXYZ = 0 0 0;"
]
UpdateManagerSvc().ConditionsOverride += [
    "Conditions/Online/Velo/MotionSystem := double ResolPosRC =0 ; double ResolPosLA = 0 ;"
]
