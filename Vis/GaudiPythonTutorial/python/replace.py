###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os

# loop over all files in current directory, replace string A with B

# A='DecodeRawEvent.opts'
# B='DecodeRawEvent.py'

#A = """DataOnDemandSvc().Algorithms += ["DATA='/Event/MC/Particles' TYPE='UnpackMCParticle'"]"""
#B = """DataOnDemandSvc().AlgMap['/Event/MC/Particles'] =  'UnpackMCParticle' """

A = """DataOnDemandSvc().Algorithms += ["DATA='/Event/MC/Vertices' TYPE='UnpackMCVertex'"]"""
B = """DataOnDemandSvc().AlgMap['/Event/MC/Vertices'] =  'UnpackMCVertex' """

for fn in os.listdir('.'):
    if fn.find('.py') < 0 or fn == 'replace.py': continue
    f = open(fn)
    xx = f.readlines()
    yy = []
    for l in xx:
        yy.append(l.replace(A, B))
    f.close()
    f = open(fn, 'w')
    f.writelines(yy)
