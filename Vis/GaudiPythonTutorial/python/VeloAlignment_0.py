###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#
# This is a randomly generated geometry for a misaligned VELO
#
from Gaudi.Configuration import *
from Configurables import UpdateManagerSvc

UpdateManagerSvc().ConditionsOverride += [
    "Conditions/Alignment/Velo/Module00 :=   double_v dPosXYZ = 0.0635349 0.04128 0.180246     ;    double_v dRotXYZ = -0.00307441 -0.00202025 0.0017405 ;",
    "Conditions/Alignment/Velo/Module01 :=   double_v dPosXYZ = 0.0289305 -0.0666127 -0.094242 ;    double_v dRotXYZ = 0.00364523 0.000529841 0.00394782 ;",
    "Conditions/Alignment/Velo/Module02 :=   double_v dPosXYZ = -0.0133454 0.0235485 -0.106556 ;    double_v dRotXYZ = -0.000342707 -0.00105386 -0.00142354 ;",
    "Conditions/Alignment/Velo/Module03 :=   double_v dPosXYZ = -0.0710579 0.00341906 0.00921009 ;  double_v dRotXYZ = -0.00206866 -0.00080156 -0.00272312 ;",
    "Conditions/Alignment/Velo/Module04 :=   double_v dPosXYZ = 0.0273991 -0.000512667 -0.00229275 ;double_v dRotXYZ = 0.00107297 0.000595099 -0.00232613 ;",
    "Conditions/Alignment/Velo/Module05 :=   double_v dPosXYZ = -0.0240594 -0.0459471 -0.0892537 ;  double_v dRotXYZ = 0.00314889 -0.00604904 -0.00117531 ;",
    "Conditions/Alignment/Velo/Module06 :=   double_v dPosXYZ = 0.0312938 -0.00934182 -0.0907303 ;  double_v dRotXYZ = 0.00353919 0.00110423 -0.000417191 ;",
    "Conditions/Alignment/Velo/Module07 :=   double_v dPosXYZ = -0.0650734 0.0251344 -0.0960423 ;   double_v dRotXYZ = -0.00213293 0.00442087 0.0010569 ;",
    "Conditions/Alignment/Velo/Module08 :=   double_v dPosXYZ = -0.0333002 -0.00525165 0.0323088 ;  double_v dRotXYZ = 0.00093199 -0.00260932 0.00051802 ;",
    "Conditions/Alignment/Velo/Module09 :=   double_v dPosXYZ = -0.0105864 -0.0170406 0.1421 ;      double_v dRotXYZ = 0.00160828 -0.00337104 0.00141321 ;",
    "Conditions/Alignment/Velo/Module10 :=   double_v dPosXYZ = -0.0241484 -0.0203007 -0.0787133 ;  double_v dRotXYZ = -0.00229586 0.000333429 0.000545724 ;",
    "Conditions/Alignment/Velo/Module11 :=   double_v dPosXYZ = 0.0110519 -0.0393883 -0.10665 ;     double_v dRotXYZ = 0.000129741 -0.000730364 -0.000107885 ;",
    "Conditions/Alignment/Velo/Module12 :=   double_v dPosXYZ = 0.0312448 -0.0136082 0.0175604 ;    double_v dRotXYZ = -0.000831239 -0.000617765 0.000807337 ;",
    "Conditions/Alignment/Velo/Module13 :=   double_v dPosXYZ = -0.0167597 -0.0258978 -0.0850397 ;  double_v dRotXYZ = 0.000917341 -0.000908287 -0.00442865 ;",
    "Conditions/Alignment/Velo/Module14 :=   double_v dPosXYZ = 0.0290706 -0.0307802 0.143788 ;     double_v dRotXYZ = 0.00166444 0.000307013 -0.00125431 ;",
    "Conditions/Alignment/Velo/Module15 :=   double_v dPosXYZ = -0.023445 0.0300957 0.0672115 ;     double_v dRotXYZ = 0.00240206 0.00275556 -0.000567685 ;",
    "Conditions/Alignment/Velo/Module16 :=   double_v dPosXYZ = -0.00745566 0.0463576 0.00529497 ;  double_v dRotXYZ = -0.000914432 -0.00140465 0.00125394 ;",
    "Conditions/Alignment/Velo/Module17 :=   double_v dPosXYZ = -0.00830967 0.0244855 0.0585261 ;   double_v dRotXYZ = 0.000257983 -0.0011153 0.000889177 ;",
    "Conditions/Alignment/Velo/Module18 :=   double_v dPosXYZ = -0.0317301 0.0459125 -0.0374645 ;   double_v dRotXYZ = -0.000904054 0.000416336 -0.00343952 ;",
    "Conditions/Alignment/Velo/Module19 :=   double_v dPosXYZ = -0.0386386 -0.0038888 0.108855 ;    double_v dRotXYZ = -0.000924575 0.000162545 0.00159484 ;",
    "Conditions/Alignment/Velo/Module20 :=   double_v dPosXYZ = -0.0381995 0.0756944 0.0815113 ;    double_v dRotXYZ = -0.000543237 -0.00368532 -0.00314846 ;",
    "Conditions/Alignment/Velo/Module21 :=   double_v dPosXYZ = 0.034402 -0.00996895 -0.0642629 ;   double_v dRotXYZ = -0.00446535 -0.000473574 0.00127147 ;",
    "Conditions/Alignment/Velo/Module22 :=   double_v dPosXYZ = -0.0659282 0.006053 -0.138067 ;     double_v dRotXYZ = 0.000609318 -0.00300358 -0.00381768 ;",
    "Conditions/Alignment/Velo/Module23 :=   double_v dPosXYZ = 0.00644277 -0.0299998 0.0275383 ;   double_v dRotXYZ = 0.00135927 -0.00118283 0.000453615 ;",
    "Conditions/Alignment/Velo/Module24 :=   double_v dPosXYZ = -0.0418298 -0.0114129 -0.154548 ;   double_v dRotXYZ = -0.00340443 0.00288921 0.000576805 ;",
    "Conditions/Alignment/Velo/Module25 :=   double_v dPosXYZ = 0.0145694 -0.0588532 0.114619 ;     double_v dRotXYZ = 0.000179737 -0.00273511 -7.68321e-05 ;",
    "Conditions/Alignment/Velo/Module26 :=   double_v dPosXYZ = -0.0342889 -0.0607145 -0.0930746 ;  double_v dRotXYZ = 0.00494307 -0.00143177 0.00196953 ;",
    "Conditions/Alignment/Velo/Module27 :=   double_v dPosXYZ = -0.00665561 0.000971879 -0.221607 ; double_v dRotXYZ = 0.00130289 0.000711379 0.00450648 ;",
    "Conditions/Alignment/Velo/Module28 :=   double_v dPosXYZ = -0.0218842 0.0325154 0.0663503 ;    double_v dRotXYZ = 0.000614064 0.00330096 -0.000391567 ;",
    "Conditions/Alignment/Velo/Module29 :=   double_v dPosXYZ = 0.0295989 0.0448393 0.0795588 ;     double_v dRotXYZ = -0.00229724 -0.00156035 -0.00216414 ;",
    "Conditions/Alignment/Velo/Module30 :=   double_v dPosXYZ = 0.00336672 -0.0100933 0.0681663 ;   double_v dRotXYZ = 0.000785313 0.00179483 -0.000178721 ;",
    "Conditions/Alignment/Velo/Module31 :=   double_v dPosXYZ = -0.0231779 0.0508176 -0.105119 ;    double_v dRotXYZ = -0.000286844 0.00135542 -0.00143943 ;",
    "Conditions/Alignment/Velo/Module32 :=   double_v dPosXYZ = 0.0060728 0.0217163 -0.00764185 ;   double_v dRotXYZ = 0.000206582 0.000967998 -0.00218142 ;",
    "Conditions/Alignment/Velo/Module33 :=   double_v dPosXYZ = -0.0114414 0.0199365 0.00297419 ;   double_v dRotXYZ = -0.000889326 0.00166145 -0.00328849 ;",
    "Conditions/Alignment/Velo/Module34 :=   double_v dPosXYZ = 0.0265193 0.0162932 0.0351011 ;     double_v dRotXYZ = 0.00154706 -0.000112057 0.000576347 ;",
    "Conditions/Alignment/Velo/Module35 :=   double_v dPosXYZ = -0.0393807 -0.0247066 -0.033558 ;   double_v dRotXYZ = -0.0019558 -0.000572795 0.0016243 ;",
    "Conditions/Alignment/Velo/Module36 :=   double_v dPosXYZ = -0.0354236 0.0354289 0.0414913 ;    double_v dRotXYZ = -0.00249071 -0.0028281 0.00171 ;",
    "Conditions/Alignment/Velo/Module37 :=   double_v dPosXYZ = 0.0192148 -0.0231848 0.0894338 ;    double_v dRotXYZ = -0.00307824 -0.00237277 -4.29462e-05 ;",
    "Conditions/Alignment/Velo/Module38 :=   double_v dPosXYZ = 0.065176 -0.00837179 -0.057486 ;    double_v dRotXYZ = -0.00122874 -9.38139e-05 -0.000832824 ;",
    "Conditions/Alignment/Velo/Module39 :=   double_v dPosXYZ = -0.00867514 0.0206465 -0.014277 ;   double_v dRotXYZ = 0.000160996 -0.0016538 -0.000193585 ;",
    "Conditions/Alignment/Velo/Module40 :=   double_v dPosXYZ = -0.0632798 0.0238603 0.0262075 ;    double_v dRotXYZ = -0.000470467 0.000697788 0.0031612 ;",
    "Conditions/Alignment/Velo/Module41 :=   double_v dPosXYZ = 0.0459037 0.019772 0.105812 ;       double_v dRotXYZ = 2.5665e-05 5.88638e-05 0.000517149 ;",
    "Conditions/Alignment/Velo/VeloLeft :=   double_v dPosXYZ = 0.237474 0.104842 0.114436 ;        double_v dRotXYZ = 0.000579897 -0.000686025 0.000383885 ;",
    "Conditions/Alignment/Velo/VeloRight :=  double_v dPosXYZ = -0.0783735 -0.0697885 0.0695369 ;   double_v dRotXYZ = 0.000157121 -0.000705915 0.000577538 ;"
]
