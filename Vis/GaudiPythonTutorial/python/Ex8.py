###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from ROOT import TH1F, TBrowser, TCanvas
# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "2010"
lhcbApp.Simulation = True

appConf = ApplicationMgr(OutputLevel=INFO, AppName='Ex8')

import GaudiPython

appMgr = GaudiPython.AppMgr()
sel = appMgr.evtsel()
sel.open(['$PANORAMIXDATA/MC2010_BsJpsiPhi__00008919_00000068_1.dst'])
evt = appMgr.evtsvc()

from LinkerInstances.eventassoc import *
GaudiPython.loaddict('libLinkerEvent')

MCParticle = GaudiPython.gbl.LHCb.MCParticle
Track = GaudiPython.gbl.LHCb.Track

appMgr.run(1)
ltrack2part = linkedTo(MCParticle, Track, 'Rec/Track/Best')
lpart2track = linkedFrom(Track, MCParticle, 'Rec/Track/Best')
for t in evt['Rec/Track/Best']:
    for mcp in ltrack2part.range(t):
        print 'MCParticle matched to track:'
        print mcp
        for tt in lpart2track.range(mcp):
            print 'track matched to MCParticle:', tt.key()
