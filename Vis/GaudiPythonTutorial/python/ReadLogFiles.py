###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import httplib, urllib, tempfile, gzip, os, sys

debug = False
new_found = []


def copytar(url):
    log_file = ''
    head = url.split('/0000/')[0] + '/0000/'
    prodnr = url.split('/0000/')[1].split('/Brunel')[0]
    tar = prodnr + '.tar.gz'
    urllib.urlretrieve(head + tar, tar)
    sc = os.system('gunzip ' + tar)
    if sc > 0:
        os.system('rm ' + tar)
    else:
        os.system('tar -xf ' + tar.split('.gz')[0])
        tmpfile = url.split('/0000/')[1]
        if os.system('ls -al ' + tmpfile) == 0:
            fc = gzip.open(tmpfile)
            log_file = fc.read()
            fc.close()
        os.system('rm -r ' + tar.split('.tar')[0])
        os.system('rm ' + tar.split('.gz')[0])
    return log_file


def readlist(fname):
    event = {'runNumber': 0, 'evtNumber': 0}
    f = open(fname)
    evtlist = []
    config = ''
    prodid = -1
    line = f.readline()
    if line.find('ProdId') < 0 or line.find('Config') < 0:
        print 'production id or configuration is missing'
        print 'Example: ProdId 1883     Config phys-v3-lumi2'
    else:
        a = line.find('ProdId') + 6
        b = line.find('Config')
        prodid = int(line[a:b])
        list = line[b + 6:].split(' ')
        for l in list:
            if l != ' ':
                config = l.replace('\n', '')
                if sys.platform.find('linux') != -1:
                    config = config.replace('\r', '')
        print 'Production ID:', prodid, '  Configuration:', config
        line = f.readline()
        while line != '':
            try:
                event['runNumber'] = int(line[line.find('Run') + 3:])
                event['evtNumber'] = int(
                    line[line.find('Event') + 5:line.find('Run')])
                print 'searching for ', event
                evtlist.append(event.copy())
                line = f.readline()
            except:
                break
    f.close()
    return prodid, config, evtlist


fn = 'http://lhcb-logs.cern.ch/storage/lhcb/production/DC06/v1r0/0000prodid/LOG/0000/000qqqqq/Brunel_0000prodid_000qqqqq_step.txt.gz'
#fn = 'http://lhcb-logs.cern.ch/storage/lhcb/production/DC06/config/0000prodid/LOG/xxxx/000qqqqq/Brunel_0000prodid_000qqqqq_step.txt.gz'
#step = 5
#fn = 'http://lhcb-logs.cern.ch/storage/lhcb/production/Stripping-DC06/config/0000prodid/LOG/xxxx/000qqqqq/Brunel_0000prodid_000qqqqq_step.txt.gz'
#step = 3
step = 2

fname = 'events.lst'
add_info = False
if len(sys.argv[1:]) > 0:
    arg = sys.argv[1:][0]
    if arg.find('-a') < 0:
        fname = sys.argv[1:][0]
    else:
        add_info = True
    if len(sys.argv[1:]) > 1:
        arg = sys.argv[1:][1]
        if arg.find('-a') > -1:
            add_info = True

prodid, config, evtlist = readlist(fname)
if prodid < 0 or config == '': stop

temp2 = fn.replace('step', '%01d' % step)
temp1 = temp2.replace('prodid', '%04d' % prodid)
temp3 = temp1.replace('config', '%s' % config)

for k in range(0, 1):
    temp = temp3.replace('xxxx', '%04d' % k)
    print 'start with ', temp
    start = k * 10000
    end = start + 3000
    for n in range(start, end):
        if len(evtlist) < 1:
            print 'all requested events found'
            break
        url = temp.replace('qqqqq', '%05d' % n)
        tmpfile = tempfile.mktemp() + '.gz'
        ii = n / 100
        modn = n - ii * 100
        if (modn == 0): print 'try now:', url
        try:
            urllib.urlretrieve(url, tmpfile)
            fc = gzip.open(tmpfile)
            log_file = fc.read()
            fc.close()
            os.remove(tmpfile)
            if debug:
                print 'SUCCESS: file found', url
        except IOError:
            if debug:
                print "Error: \tThe url %s is not available or doesn't exist.\n" % url
# check for tar file
            log_file = copytar(url)

# old scheme, turned out to be too slow
        a = 0
        lnr = 1
        while lnr > -1:
            lnr = log_file[a:].find('Brunel               INFO Evt')
            if lnr < 0: break
            a += lnr + 25
            b = log_file[a:].find('\n') + a
            evt_run = log_file[a:b]
            list = evt_run.split(',')
            evnr = '-1'
            runr = '-1'
            for l in list:
                evta = l.find('Evt')
                runa = l.find('Run')
                if evta > -1:
                    evnr = int(l[evta + 4:])
                if runa > -1:
                    runr = int(l[runa + 4:])
#####  print evt_run,evnr,runr
            new_found = []
            for event in evtlist:
                if event['runNumber'] == runr and event[
                        'evtNumber'] == evnr or event['evtNumber'] == evnr:
                    ###     print 'SUCCESS: file found',url
                    ###     dba = log_file[:b].rfind('PoolDbDatabaseCnv')+59
                    ###     dbe = log_file[dba:].find('.root')+dba
                    ###     print 'event found ',event,'on tape',log_file[dba:dbe]+'_'+str(step)+'.dst'
                    event['runNumber'] = runr
                    event['evtNumber'] = evnr
                    dba = log_file[:b].rfind('EventDataSvc') + 35
                    dbe = log_file[dba:].find('.dst') + dba + 4
                    print 'event found ', event, 'on tape', '%04d' % k + '/' + log_file[
                        dba:dbe]
                    new_found.append(event)
                    if add_info:
                        next = log_file[a:].find(
                            'Brunel               INFO Evt')
                        prev = log_file[a:].find('\n')
                        if next < 0:
                            next = log_file[a:].find(
                                'EventLoopMgr         INFO No more')
                        if next > 0 and prev > 0:
                            print log_file[prev + a:next + a - 1]
            for n in new_found:
                evtlist.remove(n)

# another way:

        if lnr == -1:
            new_found = []
            for event in evtlist:
                log_test = log_file
                n = 1
                while n > 0:
                    n = log_test.find(str(event['evtNumber']))
                    if n < 0: continue
                    line = log_test[n - 10:n + 50]
                    if line.find('INFO') < 0:
                        log_test = log_test[n + 50:]
                        continue
                    a = line.find('Run')
                    if a < 0: continue
                    b = line[a:].find(',') + a
                    runr = line[a + 4:b]
                    c = line[10:].find(',') + 10
                    evnr = line[10:c]
                    print 'run number found', str(
                        event['evtNumber']), evnr, line, url
                    if runr != str(event['runNumber']):
                        log_test = log_test[n + 50:]
                        continue
                    c = log_test.find('DstWriter.Output =')
                    a = log_test[c + 10:].find('DstWriter.Output =') + c
                    b = log_test[a:].find('.dst') + a + 4
                    tape = log_test[a:b]
                    print 'event found ', event, 'on tape', tape, ' ', line
                    new_found.append(event)
                    log_test = log_test[n + 50:]
            for n in new_found:
                evtlist.remove(n)

print "+++++++++++++++ final result +++++++++++++++++++++"
print new_found
