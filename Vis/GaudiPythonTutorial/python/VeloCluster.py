###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
MCdata = False
debug = False
mcfiles = []
files = []
from ROOT import TH1F, TCanvas
import os
argn = len(os.sys.argv)
if argn > 1:
    for n in range(1, argn):
        files.append(os.sys.argv[n])
else:
    files = ['$PANORAMIXDATA/Bu2JpsiK_00011917_00000048_1.dst']

# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "2010"
if MCdata: lhcbApp.Simulation = True

appConf = ApplicationMgr(OutputLevel=4, AppName='VeloCluster')
EventSelector().PrintFreq = 1000
from Configurables import UpdateManagerSvc
UpdateManagerSvc().ConditionsOverride += [
    "Conditions/Online/Velo/MotionSystem := double ResolPosLA = 0.; double ResolPosRC =0.;"
]

from Configurables import CondDB, COOLConfSvc


def disableLFC():
    COOLConfSvc(UseLFCReplicaSvc=False)
    CondDB().IgnoreHeartBeat = True


appendPostConfigAction(disableLFC)

import GaudiPython

# lets define Algorithm for event loop


class myAlg(GaudiPython.PyAlgorithm):
    def execute(self):

        vc = evt['Raw/Velo/Clusters']
        for cl in vc:
            sensorNr = cl.channelID().sensor()
            if hlist.has_key(sensorNr):
                h = hlist[sensorNr]
                hl = hlandau[sensorNr]
                hc = hclustsize[sensorNr]
            else:
                sensor = velo.sensor(sensorNr)
                title = (sensor.name()).replace(velo_loc, '')
                name = title.split('/')[2] + sensor.type()
                h = TH1F(name, title, sensor.numberOfStrips(), 0.,
                         sensor.numberOfStrips())
                hl = TH1F('L_' + name, title, 300, 0., 300.)
                hc = TH1F('C_' + name, title, 10, 0., 10.)
                hlist[sensorNr] = h
                hlandau[sensorNr] = hl
                hclustsize[sensorNr] = hc
            for i in range(cl.size()):
                success = h.Fill(cl.strip(i))

            success = hl.Fill(cl.totalCharge())
            success = hc.Fill(cl.size())
        return True


#
#/////////////////////////////////////////////////////////////////////////////
#  Do the execution
#/////////////////////////////////////////////////////////////////////////////

# helpful if you want to reload without re-executing all this
if __name__ == '__main__':

    appMgr = GaudiPython.AppMgr()

    # define location of geometry
    det = appMgr.detsvc()

    # add private algorithm to event loop
    appMgr.addAlgorithm(myAlg())

    velo_loc = '/dd/Structure/LHCb/BeforeMagnetRegion/Velo'
    velo = det[velo_loc]

    # open event input file
    sel = appMgr.evtsel()
    if MCdata: sel.open(mcfiles)
    else: sel.open(files)

    evt = appMgr.evtsvc()

    hlist = {}
    hlandau = {}
    hclustsize = {}

    appMgr.run(2500)

    c1 = TCanvas('c1', 'Velo sensors hitmaps', 1400, 1100)
    c1.Divide(10, 9)

    i = 0
    for h in hlist.values():
        i += 1
        c1.cd(i)
        h.DrawCopy()

    cl = TCanvas('cl', 'Velo sensors Landau', 1400, 1100)
    cl.Divide(10, 9)
    i = 0
    for h in hlandau.values():
        i += 1
        cl.cd(i)
        h.DrawCopy()

    cc = TCanvas('cc', 'Velo sensors clustersizes', 1400, 1100)
    cc.Divide(10, 9)

    i = 0
    for h in hclustsize.values():
        i += 1
        cc.cd(i)
        h.DrawCopy()

    c1.Update()
    cl.Update()
    cc.Update()
    c1.Print('Velo_hitmap.jpg')
    cl.Print('Velo_Landau.jpg')
    cc.Print('Velo_cluster.jpg')

    from ROOT import TFile, gROOT
    f = TFile('VeloCluster.root', 'recreate')
    for h in gROOT.GetList():
        h.Write()
    f.Close()
