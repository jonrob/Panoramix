###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# my private high level configuration
#os.system('setenv PYTHONPATH /afs/cern.ch/sw/lcg/external/processing/0.51/${CMTCONFIG}/lib/python2.5/site-packages:$PYTHONPATH')

dst = True
nevents = 2000
patgeneric = True
multicore = True
usecuts = True
linear = False
hlterror = False

# defining list of histograms
from ROOT import TH1F, TCanvas, TFile, gROOT, TH2F, Double, TMath, TCanvas, gStyle, TF1, TText

# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "DC06"

appConf = ApplicationMgr(OutputLevel=INFO, AppName='HLTandReco')

appConf.ExtSvc += ['TransportSvc']

if dst:
    #FIXES to be included if running over reconstructed data
    from Configurables import TESCheck, EventNodeKiller
    appConf.TopAlg = ['TESCheck/EvtCheck', 'EventNodeKiller/EventNodeKiller1']
    EvtCheck = TESCheck('EvtCheck')
    EvtCheck.Inputs = ["Link/Rec/Track/Best"]
    EventNodeKiller1 = EventNodeKiller('EventNodeKiller1')
    EventNodeKiller1.Nodes = ["Rec", "Link/Rec"]

from Configurables import ProcessPhase

# Reconstruction phase
appConf.TopAlg += ["ProcessPhase/Init"]
# Initialisation phase
Init = ProcessPhase('Init')
Init.DetectorList = ["Etc", "Brunel"]

from Configurables import (
    Tf__PatVeloRTracking, Tf__PatVeloSpaceTool, Tf__PatVeloSpaceTracking,
    Tf__PatVeloGeneralTracking, Tf__PatVeloTrackTool, PatForward,
    Tf__PatVeloRHitManager, Tf__DefaultVeloRHitManager,
    Tf__PatVeloPhiHitManager, Tf__PatVeloGeneralTracking, PatForwardTool,
    PatFwdTool)

## Velo tracking
patVeloRTracking = Tf__PatVeloRTracking("PatVeloRTracking")
patVeloRTracking.OutputLevel = 3
patVeloRTracking.HitManagerName = 'defVeloRHitManager'

patVeloSpaceTracking = Tf__PatVeloSpaceTracking("PatVeloSpaceTracking")
patVeloSpaceTracking.OutputLevel = 3
# this is a private tool
patVeloSpaceTracking.addTool(Tf__PatVeloSpaceTool, 'VeloSpaceTool')
patVeloSpaceTracking.SpaceToolName = 'VeloSpaceTool'
patVeloSpaceTracking.VeloSpaceTool.MarkClustersUsed = True

# these are public tools used by the VeloSpaceTool
patVeloSpaceTracking.VeloSpaceTool.TrackToolName = 'VeloTrackTool'
patVeloSpaceTracking.VeloSpaceTool.RHitManagerName = 'VeloRHitManager'
patVeloSpaceTracking.VeloSpaceTool.PhiHitManagerName = 'VeloPhiHitManager'

patVeloTrackTool = Tf__PatVeloTrackTool('VeloTrackTool')
patVeloTrackTool.RHitManagerName = 'VeloRHitManager'
patVeloTrackTool.PhiHitManagerName = 'VeloPhiHitManager'

veloGeneral = Tf__PatVeloGeneralTracking("PatVeloGeneralTracking")
# tools already created before
veloGeneral.TrackToolName = 'VeloTrackTool'
veloGeneral.RHitManagerName = 'VeloRHitManager'
veloGeneral.PhiHitManagerName = 'VeloPhiHitManager'

#importOptions( "$STTOOLSROOT/options/Brunel.opts" )
from STTools import STOfflineConf
STOfflineConf.DefaultConf().configureTools()

#// proposed new tune
##ToolSvc.ITClusterPosition.ErrorVec ={ 0.22, 0.11, 0.24, 0.2};

#importOptions("$PATALGORITHMSROOT/options/PatForward.py")
#from PatAlgorithms import PatAlgConf
#PatAlgConf.ForwardConf().configureAlg()

patForward = PatForward("PatForward")
patForward.addTool(PatForwardTool("PatForwardTool"))
patForward.PatForwardTool.AddTTClusterName = ""
#patForward.PatForwardTool.AddTTClusterName = "PatAddTTCoord"

from Configurables import TrackAssociator
ta_forward = TrackAssociator('Forward')
ta_forward.TracksInContainer = 'Rec/Track/Forward'

from TrackSys.Configuration import *
from Configurables import (TrackEventFitter, TrackMasterFitter,
                           TrackMasterExtrapolator, TrackKalmanFilter,
                           TrackProjectorSelector, TrajOTProjector)

# Forward fit
TrackEventFitter("FitForward").TracksInContainer = "Rec/Track/Forward"
TrackEventFitter("FitForward").addTool(TrackMasterFitter, name="Fitter")
TrackEventFitter("FitForward").Fitter.NumberFitIterations = 2
TrackEventFitter("FitForward").Fitter.MakeNodes = False
TrackEventFitter("FitForward").Fitter.addTool(
    TrackMasterExtrapolator, name="Extrapolator")
TrackEventFitter("FitForward").Fitter.addTool(
    TrackKalmanFilter, name="NodeFitter")
TrackEventFitter("FitForward").Fitter.NodeFitter.addTool(
    TrackMasterExtrapolator, name="Extrapolator")
TrackEventFitter(
    "FitForward").Fitter.MaterialLocator = "DetailedMaterialLocator"
TrackEventFitter(
    "FitForward"
).Fitter.Extrapolator.MaterialLocator = "DetailedMaterialLocator"
TrackEventFitter(
    "FitForward"
).Fitter.NodeFitter.Extrapolator.MaterialLocator = "DetailedMaterialLocator"
TrackEventFitter("PreFitForward").TracksInContainer = "Rec/Track/Forward"
TrackEventFitter("PreFitForward").addTool(TrackMasterFitter, name="Fitter")
TrackEventFitter("PreFitForward").Fitter.NumberFitIterations = 2
TrackEventFitter("PreFitForward").Fitter.MaxNumberOutliers = 0
TrackEventFitter("PreFitForward").Fitter.ErrorX2 = 40.0
TrackEventFitter("PreFitForward").Fitter.ErrorTx2 = 1e-3
TrackEventFitter("PreFitForward").Fitter.ErrorTy2 = 2e-3
TrackEventFitter("PreFitForward").Fitter.addTool(
    TrackKalmanFilter, name="NodeFitter")
TrackEventFitter("PreFitForward").Fitter.NodeFitter.addTool(
    TrackProjectorSelector, name="Projector")
TrackEventFitter(
    "PreFitForward"
).Fitter.NodeFitter.Projector.OT = "TrajOTProjector/OTNoDrifttimesProjector"
TrajOTProjector("OTNoDrifttimesProjector").UseDrift = False
TrackEventFitter("PreFitForward").Fitter.addTool(
    TrackMasterExtrapolator, name="Extrapolator")
TrackEventFitter("PreFitForward").Fitter.NodeFitter.addTool(
    TrackMasterExtrapolator, name="Extrapolator")
TrackEventFitter(
    "PreFitForward").Fitter.MaterialLocator = "DetailedMaterialLocator"
TrackEventFitter(
    "PreFitForward"
).Fitter.Extrapolator.MaterialLocator = "DetailedMaterialLocator"
TrackEventFitter(
    "PreFitForward"
).Fitter.NodeFitter.Extrapolator.MaterialLocator = "DetailedMaterialLocator"

Fit = GaudiSequencer("Fit")
Fit.Members += [
    TrackEventFitter("PreFitForward"),
    TrackEventFitter("FitForward")
]

reco = GaudiSequencer("Reco")
if patgeneric:
    reco.Members += [
        patVeloRTracking, patVeloSpaceTracking, veloGeneral, patForward, Fit,
        ta_forward
    ]
else:
    reco.Members += [
        patVeloRTracking, patVeloSpaceTracking, patForward, Fit, ta_forward
    ]
appConf.TopAlg += [reco]
patForward.OutputLevel = 3

### OFFLINE FINISHED
HistogramPersistencySvc.OutputFile = "HLTandOffline.root"
### START HLT CONFIGURATION
from Configurables import HltInsertTrackErrParam
#importOptions('$HLTSYSROOT/options/Hlt2.opts')
HltRecoRZVelo = Tf__PatVeloRTracking("HltRecoRZVelo")
HltRecoRZVelo.HitManagerName = 'HltdefVeloRHitManager'

HltRecoVelo = Tf__PatVeloSpaceTracking('HltRecoVelo')
HltRecoVelo.addTool(Tf__PatVeloSpaceTool, 'HltVeloSpaceTool')
HltRecoVelo.SpaceToolName = 'HltVeloSpaceTool'

HltVeloTrackTool = Tf__PatVeloTrackTool('HltVeloTrackTool')
HltRecoVelo.HltVeloSpaceTool.TrackToolName = 'HltVeloTrackTool'
HltRecoVelo.HltVeloSpaceTool.MarkClustersUsed = True
HltRecoVelo.HltVeloSpaceTool.RHitManagerName = 'HltVeloRHitManager'
HltRecoVelo.HltVeloSpaceTool.PhiHitManagerName = 'HltVeloPhiHitManager'

HltVeloTrackTool = Tf__PatVeloTrackTool('HltVeloTrackTool')
HltVeloTrackTool.RHitManagerName = 'HltVeloRHitManager'
HltVeloTrackTool.PhiHitManagerName = 'HltVeloPhiHitManager'

HltRecoVelo.OutputLevel = 3
HltRecoRZVelo.OutputLevel = 3

HltRecoForward = PatForward("HltForward")
HltRecoRZVelo.OutputTracksName = "Hlt/Track/RZVelo"

HltRecoVelo.InputTracksName = "Hlt/Track/RZVelo"
HltRecoVelo.OutputTracksName = "Hlt/Track/Velo"
HltRecoVelo.HltVeloSpaceTool.OutputLevel = 4

HltRecoForward.InputTracksName = "Hlt/Track/Velo"
HltRecoForward.OutputTracksName = "Hlt/Track/Forward"

ta_hltforward = TrackAssociator('Hlt_Forward')
ta_hltforward.TracksInContainer = 'Hlt/Track/Forward'

HltTrackReco = GaudiSequencer("HltTrackReco")
HltInsertTrackErrParam = HltInsertTrackErrParam('HltInsertTrackErrParam')
HltInsertTrackErrParam.InputLocation = "Hlt/Track/Forward"
HltInsertTrackErrParam.OutputLevel = 3
if hlterror:
    HltTrackReco.Members += [
        HltRecoRZVelo, HltRecoVelo, HltRecoForward, HltInsertTrackErrParam,
        ta_hltforward
    ]
else:
    HltTrackReco.Members += [
        HltRecoRZVelo, HltRecoVelo, HltRecoForward, ta_hltforward
    ]

appConf.TopAlg += [HltTrackReco]
EventSelector().PrintFreq = 50

import GaudiPython
import GaudiKernel.SystemOfUnits as units
from gaudigadgets import *

MCParticle = GaudiPython.gbl.LHCb.MCParticle
Track = GaudiPython.gbl.LHCb.Track
longTrack = Track().Long


def n_true(cont, type=None):
    n = 0
    ltrack2part = linkedTo(MCParticle, Track, cont)
    for t in evt[cont]:
        if type and t.type() != type: continue
        if ltrack2part.range(t).size() > 0: n += 1
    return float(n)


contoff = 'Rec/Track/Forward'
conthlt = 'Hlt/Track/Forward'

import LinkerInstances.eventassoc as eventassoc

begEv = GaudiPython.gbl.Incident('BeginEvent', 'BeginEvent')

from processing import Pool
from pickleROOT import *

h_forw = TH1F('h_forw', 'nr tracks MC truth  as function of pt  offline', 100,
              0., 5.)
h_hforw = TH1F('h_hforw', 'nr tracks MC truth as function of pt  online', 100,
               0., 5.)
h_match = TH1F('h_match', 'nr of matched MCParticles offline', 5, -0.5, 4.5)
h_hmatch = TH1F('h_hmatch', 'nr of matched MCParticles online', 5, -0.5, 4.5)
#
Off = {}
Off['h_IP'] = TH2F('h_IP', ' IP for long tracks vs. 1/pt', 50, 0., 10., 100,
                   -0.5, 0.5)
Off['h_IPx'] = TH2F('h_IPx', ' IPx for long tracks vs. 1/pt', 50, 0., 10., 100,
                    -0.5, 0.5)
Off['h_IPy'] = TH2F('h_IPy', ' IPy for long tracks vs. 1/pt', 50, 0., 10., 100,
                    -0.5, 0.5)
Off['h_IPz'] = TH2F('h_IPz', ' IPz for long tracks vs. 1/pt', 50, 0., 10., 100,
                    -0.5, 0.5)
Off['h_P'] = TH2F('h_P', ' delp/p for long tracks vs. p', 25, 0., 150., 100,
                  -0.04, 0.04)
Off['h_sx'] = TH2F('h_sx', ' res slope x for long tracks vs. p', 25, 0., 150.,
                   100, -0.01, 0.01)
Off['h_sy'] = TH2F('h_sy', ' res slope y for long tracks vs. p', 25, 0., 150.,
                   100, -0.01, 0.01)
Off['p_P'] = TH2F('p_P', ' pull delp/p for long tracks vs. p', 25, 0., 150.,
                  100, -5.0, 5.0)
Off['p_IPx'] = TH2F('p_IPx', ' pull delx for long tracks vs. pt', 50, 0., 10.,
                    100, -5.0, 5.0)
Off['p_IPy'] = TH2F('p_IPy', ' pull dely for long tracks vs. pt', 50, 0., 10.,
                    100, -5.0, 5.0)
Off['p_sx'] = TH2F('p_sx', 'pull res slope x for long tracks vs. p', 25, 0.,
                   150., 100, -5.0, 5.0)
Off['p_sy'] = TH2F('p_sy', 'pull res slope y for long tracks vs. p', 25, 0.,
                   150., 100, -5.0, 5.0)
Off['h_pmc'] = TH2F('h_pmc', 'p mc vs p rec ', 25, 0., 150., 25, 0., 150.)
#
Hlt = {}
Hlt['h_IP'] = TH2F('h_Hlt_IP', ' IP for long tracks vs. 1/pt', 50, 0., 10.,
                   100, -0.5, 0.5)
Hlt['h_IPx'] = TH2F('h_Hlt_IPx', ' IPx for long tracks vs. 1/pt', 50, 0., 10.,
                    100, -0.5, 0.5)
Hlt['h_IPy'] = TH2F('h_Hlt_IPy', ' IPy for long tracks vs. 1/pt', 50, 0., 10.,
                    100, -0.5, 0.5)
Hlt['h_IPz'] = TH2F('h_Hlt_IPz', ' IPz for long tracks vs. 1/pt', 50, 0., 10.,
                    100, -0.5, 0.5)
Hlt['h_P'] = TH2F('h_Hlt_P', ' delp/p for long tracks vs. p', 25, 0., 150.,
                  100, -0.04, 0.04)
Hlt['h_sx'] = TH2F('h_Hlt_sx', ' res slope x for long tracks vs. p', 25, 0.,
                   150., 100, -0.01, 0.01)
Hlt['h_sy'] = TH2F('h_Hlt_sy', ' res slope y for long tracks vs. p', 25, 0.,
                   150., 100, -0.01, 0.01)
Hlt['p_P'] = TH2F('p_Hlt_P', ' pull delp/p for long tracks vs. p', 25, 0.,
                  150., 100, -5.0, 5.0)
Hlt['p_IPx'] = TH2F('p_Hlt_IPx', ' pull delx for long tracks vs. pt', 50, 0.,
                    10., 100, -5.0, 5.0)
Hlt['p_IPy'] = TH2F('p_Hlt_IPy', ' pull dely for long tracks vs. pt', 50, 0.,
                    10., 100, -5.0, 5.0)
Hlt['p_sx'] = TH2F('p_Hlt_sx', 'pull res slope x for long tracks vs. p', 25,
                   0., 150., 100, -5.0, 5.0)
Hlt['p_sy'] = TH2F('p_Hlt_sy', 'pull res slope y for long tracks vs. p', 25,
                   0., 150., 100, -5.0, 5.0)
Hlt['h_pmc'] = TH2F('h_Hlt_pmc', 'p mc vs p rec ', 25, 0., 150., 25, 0., 150.)
#
Hlt['h_diffHlt_IP'] = TH2F('h_diffHlt_IP', ' IP for long tracks vs. 1/pt', 50,
                           0., 10., 100, -0.5, 0.5)
Hlt['h_diffHlt_IPx'] = TH2F('h_diffHlt_IPx', ' IPx for long tracks vs. 1/pt',
                            50, 0., 10., 100, -0.5, 0.5)
Hlt['h_Hlt_IPxcor'] = TH2F(
    'h_Hlt_IPxcor', ' IPx for long tracks vs. 1/pt, B field corrected ', 50,
    0., 10., 100, -0.5, 0.5)
Hlt['h_diffHlt_IPxq'] = TH2F('h_diffHlt_IPxq',
                             ' IPx for long tracks vs. 1/pt * charge', 50, 0.,
                             10., 100, -0.5, 0.5)
Hlt['h_diffHlt_IPy'] = TH2F('h_diffHlt_IPy', ' IPy for long tracks vs. 1/pt',
                            50, 0., 10., 100, -0.5, 0.5)
Hlt['h_diffHlt_P'] = TH2F('h_diffHlt_P', ' delp/p for long tracks vs. p', 25,
                          0., 150., 100, -0.04, 0.04)
Hlt['h_diffHlt_sx'] = TH2F('h_diffHlt_sx',
                           ' res slope x for long tracks vs. p', 25, 0., 150.,
                           100, -0.5, 0.5)
Hlt['h_diffHlt_sy'] = TH2F('h_diffHlt_sy',
                           ' res slope y for long tracks vs. p', 25, 0., 150.,
                           100, -0.5, 0.5)
Hlt['h_param_IPx'] = TH2F('h_param_IPx',
                          ' delta IPx/mean z for Hlt2 tracks vs. pz ', 50, 0.,
                          10., 100, -0.001, 0.001)
Hlt['h_param_IPxt'] = TH2F('h_param_IPxt',
                           ' delta IPx/mean z for Hlt2 tracks vs. pt ', 50, 0.,
                           1., 100, -0.001, 0.001)
Hlt['h_param_IPxy'] = TH2F('h_param_IPxy',
                           ' delta IPx/mean z for Hlt2 tracks vs. py ', 50, 0.,
                           1., 100, -0.001, 0.001)
#

LineTraj = GaudiPython.gbl.LHCb.LineTraj
State = GaudiPython.gbl.LHCb.State
MCParticle = GaudiPython.gbl.LHCb.MCParticle
Track = GaudiPython.gbl.LHCb.Track
XYZPoint = LHCbMath.XYZPoint
XYZVector = LHCbMath.XYZVector
Range = GaudiPython.gbl.std.pair('double', 'double')
valid = Range(-1000., 1000.)
g = TF1('g', 'gaus')


def fill_histo(mcp, t, hist, extrap, poca):
    ovx = mcp.originVertex().position()
    one_over_pt = 1. / mcp.pt() * units.GeV
    p = mcp.p()
    # extrapolate to mc origin vertex
    astate = t.firstState().clone()
    result = extrap.propagate(astate, ovx.z())
    apoint = astate.position()
    adirec = astate.slopes()
    traj = LineTraj(apoint, adirec, valid)
    dis = XYZVector()
    s = Double(0.1)
    a = Double(0.0005)
    success = poca.minimize(traj, s, ovx, dis, a)
    if success.isFailure() > 0: print 'poca failure'
    ip = dis.r()
    if dis.z() < 0:
        ip = -ip
    p_ontrack = traj.position(s)
    ipx = p_ontrack.x() - ovx.x()
    ipy = p_ontrack.y() - ovx.y()
    ipz = p_ontrack.z() - ovx.z()
    delp = (t.p() - p) / p
    mom = mcp.momentum()
    delsx = (astate.tx() - mom.x() / mom.z())
    delsy = (astate.ty() - mom.y() / mom.z())
    #calculate average extrap distance
    charge = mcp.particleID().threeCharge() / 3.
    deltaz = (t.states()[1].z() - t.firstState().z()) / charge
    ###no electrons
    cut = mcp.particleID().abspid() == 11
    ###only take tracks close to beamline
    cut = cut and ovx.rho() > 1.
    tuple = {
        'mcP': p,
        'mcPt': mcp.pt(),
        'pid': mcp.particleID().abspid(),
        'IP': abs(ip),
        'IPx': ipx,
        'IPy': ipy,
        'delsx': delsx,
        'delsy': delsy,
        'P': t.p(),
        'cut': cut,
        'deltaz': deltaz
    }
    #### Fill histograms
    if not cut or not usecuts:
        hist['h_IP'].Fill(one_over_pt, ip)
        hist['h_IPx'].Fill(one_over_pt, ipx)
        hist['h_IPy'].Fill(one_over_pt, ipy)
        hist['h_IPz'].Fill(one_over_pt, ipz)
        hist['h_P'].Fill(p / units.GeV, delp)
        hist['h_pmc'].Fill(t.p() / units.GeV, p / units.GeV)
        hist['h_sx'].Fill(p / units.GeV, delsx)
        hist['h_sy'].Fill(p / units.GeV, delsy)
        # pull plots
        hist['p_P'].Fill(p / units.GeV,
                         delp / (TMath.Sqrt(astate.errQOverP2()) * t.p()))
        hist['p_IPx'].Fill(one_over_pt, ipx / TMath.Sqrt(astate.errX2()))
        hist['p_IPy'].Fill(one_over_pt, ipy / TMath.Sqrt(astate.errY2()))
        hist['p_sx'].Fill(p / units.GeV, delsx / TMath.Sqrt(astate.errTx2()))
        hist['p_sy'].Fill(p / units.GeV, delsy / TMath.Sqrt(astate.errTy2()))
    return tuple


def processFile(file):
    appMgr = GaudiPython.AppMgr()
    poca = appMgr.toolsvc().create('TrajPoca', interface='ITrajPoca')
    if not linear:
        ###  extrap = appMgr.toolsvc().create('TrackMasterExtrapolator', interface='ITrackExtrapolator')
        extrap = appMgr.toolsvc().create(
            'TrackParabolicExtrapolator', interface='ITrackExtrapolator')
    else:
        extrap = appMgr.toolsvc().create(
            'TrackLinearExtrapolator', interface='ITrackExtrapolator')
    sel = appMgr.evtsel()
    sel.open([file])
    evt = appMgr.evtsvc()
    appMgr.start()
    appMgr.algorithm('HltTrackReco').Enable = False
    appMgr.algorithm('Reco').Enable = False
    incSvc = appMgr.service('IncidentSvc', 'IIncidentSvc')
    for n in range(nevents):
        appMgr.run(1)
        if not evt['DAQ/RawEvent']: break
        appMgr.algorithm('Reco').execute()
        nRZ = evt['Rec/Track/RZVelo'].size()
        n3d = evt['Rec/Track/Velo'].size()
        nFor = evt['Rec/Track/Forward'].size()
        lrec2part = eventassoc.linkedTo(MCParticle, Track, 'Rec/Track/Forward')
        nForTrue = 0
        matchList = {}
        for t in evt['Rec/Track/Forward']:
            nmatch = lrec2part.range(t).size()
            h_match.Fill(nmatch)
            if nmatch > 0:
                new = False
                for mcp in lrec2part.range(t):
                    if not matchList.has_key(mcp.key()):
                        new = True
                        nForTrue += 1
                        mom = mcp.momentum()
                        h_forw.Fill(mom.pt() / units.GeV)
                        break
                if new:
                    matchList[mcp.key()] = fill_histo(mcp, t, Off, extrap,
                                                      poca)
# start HLT sequence
        incSvc.fireIncident(begEv)
        appMgr.algorithm('HltTrackReco').execute()
        HnRZ = evt['Hlt/Track/RZVelo'].size()
        Hn3d = evt['Hlt/Track/Velo'].size()
        HnFor = evt['Hlt/Track/Forward'].size()
        lhlt2part = eventassoc.linkedTo(MCParticle, Track, 'Hlt/Track/Forward')
        HnForTrue = 0
        HmatchList = {}
        for t in evt['Hlt/Track/Forward']:
            nmatch = lhlt2part.range(t).size()
            h_hmatch.Fill(nmatch)
            if nmatch > 0:
                for mcp in lhlt2part.range(t):
                    new = False
                    if not HmatchList.has_key(mcp.key()):
                        HnForTrue += 1
                        HmatchList[mcp.key()] = True
                        mom = mcp.momentum()
                        h_hforw.Fill(mom.pt() / units.GeV)
                        new = True
                        break
                if new:
                    hlt_tuple = fill_histo(mcp, t, Hlt, extrap, poca)
                    # compare HLT track with Offline track
                    if matchList.has_key(mcp.key()):
                        diff_ip = matchList[mcp.key()]['IP'] - hlt_tuple['IP']
                        diff_ipx = matchList[mcp.
                                             key()]['IPx'] - hlt_tuple['IPx']
                        diff_ipy = matchList[mcp.
                                             key()]['IPy'] - hlt_tuple['IPy']
                        diff_P = 0.5 * (
                            matchList[mcp.key()]['P'] - hlt_tuple['P']) / (
                                matchList[mcp.key()]['P'] + hlt_tuple['P'])
                        diff_sx = 0.5 * (matchList[mcp.key(
                        )]['delsx'] - hlt_tuple['delsx']) / (
                            matchList[mcp.key()]['delsx'] + hlt_tuple['delsx'])
                        diff_sy = 0.5 * (matchList[mcp.key(
                        )]['delsy'] - hlt_tuple['delsy']) / (
                            matchList[mcp.key()]['delsy'] + hlt_tuple['delsy'])
                        deltaz = hlt_tuple['deltaz']
                        pz = mcp.momentum().z() / units.GeV
                        one_over_pt = 1. / mom.pt() * units.GeV
                        p = mcp.p() / units.GeV
                        charge = mcp.particleID().threeCharge() / 3.
                        if not matchList[mcp.key()]['cut'] or not usecuts:
                            Hlt['h_diffHlt_IP'].Fill(one_over_pt, diff_ip)
                            Hlt['h_diffHlt_IPx'].Fill(one_over_pt, diff_ipx)
                            Hlt['h_Hlt_IPxcor'].Fill(
                                one_over_pt, hlt_tuple['IPx'] +
                                0.015 * one_over_pt * charge)
                            Hlt['h_diffHlt_IPxq'].Fill(one_over_pt,
                                                       diff_ipx * charge)
                            Hlt['h_diffHlt_IPy'].Fill(one_over_pt, diff_ipy)
                            Hlt['h_diffHlt_P'].Fill(p, diff_P)
                            Hlt['h_diffHlt_sx'].Fill(p, diff_sx)
                            Hlt['h_diffHlt_sy'].Fill(p, diff_sy)
                            Hlt['h_param_IPx'].Fill(pz,
                                                    hlt_tuple['IPx'] / deltaz)
                            Hlt['h_param_IPxt'].Fill(1. / one_over_pt,
                                                     hlt_tuple['IPx'] / deltaz)
                            Hlt['h_param_IPxy'].Fill(
                                mcp.momentum().y() / units.GeV,
                                hlt_tuple['IPx'] / deltaz)

#    print 'diff HLT VeloRZ   : ',HnRZ-nRZ, HnRZ,nRZ
#    print 'diff HTL Velo     : ',Hn3d-n3d, Hn3d,n3d
#    print 'diff HLT Forward  : ',HnFor-nFor,HnFor,nFor
#    print 'diff HLT Forward True : ',HnForTrue-nForTrue,HnForTrue,nForTrue
# collect all histograms
    hlist = []
    for h in gROOT.GetList():
        hlist.append(h)
    return hlist


###########################################################################
def myFitSliceY(h):
    N = h.GetNbinsX()
    name = h.GetName()
    t = TCanvas('t_' + name, h.GetTitle(), 1600, 1200)
    #
    myprof = TH1F()
    h.ProjectionX().Copy(myprof)
    myprof.SetName(name + '_myprof')
    myprof.SetTitle('sigma, ' + h.GetTitle())
    #
    nw = int(TMath.Sqrt(N) + 0.5)
    nh = int(N / nw)
    while nh * nw < N:
        nh += 1
    t.Divide(nw, nh)
    for n in range(1, N + 1):
        test = h.ProjectionY(name + '_' + str(n), n, n)
        test.SetTitle(h.GetTitle())
        if test.GetEntries() > 50:
            t.cd(n)
            test.Fit(g)
            test.DrawCopy()
            ypos = test.GetMaximum() * 0.1
            sigma = g.GetParameter(2)
            error = g.GetParError(2)
            txt = 'Sigma=' + '%4.2f' % (sigma)
            tx = TText(-0.04, ypos, txt)
            tx.DrawText(-0.04, ypos, txt)
            myprof.SetBinContent(n, sigma)
            myprof.SetBinError(n, error)
    return t, myprof


def print_sigma(h):
    g = TF1('g', 'gaus')
    h.Fit(g)
    h.DrawCopy()
    fun = h.GetFunction('g')
    a0 = fun.GetParameter(1)
    a1 = fun.GetParameter(2)
    ea0 = fun.GetParError(1)
    ea1 = fun.GetParError(2)
    if a0 < 0.01:
        a0 = a0 * 1000.
        ea0 = ea0 * 1000.
        txt = 'Mean=' + '(%4.2f' % (a0) + '+/-' + '%4.2f' % (ea0) + ')*10E-3'
    else:
        txt = 'Mean=' + '%4.2f' % (a0) + '+/-' + '%4.2f' % (ea0)
    height = h.GetMaximum() * 0.94
    posx = h.GetBinLowEdge(2)
    tx = TText(posx, height, txt)
    tx.DrawText(posx, height, txt)
    if a1 < 0.01:
        a1 = a1 * 1000.
        ea1 = ea1 * 1000.
        txt = 'Sigma=' + '(%4.2f' % (a1) + '+/-' + '%4.2f' % (ea1) + ')*10E-3'
    else:
        txt = 'Sigma=' + '%4.2f' % (a1) + '+/-' + '%4.2f' % (ea1)
    height = h.GetMaximum() * 0.86
    tx = TText(posx, height, txt)
    tx.DrawText(posx, height, txt)


def do_analysis(hists, tag):
    g = TF1('g', 'gaus')
    c1 = TCanvas('c1', '', 750, 500)
    tcp = TCanvas('tcp', 'momentum resolution', 750, 500)
    tcp.Divide(1, 1)
    tcp.cd(1)
    gStyle.SetOptFit(111)
    h = 'h_' + tag + 'P'
    hists[h].FitSlicesY(g)
    h_P_prof = gROOT.FindObjectAny(hists[h].GetName() + '_2')
    h_P_prof.SetMaximum(0.015)
    h_P_prof.SetMinimum(0.0)
    h_P_prof.SetTitle('Momentum resolution as function of p')
    h_P_prof.Draw()
    h_P_prof.Fit('pol1')
    tcp.Print(tag + 'MomentumResolution.jpg')
    names = ['IPx', 'IPy', 'IPz']
    proj = {}
    for a in names:
        h = 'h_' + tag + a
        tip, proj[a] = myFitSliceY(hists[h])
        gROOT.FindObjectAny('c1').cd()
        proj[a].Draw()
        proj[a].Fit('pol1')
        fun = gROOT.FindObjectAny('pol1')
        a0 = fun.GetParameter(0) * 1000.
        a1 = fun.GetParameter(1) * 1000.
        txt = 'Sigma=' + '%4.1f' % (a0) + '+' + '%4.1f' % (a1) + '/pt'
        tx = TText(3., 0.05, txt)
        tx.DrawText(3., 0.05, txt)
        gROOT.FindObjectAny('c1').Print(tag + a + 'resolution.jpg')
        hists[h].Draw()
        gROOT.FindObjectAny('c1').Print(tag + a + '_2d.jpg')
    h_IPxy_myprof = TH1F()
    proj['IPx'].Copy(h_IPxy_myprof)
    h_IPxy_myprof.SetName('h_IPxy_myprof')
    h_IPxy_myprof.SetTitle('sigma IP as function of 1/pt')
    for n in range(2, proj['IPx'].GetNbinsX() + 1):
        sx = proj['IPx'].GetBinContent(n)
        sy = proj['IPy'].GetBinContent(n)
        sz = proj['IPz'].GetBinContent(n)
        sigma = TMath.Sqrt(sx * sx + sy * sy + sz * sz)
        h_IPxy_myprof.SetBinContent(n, sigma)
        ex = proj['IPx'].GetBinError(n)
        ey = proj['IPy'].GetBinError(n)
        ez = proj['IPz'].GetBinError(n)
        error = TMath.Sqrt(sx * sx * ex * ex + sy * sy * ey * ey +
                           sz * sz * ez * ez) / sigma
        h_IPxy_myprof.SetBinError(n, error)
    gStyle.SetOptStat(0)
    gROOT.FindObjectAny('c1').cd()
    h_IPxy_myprof.SetMinimum(0.)
    h_IPxy_myprof.SetMaximum(0.4)
    h_IPxy_myprof.Fit('pol1')
    fun = gROOT.FindObjectAny('pol1')
    a0 = fun.GetParameter(0) * 1000.
    a1 = fun.GetParameter(1) * 1000.
    txt = 'Sigma=' + '%4.1f' % (a0) + '+' + '%4.1f' % (a1) + '/pt'
    tx = TText(3.0, 0.05, txt)
    tx.DrawText(3.0, 0.05, txt)
    gROOT.FindObjectAny('c1').Print(tag + 'IPresolution.jpg')
    h = 'h_' + tag + 'P'
    tp, h_p_myprof = myFitSliceY(hists[h])
    gStyle.SetOptFit(0)
    tpull = TCanvas('tpull', 'pull distributions', 1200, 800)
    tpull.Divide(3, 2)
    tpull.cd(1)
    h = hists['p_' + tag + 'IPx'].ProjectionY()
    h.SetTitle('Pull distribution IPx')
    print_sigma(h)
    tpull.cd(2)
    h = hists['p_' + tag + 'IPy'].ProjectionY()
    h.SetTitle('Pull distribution IPy')
    print_sigma(h)
    tpull.cd(4)
    h = hists['p_' + tag + 'sx'].ProjectionY()
    h.SetTitle('Pull distribution slope x')
    print_sigma(h)
    tpull.cd(5)
    h = hists['p_' + tag + 'sy'].ProjectionY()
    h.SetTitle('Pull distribution slope y')
    print_sigma(h)
    tpull.cd(6)
    h = hists['p_' + tag + 'P'].ProjectionY()
    h.SetTitle('Pull distribution p')
    print_sigma(h)
    tpull.Print(tag + 'pulls.jpg')
    #
    tres = TCanvas('tres', 'resolutions', 1200, 800)
    tres.Divide(3, 2)
    tres.cd(1)
    h = hists['h_' + tag + 'IPx'].ProjectionY()
    h.SetTitle('resolution IPx')
    print_sigma(h)
    tres.cd(2)
    h = hists['h_' + tag + 'IPy'].ProjectionY()
    h.SetTitle('resolution IPy')
    print_sigma(h)
    tres.cd(4)
    h = hists['h_' + tag + 'sx'].ProjectionY()
    h.SetTitle('resolution slope x')
    print_sigma(h)
    tres.cd(5)
    h = hists['h_' + tag + 'sy'].ProjectionY()
    h.SetTitle('resolution slope y')
    print_sigma(h)
    tres.cd(6)
    h = hists['h_' + tag + 'P'].ProjectionY()
    h.SetTitle('resolution p')
    print_sigma(h)
    tres.Print(tag + 'resolution.jpg')
    cdi = TCanvas('cdi', '', 750, 500)
    cdi.Divide(1, 1)


def do_analysis2(hists, tag):
    tres = TCanvas('tdiff', 'online - offline', 1200, 800)
    tres.Divide(3, 2)
    tres.cd(1)
    h = hists['h_diffHlt_IPx'].ProjectionY()
    h.SetTitle('difference IPx')
    print_sigma(h)
    tres.cd(2)
    h = hists['h_diffHlt_IPy'].ProjectionY()
    h.SetTitle('difference IPy')
    print_sigma(h)
    tres.cd(4)
    h = hists['h_diffHlt_sx'].ProjectionY()
    h.SetTitle('difference slope x')
    print_sigma(h)
    tres.cd(5)
    h = hists['h_diffHlt_sy'].ProjectionY()
    h.SetTitle('difference slope y')
    print_sigma(h)
    tres.cd(6)
    h = hists['h_diffHlt_P'].ProjectionY()
    h.SetTitle('difference p')
    print_sigma(h)
    tres.Print(tag + 'difference.jpg')
    c1 = TCanvas('c1', '', 750, 500)
    c1.cd(1)
    names = ['h_diffHlt_IPx', 'h_diffHlt_IPy', 'h_Hlt_IPxcor']
    proj = {}
    for h in names:
        hists[h].Draw()
        c1.Print(h + '_2d.jpg')
        tip, proj[h] = myFitSliceY(hists[h])
        tip.Print(h + '_slices.jpg')
        c1.cd()
        proj[h].Draw()
        proj[h].Fit('pol1')
        fun = gROOT.FindObjectAny('pol1')
        a0 = fun.GetParameter(0) * 1000.
        a1 = fun.GetParameter(1) * 1000.
        txt = 'Sigma=' + '%4.1f' % (a0) + '+' + '%4.1f' % (a1) + '/pt'
        tx = TText(0.25, 0.1, txt)
        tx.DrawText(0.25, 0.1, txt)
        c1.Print(h + '_fit.jpg')


if __name__ == '__main__':
    files = [
        'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/v1r0/00002042/DST/0000/00002042_00000001_2.dst',
        'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/v1r0/00002042/DST/0000/00002042_00000003_2.dst',
        'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/v1r0/00002042/DST/0000/00002042_00000005_2.dst',
        'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/v1r0/00002042/DST/0000/00002042_00000006_2.dst',
        'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/v1r0/00002042/DST/0000/00002042_00000008_2.dst',
        'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/v1r0/00002042/DST/0000/00002042_00000009_2.dst',
        'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/v1r0/00002042/DST/0000/00002042_00000014_2.dst',
        'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/v1r0/00002042/DST/0000/00002042_00000015_2.dst',
        'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/v1r0/00002042/DST/0000/00002042_00000016_2.dst',
        'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/v1r0/00002042/DST/0000/00002042_00000017_2.dst'
    ]

    myHists = {}
    if multicore:
        #------Use up to 8 processes
        n = 8
        pool = Pool(n)
        result = pool.map_async(processFile, files)
        for hlist in result.get(timeout=10000):
            for x in hlist:
                if not myHists.has_key(x.GetName()):
                    myHists[x.GetName()] = x
                else:
                    myHists[x.GetName()].Add(x)
    else:
        hlist = processFile(files[0])
        for x in hlist:
            myHists[x.GetName()] = x

    myHists['h_hforw'].Sumw2()
    myHists['h_forw'].Sumw2()
    hdiv = h_hforw.Clone()
    hdiv.Divide(myHists['h_hforw'], myHists['h_forw'], 1., 1., 'B')
    hdiv.SetMaximum(1.05)
    hdiv.SetMinimum(0.95)

    if patgeneric:
        f = TFile('HltandOffline_gen.root', 'recreate')
    else:
        f = TFile('HltandOffline.root', 'recreate')
    for h in myHists:
        myHists[h].Write()
    f.Close()
    do_analysis(myHists, '')
    do_analysis(myHists, 'Hlt_')
    do_analysis2(myHists, 'Hlt_')
