###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# setenv PYTHONPATH /afs/cern.ch/sw/lcg/external/processing/0.51/${CMTCONFIG}/lib/python2.5/site-packages:$PYTHONPATH
from ROOT import Double, TFile, TH1F, TH2F, TCanvas, TBrowser, gStyle, TText, TMath, TF1, gROOT, MakeNullPointer, gSystem
import time, math
nmax = 1500
debug = False

# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "2008"
lhcbApp.Simulation = False

appConf = ApplicationMgr(OutputLevel=INFO, AppName='rDST')
EventSelector().PrintFreq = 10000

import GaudiPython
import GaudiKernel.SystemOfUnits as units
GaudiPython

gbl = GaudiPython.gbl
xx = gbl.LHCb.Particle
LorentzVector = gbl.Gaudi.LorentzVector
Particle = gbl.LHCb.Particle
ParticleID = gbl.LHCb.ParticleID
Vertex = gbl.LHCb.Vertex

h_runs = TH1F('h_runs', 'run  number', 100, 0., 100000.)
h_pt_rec = TH1F('h_pt_rec', 'pt of tracks', 100, 0., 10000.)
h_Vx = TH1F('h_Vx', 'vertex chi2', 100, 0., 100.)
h_Jpsi = TH1F('h_Jpsi', 'mass of two muons', 500, 500., 11000.)
h_muondll = TH1F('h_muondll', 'muondll', 100, -10., 10.)
h_muondlljpsi = TH1F('h_muondlljpsi', 'muondll', 100, -10., 10.)

myHistos = {}
for h in gROOT.GetList():
    myHistos[h.GetName()] = h


def processFile(file):
    print '+++++ Start with file ', file
    htemp = {}
    for h in myHistos.keys():
        htemp[h] = myHistos[h].Clone()

    appMgr = GaudiPython.AppMgr()
    sel = appMgr.evtsel()
    sel.open(file)
    evt = appMgr.evtsvc()
    partsvc = appMgr.ppSvc()
    mass_mu = partsvc.find(ParticleID(13)).mass()
    OfflineVertexFitter = appMgr.toolsvc().create(
        'OfflineVertexFitter', interface='IVertexFit')
    m_p2s = appMgr.toolsvc().create(
        'Particle2State', interface='IParticle2State')
    while 0 < 1:
        appMgr.run(1)
        # check if there are still valid events
        if not evt['Rec/Header']: break
        rc = htemp['h_runs'].Fill(evt['Rec/Header'].runNumber())
        # go for dimuon
        muplus = []
        muminus = []
        res = Double()
        for p in evt['Rec/ProtoP/Charged']:
            if not p.hasInfo(p.MuonPIDStatus): continue
            muonDll = p.info(p.MuonMuLL, res)
            rc = htemp['h_muondll'].Fill(muonDll)
            # cut on dll
            if muonDll > -5:
                tr = p.track()
                pvec = tr.momentum()
                pt = tr.pt()
                rc = htemp['h_pt_rec'].Fill(pt)
                if pt > 2000.:
                    px = pvec.x()
                    py = pvec.y()
                    pz = pvec.z()
                    ptot = pvec.r()
                    E = math.sqrt(ptot * ptot + mass_mu * mass_mu)
                    mom = LorentzVector(px, py, pz, E)
                    mu = Particle()
                    mu.setMomentum(mom)
                    pref = gbl.SmartRef('LHCb::ProtoParticle')(p)
                    mu.setProto(pref)
                    mu.setMeasuredMass(mass_mu)
                    mu.setMeasuredMassErr(0)
                    usedState = tr.closestState(0)
                    mu.setParticleID(ParticleID(13 * int(tr.charge())))
                    m_p2s.state2Particle(usedState, mu)
                    if tr.charge() > 0:
                        muplus.append(mu)
                    else:
                        muminus.append(mu)
        for mp in muplus:
            for mm in muminus:
                # make jpsi vertex
                JpsiVx = Vertex()
                Jpsi = Particle()
                OfflineVertexFitter.fit(mp, mm, JpsiVx)
                ch2 = JpsiVx.chi2()
                rc = htemp['h_Vx'].Fill(ch2)
                if ch2 < 16:
                    ptot = mp.momentum() + mm.momentum()
                    rc = htemp['h_Jpsi'].Fill(ptot.mass())
                    muonDll = mp.proto().info(mp.proto().CombDLLmu, res)
                    rc = htemp['h_muondlljpsi'].Fill(muonDll)
                    muonDll = mm.proto().info(mm.proto().CombDLLmu, res)
                    rc = htemp['h_muondlljpsi'].Fill(muonDll)

    print 'finished ', file, 'entries ', htemp['h_pt_rec'].GetEntries()
    appMgr.stop()
    return htemp


if __name__ == '__main__':
    files = []
    file = 'PFN:/castor/cern.ch/grid/lhcb/data/2009/RDST/00004602/0000/00004602_0000XXXX_1.rdst'
    #file  = 'PFN:/castor/cern.ch/grid/lhcb/data/2009/RDST/00003996/0000/00003996_0000XXXX_1.rdst'
    if not debug:
        for n in range(1, nmax):
            ff = file.replace('XXXX', '%(X)04d' % {'X': n})
            x = os.system('nsls ' + ff.replace('PFN:', ''))
            if x == 0: files.append(ff)
    else:
        # use jpsi tape
        files = [
            'PFN:/castor/cern.ch/grid/lhcb/MC/2008/DST/00003392/0000/00003392_00000004_5.dst'
        ]
        #------Use up to 8 processes
    start = time.time()
    if not debug:
        from processing import Pool
        n = 8
        pool = Pool(n)
        result = pool.map_async(processFile, files[:10])
        for hlist in result.get(timeout=100000):
            for h in hlist:
                if myHistos[h]: myHistos[h].Add(hlist[h])
                else: myHistos[h] = hlist[h]
    else:
        myHistos = processFile(files[0])

    myHistos['h_Jpsi'].Draw()
    end = time.time()
    print 'execution time:', end - start, myHistos['h_runs'].GetEntries()
