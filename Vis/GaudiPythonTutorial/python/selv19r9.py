###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#A python script to output a dst containing only events selected by a certain selection

from ROOT import TH1F, TBrowser, TCanvas, MakeNullPointer
# get the basic configuration from here
from LHCbConfig import *
appConf = ApplicationMgr(
    OutputLevel=INFO, AppName='SelectEvents', OutStream=[InputCopyStream()])

###############################################
#Example selection options and data cards file#
lhcbApp.DataType = "MC09"
lhcbApp.Simulation = True
AnalysisConf().DataType = lhcbApp.DataType
PhysConf().DataType = lhcbApp.DataType
PhysConf().InputType = 'DST'
appConf.TopAlg += [PhysConf().initSequence(), AnalysisConf().initSequence()]

from StrippingSelections.StrippingBs2JpsiPhi import StrippingBs2JpsiPhiConf
from PhysSelPython.Wrappers import SelectionSequence
combineP = StrippingBs2JpsiPhiConf().Bs2JpsiPhi()
appConf.TopAlg += [SelectionSequence("Bs2JpsiPhiSeq", combineP).sequence()]
# container name
candidates = 'Phys/SelBs2JpsiPhi/Particles'
EventSelector().Input = [
    "DATAFILE='PFN:castor:/castor/cern.ch/grid/lhcb/MC/MC09/DST/00004879/0000/00004879_00000013_1.dst' TYP='POOL_ROOT TREE' OPT='READ'"
]
###############################################

from Configurables import DstConf
DstConf().SimType = 'Full'
DstConf().EnableUnpack = True
DstConf().Writer = 'InputCopyStream'

##############  The output DST file
InputCopyStream(
).Output = "DATAFILE='PFN:test.dst' TYP='POOL_ROOTTREE'OPT='REC' "
##############################

DataOnDemandSvc()
appConf.ExtSvc += ['DataOnDemandSvc', 'ParticlePropertySvc']

import GaudiPython
from gaudigadgets import *
EventSelector().PrintFreq = 10
appMgr = GaudiPython.AppMgr()

sel = appMgr.evtsel()
evt = appMgr.evtsvc()
appMgr.algorithm('InputCopyStream').Enable = False

nevent = 0
nacc = 0

############### Run over 100 events

while nevent < 100:

    acc = 0
    accdd = 0
    accll = 0
    nevent = nevent + 1
    appMgr.run(1)
    if nevent == 1:
        print '*** Printing data on the TES now for event ', nevent, ' ***'
        evt.dump()

    #Signifies end of input
    if not evt['Rec/Header']: break
    sel = evt["Phys/Selections"]

    ######   The location of your selection(s) on the TES ######
    sel = evt['Phys/SelBs2JpsiPhi/Particles']
    if sel:
        if sel.size() > 0: accdd = 1
    sel = evt['Phys/SelBs2JpsiPhi/Particles']
    if sel:
        if sel.size() > 0: accll = 1
    #############################################################

    sum = acc + accdd * 2 + accll * 4
    if sum > -1:
        nacc = nacc + 1
        #Write the event
        print '*** write ', acc, accdd, accll, ' nacc-tot=', nacc, ' ***'
        appMgr.algorithm('InputCopyStream').execute()

print '*** accepted events=', nacc, ' ***'
