###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
debug = False

from ROOT import TFile, TCanvas, TH1F, TH2F, TH3F, TBrowser, gROOT, TF1, gStyle, TText, TMinuit, gSystem, Double
import math

# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "MC09"
lhcbApp.Simulation = True

importOptions('$ASSOCIATORSROOT/options/Brunel.opts')

appConf = ApplicationMgr(OutputLevel=INFO, AppName='checkMeasurements')
EventSelector().PrintFreq = 1000

import GaudiPython
from gaudigadgets import *
from LinkerInstances.eventassoc import *

appMgr = GaudiPython.AppMgr()

sel = appMgr.evtsel()
evt = appMgr.evtsvc()
his = appMgr.histsvc()
det = appMgr.detsvc()
part = appMgr.ppSvc()

sel.open([
    'PFN:/castor/cern.ch/grid/lhcb/MC/MC09/XDST/00004924/0000/00004924_00000001_1.xdst',
    'PFN:/castor/cern.ch/grid/lhcb/MC/MC09/XDST/00004924/0000/00004924_00000002_1.xdst'
])

myhists = {}
myhists['h_TT_midPointLocal_z'] = TH1F('h_TT_midPointLocal_z',
                                       ' midpoint local z for all sensors TT',
                                       100, -0.01, 0.01)
myhists['h_IT_midPointLocal_z'] = TH1F('h_IT_midPointLocal_z',
                                       ' midpoint local z for all sensors IT',
                                       100, -0.01, 0.01)

myhists['h_TT_resol'] = TH1F('h_TT_resol', ' resolution for all sensors TT',
                             100, -0.5, 0.5)
myhists['h_IT_resol'] = TH1F('h_IT_resol', ' resolution for all sensors IT',
                             100, -0.5, 0.5)

gbl = GaudiPython.gbl
XYZPoint = LHCbMath.XYZPoint
XYZVector = LHCbMath.XYZVector
Rotation3D = gbl.Math.Rotation3D
EulerAngles = gbl.Math.EulerAngles
AxisAngle = gbl.Math.AxisAngle

ParticleID = gbl.LHCb.ParticleID
MCParticle = gbl.LHCb.MCParticle
MCHit = gbl.LHCb.MCHit
STCluster = gbl.LHCb.STCluster
lhcbid = gbl.LHCb.LHCbID
STChannelID = gbl.LHCb.STChannelID
LineTraj = gbl.LHCb.LineTraj

tt = det['/dd/Structure/LHCb/BeforeMagnetRegion/TT']
it = det['/dd/Structure/LHCb/AfterMagnetRegion/T/IT']
mydet = {'TT': tt, 'IT': it}
poca = appMgr.toolsvc().create('TrajPoca', interface='ITrajPoca')


def midPointLocal():
    for d in ['TT', 'IT']:
        hf = 'h_XX_midPointLocal_z'.replace('XX', d)
        loc = 'MC/XX/Hits'.replace('XX', d)
        contmc = evt[loc]
        for h in contmc:
            en = h.entry()
            ex = h.exit()
            delz = ex.z() - en.z()
            if delz > 0.290:
                p = h.midPoint()
                detid = STChannelID(h.sensDetID())
                sensdet = gbl.DeSTDetector.findSector(mydet[d], detid)
                p_loc = sensdet.geometry().toLocal(p)
                myhists[hf].Fill(p_loc.z())


def resolution():
    for d in ['TT', 'IT']:
        hres = 'h_XX_resol'.replace('XX', d)
        loc = 'Raw/XX/Clusters'.replace('XX', d)
        ####MC relation
        lsthit = linkedFrom(MCHit, MCParticle, 'MC/Particles2MCXXHits'.replace(
            'XX', d))
        lstpart = linkedTo(MCParticle, STCluster, loc)
        if lstpart.notFound() > 0 or lsthit.notFound() > 0:
            print ' no MCHit links found, program can stop'
            evt.dump()
            break
        for cl in evt[loc]:
            vecpart = lstpart.range(cl)
            if vecpart.size() != 1: continue
            pmc = vecpart[0].momentum()
            oVx = vecpart[0].originVertex()
            tx = pmc.px() / (pmc.pz() + 0.001)
            ty = pmc.py() / (pmc.pz() + 0.001)
            slope = math.sqrt(tx * tx + ty * ty)
            if pmc.r() < 1000.: continue
            if oVx.position().rho() > 1.: continue
            if abs(tx) > 0.4 or abs(ty) > 0.4: continue
            vechits = lsthit.range(vecpart[0])
            clid = cl.channelID().channelID() - cl.channelID().strip()
            if vechits.size() > 0:
                stmchit = vechits[0]
                success = -1
                for m in vechits:
                    ######## maybe check for sensitive det id and cluster channelID
                    if m.sensDetID() == clid:
                        sstmchit = m
                        success = 1
                        break


###### get trajectory
                if success == -1:
                    print 'no mchit found, should never happen', clid
                    break
                inters = cl.interStripFraction()
                strip = cl.channelID()
                traj = mydet[d].trajectory(lhcbid(strip), inters)
                ###### find minimum distance betweeen point and traj
                p = XYZPoint(m.midPoint())
                dis = XYZVector()
                a = Double(0.001)
                s = Double(0.1 + 0.0)
                s2 = Double(0.1 + 0.0)
                # calulate closest distance to MC midpoint
                success = poca.minimize(traj.get(), s, p, dis, a)
                signDist = -1.
                if dis.Cross(traj.get().direction(s)).z() > 0.0:
                    signDist = 1.
                myhists[hres].Fill(dis.r() * signDist)

nevents = 0
while 1 > 0:
    appMgr.run(1)
    nevents += 1
    if debug and nevents > 1: break
    if not evt['MC/Header']: break
    midPointLocal()
    resolution()

g = TF1('g', 'gaus')
gStyle.SetOptFit(111)
tc = TCanvas('tc', 'ST Measurement checks', 900, 760)
tc.Divide(2, 2)
tc.cd(1)
myhists['h_IT_resol'].Fit(g)
tc.cd(1).SetLogy(1)
tc.cd(2)
myhists['h_TT_resol'].Fit(g)
tc.cd(2).SetLogy(1)
tc.cd(3)
myhists['h_IT_midPointLocal_z'].Draw()
tc.cd(4)
myhists['h_TT_midPointLocal_z'].Draw()
tc.Print('STcheck.jpg')
