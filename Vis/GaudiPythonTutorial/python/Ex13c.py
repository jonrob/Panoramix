###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# get the basic configuration from here
from LHCbConfig import *
inputFile = '/castor/cern.ch/grid/lhcb/MC/2010/XDST/00006198/0000/00006198_00000001_1.xdst'
temp = inputFile.split('/')
outFile = 'Sel_' + temp[len(temp) - 1]
appConf = ApplicationMgr(OutputLevel=INFO, AppName='Ex13c')

InputCopyStream(
).Output = "DATAFILE='PFN:myEvents.dst' 'SVC='Gaudi::RootCnvSvc' OPT='REC' "
appConf.OutStream = [InputCopyStream()]
EventSelector().PrintFreq = 100

import GaudiPython
from gaudigadgets import *

appMgr = GaudiPython.AppMgr()

sel = appMgr.evtsel()
sel.open([inputFile])
evt = appMgr.evtsvc()

appMgr.algorithm('InputCopyStream').Enable = False

for n in range(10):
    appMgr.run(1)
    if not evt['Rec/Header']: break
    print 'write event', evt['Rec/Header']
    appMgr.algorithm('InputCopyStream').execute()
