###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from processing import Process, Queue, Pool
import popen2, os, time

from random import *
from ROOT import *


#------The function to run in each 'process'
def g(n):
    print 'doing the next iteration to calc something [%i]' % n
    child_stdin, child_stdout = os.popen4('python rrandom.py ' + str(n))
    return child_stdout.readlines()


#------Use up to 4 processes
p = Pool(4)

workitems = range(20)
result = p.map_async(g, workitems)

print result.get(timeout=1000)
