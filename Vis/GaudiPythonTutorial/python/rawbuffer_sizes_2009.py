###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
MCdata = False
debug = False
L0_Accept = False
Elastic = False
No_Spill_over = False
Hlt = False

# filter out beam gas
# run 063567:
#bunchId= 1654.0   : 1661.0
#bunchId= 2545.0   : 4860.0
#bunchId= 2991.0   : 4790.0
#bunchId= 3436.0   : 1956.0

collisions = (2545, 2991)
beamgas = (1654, 3436)

filter_collisions = True

# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "2009"
if MCdata: lhcbApp.Simulation = True

# example for adding different conditions
#from Configurables import ( CondDB, CondDBAccessSvc )
#otCalib = CondDBAccessSvc( 'OTCalib' )
#otCalib.ConnectionString = 'sqlite_file:/afs/cern.ch/user/w/wouter/public/AlignDB/OTCalibration2009.11.22.db/LHCBCOND'
#CondDB().addLayer( otCalib )

EventSelector().PrintFreq = 1000

appConf = ApplicationMgr(OutputLevel=INFO, AppName='RawBufferSizes')

import GaudiPython, sys, math, os
from ROOT import TH1F, TCanvas, TFile
TH1F.AddDirectory(
    False)  # disable the automatic association of  histograms to directories
from gaudigadgets import *

bb = getEnumNames('LHCb::RawBank')
BankType = bb['BankType']

VeloTell1 = []
for k in range(42):
    VeloTell1.append(k)
for k in range(64, 106):
    VeloTell1.append(k)
for k in range(128, 132):
    VeloTell1.append(k)

hlist = {}
h_all = TH1F('h_all', 'raw eventsize', 500, -0.5, 100000.)
lumi2 = False
lumi5 = False

appMgr = GaudiPython.AppMgr()
evt = appMgr.evtsvc()
sel = appMgr.evtsel()

if not MCdata:
    sel.open(
        '/home/truf/cmtuser/FirstCollisions/Velo_20mm_063567_0000000001.raw')

nevent = 10000
first_event = 0


def empty():
    flag = False
    if evt['Gen/Collisions'].size() == 1:
        col = evt['Gen/Collisions'][0]
        if col.processType() == 91: flag = True
    return flag


def nospillover():
    flag = False
    next = evt['Next/MC/Header']
    prev = evt['Prev/MC/Header']
    preprev = evt['PrevPrev/MC/Header']
    if not next and not prev and not preprev: flag = True
    return flag


while nevent > 0:
    appMgr.run(1)
    rb = evt['DAQ/RawEvent']
    if not rb:
        print 'end of input files reached:', first_event
        break

    nr = evt['DAQ/ODIN'].bunchId()
    if filter_collisions:
        if nr not in collisions: continue
    else:
        if nr not in beamgas: continue

# look for L0 decision:
    if L0_Accept and evt['Trig/L0/L0DUReport'].decision() == 0: continue
    if No_Spill_over and not nospillover(): continue
    if Elastic and not empty(): continue

    n_raw = 0
    nevent -= 1
    for k in BankType:
        nTell1 = rb.banks(k).size()
        name = BankType[k]
        if first_event == 0 and nTell1 > 0:
            print k, name, nTell1
        ntot = 0
        for t in range(nTell1):
            nword = rb.banks(k).at(t).totalSize()
            ntot += nword
            hname = name + '_T_' + str(t)
            if not hlist.has_key(hname):
                hlist[hname] = TH1F(hname, hname, 200, -0.5, 1000.)
            result = hlist[hname].Fill(nword)
        if not hlist.has_key(name):
            hlist[name] = TH1F(name, 'banktype ' + name, 1000, -0.5, 100000.)
        if nTell1 > 0:
            result = hlist[name].Fill(ntot)
        n_raw += ntot

    h_all.Fill(n_raw)
    first_event += 1

nTell1s = {}
f = TFile('rawbuffer.root', 'recreate')
for k in BankType:
    name = BankType[k]
    if hlist.has_key(name):
        print '%20s' % (name), 'mean size = ', '%10.2f' % (
            hlist[name].GetMean())
        result = hlist[name].Write()
        for h in hlist:
            if h.find(name + '_T_') > -1:
                if nTell1s.has_key(name):
                    nTell1s[name] += 1
                else:
                    nTell1s[name] = 1
                print '%25s' % (h), '%10.2f' % (hlist[h].GetMean())

print "+++++++++++ compact summary +++++++++++++"
for k in BankType:
    name = BankType[k]
    if not nTell1s.has_key(name): nTell1s[name] = 0
    if hlist.has_key(name):
        print '%20s %4i' % (name, nTell1s[name]), 'mean size = ', '%10.2f' % (
            hlist[name].GetMean())
        result = hlist[name].Write()

result = h_all.Write()
for h in hlist:
    hlist[h].Write()
f.Close()
print 'total event size in memory:', '%10.2f' % (h_all.GetMean())
print 'number of events read', '%10.2f' % (first_event)
#
from ROOT import TH1F, TCanvas, TFile
f = TFile('rawbuffer.root')
