###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# count number of events on tape
import sys
if len(sys.argv) == 1:
    print 'give input file'
    exit
fn = sys.argv[1]
dump = False
if len(sys.argv) > 2: dump = True

from LHCbConfig import *
lhcbApp.DataType = "2009"

appConf = ApplicationMgr(OutputLevel=INFO)
EventSelector().PrintFreq = 10000

import GaudiPython
# load some additional gadgets
import gaudigadgets

appMgr = GaudiPython.AppMgr()
sel = appMgr.evtsel()
evt = appMgr.evtsvc()

sel.open([fn])

runs = {}
lumiev = {}

n = 0
while 1 > n:
    appMgr.run(1)
    if not evt['DAQ']: break
    runnr = evt['DAQ/ODIN'].runNumber()
    if lumiev.has_key(runnr):
        lumiev[runnr] += 1
    else:
        lumiev[runnr] = 1

    if not gaudigadgets.physicstrigger(): continue
    if dump:
        print evt['MC/Header']
        print evt['MC/DigiHeader']
        print evt['Rec/Header']
        n = 1
    if runs.has_key(runnr):
        runs[runnr] += 1
    else:
        runs[runnr] = 1

allevents = 0
for r in runs:
    print ' number of events in run', r, ':', runs[r], lumiev[r] - runs[r]
    allevents += runs[r]
print ' all events read', allevents
