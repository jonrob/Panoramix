###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from ROOT import TH1F, TBrowser, TCanvas, MakeNullPointer

# get the basic configuration from here
from LHCbConfig import *
appConf = ApplicationMgr(OutputLevel=INFO, AppName='Ex12')

lhcbApp.DataType = "2010"
lhcbApp.Simulation = True
# container name
candidates = 'AllStreams/Phys/Bs2JpsiPhiDetachedLine/Particles'

EventSelector().PrintFreq = 100

import GaudiPython
import gaudigadgets

appMgr = GaudiPython.AppMgr()

sel = appMgr.evtsel()
sel.open(['$PANORAMIXDATA/MC2010_BsJpsiPhi__00008919_00000068_1.dst'])
evt = appMgr.evtsvc()

part = MakeNullPointer(GaudiPython.gbl.LHCb.MCParticle)
MCDecayFinder = appMgr.toolsvc().create(
    'MCDecayFinder', interface='IMCDecayFinder')
MCDecayFinder.setDecay('[B0,B+,B_s0]cc')
MCDebugTool = appMgr.toolsvc().create(
    'PrintMCDecayTreeTool', interface='IPrintMCDecayTreeTool')
PrintDecayTreeTool = appMgr.toolsvc().create(
    'PrintDecayTreeTool', interface='IPrintDecayTreeTool')

appMgr.run(1)

# search for reconstructed signal
signalcontainer = evt[candidates]
found = False
for n in range(100):
    if signalcontainer:
        if signalcontainer.size() > 0:
            found = True
            break
    appMgr.run(1)
    if not evt['Rec/Header']: break
    signalcontainer = evt[candidates]
if not found:
    print 'ERROR: no B candidate found, exit'
    exit()

for p in signalcontainer:
    PrintDecayTreeTool.printTree(p, -1)

_ = raw_input('press enter to continue...')
# look at MC truth
decaylist = []
mc = evt['MC/Particles']
MCDecayFinder.hasDecay(mc)

while MCDecayFinder.findDecay(mc, part) > 0:
    print 'append', part.key()
    print part
    decaylist.append(part.clone())

maxDepth = 2
for decay in decaylist:
    MCDebugTool.printTree(decay, maxDepth)

_ = raw_input('press enter to continue...')

daughters = GaudiPython.gbl.std.vector('const LHCb::MCParticle*')()
for decay in decaylist:
    MCDecayFinder.descendants(decay, daughters)
    print '+++++ next decay tree +++++++++++++++++++++++'
    for p in daughters:
        print p
