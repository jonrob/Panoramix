###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from ROOT import TH1F, TBrowser, TCanvas
from LHCbConfig import *
lhcbApp.DataType = "2011"

#importOptions('$COMMONPARTICLESROOT/python/CommonParticles/StandardPhotons.py')
#importOptions('$COMMONPARTICLESROOT/python/CommonParticles/StandardElectrons.py')
importOptions(
    '$COMMONPARTICLESROOT/python/CommonParticles/StandardBasicCharged.py')
importOptions(
    '$COMMONPARTICLESROOT/python/CommonParticles/StandardBasicNeutral.py')

appConf = ApplicationMgr(OutputLevel=INFO, AppName='Ex3f')

import GaudiPython

appMgr = GaudiPython.AppMgr()
sel = appMgr.evtsel()
sel.open(['$PANORAMIXDATA/Sel_Bsmumu_2highest.dst'])
evt = appMgr.evtsvc()

appMgr.run(1)

evt['Phys/StdLoosePhotons/Particles'].size()
evt['Phys/StdLooseElectrons/Particles'].size()
for container in DataOnDemandSvc().AlgMap.keys():
    if container.find('/Particles') > -1 and container.find('Phys/') > -1:
        particles = container.split('/')[1]
        print particles, ':', evt[container].size()

appMgr.exit()
