###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Imports and typedefs
from ROOT import TCanvas, TH1F, TH2F
# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "2011"

appConf = ApplicationMgr(OutputLevel=INFO)
EventSelector().PrintFreq = 10000

import GaudiPython
import GaudiKernel.SystemOfUnits as units

appMgr = GaudiPython.AppMgr()
sel = appMgr.evtSel()
evt = appMgr.evtSvc()
tools = appMgr.toolsvc()
partSvc = appMgr.ppSvc()

h_mom = TH1F('h_mon', 'track momentum', 100, 5.1, 5.6)
h_n = TH1F('h_n', 'reack multiplicity', 10, -0.5, 9.5)

sel.open(['$PANORAMIXDATA/Sel_Bsmumu_2highest.dst'])


def my_loop():
    while 0 < 1:
        appMgr.run(1)
        if not evt['Rec/Header']: break
        tracks = evt['Rec/Track/Best']
        if tracks:
            h_n.Fill(tracks.size())
            for t in tracks:
                rc = h_mom.Fill(t.p() / units.GeV)


import time
start = time.clock()
my_loop()
end = time.clock()

nevents = 0
from gaudigadgets import panorewind
panorewind()
nevents = 0
while 0 < 1:
    appMgr.run(1)
    if not evt['Rec/Header']: break
    nevents += 1

print 'time used per event', (end - start) / float(nevents)
h_mom.Draw()

_ = raw_input('press enter to continue...')

h_p_vs_pt = TH2F('h_p_vs_pt', 'p vs pt', 100, 0., 10., 100, 5.0, 5.4)


def my_loop2(nevents):
    for n in range(nevents):
        appMgr.run(1)
        tracks = evt['Rec/Track/Best']
        if tracks:
            for t in tracks:
                rc = h_p_vs_pt.Fill(t.pt() / units.GeV, t.p() / units.GeV)


print h_n.GetEntries(), h_n.GetMean()
from gaudigadgets import panorewind
panorewind()
start = time.clock()
my_loop2(nevents)
end = time.clock()
print 'time used per event', (end - start) / float(nevents)
