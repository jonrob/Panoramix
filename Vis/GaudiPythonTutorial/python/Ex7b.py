###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from ROOT import TH1F, TBrowser, TCanvas
# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "2009"

appConf = ApplicationMgr(AppName='Ex7b', OutputLevel=INFO)

import GaudiPython

appMgr = GaudiPython.AppMgr()
sel = appMgr.evtsel()
det = appMgr.detSvc()
lhcb = det['/dd/Structure/LHCb']
sel.open(['$PANORAMIXDATA/Sel_Bsmumu_2highest.dst'])
appMgr.run(1)
partSvc = appMgr.ppSvc()
ParticleID = GaudiPython.gbl.LHCb.ParticleID

print 'Name of B meson', partSvc.find(ParticleID(521)).name()
print 'Mass of B meson', partSvc.find(ParticleID(521)).mass()
print 'Lifetime of B meson', partSvc.find(ParticleID(521)).lifetime()
print partSvc.find(ParticleID(521))
