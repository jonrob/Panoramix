###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from ROOT import TMinuit, Long, Double
from array import array


def fcn(npar, gin, f, par, iflag):
    nbins = 5
    z = [2.9, 8.1, 12.8, 18.5, 22.4]
    #calculate chisquare
    chisq = 0
    for i in range(nbins):
        delta = (z[i] - (par[0] + par[1] * i))
        chisq += delta * delta
    f[0] = chisq
    return


gMinuit = TMinuit(2)
# initialize TMinuit with a maximum of 2 params
gMinuit.SetFCN(fcn)

arglist = array('d', [500, 1.])
vstart = array('d', [3., 5.])
step = array('d', [0.001, 0.001])

ierflg = Long(0)

gMinuit.mnparm(0, "const", vstart[0], step[0], 0, 0, ierflg)
gMinuit.mnparm(1, "slope", vstart[1], step[1], 0, 0, ierflg)

gMinuit.mnexcm("SIMPLEX", arglist, 2, ierflg)
gMinuit.mnexcm("MIGRAD", arglist, 2, ierflg)
gMinuit.mnexcm("MINOS", arglist, 2, ierflg)

# in case
amin = Double(0)
edm = Double(0)
errdef = Double(0)
nvpar = Long(0)
nparx = Long(0)
icstat = Long(0)
gMinuit.mnstat(amin, edm, errdef, nvpar, nparx, icstat)
gMinuit.mnprin(3, amin)
gMinuit.mnhelp('')
