###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# simple script to be used within Panoramix session for example
from ROOT import TH2F, TBrowser, TCanvas

bid = TH2F('bid', 'bunchid vs velo multipl', 3500, -300.5, 3199.5, 100, 0.,
           2000.)

for n in range(5000):
    appMgr.run(1)
    nr = evt['DAQ/ODIN'].bunchId()
    mul = evt['Raw/Velo/Clusters'].size()
    rc = bid.Fill(nr, mul)

sel.rewind()
zIP = TH1F('zIP', 'z with smallest IP', 1000, -5000., 5000.)
for n in range(2000):
    appMgr.run(1)
    for t in evt['Rec/Track/Velo']:
        z = t.firstState().z()
        rc = zIP.Fill(z)

sel.rewind()
zPV = TH1F('zPV', 'z of a PV', 1000, -5000., 5000.)
xyPV = TH2F('xyPV', 'xy of a PV', 100, -1., 1., 100, -1., 1.)
for n in range(5000):
    appMgr.run(1)
    for vx in evt['Rec/Vertex/Primary']:
        pos = vx.position()
        rc = zPV.Fill(pos.z())
        rc = xyPV.Fill(pos.x(), pos.y())
