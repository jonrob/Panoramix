###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
sim = "Bd2Kpi"
sim = "mbias"
#---Enable Tab completion-----------------------------------------
try:
    import rlcompleter, readline
    readline.parse_and_bind('tab: complete')
    readline.parse_and_bind('set show-all-if-ambiguous On')
except:
    pass

import os
if os.environ.has_key('myProdFile'):
    outputName = os.environ['myProdFile']
else:
    outputName = 'gauss'
if os.environ.has_key('myProdRunnr'):
    runnr = int(os.environ['myProdRunnr'])
else:
    runnr = 13
if os.environ.has_key('myProdSize'):
    nevents = int(os.environ['myProdSize'])
else:
    nevents = 500
if len(os.sys.argv) > 1:
    cycle = str(os.sys.argv[1])
    firstEvent = int(cycle) * nevents
else:
    firstEvent = 1
    cycle = ''

nevents = 1000
print 'myGauss, environment picked up:', outputName, runnr, nevents

from Gaudi.Configuration import *
import GaudiKernel.SystemOfUnits as units
from Gauss.Configuration import *
from Configurables import GaudiSequencer, DDDBConf, CondDB
from Configurables import GenInit, SimInit, Generation, EvtGenDecay, EventClockSvc, FakeEventTime, UpdateManagerSvc

LHCbApp().DDDBtag = "MC11-20111102"
LHCbApp().CondDBtag = "sim-20111111-vc-mu100"
DDDBConf(DataType="2011")
LHCbApp().EvtMax = nevents

# settings for intermediate energy 1.35 TeV early 2011
UpdateManagerSvc().ConditionsOverride += [
    "Conditions/Online/Velo/MotionSystem := double ResolPosRC =-5.0 ; double ResolPosLA = 5.0 ;"
]
Gauss().BeamMomentum = 3.5 * SystemOfUnits.TeV

appConf = ApplicationMgr(OutputLevel=INFO, AppName='myGauss')

# Gauss sequence
Generator = GaudiSequencer('Generator')
GaussGen = GenInit('GaussGen')
Generator.Members += [GaussGen]
GaussGen.FirstEventNumber = firstEvent
GaussGen.RunNumber = runnr

# generator part
from Configurables import FixedNInteractions, SignalRepeatedHadronization, BeamSpotSmearVertex, FlatZSmearVertex
from Configurables import SignalForcedFragmentation, SignalPlain, GenerationToSimulation, EvtGenDecay

generation = Generation()
Generator.Members += [generation]
EvtGenDecay().DecayFile = '$DECFILESROOT/dkfiles/DECAY.DEC'
generation.addTool(FixedNInteractions, name='FixedNInteractions')
generation.PileUpTool = "FixedNInteractions"
generation.FixedNInteractions.NInteractions = 1

if cycle != '': outputName = outputName + "_" + cycle
OutputStream(
    "GaussTape"
).Output = "DATAFILE='PFN:" + outputName + ".sim ' TYP='POOL_ROOTTREE' OPT='REC'"

if sim.find("Bd2Kpi") > -1:
    # ASCII decay Descriptor: {[[B0]nos -> K+ pi-]cc, [[B0]os -> K- pi+]cc}
    generation.EventType = 11102003
    generation.SampleGenerationTool = "SignalRepeatedHadronization"
    generation.addTool(
        SignalRepeatedHadronization, name="SignalRepeatedHadronization")
    generation.SignalRepeatedHadronization.ProductionTool = "PythiaProduction"
    EvtGenDecay(
    ).UserDecayFile = "$DECFILESROOT/dkfiles/Bd_K+pi-=CPV,DecProdCut.dec"
    generation.SignalRepeatedHadronization.CutTool = "DaughtersInLHCb"
    generation.SignalRepeatedHadronization.SignalPIDList = [511, -511]
    OutputStream("GaussTape").ItemList += ["/Event/Gen/SignalDecayTree#1"]

if sim.find("mbias") > -1:
    generation.EventType = 30000000
    generation.SampleGenerationTool = "MinimumBias"
    generation.addTool(MinimumBias, name="MinimumBias")
    generation.MinimumBias.ProductionTool = "PythiaProduction"
    generation.addTool(
        SignalRepeatedHadronization, name="SignalRepeatedHadronization")
    generation.SignalRepeatedHadronization.ProductionTool = "PythiaProduction"
    EvtGenDecay().UserDecayFile = "$DECFILESROOT/dkfiles/minbias.dec"

if sim.find("bincl") > -1 or sim.find("Bd2Kpi") > -1:
    generation.addTool(BeamSpotSmearVertex, "BeamSpotSmearVertex")
    generation.BeamSpotSmearVertex.SigmaX = 0.200 * units.mm
    generation.BeamSpotSmearVertex.SigmaY = 0.200 * units.mm
    #
    generation.addTool(FlatZSmearVertex, name="FlatZSmearVertex")
    generation.FlatZSmearVertex.SigmaX = 0.280 * units.mm
    generation.FlatZSmearVertex.SigmaY = 0.280 * units.mm

    generation.addTool(
        SignalForcedFragmentation, name="SignalForcedFragmentation")
    generation.addTool(SignalPlain, name="SignalPlain")
    generation.SignalRepeatedHadronization.Clean = True
    generation.SignalForcedFragmentation.Clean = True
    generation.SignalPlain.Clean = True
    # use signal only for Geant4
    # seems to depend on Gauss version
    generatorToG4 = GenerationToSimulation("GenerationToSimulation")
    #generatorToG4 = GenerationToSimulation("GeneratorToG4")
    generatorToG4.HepMCEventLocation = 'Gen/SignalDecayTree'
    # generatorToG4.OutputLevel = 2

FileCatalog().Catalogs = ["xmlcatalog_file:" + outputName + "_Catalog.xml"]

HistogramPersistencySvc().OutputFile = outputName + '_' + cycle + '_Gauss.root'

import GaudiPython

appMgr = GaudiPython.AppMgr()

# stop Geant4 for the moment
#appMgr.algorithm('Simulation').enable = False
#appMgr.algorithm('GaussTape').enable = False

# generate n events
for events in range(nevents):
    appMgr.run(1)
evt = appMgr.evtsvc()
