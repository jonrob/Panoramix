/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef SoEvent_VertexType_h
#define SoEvent_VertexType_h

// Inheritance :
#include "Kernel/IParticlePropertySvc.h"
#include "Type.h"

#include "Event/Vertex.h" // The data class.

class IUserInterfaceSvc;
class ISoConversionSvc;
class IDataProviderSvc;
class IParticlePropertySvc;

class VertexType : public SoEvent::Type<LHCb::Vertex> {
public: // Lib::IType
  virtual Lib::Variable value( Lib::Identifier, const std::string&, void* );

public:
  VertexType( IUserInterfaceSvc*, ISoConversionSvc*, IDataProviderSvc*, LHCb::IParticlePropertySvc* );
  Lib::IIterator* iterator();

private:
  LHCb::IParticlePropertySvc* fParticlePropertySvc;
};

#endif
