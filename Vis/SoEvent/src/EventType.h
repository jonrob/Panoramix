/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef SoEvent_EventType_h
#define SoEvent_EventType_h

// Inheritance :
#include "Lib/BaseType.h"
#include "Lib/Interfaces/IIterator.h"

class IUserInterfaceSvc;
class IDataProviderSvc;
class DataObject;

class EventType : public Lib::BaseType {
public:
  EventType( IUserInterfaceSvc*, IDataProviderSvc* );

public: // Lib::IType
  virtual std::string     name() const;
  virtual Lib::IIterator* iterator();
  virtual Lib::Variable   value( Lib::Identifier, const std::string&, void* );

private:
  DataObject* getObject( const std::string& );

private:
  std::string        fType;
  IUserInterfaceSvc* fUISvc;
  IDataProviderSvc*  fDataProviderSvc;
};

#endif
