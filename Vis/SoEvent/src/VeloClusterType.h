/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef SoEvent_VeloClusterType_h
#define SoEvent_VeloClusterType_h

// Inheritance :
#include "Type.h"

#include "Event/VeloCluster.h" //The data class.

class IUserInterfaceSvc;
class ISoConversionSvc;
class IDataProviderSvc;
class DeVelo;

class VeloClusterType : public SoEvent::Type<LHCb::VeloCluster> {
public: // Lib::IType
  virtual Lib::Variable value( Lib::Identifier, const std::string&, void* );

public: // OnX::IType
  virtual void visualize( Lib::Identifier, void* );

public:
  VeloClusterType( IUserInterfaceSvc*, ISoConversionSvc*, IDataProviderSvc*, IDataProviderSvc* );

private:
  DeVelo* deVelo() const;

private:
  IDataProviderSvc* fDetectorDataSvc;
  void              visualizeMCParticle( LHCb::VeloCluster& );
};

#endif
