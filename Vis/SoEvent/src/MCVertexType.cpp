/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// this :
#include "MCVertexType.h"

// Lib :
#include "Lib/Interfaces/IIterator.h"
#include "Lib/Interfaces/ISession.h"
#include "Lib/Out.h"
#include "Lib/Variable.h"

// Gaudi :
#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/ISvcLocator.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"

#include "OnXSvc/ISoConversionSvc.h"
#include "OnXSvc/IUserInterfaceSvc.h"

// Event model :
#include "Event/MCParticle.h"
//#include "Event/L1Score.h"

//////////////////////////////////////////////////////////////////////////////
MCVertexType::MCVertexType( IUserInterfaceSvc* aUISvc, ISoConversionSvc* aSoCnvSvc, IDataProviderSvc* aDataProviderSvc,
                            LHCb::IParticlePropertySvc* aParticlePropertySvc )
    : SoEvent::Type<LHCb::MCVertex>( LHCb::MCVertices::classID(), "MCVertex",
                                     "/Event/" + LHCb::MCVertexLocation::Default, aUISvc, aSoCnvSvc, aDataProviderSvc )
    , fParticlePropertySvc( aParticlePropertySvc )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  addProperty( "type", Lib::Property::STRING );
  addProperty( "x", Lib::Property::DOUBLE, 0, "(mm)" );
  addProperty( "y", Lib::Property::DOUBLE, 0, "(mm)" );
  addProperty( "z", Lib::Property::DOUBLE, 0, "(mm)" );
  // FIXME addProperty("timeOfFlight",Lib::Property::DOUBLE,0,"(ns)");
  addProperty( "mother", Lib::Property::STRING );
  addProperty( "address", Lib::Property::POINTER );
}
//////////////////////////////////////////////////////////////////////////////
Lib::Variable MCVertexType::value( Lib::Identifier aIdentifier, const std::string& aName, void* )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  fUISvc->session()->setParameter( "modeling.what", "no" );
  LHCb::MCVertex* obj = (LHCb::MCVertex*)aIdentifier;
  if ( aName == "address" ) {
    return Lib::Variable( printer(), (void*)obj );
  } else if ( aName == "x" ) {
    return Lib::Variable( printer(), obj->position().x() );
  } else if ( aName == "y" ) {
    return Lib::Variable( printer(), obj->position().y() );
  } else if ( aName == "z" ) {
    return Lib::Variable( printer(), obj->position().z() );
  } else if ( aName == "mother" ) {
    const LHCb::MCParticle* mcParticle = obj->mother();
    if ( mcParticle ) {
      if ( fParticlePropertySvc ) {
        const LHCb::ParticleProperty* pp = fParticlePropertySvc->find( mcParticle->particleID() );
        if ( pp ) return Lib::Variable( printer(), pp->particle() );
      }
    }
    return Lib::Variable( printer(), std::string( "nil" ) );
    /*FIXME
  } else if(aName=="timeOfFlight") {
    return Lib::Variable(printer(),obj->timeOfFlight());
    */
  } else {
    return Lib::Variable( printer() );
  }
}
//////////////////////////////////////////////////////////////////////////////
void MCVertexType::visualize( Lib::Identifier aIdentifier, void* /*aData*/
                              )
//////////////////////////////////////////////////////////////////////////////
// Dummy for the moment.
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  if ( !aIdentifier ) return;
  // if(!fUISvc) return;
  // if(!fUISvc->session()) return;
  // this->SoEvent::Type<MCParticle>::visualize(aIdentifier,aData);
}
