/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// this :
#include "SoEventSvc.h"

#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/IToolSvc.h"

// OnXSvc :
#include "OnXSvc/ISoConversionSvc.h"
#include "OnXSvc/IUserInterfaceSvc.h"

// SoEvent :
#include "EventType.h"
#include "FTClusterType.h"
#include "L0MuonCoordType.h"
#include "MCHitType.h"
#include "MCParticleType.h"
#include "MCVertexType.h"
#include "MuonCoordType.h"
#include "OTTimeType.h"
#include "ParticleType.h"
#include "RecVertexType.h"
#include "STClusterType.h"
#include "TrackType.h"
#include "VPClusterType.h"
#include "VeloClusterType.h"
#include "VertexBaseType.h"
#include "VertexType.h"

DECLARE_COMPONENT( SoEventSvc )

//////////////////////////////////////////////////////////////////////////////
SoEventSvc::SoEventSvc( const std::string& aName, ISvcLocator* aSvcLoc )
    : Service( aName, aSvcLoc )
    , m_uiSvc( 0 )
    , m_soConSvc( 0 )
    , m_evtDataSvc( 0 )
    , m_detDataSvc( 0 )
    , m_pPropSvc( 0 )
    , m_toolSvc( 0 )
    , m_msgStream( 0 ) {}
//////////////////////////////////////////////////////////////////////////////
SoEventSvc::~SoEventSvc() { delete m_msgStream; }
//////////////////////////////////////////////////////////////////////////////
StatusCode SoEventSvc::initialize() {
  StatusCode sc = Service::initialize();
  if ( sc.isFailure() ) return sc;

  debug() << "Initialize" << endmsg;

  setProperties();

  if ( !( getService( "OnXSvc", m_uiSvc ) && getService( "SoConversionSvc", m_soConSvc ) &&
          getService( "EventDataSvc", m_evtDataSvc ) && getService( "LHCb::ParticlePropertySvc", m_pPropSvc ) &&
          getService( "DetectorDataSvc", m_detDataSvc ) && getService( "ToolSvc", m_toolSvc ) ) )
    return StatusCode::FAILURE;

  if ( m_evtDataSvc ) {
    m_uiSvc->addType( new EventType( m_uiSvc, m_evtDataSvc ) );
    if ( m_soConSvc ) {
      // MC :
      if ( m_pPropSvc ) {
        m_uiSvc->addType( new MCParticleType( m_uiSvc, m_soConSvc, m_evtDataSvc, m_pPropSvc ) );
        m_uiSvc->addType( new MCVertexType( m_uiSvc, m_soConSvc, m_evtDataSvc, m_pPropSvc ) );
        m_uiSvc->addType( new MCHitType( m_uiSvc, m_soConSvc, m_evtDataSvc, m_pPropSvc, "Velo" ) );
        m_uiSvc->addType( new MCHitType( m_uiSvc, m_soConSvc, m_evtDataSvc, m_pPropSvc, "OT" ) );
      }

      // Rec :
      m_uiSvc->addType( new TrackType( m_uiSvc, m_soConSvc, m_evtDataSvc, m_toolSvc, msgStream() ) );
      m_uiSvc->addType( new STClusterType( m_uiSvc, m_soConSvc, m_evtDataSvc ) );
      m_uiSvc->addType( new OTTimeType( m_uiSvc, m_soConSvc, m_evtDataSvc ) );
      m_uiSvc->addType( new VeloClusterType( m_uiSvc, m_soConSvc, m_evtDataSvc, m_detDataSvc ) );
      m_uiSvc->addType( new VPClusterType( m_uiSvc, m_soConSvc, m_evtDataSvc, m_detDataSvc ) );
      m_uiSvc->addType( new FTClusterType( m_uiSvc, m_soConSvc, m_evtDataSvc, m_detDataSvc ) );
      m_uiSvc->addType( new MuonCoordType( m_uiSvc, m_soConSvc, m_evtDataSvc ) );
      m_uiSvc->addType( new L0MuonCoordType( m_uiSvc, m_soConSvc, m_evtDataSvc ) );
      m_uiSvc->addType( new VertexBaseType( m_uiSvc, m_soConSvc, m_evtDataSvc ) );
      // Phys :
      if ( m_pPropSvc ) {
        m_uiSvc->addType( new RecVertexType( m_uiSvc, m_soConSvc, m_evtDataSvc, m_pPropSvc ) );
        m_uiSvc->addType( new VertexType( m_uiSvc, m_soConSvc, m_evtDataSvc, m_pPropSvc ) );
        m_uiSvc->addType( new ParticleType( m_uiSvc, m_soConSvc, m_evtDataSvc, m_pPropSvc, m_toolSvc, msgStream() ) );
      }
    }
  }

  return sc;
}
//////////////////////////////////////////////////////////////////////////////
StatusCode SoEventSvc::finalize() {
  releaseSvc( m_uiSvc );
  releaseSvc( m_soConSvc );
  releaseSvc( m_evtDataSvc );
  releaseSvc( m_detDataSvc );
  releaseSvc( m_pPropSvc );
  releaseSvc( m_toolSvc );
  debug() << "Finalized successfully" << endmsg;
  return Service::finalize();
}
