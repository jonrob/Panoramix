/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef SoEvent_VPClusterType_h
#define SoEvent_VPClusterType_h

// Inheritance :
#include "Type.h"

#include "Event/VPCluster.h" //The data class.

class IUserInterfaceSvc;
class ISoConversionSvc;
class IDataProviderSvc;
class DeVP;

class VPClusterType : public SoEvent::Type<LHCb::VPCluster> {
public: // Lib::IType
  virtual Lib::Variable value( Lib::Identifier, const std::string&, void* );

public: // OnX::IType
  virtual void visualize( Lib::Identifier, void* );

public:
  VPClusterType( IUserInterfaceSvc*, ISoConversionSvc*, IDataProviderSvc*, IDataProviderSvc* );

private:
  DeVP* deVP() const;

private:
  IDataProviderSvc* fDetectorDataSvc;
  void              visualizeMCParticle( LHCb::VPCluster& );
};

#endif
