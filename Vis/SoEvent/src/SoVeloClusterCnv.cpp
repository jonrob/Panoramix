/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// To fix clashes between Gaudi and Windows :
#include "OnXSvc/Win32.h"

// this :
#include "SoVeloClusterCnv.h"

// Inventor :
#include "Inventor/nodes/SoCoordinate3.h"
#include "Inventor/nodes/SoDrawStyle.h"
#include "Inventor/nodes/SoIndexedLineSet.h"
#include "Inventor/nodes/SoLightModel.h"
#include "Inventor/nodes/SoSeparator.h"

// HEPVis :
#include "HEPVis/nodes/SoMarkerSet.h"
#include "HEPVis/nodes/SoSceneGraph.h"
typedef HEPVis_SoMarkerSet SoMarkerSet;
#include "HEPVis/misc/SoStyleCache.h"
#include "HEPVis/nodes/SoHighlightMaterial.h"

// Lib :
#include "Lib/Interfaces/ISession.h"
#include "Lib/smanip.h"

// CLHEP :
#include "CLHEP/Units/PhysicalConstants.h"

// Gaudi :
#include "GaudiKernel/IToolSvc.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/SmartDataPtr.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"

// LHCb :
#include "TrackInterfaces/IVeloClusterPosition.h"
#include "VeloDet/DeVelo.h"

// SoUtils :
#include "SoUtils/SbProjector.h"

// OnXSvc :
#include "OnXSvc/ClassID.h"
#include "OnXSvc/Filter.h"
#include "OnXSvc/Helpers.h"
#include "OnXSvc/IUserInterfaceSvc.h"

// this :
#include "VeloCoord.h"

DECLARE_CONVERTER( SoVeloClusterCnv )

//////////////////////////////////////////////////////////////////////////////
SoVeloClusterCnv::SoVeloClusterCnv( ISvcLocator* aSvcLoc )
    : SoEventConverter( aSvcLoc, SoVeloClusterCnv::classID() )
    , m_velo( 0 )
    , m_pointMaker( 0 )
    , fToolSvc( 0 )
    , fVeloPositionTool( 0 )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{}
//////////////////////////////////////////////////////////////////////////////
SoVeloClusterCnv::~SoVeloClusterCnv()
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{}
//////////////////////////////////////////////////////////////////////////////
StatusCode SoVeloClusterCnv::initialize()
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  SoEventConverter::initialize();
  MsgStream log( msgSvc(), "SoVeloClusterCnv" );
  m_velo = SmartDataPtr<DeVelo>( fDetectorDataSvc, DeVeloLocation::Default );
  if ( m_velo == 0 ) {
    log << MSG::ERROR << "Unable to retrieve Velo detector element." << endmsg;
    return StatusCode::FAILURE;
  }
  return StatusCode::SUCCESS;
}
//////////////////////////////////////////////////////////////////////////////
StatusCode SoVeloClusterCnv::createRep( DataObject* aObject, IOpaqueAddress*& aAddr )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  MsgStream log( msgSvc(), "SoVeloClusterCnv" );
  // log << MSG::INFO << "SoVeloCluster::createRep." << endmsg;

  if ( !fUISvc ) {
    log << MSG::INFO << " UI service not found" << endmsg;
    return StatusCode::SUCCESS;
  }

  if ( !fParticlePropertySvc ) {
    log << MSG::INFO << " ParticleProperty service not found" << endmsg;
    return StatusCode::SUCCESS;
  }

  ISession* session = fUISvc->session();
  if ( !session ) {
    log << MSG::INFO << " can't get ISession." << endmsg;
    return StatusCode::FAILURE;
  }

  SoRegion* region = 0;
  if ( aAddr ) {
    // Optimization.
    // If having a not null aAddr, we expect a SoRegion.
    // See SoEvent/Type.h/SoEvent::Type<>::beginVisualize.
    region = (SoRegion*)aAddr;
  } else {
    region = fUISvc->currentSoRegion();
  }
  if ( !region ) {
    log << MSG::INFO << " can't get viewing region." << endmsg;
    return StatusCode::FAILURE;
  }

  if ( !aObject ) {
    log << MSG::INFO << " NULL object." << endmsg;
    return StatusCode::FAILURE;
  }

  LHCb::VeloClusters* clusters = dynamic_cast<LHCb::VeloClusters*>( aObject );
  if ( !clusters ) {
    log << MSG::INFO << " bad object type." << endmsg;
    return StatusCode::FAILURE;
  }
  if ( !clusters->size() ) {
    log << MSG::INFO << " collection is empty." << endmsg;
    return StatusCode::SUCCESS;
  }

  if ( !fToolSvc ) {
    StatusCode sc = service( "ToolSvc", fToolSvc, true );
    if ( sc.isFailure() ) { log << MSG::FATAL << "Unable to retrieve ToolSvc " << endmsg; }
  }
  if ( !fToolSvc ) {
    log << MSG::FATAL << "No ToolSvc around." << endmsg;
    return StatusCode::FAILURE;
  }

  // get position tool
  if ( !fVeloPositionTool ) {
    StatusCode sc = fToolSvc->retrieveTool( "VeloClusterPosition", fVeloPositionTool );
    if ( sc.isFailure() ) {
      log << MSG::FATAL << "Unable to retrieve VeloPosition Tool " << endmsg;
      return sc;
    }
  }
  if ( !fVeloPositionTool ) {
    log << MSG::FATAL << "No VeloPosition Tool around." << endmsg;
    return StatusCode::FAILURE;
  }

  // Representation attributes :
  // Get color (default is grey (valid on black or white background) ):
  std::string value;
  double      r = 0.5, g = 0.5, b = 0.5;
  if ( session->parameterValue( "modeling.color", value ) ) Lib::smanip::torgb( value, r, g, b );
  double hr = 1.0, hg = 1.0, hb = 0.0;
  if ( session->parameterValue( "modeling.highlightColor", value ) ) Lib::smanip::torgb( value, hr, hg, hb );
  double lineWidth = 0;
  if ( session->parameterValue( "modeling.lineWidth", value ) )
    if ( !Lib::smanip::todouble( value, lineWidth ) ) lineWidth = 0;
  // Non linear projections :
  session->parameterValue( "modeling.projection", value );
  SoUtils::SbProjector projector( value.c_str() );

  SoStyleCache* styleCache        = region->styleCache();
  SoLightModel* lightModel        = styleCache->getLightModelBaseColor();
  SoDrawStyle*  drawStyle         = styleCache->getLineStyle( SbLinePattern_solid, float( lineWidth ) );
  SoMaterial*   highlightMaterial = styleCache->getHighlightMaterial( float( r ), float( g ), float( b ), float( hr ),
                                                                    float( hg ), float( hb ), 0, TRUE );

  SoSeparator* separator = new SoSeparator;

  SoCoordinate3* coordinate3 = new SoCoordinate3;
  separator->addChild( coordinate3 );
  SbBool  emptyLine = TRUE;
  int     icoord    = 0;
  int32_t coordIndex[25];
  SbBool  empty = TRUE;

  for ( const VeloCluster* cluster : *clusters ) {

    // unsigned int sensor = cluster->channelID().sensor();
    // const DeVeloSensor* veloSens = m_velo->sensor(sensor);

    int      pointn = 0;
    SbVec3f* points = 0;
    // Gaudi::XYZPoint begin,end;

    // using trajectory
    // online info only
    //    std::auto_ptr<LHCb::Trajectory> traj_old =
    //    m_velo->trajectory(cluster->channelID(),cluster->interStripFraction());
    // another possibility
    const DeVelo*                             det = dynamic_cast<const DeVelo*>( m_velo );
    std::unique_ptr<LHCb::Trajectory<double>> traj;
    auto                                      info = fVeloPositionTool->position( cluster );
    if ( cluster->isRType() || cluster->isPileUp() ) {
      DeVeloRType const* rDet = det->rSensor( cluster->channelID().sensor() );
      traj = std::make_unique<LHCb::CircleTraj>( rDet->trajectory( info.strip, info.fractionalPosition ) );
    } else if ( cluster->isPhiType() ) {
      DeVeloPhiType const* phiDet = det->phiSensor( cluster->channelID() );
      traj = std::make_unique<LHCb::LineTraj<double>>( phiDet->trajectory( info.strip, info.fractionalPosition ) );
    } else {
      log << MSG::FATAL << "unknown velo cluster type" << endmsg;
      return StatusCode::FAILURE;
    }

    Gaudi::XYZPoint mpoint;
    // add trajctory:
    if ( cluster->isRType() || cluster->isPileUp() ) {
      pointn = 24;
      if ( projector.isZR() ) { pointn = 1; }
      points       = new SbVec3f[pointn];
      double start = traj->beginRange();
      // for signed rz projection ignore overlap and plot only one r value
      if ( projector.isZR() ) {
        mpoint = traj->position( ( traj->endRange() - start ) / 2. );
        points[0].setValue( float( mpoint.x() ), float( mpoint.y() ), float( mpoint.z() ) );
        coordIndex[0] = icoord + 0;
        coordIndex[1] = SO_END_LINE_INDEX;
      } else {
        double del = ( traj->endRange() - start ) / double( pointn - 1 );
        for ( int index = 0; index < pointn; index++ ) {
          mpoint = traj->position( start );
          start += del;
          points[index].setValue( float( mpoint.x() ), float( mpoint.y() ), float( mpoint.z() ) );
          coordIndex[index] = icoord + index;
        }
        coordIndex[pointn] = SO_END_LINE_INDEX;
      }
    } else {
      pointn                = 2;
      points                = new SbVec3f[pointn];
      Gaudi::XYZPoint begin = traj->beginPoint();
      Gaudi::XYZPoint end   = traj->endPoint();
      if ( projector.isZR() ) {
        if ( begin.rho() > end.rho() ) {
          begin = traj->endPoint();
          end   = traj->beginPoint();
        }
        if ( begin.x() * end.x() < 0 ) {
          double rho = begin.rho();
          begin.SetX( rho * end.x() / fabs( end.x() ) );
          begin.SetY( 0. );
        }
      }
      points[0].setValue( float( begin.x() ), float( begin.y() ), float( begin.z() ) );
      points[1].setValue( float( end.x() ), float( end.y() ), float( end.z() ) );
      coordIndex[0] = icoord + 0;
      coordIndex[1] = icoord + 1;
      coordIndex[2] = SO_END_LINE_INDEX;
    }

    if ( pointn ) {
      // Build picking string id :
      char sid[64];
      ::sprintf( sid, "VeloCluster/0x%lx", (unsigned long)cluster );

      SoSceneGraph* sep = new SoSceneGraph;
      sep->setString( sid );
      separator->addChild( sep );

      // Material :
      sep->addChild( highlightMaterial );

      sep->addChild( lightModel );
      sep->addChild( drawStyle );

      projector.project( pointn, points );

      if ( ( cluster->isRType() || cluster->isPileUp() ) && projector.isZR() ) {
        // There is no SoIndexedMarkerSet. Then have a local SoCoordinate3.
        SoCoordinate3* coord = new SoCoordinate3;
        coord->point.setValues( 0, pointn, points );
        sep->addChild( coord );

        SoMarkerSet* markerSet = new SoMarkerSet;
        markerSet->numPoints   = 1;
        markerSet->markerIndex = SoMarkerSet::SWISS_CROSS_FILLED_9_9;
        sep->addChild( markerSet );
      } else {
        emptyLine = FALSE;
        coordinate3->point.setValues( icoord, pointn, points );
        icoord += pointn;

        SoIndexedLineSet* lineSet = new SoIndexedLineSet;
        lineSet->coordIndex.setValues( 0, pointn + 1, coordIndex );
        sep->addChild( lineSet );
      }
      delete[] points;
      empty = FALSE;
    }
  }

  if ( emptyLine == TRUE ) { separator->removeChild( coordinate3 ); }

  if ( empty == TRUE ) {
    separator->unref();
  } else {
    //  Send scene graph to the viewing region
    // (in the "dynamic" sub-scene graph) :
    region_addToDynamicScene( *region, separator );
  }

  return StatusCode::SUCCESS;
}
//////////////////////////////////////////////////////////////////////////////
const CLID& SoVeloClusterCnv::classID()
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return LHCb::VeloClusters::classID();
}
//////////////////////////////////////////////////////////////////////////////
unsigned char SoVeloClusterCnv::storageType()
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return So_TechnologyType;
}
