/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef SoEvent_TrackType_h
#define SoEvent_TrackType_h

// Inheritance :
#include "Type.h"

#include "Event/Track.h" //The data class.

// Tool interfaces
#include "TrackInterfaces/IHitExpectation.h"

class IUserInterfaceSvc;
class ISoConversionSvc;
class IDataProviderSvc;
class IToolSvc;
class ISvcLocator;
class MsgStream;

class TrackType : public SoEvent::Type<LHCb::Track> {
public: // Lib::IType
  virtual Lib::IIterator* iterator();
  virtual Lib::Variable   value( Lib::Identifier, const std::string&, void* );

public: // OnX::IType
  virtual void visualize( Lib::Identifier, void* );

public:
  TrackType( IUserInterfaceSvc*, ISoConversionSvc*, IDataProviderSvc*, IToolSvc*, MsgStream& );

private:
  void visualizeMeasurements( LHCb::Track& ) const;    ///< Visualize the associated Measurements
  void visualizeMCParticle( LHCb::Track& ) const;      ///< Visualize the associated MCParticle
  void visualizeChargeConjugate( LHCb::Track& ) const; ///< Visualize the associated charge conjugate trajectory
private:
  /// Draw the data container
  template <typename TYPE>
  inline void drawContainer( TYPE& objs ) const {
    if ( !objs.empty() ) {
      IOpaqueAddress* addr = 0;
      StatusCode      sc   = fSoCnvSvc->createRep( &objs, addr );
      if ( sc.isSuccess() ) sc = fSoCnvSvc->fillRepRefs( addr, &objs );
      if ( sc.isFailure() ) error() << "Cannot visualize " << System::typeinfoName( typeid( objs ) ) << endmsg;
      objs.clear();
    }
  }

private:
  /// Access to Message stream
  MsgStream& msgStream() const { return m_msgStream; }
  /// Debug message
  MsgStream& always() const { return msgStream() << MSG::ALWAYS; }
  /// Debug message
  MsgStream& verbose() const { return msgStream() << MSG::VERBOSE; }
  /// Debug message
  MsgStream& debug() const { return msgStream() << MSG::DEBUG; }
  /// Info message
  MsgStream& info() const { return msgStream() << MSG::INFO; }
  /// Warning message
  MsgStream& error() const { return msgStream() << MSG::WARNING; }
  /// Error message
  MsgStream& warning() const { return msgStream() << MSG::ERROR; }

private:
  IToolSvc*  m_toolSvc = nullptr; ///< Tool service
  MsgStream& m_msgStream;         ///< Message stream object
};

#endif
