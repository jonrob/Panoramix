/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// To fix clashes between Gaudi and Windows :
#include "OnXSvc/Win32.h"

// this :
#include "SoFTClusterCnv.h"

// Inventor :
#include "Inventor/nodes/SoCoordinate3.h"
#include "Inventor/nodes/SoDrawStyle.h"
#include "Inventor/nodes/SoIndexedLineSet.h"
#include "Inventor/nodes/SoLightModel.h"
#include "Inventor/nodes/SoSeparator.h"
#include "Inventor/nodes/SoTransform.h"

// HEPVis :
#include "HEPVis/misc/SoStyleCache.h"
#include "HEPVis/nodes/SoHighlightMaterial.h"
#include "HEPVis/nodes/SoMarkerSet.h"
#include "HEPVis/nodes/SoSceneGraph.h"
typedef HEPVis_SoMarkerSet SoMarkerSet;

// Lib :
#include "Lib/Interfaces/ISession.h"
#include "Lib/smanip.h"

// Gaudi :
#include "GaudiKernel/IToolSvc.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/SmartDataPtr.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"
// LHCb :
#include "Event/FTLiteCluster.h"
#include "Kernel/FastClusterContainer.h"
typedef FastClusterContainer<LHCb::FTLiteCluster, int> FTLiteClusters;
#include "Event/FTCluster.h"
#include "FTDet/DeFTDetector.h"
#include "Kernel/Trajectory.h"

// OnXSvc :
#include "OnXSvc/ClassID.h"
#include "OnXSvc/Filter.h"
#include "OnXSvc/Helpers.h"
#include "OnXSvc/IUserInterfaceSvc.h"
// SoUtils :
#include "SoUtils/SbProjector.h"

DECLARE_CONVERTER( SoFTClusterCnv )

//////////////////////////////////////////////////////////////////////////////
SoFTClusterCnv::SoFTClusterCnv( ISvcLocator* aSvcLoc )
    : SoEventConverter( aSvcLoc, SoFTClusterCnv::classID() )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{}
//////////////////////////////////////////////////////////////////////////////
StatusCode SoFTClusterCnv::createRep( DataObject* aObject, IOpaqueAddress*& aAddr )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  MsgStream log( msgSvc(), "SoFTClusterCnv" );
  log << MSG::INFO << "FTCluster createReps" << endmsg;

  if ( !fUISvc ) {
    log << MSG::INFO << " UI service not found" << endmsg;
    return StatusCode::SUCCESS;
  }

  if ( !fParticlePropertySvc ) {
    log << MSG::INFO << " ParticleProperty service not found" << endmsg;
    return StatusCode::SUCCESS;
  }

  ISession* session = fUISvc->session();
  if ( !session ) {
    log << MSG::INFO << " can't get ISession." << endmsg;
    return StatusCode::FAILURE;
  }

  SoRegion* region = 0;
  if ( aAddr ) {
    // Optimization.
    // If having a not null aAddr, we expect a SoRegion.
    // See SoEvent/Type.h/SoEvent::Type<>::beginVisualize.
    region = (SoRegion*)aAddr;
  } else {
    region = fUISvc->currentSoRegion();
  }
  if ( !region ) {
    log << MSG::INFO << " can't get viewing region." << endmsg;
    return StatusCode::FAILURE;
  }

  if ( !aObject ) {
    log << MSG::INFO << " NULL object." << endmsg;
    return StatusCode::FAILURE;
  }
  // Non linear projections :
  std::string value;
  session->parameterValue( "modeling.projection", value );
  std::string projection = value;
  std::string proj_opt   = value;
  if ( projection == "-ZR" || projection == "-ZRY" ) { projection = "ZR"; }
  SoUtils::SbProjector projector( value.c_str() );
  LHCb::FTClusters*    clusters = dynamic_cast<LHCb::FTClusters*>( aObject );
  if ( !clusters ) {
    log << MSG::INFO << " bad object type." << endmsg;
    return StatusCode::FAILURE;
  }

  bool deleteVector = false;
  // Filter :
  Filter<LHCb::FTCluster> filter( *fUISvc, log );
  const std::string&      cuts = fUISvc->cuts();
  if ( cuts != "" ) {
    log << MSG::INFO << " cuts \"" << cuts << "\"" << endmsg;
    clusters = filter.collect( *clusters, "FTCluster", cuts );
    if ( !clusters ) return StatusCode::SUCCESS;
    // filter.dump(*clusters,"FTCluster");
    deleteVector = true;
    ;
  }

  if ( !clusters->size() ) {
    log << MSG::INFO << " collection is empty." << endmsg;
    return StatusCode::SUCCESS;
  }

  // get geometry

  IToolSvc*  toolSvc = 0;
  StatusCode sc      = service( "ToolSvc", toolSvc, true );
  if ( sc.isFailure() ) { log << MSG::FATAL << "Unable to retrieve ToolSvc " << endmsg; }

  const DeFTDetector* FTdet = SmartDataPtr<DeFTDetector>( fDetectorDataSvc, DeFTDetectorLocation::Default );
  if ( !FTdet ) {
    log << MSG::ERROR << "Unable to retrieve FT detector element"
        << " from xml." << endmsg;
    return StatusCode::FAILURE;
  }

  // Representation attributes :
  // Get color (default is grey (valid on black or white background) ):
  double r = 0.5, g = 0.5, b = 0.5;
  double hr = 1.0, hg = 1.0, hb = 0.0;
  if ( session->parameterValue( "modeling.color", value ) ) Lib::smanip::torgb( value, r, g, b );
  if ( session->parameterValue( "modeling.highlightColor", value ) ) Lib::smanip::torgb( value, hr, hg, hb );
  double lineWidth = 0;
  if ( session->parameterValue( "modeling.lineWidth", value ) )
    if ( !Lib::smanip::todouble( value, lineWidth ) ) lineWidth = 0;
  int markerWidth = 0;
  if ( session->parameterValue( "modeling.markerWidth", value ) )
    if ( !Lib::smanip::toint( value, markerWidth ) ) markerWidth = 0;

  SoStyleCache* styleCache        = region->styleCache();
  SoLightModel* lightModel        = styleCache->getLightModelBaseColor();
  SoDrawStyle*  drawStyle         = styleCache->getLineStyle( SbLinePattern_solid, float( lineWidth ) );
  SoMaterial*   highlightMaterial = styleCache->getHighlightMaterial( float( r ), float( g ), float( b ), float( hr ),
                                                                    float( hg ), float( hb ), 0, TRUE );

  SoSeparator* separator = new SoSeparator;

  SoCoordinate3* coordinate3 = new SoCoordinate3;
  separator->addChild( coordinate3 );
  int     icoord = 0;
  int32_t coordIndex[3];
  SbBool  empty = TRUE;

  // One scene graph per FTCluster :

  for ( auto it = clusters->begin(); it != clusters->end(); it++ ) {
    LHCb::FTCluster*  cluster = ( *it );
    LHCb::FTChannelID chan    = cluster->channelID();

    // Build name :
    char sid[64];
    ::sprintf( sid, "FTCluster/0x%lx", (unsigned long)cluster );

    SoSceneGraph* sep = new SoSceneGraph;
    sep->setString( sid );

    separator->addChild( sep );

    sep->addChild( highlightMaterial );

    sep->addChild( lightModel );
    sep->addChild( drawStyle );

    log << MSG::INFO << "FTCluster clusterposition " << &cluster << "  " << FTdet << endmsg;
    Gaudi::XYZPoint start;
    Gaudi::XYZPoint stop;
    const DeFTMat*  ftMat = FTdet->findMat( chan );
    auto            traj  = ftMat->trajectory( cluster->channelID(), cluster->fraction() );

    // endpoints of trajectory
    start = traj.beginPoint();
    stop  = traj.endPoint();

    int     pointn = 2;
    SbVec3f points[2];
    points[0].setValue( float( start.x() ), float( start.y() ), float( start.z() ) );
    points[1].setValue( float( stop.x() ), float( stop.y() ), float( stop.z() ) );
    coordIndex[0] = icoord + 0;
    coordIndex[1] = icoord + 1;
    coordIndex[2] = SO_END_LINE_INDEX;

    coordinate3->point.setValues( icoord, pointn, points );
    icoord += pointn;
    projector.project( pointn, points );
    SoIndexedLineSet* lineSet = new SoIndexedLineSet;
    lineSet->coordIndex.setValues( 0, pointn + 1, coordIndex );
    sep->addChild( lineSet );

    empty = FALSE;
  }

  if ( deleteVector ) {
    // We have first to empty the vector :
    while ( clusters->size() ) { clusters->remove( *( clusters->begin() ) ); }
    // Then we can delete it :
    delete clusters;
  }

  if ( empty == TRUE ) {
    separator->unref();
  } else {
    //  Send scene graph to the viewing region
    // (in the "dynamic" sub-scene graph) :
    region_addToDynamicScene( *region, separator );
  }

  return StatusCode::SUCCESS;
}
//////////////////////////////////////////////////////////////////////////////
const CLID& SoFTClusterCnv::classID()
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return LHCb::FTClusters::classID();
}
//////////////////////////////////////////////////////////////////////////////
unsigned char SoFTClusterCnv::storageType()
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return So_TechnologyType;
}
