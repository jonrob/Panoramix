/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// this :
#include "VeloClusterType.h"

// Lib :
#include "Lib/Interfaces/IIterator.h"
#include "Lib/Interfaces/ISession.h"
#include "Lib/Out.h"
#include "Lib/Variable.h"

// Gaudi :
#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/SmartDataPtr.h"

#include "OnXSvc/ISoConversionSvc.h"
#include "OnXSvc/IUserInterfaceSvc.h"

#include "Linker/LinkedTo.h"

#include "VeloDet/DeVelo.h"

// Event model :
#include "Event/MCParticle.h"

//////////////////////////////////////////////////////////////////////////////
VeloClusterType::VeloClusterType( IUserInterfaceSvc* aUISvc, ISoConversionSvc* aSoCnvSvc,
                                  IDataProviderSvc* aDataProviderSvc, IDataProviderSvc* aDetectorDataSvc )
    : SoEvent::Type<LHCb::VeloCluster>( LHCb::VeloClusters::classID(), "VeloCluster",
                                        LHCb::VeloClusterLocation::Default, aUISvc, aSoCnvSvc, aDataProviderSvc )
    , fDetectorDataSvc( aDetectorDataSvc ) {
  addProperty( "key", Lib::Property::INTEGER );
  addProperty( "z", Lib::Property::DOUBLE );
  addProperty( "isR", Lib::Property::BOOLEAN );
  addProperty( "r", Lib::Property::DOUBLE );
  addProperty( "zone", Lib::Property::INTEGER );
  addProperty( "size", Lib::Property::INTEGER );
  addProperty( "sensor", Lib::Property::INTEGER );
  addProperty( "strip", Lib::Property::INTEGER );
  addProperty( "charge", Lib::Property::DOUBLE );
  addProperty( "address", Lib::Property::POINTER );
  addProperty( "halfzone", Lib::Property::INTEGER );
}
//////////////////////////////////////////////////////////////////////////////
DeVelo* VeloClusterType::deVelo() const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  static bool    alreadyTried = false;
  static DeVelo* fVelo( NULL );
  if ( !fVelo && fDetectorDataSvc && !alreadyTried ) {
    alreadyTried = true;
    fVelo        = (DeVelo*)SmartDataPtr<DeVelo>( fDetectorDataSvc, DeVeloLocation::Default );
    if ( fVelo == 0 ) {
      Lib::Out out( printer() );
      out << "ERROR : VeloClusterType : "
          << "Unable to retrieve Velo detector element at " << DeVeloLocation::Default << Lib::endl;
    }
  }
  return fVelo;
}
//////////////////////////////////////////////////////////////////////////////
Lib::Variable VeloClusterType::value( Lib::Identifier aIdentifier, const std::string& aName, void* )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  fUISvc->session()->setParameter( "modeling.what", "no" );
  LHCb::VeloCluster* obj    = (LHCb::VeloCluster*)aIdentifier;
  int                sensnr = obj->channelID().sensor();

  if ( aName == "address" ) {
    return Lib::Variable( printer(), (void*)obj );
  } else if ( aName == "z" ) {
    if ( deVelo() ) {
      unsigned int sensor = obj->channelID().sensor();
      double       z      = deVelo()->sensor( sensor )->z();
      return Lib::Variable( printer(), z );
    } else {
      return Lib::Variable( printer(), 0. );
    }
  } else if ( aName == "key" ) {
    return Lib::Variable( printer(), (int)( obj->key() ) );
  } else if ( aName == "sensor" ) {
    int sensless = obj->channelID().sensor();
    return Lib::Variable( printer(), sensless );
  } else if ( aName == "strip" ) {
    int stripper = obj->channelID().strip();
    return Lib::Variable( printer(), stripper );
  } else if ( aName == "charge" ) {
    return Lib::Variable( printer(), obj->totalCharge() );
  } else if ( aName == "size" ) {
    int sizer = obj->size();
    return Lib::Variable( printer(), sizer );
  } else if ( aName == "isR" ) {
    return Lib::Variable( printer(), ( ( obj->isRType() || obj->isPileUp() ) ) ? true : false );
  } else if ( aName == "r" ) {
    if ( deVelo() && ( obj->isRType() || obj->isPileUp() ) ) {
      double r = deVelo()->rSensor( sensnr )->rOfStrip( obj->strip( 0 ), obj->interStripFraction() );
      return Lib::Variable( printer(), r );
    } else {
      return Lib::Variable( printer(), 0. );
    }
  } else if ( aName == "zone" ) {
    int zone;
    if ( deVelo() && ( obj->isRType() || obj->isPileUp() ) ) {
      zone = deVelo()->rSensor( sensnr )->zoneOfStrip( obj->strip( 0 ) );
    } else {
      zone = deVelo()->phiSensor( sensnr )->zoneOfStrip( obj->strip( 0 ) );
    }
    return Lib::Variable( printer(), zone );
  } else if ( aName == "halfzone" ) {
    if ( deVelo() && ( obj->isRType() || obj->isPileUp() ) ) {
      int    zone   = deVelo()->rSensor( sensnr )->zoneOfStrip( obj->strip( 0 ) );
      int    strip  = obj->strip( 0 );
      double radout = 0;
      double phiMin = 0;
      double phiMax = 0;

      deVelo()->rSensor( sensnr )->stripLimits( strip, radout, phiMin, phiMax );

      if ( phiMin < 0. ) phiMin += 6.283185482;
      if ( phiMin > 1.55 && phiMin < 3.95 ) zone += 4;

      return Lib::Variable( printer(), zone );
    } else {
      return Lib::Variable( printer(), -1 );
    }
  } else {
    return Lib::Variable( printer() );
  }
}

//////////////////////////////////////////////////////////////////////////////
void VeloClusterType::visualize( Lib::Identifier aIdentifier, void* aData )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{

  if ( !aIdentifier ) return;
  if ( !fUISvc ) return;
  if ( !fUISvc->session() ) return;
  std::string value;
  fUISvc->session()->parameterValue( "modeling.what", value );
  if ( value == "MCParticle" ) {

    LHCb::VeloCluster* object = (LHCb::VeloCluster*)aIdentifier;
    visualizeMCParticle( *object );

  } else {

    this->SoEvent::Type<LHCb::VeloCluster>::visualize( aIdentifier, aData );
  }
}
//////////////////////////////////////////////////////////////////////////////
void VeloClusterType::visualizeMCParticle( LHCb::VeloCluster& aVeloCluster )
//////////////////////////////////////////////////////////////////////////////

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  LinkedTo<LHCb::MCParticle, LHCb::VeloCluster> VeloClusterLink( fDataProviderSvc, 0,
                                                                 LHCb::VeloClusterLocation::Default );

  LHCb::MCParticle* part = VeloClusterLink.first( &aVeloCluster );
  while ( NULL != part ) {
    LHCb::MCParticles* objs = new LHCb::MCParticles;
    objs->add( part );
    // Convert it :
    IOpaqueAddress* addr = 0;
    StatusCode      sc   = fSoCnvSvc->createRep( objs, addr );
    if ( sc.isSuccess() ) sc = fSoCnvSvc->fillRepRefs( addr, objs );
    objs->remove( part );
    delete objs;
    part = VeloClusterLink.next();
  }
}
