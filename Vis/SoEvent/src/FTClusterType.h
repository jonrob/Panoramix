/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef SoEvent_FTClusterType_h
#define SoEvent_FTClusterType_h

// Inheritance :
#include "Type.h"

//#include "Event/FTRawCluster.h" //The data class.
#include "Event/FTCluster.h"     //The data class.
#include "Event/FTLiteCluster.h" //The data class.
#include "Kernel/FastClusterContainer.h"
typedef FastClusterContainer<LHCb::FTLiteCluster, int> FTLiteClusters;

class IUserInterfaceSvc;
class ISoConversionSvc;
class IDataProviderSvc;
class DeFTDetector;

class FTClusterType : public SoEvent::Type<LHCb::FTCluster> {
public: // Lib::IType
  virtual Lib::Variable value( Lib::Identifier, const std::string&, void* );

public: // OnX::IType
  virtual void visualize( Lib::Identifier, void* );

public:
  FTClusterType( IUserInterfaceSvc*, ISoConversionSvc*, IDataProviderSvc*, IDataProviderSvc* );

private:
  DeFTDetector* deFTDetector() const;

private:
  IDataProviderSvc* fDetectorDataSvc;
  void              visualizeMCParticle( LHCb::FTCluster& );
};

#endif
