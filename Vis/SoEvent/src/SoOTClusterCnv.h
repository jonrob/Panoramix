/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef SoEvent_SoOTClusterCnv_h
#define SoEvent_SoOTClusterCnv_h

#include "SoEventConverter.h"

class SoOTClusterCnv : public SoEventConverter {
public:
  SoOTClusterCnv( ISvcLocator* );
  virtual StatusCode createRep( DataObject*, IOpaqueAddress*& );

public:
  static const CLID&   classID();
  static unsigned char storageType();
};

#endif
