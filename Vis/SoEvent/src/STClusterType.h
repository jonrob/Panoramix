/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef SoEvent_STClusterType_h
#define SoEvent_STClusterType_h

// Inheritance :
#include "Type.h"

#include "Event/STCluster.h" //The data class.

class IUserInterfaceSvc;
class ISoConversionSvc;
class IDataProviderSvc;

class STClusterType : public SoEvent::Type<LHCb::STCluster> {
public: // Lib::IType
  virtual Lib::Variable value( Lib::Identifier, const std::string&, void* );

public: // OnX::IType
  virtual void visualize( Lib::Identifier, void* );

public:
  STClusterType( IUserInterfaceSvc*, ISoConversionSvc*, IDataProviderSvc* );

private:
  void visualizeMCParticle( LHCb::STCluster& );
};

#endif
