/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// CVS tag $Name: not supported by cvs2svn $
// ============================================================================
// $Log: not supported by cvs2svn $
// Revision 1.4  2010/02/18 07:17:54  truf
// improve display of backward tracks
//
// Revision 1.3  2008/12/12 07:33:47  truf
// another fix for warnings
//
// Revision 1.2  2008/12/11 10:29:39  truf
// fix some compile warnings from nightlies
//
// Revision 1.1  2008/09/24 15:00:33  truf
// *** empty log message ***
//
//
// ============================================================================
// Include files
// Lib :
#include "Lib/Interfaces/IIterator.h"
#include "Lib/Interfaces/ISession.h"
#include "Lib/Variable.h"

#include "OnXSvc/ISoConversionSvc.h"
#include "OnXSvc/IUserInterfaceSvc.h"

#include <Linker/LinkedTo.h>
// Event model :
#include <Event/MCParticle.h>

// this :
#include "L0MuonCoordType.h"

#include "Event/L0MuonCandidate.h"

// ============================================================================
/** @file L0MuonCoordType.cpp
 *
 *  Implementation file for class L0MuonCoordType
 *
 *  @author Fernando Rodrigues flfr@if.ufrj.br
 *  @date   18/08/2008
 *
 */
// ============================================================================

// ============================================================================
/**
 * Files used for L0 Calo Visualization:
 *    Event/L0MuonCandidate.h
 * Changed:
 *    SoEventSvc.h(.cpp)
 *    MCParticleType.cpp
 *    cmt/requirements
 * Created:
 *    L0MuonCoordType.h(.cpp)
 *    SoL0MuonCoordCnv.h(.cpp)
 *
 *
 */
// ============================================================================

// ============================================================================
/** L0MuonCandidate Summary:
 * CLID = 6002
 * Location: "Trig/L0/MuonCtrl", "Trig/L0/MuonCtrl", "Trig/L0/MuonCtrlBCSU",
 *           "Trig/L0/MuonBCSU", "Trig/L0/MuonPU"
 * Constructor: L0MuonCandidate(
 *              double pt, double theta, double phi,
 *              const std::vector<MuonTileID>& pads, int encodedPt);
 * Variables names:
 * double pt() const;     << Pt of the candidate
 * double theta() const;  << Theta at IP of the candidate
 * double phi() const;    << Phi at IP of the candidate
 * std::vector<MuonTileID> muonTileIDs(unsigned int station) const; << Accessor
 *                                to the list of MuonTileIDs in a given station
 * int encodedPt() const; << encoded Pt of the candidate
 ***
 * Definition of Keyed Container for L0MuonCandidate
 * typedef KeyedContainer<L0MuonCandidate,Containers::HashMap> L0MuonCandidates;
 */
// ============================================================================

// ============================================================================
/** MuonCoord Summary:
 * CLID = 11040
 * Location: "Raw/Muon/Coords"
 * Constructor: MuonCoord() : m_uncrossed(), m_digitTile(), m_digitTDC1(0),
 *                            m_digitTDC2(0){}
 * Variables names:
 * bool          m_uncrossed; << Flag set if this pad should have crossed
 *                               another logical channel and did not
 * std::vector<LHCb::MuonTileID> m_digitTile; << list of address of MuonDgit
 *                                               used to cretate this MuonCoord
 * unsigned int  m_digitTDC1; << TDC time of the first digit
 * unsigned int  m_digitTDC2; << TDC time of the second (if present) digit
 ***
 * Definition of Keyed Container for MuonCoord
 * typedef KeyedContainer<MuonCoord, Containers::HashMap> MuonCoords;
 */
// ============================================================================

// ============================================================================
/** MuonTileID Summary:
 * ($LHCBRELEASES/LHCB/LHCB_v25r0/InstallArea/include/Kernel/MuonTileID.h)
 * Constructor:
 *  Default : MuonTileID() : m_muonid(0) {}
 *  With all the arguments :  MuonTileID(int station,
 *                                       const IMuonLayout& lay,
 *                                       int region,
 *                                       int quarter,
 *                                       int x,
 *                                       int y);
 *  From similar MuonTileID: MuonTileID(const MuonTileID& id,
 *                                      const unsigned int region,
 *                                      const unsigned int quarter,
 *                                      const unsigned int x,
 *                                      const unsigned int y);
 *  From relative position in the containing MuonTile MuonTileID:
 *                           MuonTileID(const MuonTileID& id,
 *                                      const IMuonLayout& layout,
 *                                      const unsigned int x,
 *                                      const unsigned int y);
 */
// ============================================================================
//////////////////////////////////////////////////////////////////////////////
L0MuonCoordType::L0MuonCoordType( IUserInterfaceSvc* aUISvc, ISoConversionSvc* aSoCnvSvc,
                                  IDataProviderSvc* aDataProviderSvc )
    : SoEvent::Type<LHCb::L0MuonCandidate>( LHCb::L0MuonCandidates::classID(), "L0MuonCandidate",
                                            LHCb::L0MuonCandidateLocation::Ctrl, aUISvc, aSoCnvSvc, aDataProviderSvc ) {
  addProperty( "pt", Lib::Property::DOUBLE );
  addProperty( "theta", Lib::Property::DOUBLE );
  addProperty( "phi", Lib::Property::DOUBLE );
  addProperty( "encodedPt", Lib::Property::INTEGER );
  // addProperty("M1_station",Lib::Property::INTEGER);
  addProperty( "M1_region", Lib::Property::INTEGER );
  addProperty( "M1_quarter", Lib::Property::INTEGER );
  // addProperty("M2_station",Lib::Property::INTEGER);
  addProperty( "M2_region", Lib::Property::INTEGER );
  addProperty( "M2_quarter", Lib::Property::INTEGER );
  // addProperty("M3_station",Lib::Property::INTEGER);
  addProperty( "M3_region", Lib::Property::INTEGER );
  addProperty( "M3_quarter", Lib::Property::INTEGER );
}
//////////////////////////////////////////////////////////////////////////////
Lib::Variable L0MuonCoordType::value( Lib::Identifier aIdentifier, const std::string& aName, void* ) {
  LHCb::L0MuonCandidate* obj = (LHCb::L0MuonCandidate*)aIdentifier;
  if ( aName == "pt" ) {
    return Lib::Variable( printer(), (double)obj->pt() );
  } else if ( aName == "theta" ) {
    return Lib::Variable( printer(), (double)obj->theta() );
  } else if ( aName == "phi" ) {
    return Lib::Variable( printer(), (double)obj->phi() );
  } else if ( aName == "encodedPt" ) {
    return Lib::Variable( printer(), (int)obj->encodedPt() );
    //} else if(aName=="M1_station") {
    //   std::vector<LHCb::MuonTileID> vec = obj->muonTileIDs(0);
    //   LHCb::MuonTileID tile = vec.at(0);
    //  return Lib::Variable(printer(),(int)tile.station());
  } else if ( aName == "M1_region" ) {
    std::vector<LHCb::MuonTileID> vec  = obj->muonTileIDs( 0 );
    LHCb::MuonTileID              tile = vec.at( 0 );
    return Lib::Variable( printer(), (int)tile.region() );
  } else if ( aName == "M1_quarter" ) {
    std::vector<LHCb::MuonTileID> vec  = obj->muonTileIDs( 0 );
    LHCb::MuonTileID              tile = vec.at( 0 );
    return Lib::Variable( printer(), (int)tile.quarter() );
    //} else if(aName=="M2_station") {
    //   std::vector<LHCb::MuonTileID> vec = obj->muonTileIDs(1);
    //   LHCb::MuonTileID tile = vec.at(0);
    //  return Lib::Variable(printer(),(int)tile.station());
  } else if ( aName == "M2_region" ) {
    std::vector<LHCb::MuonTileID> vec  = obj->muonTileIDs( 1 );
    LHCb::MuonTileID              tile = vec.at( 0 );
    return Lib::Variable( printer(), (int)tile.region() );
  } else if ( aName == "M2_quarter" ) {
    std::vector<LHCb::MuonTileID> vec  = obj->muonTileIDs( 1 );
    LHCb::MuonTileID              tile = vec.at( 0 );
    return Lib::Variable( printer(), (int)tile.quarter() );
    //} else if(aName=="M3_station") {
    //   std::vector<LHCb::MuonTileID> vec = obj->muonTileIDs(2);
    //   LHCb::MuonTileID tile = vec.at(0);
    //  return Lib::Variable(printer(),(int)tile.station());
  } else if ( aName == "M3_region" ) {
    std::vector<LHCb::MuonTileID> vec  = obj->muonTileIDs( 2 );
    LHCb::MuonTileID              tile = vec.at( 0 );
    return Lib::Variable( printer(), (int)tile.region() );
  } else if ( aName == "M3_quarter" ) {
    std::vector<LHCb::MuonTileID> vec  = obj->muonTileIDs( 2 );
    LHCb::MuonTileID              tile = vec.at( 0 );
    return Lib::Variable( printer(), (int)tile.quarter() );
  } else {
    return Lib::Variable( printer(), -1 );
  }
}
//////////////////////////////////////////////////////////////////////////////
void L0MuonCoordType::visualize( Lib::Identifier aIdentifier, void* aData ) {

  if ( !aIdentifier ) return;
  if ( !fUISvc ) return;
  if ( !fUISvc->session() ) return;
  std::string value;
  fUISvc->session()->parameterValue( "modeling.what", value );
  if ( value == "MCParticle" ) {

    // LHCb::L0MuonCandidate* object = (LHCb::L0MuonCandidate*)aIdentifier;
    // visualizeMCParticle(*object);

  } else {

    this->SoEvent::Type<LHCb::L0MuonCandidate>::visualize( aIdentifier, aData );
  }
}
//////////////////////////////////////////////////////////////////////////////
/*
void L0MuonCoordType::visualizeMCParticle(
    LHCb::L0MuonCandidate& aMuonCoord
)
{
     LinkedTo<LHCb::MCParticle,LHCb::L0MuonCandidate> MuonCoordLink
(fDataProviderSvc,0,LHCb::L0MuonCandidateLocation::Ctrl); LHCb::MCParticle * part  = MuonCoordLink.first(&aMuonCoord);
     while ( NULL != part) {
        LHCb::MCParticles* objs = new LHCb::MCParticles;
        objs->add(part);
        // Convert it :
        IOpaqueAddress* addr = 0;
        StatusCode sc = fSoCnvSvc->createRep(objs, addr);
        if (sc.isSuccess()) sc = fSoCnvSvc->fillRepRefs(addr,objs);
        objs->remove(part);
        delete objs;
        part = MuonCoordLink.next();
    }
}
*/
