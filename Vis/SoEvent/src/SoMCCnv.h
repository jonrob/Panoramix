/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef SoEvent_SoMCCnv_h
#define SoEvent_SoMCCnv_h

#include "SoEventConverter.h"
#include <map>
#include <vector>
namespace LHCb {
  class MCParticle;
  class MCHit;
} // namespace LHCb

class SoMCCnv : public SoEventConverter {
public:
  SoMCCnv( ISvcLocator* );
  virtual StatusCode createRep( DataObject*, IOpaqueAddress*& );

private:
  typedef std::vector<const LHCb::MCHit*>            HVector;
  typedef std::map<const LHCb::MCParticle*, HVector> P2HMap;

  void fillParticlesAndHits( DataObject*, P2HMap& );
  void addHitToMap( const LHCb::MCParticle*, const LHCb::MCHit*, P2HMap& );
  void sortHitMap( P2HMap& );

public:
  static const CLID&   classID();
  static unsigned char storageType();
};

#endif
