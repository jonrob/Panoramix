/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// this :
#include "SoEventConverter.h"

// Gaudi :
#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/ISvcLocator.h"
#include "Kernel/IParticlePropertySvc.h"

// OnXSvc :
#include "OnXSvc/ClassID.h"
#include "OnXSvc/ISoConversionSvc.h"
#include "OnXSvc/IUserInterfaceSvc.h"

//////////////////////////////////////////////////////////////////////////////
SoEventConverter::SoEventConverter( ISvcLocator* aSvcLoc, CLID aCLID )
    : Converter( So_TechnologyType, aCLID, aSvcLoc )
    , fSoCnvSvc( 0 )
    , fUISvc( 0 )
    , fParticlePropertySvc( 0 )
    , fDetectorDataSvc( 0 )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{}
//////////////////////////////////////////////////////////////////////////////
StatusCode SoEventConverter::initialize()
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  StatusCode status = Converter::initialize();
  if ( status.isFailure() ) return status;
  status = serviceLocator()->service( "SoConversionSvc", fSoCnvSvc );
  if ( status.isFailure() ) return status;
  status = serviceLocator()->service( "OnXSvc", fUISvc );
  if ( status.isFailure() ) return status;
  status = serviceLocator()->service( "LHCb::ParticlePropertySvc", fParticlePropertySvc );
  if ( status.isFailure() ) return status;
  status = serviceLocator()->service( "DetectorDataSvc", fDetectorDataSvc );
  if ( status.isFailure() ) return status;
  return status;
}
//////////////////////////////////////////////////////////////////////////////
StatusCode SoEventConverter::finalize()
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  // debug if(fSoCnvSvc) fSoCnvSvc->release();
  // debug if(fUISvc) fUISvc->release();
  // debug if(fParticlePropertySvc) fParticlePropertySvc->release();
  return Converter::finalize();
}
//////////////////////////////////////////////////////////////////////////////
long SoEventConverter::repSvcType() const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return i_repSvcType();
}
