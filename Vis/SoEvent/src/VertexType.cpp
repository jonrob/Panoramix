/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// this :
#include "VertexType.h"

// Lib :
#include "Lib/Interfaces/IIterator.h"
#include "Lib/Interfaces/ISession.h"
#include "Lib/Out.h"
#include "Lib/Variable.h"

#include "OnXSvc/ISoConversionSvc.h"
#include "OnXSvc/IUserInterfaceSvc.h"

//////////////////////////////////////////////////////////////////////////////
VertexType::VertexType( IUserInterfaceSvc* aUISvc, ISoConversionSvc* aSoCnvSvc, IDataProviderSvc* aDataProviderSvc,
                        LHCb::IParticlePropertySvc* aParticlePropertySvc )
    : SoEvent::Type<LHCb::Vertex>( LHCb::Vertices::classID(), "Vertex",
                                   "", // No location. Set in iterator method.
                                   aUISvc, aSoCnvSvc, aDataProviderSvc )
    , fParticlePropertySvc( aParticlePropertySvc )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  addProperty( "key", Lib::Property::INTEGER );
  addProperty( "x", Lib::Property::DOUBLE );
  addProperty( "y", Lib::Property::DOUBLE );
  addProperty( "z", Lib::Property::DOUBLE );
  addProperty( "chi2", Lib::Property::DOUBLE );
  addProperty( "ntracks", Lib::Property::INTEGER );
  addProperty( "address", Lib::Property::POINTER );
}
//////////////////////////////////////////////////////////////////////////////
Lib::Variable VertexType::value( Lib::Identifier aIdentifier, const std::string& aName, void* )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  LHCb::Vertex* obj = (LHCb::Vertex*)aIdentifier;
  if ( aName == "address" ) {
    return Lib::Variable( printer(), (void*)obj );
  } else if ( aName == "key" ) {
    return Lib::Variable( printer(), obj->key() );
  } else if ( aName == "x" ) {
    return Lib::Variable( printer(), obj->position().x() );
  } else if ( aName == "y" ) {
    return Lib::Variable( printer(), obj->position().y() );
  } else if ( aName == "z" ) {
    return Lib::Variable( printer(), obj->position().z() );
  } else if ( aName == "chi2" ) {
    return Lib::Variable( printer(), obj->chi2PerDoF() );
  } else if ( aName == "ntracks" ) {
    return Lib::Variable( printer(), obj->outgoingParticles().size() );
  } else {
    return Lib::Variable( printer() );
  }
}
//////////////////////////////////////////////////////////////////////////////
Lib::IIterator* VertexType::iterator()
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  class Iterator : public Lib::IIterator {
  public: // Lib::IIterator
    virtual Lib::Identifier object() {
      if ( fIndex >= fNumber ) return 0;
      return ( *fList )[fIndex];
    }
    virtual void  next() { fIndex++; }
    virtual void* tag() { return 0; }

  public:
    Iterator( std::vector<LHCb::Vertex*>* aList = 0 ) : fIndex( 0 ), fList( aList ), fNumber( 0 ) {
      if ( fList ) fNumber = fList->size();
    }
    virtual ~Iterator() { delete fList; }

  private:
    unsigned int                fIndex;
    std::vector<LHCb::Vertex*>* fList;
    unsigned int                fNumber;
  };

  const bool verbose = true;

  // Get location :
  if ( fUISvc ) {
    ISession* session = fUISvc->session();
    if ( session ) {
      std::string value;
      if ( session->parameterValue( "Vertex.location", value ) && ( value != "" ) ) {
        if ( verbose ) {
          Lib::Out out( printer() );
          out << "VertexType :"
              << " set location \"" << value << "\"" << Lib::endl;
        }
        setLocation( value );
      }
    }
  }

  if ( !fDataProviderSvc ) {
    Lib::Out out( printer() );
    out << "VertexType::iterator :"
        << " no data provider found." << Lib::endl;
    return new Iterator(); // Return an empty iterator.
  }

  if ( location() == "" ) {
    Lib::Out out( printer() );
    out << "VertexType::iterator :"
        << " no location given." << Lib::endl;
    return new Iterator();
  }

  SmartDataPtr<LHCb::Vertices> vertices( fDataProviderSvc, location() );
  if ( !vertices ) {
    Lib::Out out( printer() );
    out << "VertexType::iterator :"
        << " can't retrieve object at location \"" << location() << "\"." << Lib::endl;
    return new Iterator();
  }
  if ( vertices->size() == 0 ) {
    if ( verbose ) {
      Lib::Out out( printer() );
      out << "VertexType::iterator :"
          << " collection is empty." << Lib::endl;
    }
    return new Iterator();
  }

  int number = vertices->size() - std::count( vertices->begin(), vertices->end(), (const LHCb::Vertex*)0 );
  if ( number <= 0 ) {
    if ( verbose ) {
      Lib::Out out( printer() );
      out << "VertexType::iterator :"
          << " collection contains no valid data." << Lib::endl;
    }
    return new Iterator();
  }

  if ( verbose ) {
    Lib::Out out( printer() );
    out << "VertexType::iterator :"
        << " retrieve " << number << " Vertices at location \"" << location() << "\"." << Lib::endl;
  }

  std::vector<LHCb::Vertex*>* vec = new std::vector<LHCb::Vertex*>;
  LHCb::Vertices::iterator    it;
  for ( it = vertices->begin(); it != vertices->end(); ++it ) {
    if ( *it ) vec->push_back( *it );
  }
  return new Iterator( vec ); // deleted by the caller.
}
