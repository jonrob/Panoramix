/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// this :
#include "EventType.h"

// Lib :
#include "Lib/Interfaces/IIterator.h"
#include "Lib/Interfaces/ISession.h"
#include "Lib/Out.h"
#include "Lib/Variable.h"

// Gaudi :
#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/SmartDataPtr.h"

#include "OnXSvc/IUserInterfaceSvc.h"

// Event model :
//#include "Event/EventHeader.h"
#include "Event/MCHeader.h"
#include "Event/ODIN.h"

//////////////////////////////////////////////////////////////////////////////
EventType::EventType( IUserInterfaceSvc* aUISvc, IDataProviderSvc* aDataProviderSvc )
    : Lib::BaseType( aUISvc->printer() )
    , fType( "Event" )
    , fUISvc( aUISvc )
    , fDataProviderSvc( aDataProviderSvc )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  addProperty( "run", Lib::Property::INTEGER, 16 );
  addProperty( "event", Lib::Property::INTEGER, 16 );
}
//////////////////////////////////////////////////////////////////////////////
std::string EventType::name() const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return fType;
}

// One shoot only iterator !
class Event_Iterator : public Lib::IIterator {
public: // Lib::IIterator
  virtual Lib::Identifier object() { return fEvent; }
  virtual void            next() { fEvent = 0; }
  virtual void*           tag() { return 0; }

public:
  Event_Iterator( DataObject* aEvent ) : fEvent( aEvent ) {}

private:
  DataObject* fEvent;
};

//////////////////////////////////////////////////////////////////////////////
Lib::IIterator* EventType::iterator()
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  if ( !fDataProviderSvc ) {
    Lib::Out out( printer() );
    out << "EventType::iterator :"
        << " no data provider." << Lib::endl;
    return 0;
  }

  std::string location   = "/Event/" + LHCb::MCHeaderLocation::Default;
  DataObject* dataObject = getObject( location );
  if ( dataObject ) {
    if ( !dynamic_cast<LHCb::MCHeader*>( dataObject ) ) {
      Lib::Out out( printer() );
      out << "EventType::iterator :"
          << " object not an MCHeader." << Lib::endl;
      return 0;
    }
  } else {
    location   = "/Event/" + LHCb::ODINLocation::Default;
    dataObject = getObject( location );
    if ( dataObject ) {
      if ( !dynamic_cast<LHCb::ODIN*>( dataObject ) ) {
        Lib::Out out( printer() );
        out << "EventType::iterator :"
            << " object not an MCHeader." << Lib::endl;
        return 0;
      }
    }
  }
  if ( !dataObject ) {
    Lib::Out out( printer() );
    out << "EventType::iterator :"
        << " no header found." << Lib::endl;
    return 0;
  }

  return new Event_Iterator( dataObject );
}
//////////////////////////////////////////////////////////////////////////////
Lib::Variable EventType::value( Lib::Identifier aIdentifier, const std::string& aName, void* )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  DataObject* dao = (DataObject*)aIdentifier;

  if ( LHCb::MCHeader* mch = dynamic_cast<LHCb::MCHeader*>( dao ) ) {
    if ( aName == "run" ) {
      return Lib::Variable( printer(), (int)mch->runNumber() );
    } else if ( aName == "event" ) {
      return Lib::Variable( printer(), (int)mch->evtNumber() );
    }
  } else if ( LHCb::ODIN* odin = dynamic_cast<LHCb::ODIN*>( dao ) ) {
    if ( aName == "run" ) {
      return Lib::Variable( printer(), (int)odin->runNumber() );
    } else if ( aName == "event" ) {
      return Lib::Variable( printer(), (int)-1 );
    }
  }

  return Lib::Variable( printer() );
}
//////////////////////////////////////////////////////////////////////////////
DataObject* EventType::getObject( const std::string& aLocation )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  if ( !fDataProviderSvc ) return 0;
  SmartDataPtr<DataObject> smartDataObject( fDataProviderSvc, aLocation );
  if ( smartDataObject ) {}
  DataObject* dataObject = 0;
  StatusCode  sc         = fDataProviderSvc->retrieveObject( aLocation, dataObject );
  if ( !sc.isSuccess() ) return 0;
  return dataObject;
}
