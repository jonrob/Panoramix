/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// this :
#include "OTTimeType.h"

// Lib :
#include "Lib/Interfaces/IIterator.h"
#include "Lib/Interfaces/ISession.h"
#include "Lib/Out.h"
#include "Lib/Variable.h"

// Gaudi :
#include "GaudiKernel/IDataProviderSvc.h"

#include "OnXSvc/ISoConversionSvc.h"
#include "OnXSvc/IUserInterfaceSvc.h"
#include <Linker/LinkedTo.h>
// Event model :
#include <Event/MCParticle.h>

//////////////////////////////////////////////////////////////////////////////
OTTimeType::OTTimeType( IUserInterfaceSvc* aUISvc, ISoConversionSvc* aSoCnvSvc, IDataProviderSvc* aDataProviderSvc )
    : SoEvent::Type<LHCb::OTTime>( LHCb::OTTimes::classID(), "OTTime", LHCb::OTTimeLocation::Default, aUISvc, aSoCnvSvc,
                                   aDataProviderSvc )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  addProperty( "key", Lib::Property::INTEGER );
  addProperty( "station", Lib::Property::INTEGER );
  addProperty( "layer", Lib::Property::INTEGER );
  addProperty( "module", Lib::Property::INTEGER );
  addProperty( "quarter", Lib::Property::INTEGER );
  addProperty( "straw", Lib::Property::INTEGER );
  addProperty( "address", Lib::Property::POINTER );
}
//////////////////////////////////////////////////////////////////////////////
Lib::Variable OTTimeType::value( Lib::Identifier aIdentifier, const std::string& aName, void* )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  LHCb::OTTime* obj = (LHCb::OTTime*)aIdentifier;
  if ( aName == "address" ) {
    return Lib::Variable( printer(), (void*)obj );
  } else if ( aName == "station" ) {
    return Lib::Variable( printer(), int( obj->channel().station() ) );
  } else if ( aName == "key" ) {
    return Lib::Variable( printer(), (int)( obj->key() ) );
  } else if ( aName == "layer" ) {
    return Lib::Variable( printer(), int( obj->channel().layer() ) );
  } else if ( aName == "module" ) {
    return Lib::Variable( printer(), int( obj->channel().module() ) );
  } else if ( aName == "quarter" ) {
    return Lib::Variable( printer(), int( obj->channel().quarter() ) );
  } else if ( aName == "straw" ) {
    return Lib::Variable( printer(), int( obj->channel().straw() ) );
  } else {
    return Lib::Variable( printer() );
  }
}
//////////////////////////////////////////////////////////////////////////////
void OTTimeType::visualize( Lib::Identifier aIdentifier, void* aData )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{

  if ( !aIdentifier ) return;
  if ( !fUISvc ) return;
  if ( !fUISvc->session() ) return;
  std::string value;
  fUISvc->session()->parameterValue( "modeling.what", value );
  if ( value == "MCParticle" ) {

    LHCb::OTTime* object = (LHCb::OTTime*)aIdentifier;
    visualizeMCParticle( *object );

  } else {

    this->SoEvent::Type<LHCb::OTTime>::visualize( aIdentifier, aData );
  }
}
//////////////////////////////////////////////////////////////////////////////
void OTTimeType::visualizeMCParticle( LHCb::OTTime& aOTTime )
//////////////////////////////////////////////////////////////////////////////

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  LinkedTo<LHCb::MCParticle> OTClusterLink( fDataProviderSvc, 0, LHCb::OTTimeLocation::Default );
  LHCb::MCParticle*          part = OTClusterLink.first( aOTTime.channel().channelID() );
  while ( NULL != part ) {
    LHCb::MCParticles* objs = new LHCb::MCParticles;
    objs->add( part );
    // Convert it :
    IOpaqueAddress* addr = 0;
    StatusCode      sc   = fSoCnvSvc->createRep( objs, addr );
    if ( sc.isSuccess() ) sc = fSoCnvSvc->fillRepRefs( addr, objs );
    objs->remove( part );
    delete objs;
    part = OTClusterLink.next();
    ;
  }
}
