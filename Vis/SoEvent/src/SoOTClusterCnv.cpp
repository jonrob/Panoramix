/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// To fix clashes between Gaudi and Windows :
#include "OnXSvc/Win32.h"

// this :
#include "SoOTClusterCnv.h"

// Inventor :
#include "Inventor/nodes/SoCoordinate3.h"
#include "Inventor/nodes/SoDrawStyle.h"
#include "Inventor/nodes/SoIndexedLineSet.h"
#include "Inventor/nodes/SoLightModel.h"
#include "Inventor/nodes/SoLineSet.h"
#include "Inventor/nodes/SoSeparator.h"

// HEPVis :
#include "HEPVis/misc/SoStyleCache.h"
#include "HEPVis/nodes/SoHighlightMaterial.h"
#include "HEPVis/nodes/SoSceneGraph.h"

// Lib :
#include "Lib/Interfaces/ISession.h"
#include "Lib/smanip.h"

// Gaudi :
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/SmartDataPtr.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"

// LHCb :
#include "Event/OTTime.h"
#include "Kernel/LHCbID.h"
#include "Kernel/Trajectory.h"
#include "OTDet/DeOTDetector.h"
#include "OTDet/DeOTModule.h"

// OnXSvc :
#include "OnXSvc/ClassID.h"
#include "OnXSvc/Filter.h"
#include "OnXSvc/Helpers.h"
#include "OnXSvc/IUserInterfaceSvc.h"

DECLARE_CONVERTER( SoOTClusterCnv )

//////////////////////////////////////////////////////////////////////////////
SoOTClusterCnv::SoOTClusterCnv( ISvcLocator* aSvcLoc )
    : SoEventConverter( aSvcLoc, SoOTClusterCnv::classID() )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{}
//////////////////////////////////////////////////////////////////////////////
StatusCode SoOTClusterCnv::createRep( DataObject* aObject, IOpaqueAddress*& aAddr )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  MsgStream log( msgSvc(), "SoOTClusterCnv" );
  // log << MSG::INFO << "SoOTCluster::createRep." << endmsg;

  if ( !fUISvc ) {
    log << MSG::INFO << " UI service not found" << endmsg;
    return StatusCode::SUCCESS;
  }

  if ( !fParticlePropertySvc ) {
    log << MSG::INFO << " ParticleProperty service not found" << endmsg;
    return StatusCode::SUCCESS;
  }

  ISession* session = fUISvc->session();
  if ( !session ) {
    log << MSG::INFO << " can't get ISession." << endmsg;
    return StatusCode::FAILURE;
  }

  SoRegion* region = 0;
  if ( aAddr ) {
    // Optimization.
    // If having a not null aAddr, we expect a SoRegion.
    // See SoEvent/Type.h/SoEvent::Type<>::beginVisualize.
    region = (SoRegion*)aAddr;
  } else {
    region = fUISvc->currentSoRegion();
  }
  if ( !region ) {
    log << MSG::INFO << " can't get viewing region." << endmsg;
    return StatusCode::FAILURE;
  }

  if ( !aObject ) {
    log << MSG::INFO << " NULL object." << endmsg;
    return StatusCode::FAILURE;
  }

  LHCb::OTTimes* times = dynamic_cast<LHCb::OTTimes*>( aObject );
  if ( !times ) {
    log << MSG::INFO << " bad object type." << endmsg;
    return StatusCode::FAILURE;
  }

  // bool deleteVector = false;
  // Filter :
  Filter<LHCb::OTTime> filter( *fUISvc, log );
  const std::string&   cuts = fUISvc->cuts();
  if ( cuts != "" ) {
    log << MSG::INFO << " cuts \"" << cuts << "\"" << endmsg;
    times = filter.collect( *times, "OTTime", cuts );
    if ( !times ) return StatusCode::SUCCESS;
    // filter.dump(*times,"OTTIme");
    // deleteVector = true;;
  }

  if ( !times->size() ) {
    log << MSG::INFO << " collection is empty." << endmsg;
    return StatusCode::SUCCESS;
  }

  // get geometry
  SmartDataPtr<DeOTDetector> tracker( fDetectorDataSvc, DeOTDetectorLocation::Default );
  if ( !tracker ) {
    log << MSG::ERROR << "Unable to retrieve OT detector element"
        << " from xml." << endmsg;
    return StatusCode::FAILURE;
  }

  // Representation attributes :
  // Get color (default is grey (valid on black or white background) ):
  std::string value;
  double      r = 0.5, g = 0.5, b = 0.5;
  if ( session->parameterValue( "modeling.color", value ) ) Lib::smanip::torgb( value, r, g, b );
  double hr = 1.0, hg = 1.0, hb = 0.0;
  if ( session->parameterValue( "modeling.highlightColor", value ) ) Lib::smanip::torgb( value, hr, hg, hb );
  double lineWidth = 0;
  if ( session->parameterValue( "modeling.lineWidth", value ) )
    if ( !Lib::smanip::todouble( value, lineWidth ) ) lineWidth = 0;

  SoStyleCache* styleCache = region->styleCache();
  SoLightModel* lightModel = styleCache->getLightModelBaseColor();
  SoDrawStyle*  drawStyle  = styleCache->getLineStyle( SbLinePattern_solid, float( lineWidth ) );
  SoMaterial*   highlightMaterial =
      styleCache->getHighlightMaterial( float( r ), float( g ), float( b ), float( hr ), float( hg ), float( hb ) );
  SoMaterial* topCircHighlightMaterial =
      styleCache->getHighlightMaterial( float( 0. ), float( 0. ), float( 1.0 ), float( hr ), float( hg ), float( hb ) );
  SoDrawStyle* topCircDrawStyle = styleCache->getLineStyle( SbLinePattern_solid, float( lineWidth ) );
  SoMaterial*  botCircHighlightMaterial =
      styleCache->getHighlightMaterial( float( 0. ), float( 1.0 ), float( 0. ), float( hr ), float( hg ), float( hb ) );
  SoDrawStyle* botCircDrawStyle = styleCache->getLineStyle( SbLinePattern_solid, float( lineWidth ) );

  SoSeparator* separator = new SoSeparator;

  SoCoordinate3* coordinate3 = new SoCoordinate3;
  separator->addChild( coordinate3 );
  int     icoord = 0;
  int32_t coordIndex[3];
  SbBool  empty = TRUE;

  bool        circles = false;
  std::string drifttimes;
  if ( session->parameterValue( "modeling.OTRawDriftTimes", drifttimes ) ) {
    if ( drifttimes == "true" ) { circles = true; }
  }
  bool        wires = true;
  std::string driftwires;
  if ( session->parameterValue( "modeling.OTRawWires", driftwires ) ) {
    if ( driftwires == "false" ) { wires = false; }
  }

  LHCb::OTTimes::iterator it;
  for ( it = times->begin(); it != times->end(); it++ ) {
    LHCb::OTTime* time = ( *it );
    if ( !time ) continue;

    LHCb::OTChannelID channel = time->channel();

    DeOTModule* module = tracker->findModule( channel );
    if ( !module ) {
      log << MSG::INFO << "channel without module" << endmsg;
      continue;
    }

    // Build name :
    char sid[64];
    ::sprintf( sid, "OTTime/0x%lx", (unsigned long)time );

    const LHCb::LHCbID lhcbid = LHCb::LHCbID( channel );
    auto               traj   = tracker->trajectory( lhcbid );

    // Add lines that show which wires were hit
    if ( wires ) {
      // endpoints of trajectory
      Gaudi::XYZPoint start = traj->beginPoint();
      Gaudi::XYZPoint stop  = traj->endPoint();

      SoSceneGraph* sep = new SoSceneGraph;
      sep->setString( sid );

      separator->addChild( sep );

      sep->addChild( highlightMaterial );

      sep->addChild( lightModel );
      sep->addChild( drawStyle );

      int     pointn = 2;
      SbVec3f points[2];
      points[0].setValue( start.x(), start.y(), start.z() );
      points[1].setValue( stop.x(), stop.y(), stop.z() );
      coordIndex[0] = icoord + 0;
      coordIndex[1] = icoord + 1;
      coordIndex[2] = SO_END_LINE_INDEX;

      coordinate3->point.setValues( icoord, pointn, points );
      icoord += pointn;

      SoIndexedLineSet* lineSet = new SoIndexedLineSet;
      lineSet->coordIndex.setValues( 0, pointn + 1, coordIndex );
      sep->addChild( lineSet );
    }

    // Add the circles that indicate drifttimes
    if ( circles ) {
      int    circpointn = 24;
      double dangle     = 2 * M_PI / ( circpointn - 1 );

      for ( int ncirc = 0; ncirc < 2; ncirc++ ) {

        SbVec3f* circpoints = new SbVec3f[circpointn];

        double          maxprop    = traj->arclength() / module->propagationVelocity();
        double          caltime    = time->calibratedTime();
        bool            drawcircle = true;
        double          r          = 0.;
        Gaudi::XYZPoint cpoint;
        SoSeparator*    circSep = 0;
        if ( ncirc == 0 ) { // Circle at endpoint
          circSep = new SoSeparator;
          circSep->addChild( topCircHighlightMaterial );
          circSep->addChild( topCircDrawStyle );
          r      = module->driftRadiusWithError( caltime ).val;
          cpoint = traj->endPoint();
        } else if ( caltime <= maxprop ) { // No need for a second circle
          drawcircle = false;
        } else { // Circle at startpoint
          circSep = new SoSeparator;
          circSep->addChild( botCircHighlightMaterial );
          circSep->addChild( botCircDrawStyle );
          r      = module->driftRadiusWithError( caltime - maxprop ).val;
          cpoint = traj->beginPoint();
        }

        if ( drawcircle ) {
          double            centermu = ( traj->endRange() - traj->beginRange() ) / 2.;
          Gaudi::XYZVector  direct   = traj->direction( centermu );
          Gaudi::Rotation3D rot( Gaudi::AxisAngle( direct, 0. ) );
          for ( int index = 0; index < circpointn; index++ ) {
            double           angCircle = dangle * index;
            Gaudi::XYZVector v1( r * cos( angCircle ), 0., r * sin( angCircle ) );
            // now rotate in space that it becomes perpendicular to the trajectory
            Gaudi::XYZVector v2 = rot * v1;
            circpoints[index].setValue( float( v2.x() + cpoint.x() ), float( v2.y() + cpoint.y() ),
                                        float( v2.z() + cpoint.z() ) );
          }
          SoCoordinate3* circleCoordinate3 = new SoCoordinate3;
          circleCoordinate3->point.setValues( 0, circpointn, circpoints );
          SoLineSet* circLineSet = new SoLineSet;
          circLineSet->numVertices.setValues( 0, 1, &circpointn );
          circSep->addChild( lightModel );
          // 	circSep->addChild(drawStyle);
          circSep->addChild( circleCoordinate3 );
          circSep->addChild( circLineSet );

          region_addToDynamicScene( *region, circSep );
        }
      }
    }

    empty = FALSE;
  }

  if ( empty == TRUE ) {
    separator->unref();
  } else {
    //  Send scene graph to the viewing region
    // (in the "dynamic" sub-scene graph) :
    region_addToDynamicScene( *region, separator );
  }

  return StatusCode::SUCCESS;
}
//////////////////////////////////////////////////////////////////////////////
const CLID& SoOTClusterCnv::classID()
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return LHCb::OTTimes::classID();
}
//////////////////////////////////////////////////////////////////////////////
unsigned char SoOTClusterCnv::storageType()
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return So_TechnologyType;
}
