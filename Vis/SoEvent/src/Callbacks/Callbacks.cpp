/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//
//  All functions here should be OnX callbacks, that is to say
// functions with signature :
//   extern "C" {
//     void callback_without_arguments(IUI&);
//     void callback_with_arguments(IUI&,const std::vector<std::string>&);
//   }
//

#include "Helpers.h"

// Lib :
#include <Lib/Out.h>

// OnX :
#include <OnX/Interfaces/IUI.h>

// Event model :
#include "Event/MCHeader.h"
#include "Event/ODIN.h"

extern "C" {

//////////////////////////////////////////////////////////////////////////////
void SoEvent_dump_header( IUI& aUI )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  ISession& session = aUI.session();
  Lib::Out  out( session.printer() );

  IDataProviderSvc* dataProviderSvc = find_eventDataSvc( session );
  if ( !dataProviderSvc ) {
    out << "SoEvent/dump_header :"
        << " no data provider found." << Lib::endl;
    return;
  }

  std::string location   = "/Event/" + LHCb::MCHeaderLocation::Default;
  DataObject* dataObject = get_dataObject( *dataProviderSvc, location );
  if ( dataObject ) {
    LHCb::MCHeader* mch = dynamic_cast<LHCb::MCHeader*>( dataObject );
    if ( !mch ) {
      out << "SoEvent/dump_header :"
          << " object not an MCHeader." << Lib::endl;
      return;
    }

    out << " Run number : " << mch->runNumber() << " Event number : " << mch->evtNumber() << Lib::endl;

  } else {
    location   = "/Event/" + LHCb::ODINLocation::Default;
    dataObject = get_dataObject( *dataProviderSvc, location );
    if ( dataObject ) {
      LHCb::ODIN* odin = dynamic_cast<LHCb::ODIN*>( dataObject );
      if ( !odin ) {
        out << "SoEvent/dump_header :"
            << " object not an MCHeader." << Lib::endl;
        return;
      }

      out << " Run number : " << odin->runNumber() << Lib::endl;
    }
  }
  if ( !dataObject ) {
    out << "SoEvent/dump_header :"
        << " no header found." << Lib::endl;
    return;
  }
}

//////////////////////////////////////////////////////////////////////////////
void SoEvent_exa_vis_MCParticles( IUI& aUI )
//////////////////////////////////////////////////////////////////////////////
// Example of visualization of MCParticle one by one.
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  IService* service = find_service( aUI.session(), "EventDataSvc" );
  if ( !service ) {
    aUI.echo( "EventDataSvc not found." );
    return;
  }

  void*      p      = 0;
  StatusCode status = service->queryInterface( IID_IDataProviderSvc, &p );
  if ( !status.isSuccess() ) return;
  IDataProviderSvc* eds = (IDataProviderSvc*)p;

  DataObject* dataObject;
  status = eds->retrieveObject( "/Event/MC/Particles", dataObject );
  if ( !status.isSuccess() ) {
    aUI.echo( "Unable to retrieve /Event/MC/Particles." );
    return;
  }
  if ( !dataObject ) {
    aUI.echo( "Unable to retrieve /Event/MC/Particles." );
    return;
  }

  ISoConversionSvc* soCnvSvc = find_soCnvSvc( aUI.session() );
  if ( !soCnvSvc ) return;

  LHCb::MCParticles* mcParticles = dynamic_cast<LHCb::MCParticles*>( dataObject );
  if ( !mcParticles ) {
    aUI.echo( "Can't dynamic_cast dataObject to LHCb::MCParticles." );
    return;
  }

  // mcParticles->size();

  LHCb::MCParticles::iterator it;
  for ( it = mcParticles->begin(); it != mcParticles->end(); it++ ) {
    LHCb::MCParticle* mcParticle = ( *it );
    KO_visualize<LHCb::MCParticle>( *soCnvSvc, *mcParticle );
  }
}
}
