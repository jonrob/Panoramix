/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef SoEvent_RecVertexType_h
#define SoEvent_RecVertexType_h

// Inheritance :
#include "Kernel/IParticlePropertySvc.h"
#include "Type.h"

#include "Event/RecVertex.h" // The data class.

class IUserInterfaceSvc;
class ISoConversionSvc;
class IDataProviderSvc;
class IParticlePropertySvc;

class RecVertexType : public SoEvent::Type<LHCb::RecVertex> {
public: // Lib::IType
  virtual Lib::Variable value( Lib::Identifier, const std::string&, void* );

public:
  RecVertexType( IUserInterfaceSvc*, ISoConversionSvc*, IDataProviderSvc*, LHCb::IParticlePropertySvc* );
  Lib::IIterator* iterator();

private:
  LHCb::IParticlePropertySvc* fParticlePropertySvc;
};

#endif
