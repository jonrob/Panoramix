/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <OnXSvc/ISoConversionSvc.h>

#include <GaudiKernel/IDataProviderSvc.h>
#include <GaudiKernel/KeyedContainer.h>
#include <GaudiKernel/Range.h>
#include <GaudiKernel/SmartDataPtr.h>
#include <map>

#if ROOT_VERSION_CODE < ROOT_VERSION( 6, 0, 0 )
struct _STLAddRflx_Map_Instances {
  std::map<int, unsigned int>    m_1;
  std::map<unsigned int, double> m_2;
};
#endif

// following is needed for visualizing individual objects
namespace SoEvent {

  template <class T>
  struct KO {
  public:
    // If object is a KeyedObject then collection should be a KeyedContainer.
    static void visualize( ISoConversionSvc& aSoCnvSvc, T& aObject ) {
      typedef KeyedContainer<T> Collection;
      Collection*               collection = new Collection;
      collection->add( &aObject );
      // Convert it :
      IOpaqueAddress* addr = 0;
      StatusCode      sc   = aSoCnvSvc.createRep( collection, addr );
      if ( sc.isSuccess() ) sc = aSoCnvSvc.fillRepRefs( addr, collection );
      collection->remove( &aObject ); // Else the next line delete the object.
      delete collection;
    }
  };

  template <class T>
  struct CO {
  public:
    // If object is a ContainedObject then collection should be an ObjectVector.
    static void visualize( ISoConversionSvc& aSoCnvSvc, T& aObject ) {
      typedef ObjectVector<T> Collection;
      Collection*             collection = new Collection;
      collection->add( &aObject );
      // Convert it :
      IOpaqueAddress* addr = 0;
      StatusCode      sc   = aSoCnvSvc.createRep( collection, addr );
      if ( sc.isSuccess() ) sc = aSoCnvSvc.fillRepRefs( addr, collection );
      collection->remove( &aObject ); // Else the next line delete the object.
      delete collection;
    }
  };

} // namespace SoEvent

//--- Templace instantiations

#include "CaloUtils/ICaloElectron.h"
#include "GaudiKernel/Map.h"
#include "HepMC/GenParticle.h"
#include "HepMC/GenVertex.h"
#include <DetDesc/ILVolume.h>
#include <Event/FTCluster.h>
#include <Event/FTLiteCluster.h>
#include <Event/MCHit.h>
#include <Event/MCParticle.h>
#include <Event/MuonCoord.h>
#include <Event/Node.h>
#include <Event/OTTime.h>
#include <Event/Particle.h>
#include <Event/RecVertex.h>
#include <Event/STCluster.h>
#include <Event/Track.h>
#include <Event/VPCluster.h>
#include <Event/VeloCluster.h>
#include <Event/VertexBase.h>
#include <GaudiKernel/HashMap.h>
#include <VeloDet/DeVeloSensor.h>

#include "Event/MCHit.h"
#include "Event/OTTime.h"
#include "Event/STCluster.h"
#include "Linker/LinkedFrom.h"
#include "Linker/LinkedTo.h"
#include "MuonDet/DeMuonDetector.h"
#include "TrackInterfaces/IMaterialLocator.h"
// temporary
#include "Kernel/DecayTree.h"
#include "SoUtils/HltUtils.h"
//#include <DetDesc/MagneticFieldGrid.h>
#include "Kernel/FastClusterContainer.h"
#include "L0MuonKernel/ProcUtilities.h"
#include "LumiAlgs/ILumiIntegrator.h"

namespace SoEvent_dict_Instantiations
// this is needed for object_visualize
{
  // Object is a KeyedObject then collection is a KeyedContainer :
  SoEvent::KO<LHCb::Vertex>      m_18;
  SoEvent::KO<LHCb::MCParticle>  m_4;
  SoEvent::KO<LHCb::RecVertex>   m_17;
  SoEvent::KO<LHCb::VertexBase>  m_19;
  SoEvent::KO<LHCb::Particle>    m_11;
  SoEvent::KO<LHCb::VeloCluster> m_16;
  SoEvent::KO<LHCb::VPCluster>   m_99;
  SoEvent::KO<LHCb::FTCluster>   m_97;
  SoEvent::KO<LHCb::MuonCoord>   m_6;
  SoEvent::KO<LHCb::OTTime>      m_10;
  SoEvent::KO<LHCb::STCluster>   m_20;
  SoEvent::KO<LHCb::Track>       m_21;
  SoEvent::KO<LHCb::CaloDigit>   m_22;

  // other things

  std::map<LHCb::MCParticle*, unsigned int>                                                   g_3;
  GaudiUtils::Map<LHCb::MCParticle*, unsigned int, std::map<LHCb::MCParticle*, unsigned int>> g_1;

  std::map<std::pair<int, unsigned>, unsigned>           g_6;
  std::map<std::pair<int, unsigned>, unsigned>::iterator i_6;
  std::pair<const std::pair<int, unsigned>, unsigned>    p_6;
  std::pair<int, unsigned>                               p_7;
  LHCb::FTLiteCluster                                    ft_1;
  FastClusterContainer<LHCb::FTLiteCluster, int>         ft_0;
  // FastClusterContainer<LHCb::VeloLiteCluster,int>  vl_0;
  DeVeloSensor::StripInfo d_1;
  LHCb::VPCluster         vp_0;
  std::vector<IPVolume*>  pv_1;

  // Object is a ContainedObject then collection is an ObjectVector :
  SoEvent::CO<LHCb::MCHit> m_2;

  LinkedTo<LHCb::MCHit, LHCb::VeloCluster>   l1( 0, 0, "" );
  LinkedTo<LHCb::MCHit, LHCb::STCluster>     l2( 0, 0, "" );
  LinkedTo<LHCb::MCHit, ContainedObject>     l3( 0, 0, "" );
  LinkedFrom<LHCb::VeloCluster, LHCb::MCHit> l11( 0, 0, "" );
  LinkedFrom<LHCb::STCluster, LHCb::MCHit>   l12( 0, 0, "" );
  LinkedFrom<ContainedObject, LHCb::MCHit>   l13( 0, 0, "" );

  IMaterialLocator::Intersections              i_1;
  std::vector<IMaterialLocator::Intersections> iv_1;

  std::vector<const LHCb::Node*>                vnode_1;
  Gaudi::Range_<std::vector<const LHCb::Node*>> rnode_1;
} // namespace SoEvent_dict_Instantiations
