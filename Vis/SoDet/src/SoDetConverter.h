/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef SoDet_SoDetConverter_h
#define SoDet_SoDetConverter_h

#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/Transform3DTypes.h"
#include "GaudiKernel/Vector3DTypes.h"

// CRJ : Following is needed until using a version of
//       LHCbDefinitions that includes this
#include "Math/Quaternion.h"
namespace Gaudi {
  typedef ROOT::Math::Quaternion Quaternion;
}

#include <string>

#include <GaudiKernel/Converter.h>

class ISoConversionSvc;
class IUserInterfaceSvc;
class ISolid;
class ILVolume;
class ISvcLocator;
class IVisualizationSvc;
class MsgStream;

class SbPolyhedron;
class SoNode;
class SoStyleCache;

class SoDetConverter : public Converter {
public:
  virtual StatusCode initialize();
  virtual StatusCode finalize();
  virtual long       repSvcType() const;

public:
  SoDetConverter( ISvcLocator*, CLID );
  virtual ~SoDetConverter();
  SoNode*    volumeToSoDetectorTreeKit( const ILVolume&, const Gaudi::Transform3D&, bool = false );
  MsgStream* out();

private:
  SbPolyhedron* solidToSbPolyhedron( const ISolid& );
  SbPolyhedron* simpleSolidToSbPolyhedron( const ISolid& );
  SbPolyhedron* booleanSolidToSbPolyhedron( const ISolid& );

protected:
  ISoConversionSvc*  fSoCnvSvc;
  IUserInterfaceSvc* fUISvc;

private:
  IVisualizationSvc* m_visSvc;
  MsgStream*         m_out;
  SoStyleCache*      fStyleCache;
  int                fRotationSteps;
  bool               fDoBooleanOperation;
};

#endif
