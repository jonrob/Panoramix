/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef SoDet_SoDetSvc_h
#define SoDet_SoDetSvc_h

// Gaudi :
#include <GaudiKernel/Service.h>

class IUserInterfaceSvc;
class IMagneticFieldSvc;

class SoDetSvc : public Service {
public: // IService
  virtual StatusCode initialize();
  virtual StatusCode finalize();
  SoDetSvc( const std::string&, ISvcLocator* );
  virtual ~SoDetSvc();

private:
  IUserInterfaceSvc* m_uiSvc;
  IMagneticFieldSvc* m_magneticFieldSvc;
};

#endif
