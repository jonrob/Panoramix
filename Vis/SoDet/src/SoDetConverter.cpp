/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// To fix clashes between Gaudi and Windows :
#include <OnXSvc/Win32.h>

// this :
#include "SoDetConverter.h"

// Inventor :
#include <Inventor/SbColor.h>
#include <Inventor/SoPickedPoint.h>
#include <Inventor/nodes/SoEventCallback.h>
#include <Inventor/nodes/SoLightModel.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoTransform.h>
//#include <Inventor/nodes/SoCube.h>
#include <Inventor/events/SoMouseButtonEvent.h>

// HEPVis :
#include <HEPVis/SbMath.h>
#include <HEPVis/SbPolyhedron.h>
#include <HEPVis/misc/SoStyleCache.h>
#include <HEPVis/nodes/SoPolyhedron.h>
#include <HEPVis/nodes/SoSceneGraph.h>
//#include <HEPVis/nodes/SoHighlightMaterial.h>
#include <HEPVis/nodekits/SoDetectorTreeKit.h>

#include <Lib/Interfaces/ISession.h>
#include <Lib/smanip.h>

#include <GaudiKernel/Transform3DTypes.h>

#include <OnXSvc/ClassID.h>
#include <OnXSvc/ISoConversionSvc.h>
#include <OnXSvc/IUserInterfaceSvc.h>

#include <GaudiKernel/ISvcLocator.h>
#include <GaudiKernel/MsgStream.h>

#include <DetDesc/ILVolume.h>
#include <DetDesc/IPVolume.h>
#include <DetDesc/ISolid.h>
#include <DetDesc/Material.h>
#include <DetDesc/Solids.h>

#include <VisSvc/IVisualizationSvc.h>

class SoDetTreeKit : public SoDetectorTreeKit {
  const ILVolume& m_lvolume;
  bool            m_modeling;
  bool            m_expanded;
  SoDetConverter* m_soConverter;

public:
  SoDetTreeKit( const ILVolume& aLVolume, bool aModeling, SoDetConverter* soConverter )
      : m_lvolume( aLVolume ), m_modeling( aModeling ), m_expanded( false ), m_soConverter( soConverter ) {}
  virtual ~SoDetTreeKit() {}
  const ILVolume& lvolume() { return m_lvolume; }
  bool            modeling() { return m_modeling; }
  bool            expanded() { return m_expanded; }
  SoDetConverter* soConverter() { return m_soConverter; }
  void            setExpanded( bool aValue ) { m_expanded = aValue; }
};

void expand( void* userData, SoEventCallback* eventCB );
void contract( void* userData, SoEventCallback* eventCB );

#define None ( -1 )

//////////////////////////////////////////////////////////////////////////////
SoDetConverter::SoDetConverter( ISvcLocator* aSvcLoc, CLID clid )
    : Converter( So_TechnologyType, clid, aSvcLoc )
    , fSoCnvSvc( 0 )
    , fUISvc( 0 )
    , m_visSvc( 0 )
    , m_out( 0 )
    , fStyleCache( 0 )
    , fRotationSteps( None )
    , fDoBooleanOperation( true )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  m_out         = new MsgStream( msgSvc(), "SoDetConverter" );
  StatusCode sc = aSvcLoc->service( "VisualizationSvc", m_visSvc, true );
  if ( !sc.isSuccess() ) { ( *m_out ) << MSG::WARNING << "Unable to get VisualizationSvc" << endmsg; }
  fStyleCache = new SoStyleCache;
  fStyleCache->ref();
}
//////////////////////////////////////////////////////////////////////////////
SoDetConverter::~SoDetConverter()
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  // printf("debug : SoStyleCache children %d\n",fStyleCache->getNumChildren());
  fStyleCache->unref();

  delete m_out;
}
//////////////////////////////////////////////////////////////////////////////
StatusCode SoDetConverter::initialize()
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  StatusCode status = Converter::initialize();
  if ( status.isFailure() ) return status;
  status = serviceLocator()->service( "SoConversionSvc", fSoCnvSvc );
  if ( status.isFailure() ) return status;
  status = serviceLocator()->service( "OnXSvc", fUISvc );
  if ( status.isFailure() ) return status;
  return status;
}
//////////////////////////////////////////////////////////////////////////////
StatusCode SoDetConverter::finalize()
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  // The below "release()" induce crash at exit :
  // debug if(fSoCnvSvc) fSoCnvSvc->release();
  // debug if(fUISvc) fUISvc->release();
  return StatusCode::SUCCESS;
}
//////////////////////////////////////////////////////////////////////////////
long SoDetConverter::repSvcType() const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return i_repSvcType();
}
//////////////////////////////////////////////////////////////////////////////
MsgStream* SoDetConverter::out() { return m_out; }
//////////////////////////////////////////////////////////////////////////////
SoNode* SoDetConverter::volumeToSoDetectorTreeKit( const ILVolume& aLVolume, const Gaudi::Transform3D& aMatrix,
                                                   bool aOpened )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  ISession* session = fUISvc->session();
  if ( !session ) return 0;

  MsgStream log( msgSvc(), "SoDetConverter" );

  // Get visualization attributes :
  std::string value;
  double      r = 0.5, g = 0.5, b = 0.5;
  // double hr = 1.0, hg = 1.0, hb = 1.0;
  double transparency  = 0;
  bool   modelingSolid = true;
  fRotationSteps       = None;
  // Look in the ISession :
  if ( session->parameterValue( "modeling.color", value ) )
    if ( !Lib::smanip::torgb( value, r, g, b ) ) {
      r = 0.5;
      g = 0.5;
      b = 0.5;
    }
  if ( session->parameterValue( "modeling.transparency", value ) )
    if ( !Lib::smanip::todouble( value, transparency ) ) transparency = 0;
  // if(session->parameterValue("modeling.highlightColor",value))
  //  if(!Lib::smanip::torgb(value,hr,hg,hb)) { hr = 1; hg = 1; hb = 1;}
  if ( session->parameterValue( "modeling.modeling", value ) ) {
    modelingSolid = ( value == "solid" ? true : false );
  } else { // Backward compatibility
    if ( session->parameterValue( "modeling.type", value ) ) { modelingSolid = ( value == "solid" ? true : false ); }
  }
  if ( session->parameterValue( "modeling.rotationSteps", value ) ) {
    if ( !Lib::smanip::toint( value, fRotationSteps ) ) fRotationSteps = None;
  }
  if ( session->parameterValue( "modeling.doBooleanOperation", value ) ) {
    if ( !Lib::smanip::tobool( value, fDoBooleanOperation ) ) fDoBooleanOperation = true;
  }

  bool modelingOpened  = aOpened;
  bool modelingVisible = true;

  // Override with VisSvc is needed :
  bool useVisSvc = true;
  if ( session->parameterValue( "modeling.useVisSvc", value ) ) {
    if ( !Lib::smanip::tobool( value, useVisSvc ) ) useVisSvc = true;
  }

  VisAttribute attr;
  if ( m_visSvc && useVisSvc ) {
    attr              = m_visSvc->visAttribute( &aLVolume );
    const Color color = attr.color();
    if ( color.isValid() ) {
      r            = color.red();
      g            = color.green();
      b            = color.blue();
      transparency = color.alpha();
    }
    if ( attr.displayMode() == VisAttribute::WIRE_FRAME ) { modelingSolid = false; }
    if ( attr.openStatus() == VisAttribute::OPENED ) { modelingOpened = true; }
    if ( attr.visible() == VisAttribute::NOT_VISIBLE ) { modelingVisible = false; }
  }

  log << MSG::DEBUG << " opened " << modelingOpened << endmsg;

  // Logical volume
  SoNode*       node  = 0;
  const ISolid* solid = aLVolume.solid();
  if ( !solid ) {
    // This may happen.
  } else {
    SbPolyhedron* result = solidToSbPolyhedron( *solid );
    if ( result ) {
      SoPolyhedron* soPolyhedron = new SoPolyhedron( *result );
      delete ( result );
      soPolyhedron->solid.setValue( modelingSolid );
      node = soPolyhedron;
    }
  }

  char sid[64];
  ::sprintf( sid, "LVolume/0x%lx", (unsigned long)&aLVolume );

  // Deal with the Transformation :
  Gaudi::Rotation3D rot;
  Gaudi::XYZVector  c;
  aMatrix.GetDecomposition( rot, c );
  c = c * -1.0;
  rot.Invert();
  // Gaudi::AxisAngle axisAngle(rot);
  const Gaudi::Quaternion q( rot );
  Gaudi::AxisAngle        axisAngle( q );
  double                  angle;
  Gaudi::XYZVector        v;
  axisAngle.GetComponents( v, angle );

  int childn = aLVolume.noPVolumes();
  log << MSG::DEBUG << " childn " << childn << " node " << node << endmsg;
  if ( childn == 0 ) {

    //  Do not put an SoDetectorTreeKit here.
    // If so, "expanding" (with "ctrl-pick") will lead to an empty
    // scene without the possibility to "contract" by "shift-pick" something !

    // Only display if a node is present and if the visualization attribute
    // is ok
    if ( node && modelingVisible ) {
      SoSceneGraph* separator = new SoSceneGraph;
      separator->setString( sid );

      if ( modelingSolid ) {
        separator->addChild( fStyleCache->getMaterial( SbColor( (float)r, (float)g, (float)b ), (float)transparency ) );
        separator->addChild( fStyleCache->getLightModelPhong() );
      } else { // SoLightModel::BASE_COLOR);
        separator->addChild(
            fStyleCache->getMaterial( SbColor( (float)r, (float)g, (float)b ), (float)transparency, TRUE ) );
        separator->addChild( fStyleCache->getLightModelBaseColor() );
      }

      // The volume is placed and rotated :
      SbMatrix mtx;
      mtx.makeIdentity();
      {
        SbMatrix tmp;
        tmp.setRotate( SbRotation( SbVec3f( (float)v.x(), (float)v.y(), (float)v.z() ), (float)angle ) );
        mtx.multLeft( tmp );
      }
      {
        SbMatrix tmp;
        tmp.setTranslate( SbVec3f( (float)c.x(), (float)c.y(), (float)c.z() ) );
        mtx.multLeft( tmp );
      } // Applied first.

      SoTransform* transform = new SoTransform;
      transform->setMatrix( mtx ); // will set the SoFields (needed for VRML).

      // The volume is placed and rotated :
      separator->addChild( transform );
      separator->addChild( node );
      return separator;

    } else {
      if ( node ) node->unref();
      return 0;
    }

  } else {

    SoDetTreeKit* dtk = new SoDetTreeKit( aLVolume, modelingSolid, this );

    // Override the default SoEventCallback :
    SoEventCallback* evtCbk = (SoEventCallback*)dtk->getPart( "callbackList[0]", FALSE );
    if ( evtCbk ) {
      SoEventCallback* eventCallback = new SoEventCallback;

      eventCallback->addEventCallback( SoMouseButtonEvent::getClassTypeId(), expand, dtk );
      eventCallback->addEventCallback( SoMouseButtonEvent::getClassTypeId(), contract, dtk );
      if ( dtk->setPart( "callbackList[0]", eventCallback ) == FALSE ) eventCallback->unref();
    }

    // The volume is placed and rotated :
    SbMatrix mtx;
    mtx.makeIdentity();
    {
      SbMatrix tmp;
      tmp.setRotate( SbRotation( SbVec3f( (float)v.x(), (float)v.y(), (float)v.z() ), (float)angle ) );
      mtx.multLeft( tmp );
    }
    {
      SbMatrix tmp;
      tmp.setTranslate( SbVec3f( (float)c.x(), (float)c.y(), (float)c.z() ) );
      mtx.multLeft( tmp );
    } // Applied first.

    SoTransform* transform = (SoTransform*)dtk->getPart( "transform", TRUE );
    transform->setMatrix( mtx ); // will set the SoFields (needed for VRML).

    // Only display if a node is present and if the visualization attributes
    // are ok (visibility and open status)

    SoSeparator* previewSeparator = (SoSeparator*)dtk->getPart( "previewSeparator", TRUE );

    bool childOpened = false;
    if ( node && modelingVisible ) {

      SoSceneGraph* separator = new SoSceneGraph;
      separator->setString( sid );

      if ( modelingSolid ) {
        separator->addChild( fStyleCache->getMaterial( SbColor( (float)r, (float)g, (float)b ), (float)transparency ) );
        separator->addChild( fStyleCache->getLightModelPhong() );
      } else { // SoLightModel::BASE_COLOR);
        separator->addChild(
            fStyleCache->getMaterial( SbColor( (float)r, (float)g, (float)b ), (float)transparency, TRUE ) );
        separator->addChild( fStyleCache->getLightModelBaseColor() );
      }

      separator->addChild( new SoTransform ); // For manipulators.
      separator->addChild( node );

      previewSeparator->addChild( separator );

      if ( modelingOpened ) dtk->setPreview( FALSE );
    } else {
      dtk->setPreview( FALSE );
      if ( node )
        node->unref();
      else
        childOpened = aOpened;
    }

    SoSeparator* fullSeparator = (SoSeparator*)dtk->getPart( "fullSeparator", TRUE );

    // Only display children if no node present or the node is opened
    if ( !node || modelingOpened ) {
      // Descend to find a level with something to represent.
      for ( int count = 0; count < childn; count++ ) {
        const IPVolume* pvolume = aLVolume[count];
        if ( !pvolume ) {
          log << MSG::ERROR << "found a null pvolume in " << aLVolume.name() << " at index " << count
              << ". Check the name of the logical volume associated to it" << endmsg;
        } else {
          if ( !pvolume->lvolume() ) {
            log << MSG::ERROR << "pvolume with a null lvolume." << endmsg;
          } else {
            SoNode* child = volumeToSoDetectorTreeKit( *( pvolume->lvolume() ), pvolume->matrix(), childOpened );
            if ( child ) fullSeparator->addChild( child );
          }
        }
      }
    }

    if ( fullSeparator->getNumChildren() == 0 ) {
      // Nothing to visualize in daughters.
      dtk->setPreview( TRUE );
      if ( previewSeparator->getNumChildren() == 0 ) {
        // Nothing to preview too.
        // Can't have an SoDetectorTreeKit, else "expanding" it
        // will lead to an empty scene.
        dtk->unref();
        return 0;
      }
    }
    // Note that we can have a dtk with daughters but
    // with nothing to preview. Have to protect the "contract".

    return dtk;
  }
}
//////////////////////////////////////////////////////////////////////////////
SbPolyhedron* SoDetConverter::solidToSbPolyhedron( const ISolid& aSolid )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  SbPolyhedron* result = simpleSolidToSbPolyhedron( aSolid );
  if ( !result ) result = booleanSolidToSbPolyhedron( aSolid );
  if ( !result ) {
    std::string type = aSolid.typeName();
    MsgStream   log( msgSvc(), "SoDetConverter::solidToSbPolyhedron" );
    log << MSG::INFO << " type " << type << " not treated." << endmsg;
  }
  return result;
}
//////////////////////////////////////////////////////////////////////////////
SbPolyhedron* SoDetConverter::simpleSolidToSbPolyhedron( const ISolid& aSolid )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
#ifdef WIN32 // FIXME. Waiting code of HEPVis of OSC-v15r0p2
  // SbPolyhedron::GetNumberOfRotationSteps() is unusable on WIN32.
  int oldRotationSteps = DEFAULT_NUMBER_OF_STEPS;
#else
  int oldRotationSteps = SbPolyhedron::GetNumberOfRotationSteps();
#endif

  if ( fRotationSteps != None ) SbPolyhedron::SetNumberOfRotationSteps( fRotationSteps );

  SbPolyhedron* result = 0;

  if ( const SolidBox* box = dynamic_cast<const SolidBox*>( &aSolid ) ) {
    result = new SbPolyhedronBox( box->xHalfLength(), box->yHalfLength(), box->zHalfLength() );
  } else if ( const SolidCons* cons = dynamic_cast<const SolidCons*>( &aSolid ) ) {
    result = new SbPolyhedronCons( cons->innerRadiusAtMinusZ(), cons->outerRadiusAtMinusZ(), cons->innerRadiusAtPlusZ(),
                                   cons->outerRadiusAtPlusZ(), cons->zHalfLength(), cons->startPhiAngle(),
                                   cons->deltaPhiAngle() );
  } else if ( const SolidPolycone* cons = dynamic_cast<const SolidPolycone*>( &aSolid ) ) {
    unsigned int nb = cons->number();
    if ( nb <= 0 ) {
      MsgStream log( msgSvc(), "SoDetConverter::simpleSolidToSbPolyhedron" );
      log << MSG::INFO << " SolidPolycone with null cons->number()." << endmsg;
    } else {
      double*      z           = new double[nb];
      double*      innerRadius = new double[nb];
      double*      outerRadius = new double[nb];
      unsigned int i           = 0;
      for ( SolidPolycone::Iterator it = cons->begin(); it < cons->end(); it++ ) {
        z[i]           = ( *it ).first;
        outerRadius[i] = ( *it ).second.first;
        innerRadius[i] = ( *it ).second.second;
        i++;
      }
      result = new SbPolyhedronPcon( cons->startPhiAngle(), cons->deltaPhiAngle(), nb, z, innerRadius, outerRadius );
      delete[] z;
      delete[] innerRadius;
      delete[] outerRadius;
    }
  } else if ( const SolidSphere* sphere = dynamic_cast<const SolidSphere*>( &aSolid ) ) {

    if ( ( sphere->deltaPhiAngle() <= 0 ) || ( sphere->deltaThetaAngle() <= 0 ) ) {
      MsgStream log( msgSvc(), "SoDetConverter::simpleSolidToSbPolyhedron" );
      log << MSG::INFO << " SolidSphere with null deltaPhiAngle or deltaThetaAngle." << endmsg;
    } else {

      // The default SbPolyhedronSphere algorithm takes
      // SbPolyhedron::DEFAULT_NUMBER_OF_STEPS (24) for a compleete sphere.

      // Have SbPolyhedron::GetNumberOfRotationSteps() steps for the
      // piece of sphere only.

      // Customize the modeling number of rotation steps :
      int nphi   = int( M_PI_2 / sphere->deltaPhiAngle() );
      int ntheta = int( M_PI / sphere->deltaThetaAngle() );

      // int rotationSteps =
      // SbMaximum(nphi,ntheta) * SbPolyhedron::GetNumberOfRotationSteps();

      int rotationSteps = SbMaximum( nphi, ntheta );
#ifdef WIN32 // FIXME. Waiting code of HEPVis of OSC-v15r0p2
      // SbPolyhedron::GetNumberOfRotationSteps() is unusable on WIN32.
      rotationSteps *= ( fRotationSteps != None ? fRotationSteps : DEFAULT_NUMBER_OF_STEPS );
#else
      rotationSteps *= SbPolyhedron::GetNumberOfRotationSteps();
#endif
      if ( rotationSteps <= 0 ) rotationSteps = DEFAULT_NUMBER_OF_STEPS;
      SbPolyhedron::SetNumberOfRotationSteps( rotationSteps );

      result = new SbPolyhedronSphere( sphere->insideRadius(), sphere->outerRadius(), sphere->startPhiAngle(),
                                       sphere->deltaPhiAngle(), sphere->startThetaAngle(), sphere->deltaThetaAngle() );
    }

  } else if ( const SolidTubs* tubs = dynamic_cast<const SolidTubs*>( &aSolid ) ) {

    if ( ( tubs->deltaPhiAngle() <= 0 ) ) {
      MsgStream log( msgSvc(), "SoDetConverter::simpleSolidToSbPolyhedron" );
      log << MSG::INFO << " SolidTubs with null deltaPhiAngle." << endmsg;
    } else {

      // The default SbPolyhedronSphere algorithm takes
      // SbPolyhedron::DEFAULT_NUMBER_OF_STEPS (24) for a compleete tubs.

      // Have SbPolyhedron::GetNumberOfRotationSteps() steps for the
      // piece of tubs only.

      // Customize the modeling number of rotation steps :
      int rotationSteps = int( M_PI_2 / tubs->deltaPhiAngle() );

#ifdef WIN32 // FIXME. Waiting code of HEPVis of OSC-v15r0p2
      // SbPolyhedron::GetNumberOfRotationSteps() is unusable on WIN32.
      rotationSteps *= ( fRotationSteps != None ? fRotationSteps : DEFAULT_NUMBER_OF_STEPS );
#else
      rotationSteps *= SbPolyhedron::GetNumberOfRotationSteps();
#endif
      if ( rotationSteps <= 0 ) rotationSteps = DEFAULT_NUMBER_OF_STEPS;
      SbPolyhedron::SetNumberOfRotationSteps( rotationSteps );

      result = new SbPolyhedronTubs( tubs->innerRadius(), tubs->outerRadius(), tubs->zHalfLength(),
                                     tubs->startPhiAngle(), tubs->deltaPhiAngle() );
    }

  } else if ( const SolidTrd* trd = dynamic_cast<const SolidTrd*>( &aSolid ) ) {
    result = new SbPolyhedronTrd2( trd->xHalfLength1(), trd->xHalfLength2(), trd->yHalfLength1(), trd->yHalfLength2(),
                                   trd->zHalfLength() );
  } else if ( const SolidTrap* trap = dynamic_cast<const SolidTrap*>( &aSolid ) ) {
    result = new SbPolyhedronTrap( trap->zHalfLength(), trap->theta(), trap->phi(), trap->dyAtMinusZ(),
                                   trap->dxAtMinusZMinusY(), trap->dxAtMinusZPlusY(), trap->alphaAtMinusZ(),
                                   trap->dyAtPlusZ(), trap->dxAtPlusZMinusY(), trap->dxAtPlusZPlusY(),
                                   trap->alphaAtPlusZ() );
  } else {
    // Continue without message.
  }

  SbPolyhedron::SetNumberOfRotationSteps( oldRotationSteps );

  return result;
}
//////////////////////////////////////////////////////////////////////////////
SbPolyhedron* SoDetConverter::booleanSolidToSbPolyhedron( const ISolid& aSolid )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  if ( const SolidSubtraction* sub = dynamic_cast<const SolidSubtraction*>( &aSolid ) ) {

    MsgStream log( msgSvc(), "SoDetConverter::booleanSolidToSbPolyhedron" );
    log << MSG::DEBUG << " SolidSubtraction" << endmsg;

    if ( !sub->first() ) return 0;

    SbPolyhedron* sbPoly = solidToSbPolyhedron( *( sub->first() ) );
    if ( !sbPoly ) return 0;
    // if debugging : sbPoly->setName(sub->first()->name().c_str());
    if ( !fDoBooleanOperation ) return sbPoly;

    SbPolyhedronProcessor sbPP;

    // int childn = sub->noChildrens();
    int childn = sub->children().size();
    for ( int count = 0; count < childn; count++ ) {
      if ( !( ( *sub )[count] ) ) {
        delete sbPoly;
        return 0;
      }
      const SolidChild* solidChild = dynamic_cast<const SolidChild*>( ( *sub )[count] );
      if ( !solidChild ) {
        delete sbPoly;
        return 0;
      }
      const ISolid* solidChildSolid = solidChild->solid();
      if ( !solidChildSolid ) {
        delete sbPoly;
        return 0;
      }
      SbPolyhedron* scsPoly = solidToSbPolyhedron( *solidChildSolid );
      if ( !scsPoly ) {
        delete sbPoly;
        return 0;
      }
      // if debugging : scsPoly->setName(solidChildSolid->name().c_str());

      // Rotated then placed (the reverse of the logical/physical placement) :
      Gaudi::XYZVector  trans;
      Gaudi::Rotation3D rot;
      solidChild->matrix().GetDecomposition( rot, trans );
      trans = trans * -1.0;
      rot.Invert();
      // Gaudi::AxisAngle axisAngle(rot);
      const Gaudi::Quaternion q( rot );
      Gaudi::AxisAngle        axisAngle( q );
      Gaudi::XYZVector        v;
      double                  angle;
      axisAngle.GetComponents( v, angle );

      scsPoly->Transform( HEPVis::SbRotation( SbVec3d( 1, 0, 0 ), 0 ), SbVec3d( trans.x(), trans.y(), trans.z() ) );
      scsPoly->Transform( HEPVis::SbRotation( SbVec3d( v.x(), v.y(), v.z() ), angle ), SbVec3d( 0, 0, 0 ) );
      sbPP.push_back( SbPolyhedronProcessor::SUBTRACTION, *scsPoly );

      delete scsPoly;
    }

    if ( sbPP.execute( *sbPoly ) == FALSE ) { log << MSG::INFO << " SolidSubtraction failed" << endmsg; }

    return sbPoly;

  } else if ( const SolidIntersection* inter = dynamic_cast<const SolidIntersection*>( &aSolid ) ) {

    MsgStream log( msgSvc(), "SoDetConverter::booleanSolidToSbPolyhedron" );
    log << MSG::DEBUG << " SolidIntersection" << endmsg;

    if ( !inter->first() ) return 0;

    SbPolyhedron* sbPoly = solidToSbPolyhedron( *( inter->first() ) );
    if ( !sbPoly ) return 0;
    // if debugging : sbPoly->setName(inter->first()->name().c_str());
    if ( !fDoBooleanOperation ) return sbPoly;

    SbPolyhedronProcessor sbPP;

    // int childn = inter->noChildrens();
    int childn = inter->children().size();
    for ( int count = 0; count < childn; count++ ) {
      if ( !( ( *inter )[count] ) ) {
        delete sbPoly;
        return 0;
      }
      const SolidChild* solidChild = dynamic_cast<const SolidChild*>( ( *inter )[count] );
      if ( !solidChild ) {
        delete sbPoly;
        return 0;
      }
      const ISolid* solidChildSolid = solidChild->solid();
      if ( !solidChildSolid ) {
        delete sbPoly;
        return 0;
      }
      SbPolyhedron* scsPoly = solidToSbPolyhedron( *solidChildSolid );
      if ( !scsPoly ) {
        delete sbPoly;
        return 0;
      }
      // if debugging : scsPoly->setName(solidChildSolid->name().c_str());
      // Rotated then placed (the reverse of the logical/physical placement) :

      Gaudi::XYZVector  trans;
      Gaudi::Rotation3D rot;
      solidChild->matrix().GetDecomposition( rot, trans );
      trans = trans * -1.0;
      rot.Invert();
      // Gaudi::AxisAngle axisAngle(rot);
      const Gaudi::Quaternion q( rot );
      Gaudi::AxisAngle        axisAngle( q );
      Gaudi::XYZVector        v;
      double                  angle;
      axisAngle.GetComponents( v, angle );

      scsPoly->Transform( HEPVis::SbRotation( SbVec3d( 1, 0, 0 ), 0 ), SbVec3d( trans.x(), trans.y(), trans.z() ) );
      scsPoly->Transform( HEPVis::SbRotation( SbVec3d( v.x(), v.y(), v.z() ), angle ), SbVec3d( 0, 0, 0 ) );
      sbPP.push_back( SbPolyhedronProcessor::INTERSECTION, *scsPoly );

      delete scsPoly;
    }

    if ( sbPP.execute( *sbPoly ) == FALSE ) { log << MSG::INFO << " SolidIntersection failed" << endmsg; }

    return sbPoly;

  } else if ( const SolidUnion* un = dynamic_cast<const SolidUnion*>( &aSolid ) ) {

    MsgStream log( msgSvc(), "SoDetConverter::booleanSolidToSbPolyhedron" );
    log << MSG::DEBUG << " SolidUnion" << endmsg;

    if ( !un->first() ) return 0;

    SbPolyhedron* sbPoly = solidToSbPolyhedron( *( un->first() ) );
    if ( !sbPoly ) return 0;
    // if debugging : sbPoly->setName(un->first()->name().c_str());
    if ( !fDoBooleanOperation ) return sbPoly;

    SbPolyhedronProcessor sbPP;

    // int childn = un->noChildrens();
    int childn = un->children().size();
    for ( int count = 0; count < childn; count++ ) {
      if ( !( ( *un )[count] ) ) {
        delete sbPoly;
        return 0;
      }
      const SolidChild* solidChild = dynamic_cast<const SolidChild*>( ( *un )[count] );
      if ( !solidChild ) {
        delete sbPoly;
        return 0;
      }
      const ISolid* solidChildSolid = solidChild->solid();
      if ( !solidChildSolid ) {
        delete sbPoly;
        return 0;
      }
      SbPolyhedron* scsPoly = solidToSbPolyhedron( *solidChildSolid );
      if ( !scsPoly ) {
        delete sbPoly;
        return 0;
      }
      // if debugging : scsPoly->setName(solidChildSolid->name().c_str());

      // Rotated then placed (the reverse of the logical/physical placement) :
      Gaudi::XYZVector  trans;
      Gaudi::Rotation3D rot;
      solidChild->matrix().GetDecomposition( rot, trans );
      trans = trans * -1.0;
      rot.Invert();
      // Gaudi::AxisAngle axisAngle(rot);
      const Gaudi::Quaternion q( rot );
      Gaudi::AxisAngle        axisAngle( q );
      Gaudi::XYZVector        v;
      double                  angle;
      axisAngle.GetComponents( v, angle );

      scsPoly->Transform( HEPVis::SbRotation( SbVec3d( 1, 0, 0 ), 0 ), SbVec3d( trans.x(), trans.y(), trans.z() ) );
      scsPoly->Transform( HEPVis::SbRotation( SbVec3d( v.x(), v.y(), v.z() ), angle ), SbVec3d( 0, 0, 0 ) );
      sbPP.push_back( SbPolyhedronProcessor::UNION, *scsPoly );

      delete scsPoly;
    }

    if ( sbPP.execute( *sbPoly ) == FALSE ) { log << MSG::INFO << " SolidUnion failed" << endmsg; }

    return sbPoly;
  }
  return 0;
}

void expand( void* userData, SoEventCallback* eventCB ) {
  if ( eventCB->isHandled() ) return;

  const SoMouseButtonEvent* event = (SoMouseButtonEvent*)eventCB->getEvent();
  // SO_MOUSE_PRESS_EVENT clashes with SoDragger controls :
  if ( !SO_MOUSE_RELEASE_EVENT( event, BUTTON1 ) ) return;
  if ( !event->wasCtrlDown() ) return;
  if ( event->wasShiftDown() ) return;

  SoHandleEventAction* handleEventAction = eventCB->getAction();
  const SoPickedPoint* pickedPoint       = handleEventAction->getPickedPoint();
  if ( !pickedPoint ) return;
  SoFullPath* path         = (SoFullPath*)pickedPoint->getPath();
  SoNode*     ancestorNode = NULL;
  for ( int i = 0; i < path->getLength(); i++ ) {
    ancestorNode = path->getNodeFromTail( i );
    if ( ancestorNode->isOfType( SoDetectorTreeKit::getClassTypeId() ) ) break;
  }

  SoDetTreeKit* This = (SoDetTreeKit*)userData;

  if ( This != ancestorNode ) return;

  SoDetConverter* soConverter = This->soConverter();
  const ILVolume& volume      = This->lvolume();

  /*
    if (event->wasShiftDown()) {
    // Ctrl + Shift : dump picked volume name :
    (*soConverter->out())
    << MSG::INFO
    << " LVolume : " << volume.name()
    << endmsg;
    const ISolid* solid = volume.solid();
    if(solid)
    (*soConverter->out())
    << MSG::INFO
    << solid
    << endmsg;
    else
    (*soConverter->out())
    << MSG::INFO
    << " no solid associated."
    << endmsg;
    return;
    }
  */

  ( *soConverter->out() ) << MSG::DEBUG << " expand : dtk expanded ? " << This->expanded() << endmsg;

  if ( This->expanded() ) {
    This->setPreview( FALSE );
  } else {
    // Detector expansion logic :
    This->setExpanded( true );
    SoSeparator* fullSeparator = (SoSeparator*)This->getPart( "fullSeparator", TRUE );
    SbBool       preview       = TRUE;
    int          childn        = volume.noPVolumes();
    for ( int count = 0; count < childn; count++ ) {
      const IPVolume* pvolume = volume[count];
      if ( !pvolume ) {
        ( *soConverter->out() ) << MSG::ERROR << "found a null pvolume in " << volume.name() << " at index " << count
                                << ". Check the name of the logical volume associated to it" << endmsg;
      } else {
        if ( !pvolume->lvolume() ) {
          ( *soConverter->out() ) << MSG::ERROR << "pvolume with a null lvolume." << endmsg;
        } else {
          SoNode* node = soConverter->volumeToSoDetectorTreeKit( *( pvolume->lvolume() ), pvolume->matrix() );
          if ( node ) {
            ( *soConverter->out() ) << MSG::DEBUG << " expand, add a node." << endmsg;
            fullSeparator->addChild( node );
            preview = FALSE;
          }
        }
      }
    }
    This->setPreview( preview );
  }
  eventCB->setHandled();
}
// HEPVis::SoDetectorTreeKit::contract code :
void contract( void* userData, SoEventCallback* eventCB ) {

  // Was the event previously handled? Is it the right kind?
  if ( eventCB->isHandled() ) return;

  const SoMouseButtonEvent* event = (SoMouseButtonEvent*)eventCB->getEvent();
  // SO_MOUSE_PRESS_EVENT clashes with SoDragger controls :
  if ( !SO_MOUSE_RELEASE_EVENT( event, BUTTON1 ) ) return;
  if ( event->wasCtrlDown() ) return;
  if ( !event->wasShiftDown() ) return;

  // Find out whether that's the one that has been picked
  SoHandleEventAction* handleEventAction = eventCB->getAction();
  const SoPickedPoint* pickedPoint       = handleEventAction->getPickedPoint();
  if ( !pickedPoint ) return;

  // Which detector is this being called for
  SoDetTreeKit*   This        = (SoDetTreeKit*)userData;
  SoDetConverter* soConverter = This->soConverter();

  // Find out whether that's the one that has been picked.
  // "This" is the lowest detector tree kit in the hierarchy.
  SoFullPath* path           = (SoFullPath*)pickedPoint->getPath();
  SoNode*     ancestorNode   = NULL;
  SbBool      firstTreeFound = FALSE;
  for ( int i = 0; i < path->getLength(); i++ ) {
    ancestorNode = path->getNodeFromTail( i );
    if ( ancestorNode->isOfType( SoDetectorTreeKit::getClassTypeId() ) ) {
      if ( !firstTreeFound ) {
        if ( This != ancestorNode ) return;
        firstTreeFound = TRUE;
      }
      SoDetTreeKit* That = (SoDetTreeKit*)ancestorNode;
      if ( !That->getPreview() ) {
        // Have to contract :
        SoSeparator* previewSep = (SoSeparator*)That->getPart( "previewSeparator", TRUE );
        if ( previewSep->getNumChildren() ) {
          //  To optimize memory, delete "full" children
          // (that will be not seen) :
          SoSeparator* fullSeparator = (SoSeparator*)That->getPart( "fullSeparator", TRUE );
          fullSeparator->removeAllChildren();
          That->setExpanded( false );
          ( *soConverter->out() ) << MSG::DEBUG << " contract & delete. " << endmsg;
          That->setPreview( TRUE );
          eventCB->setHandled();
          return;
        }
      }
    }
  }
}
