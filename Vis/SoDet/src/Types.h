/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef SoDet_Types_h
#define SoDet_Types_h

class ILVolume;
class ILVolumeVisitor;

#include <Lib/BaseType.h>
#include <Lib/Interfaces/IIterator.h>
#include <Lib/Interfaces/IPrinter.h>

class LVolumeType : public Lib::BaseType {
public:
  LVolumeType( IPrinter& );

public: // Lib::IType
  virtual std::string   name() const;
  virtual Lib::Variable value( Lib::Identifier, const std::string&, void* );

private:
  std::string fType;
};

#include <OnX/Core/BaseType.h>

class IUserInterfaceSvc;
class IMagneticFieldSvc;

class MagneticFieldType : public OnX::BaseType {
public: // Lib::IType
  virtual std::string     name() const;
  virtual Lib::IIterator* iterator();
  virtual Lib::Variable   value( Lib::Identifier, const std::string&, void* );

public: // OnX::IType
  virtual void visualize( Lib::Identifier, void* );

public:
  MagneticFieldType( IUserInterfaceSvc*, IMagneticFieldSvc* );

private:
  std::string        fName;
  IUserInterfaceSvc* fUISvc;
  IMagneticFieldSvc* fMagneticFieldSvc;
};

#endif
