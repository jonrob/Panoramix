###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# options common to all Panoramix use cases
#============================================================================
from Gaudi.Configuration import *
# ROOT persistency for histograms
# The below must come before POOL (that uses ROOT to do the IO).
# It permits to initialize ROOT in graphic mode (whilst
# POOL initialize it in batch mode).
importOptions('$STDOPTS/RootHist.opts')
from Configurables import RootHistCnv__PersSvc, DecodeRawEvent
RootHistCnv__PersSvc('RootHistCnv').ForceAlphaIds = True
RootHistSvc('RootHistSvc').OutputFile = 'histo.root'
ApplicationMgr().ExtSvc += ["RootSvc"]

# for backward compatibility with units in opts files
import os.path, GaudiKernel.ProcessJobOptions
GaudiKernel.ProcessJobOptions._parser._parse_units(
    os.path.expandvars("$STDOPTS/units.opts"))

importOptions('$GAUDIPOOLDBROOT/options/GaudiPoolDbRoot.opts')

# Get the event time (for CondDb) from ODIN
from Configurables import EventClockSvc
EventClockSvc().EventTimeDecoder = "OdinTimeDecoder"

from GaudiConf.Configuration import LHCbApp
lhcbApp = LHCbApp()
# use default tag == latest tag
# for simulation, tag should be the same as used in Gauss
lhcbApp.DDDBtag = "default"
lhcbApp.CondDBtag = "default"

appConf = ApplicationMgr()
appConf.ExtSvc += ['DataOnDemandSvc', 'LHCb::ParticlePropertySvc']
# for particle property service
import PartProp.Service, PartProp.decorators

# for 2009 DSTs
from Configurables import DstConf
DstConf().EnableUnpack = True

# provide support for unpacking MC data
# not done centrally
DataOnDemandSvc().AlgMap['/Event/MC/Particles'] = 'UnpackMCParticle'
DataOnDemandSvc().AlgMap['/Event/MC/Vertices'] = 'UnpackMCVertex'
DataOnDemandSvc(
).AlgMap['/Event/Link/Raw/Muon/Coords'] = 'MuonCoord2MCParticleLink'

# support for decoding raw data
DecodeRawEvent().DataOnDemand = True
importOptions("$L0TCK/L0DUConfig.opts")
# persistency service for IO of MDF files
EventPersistencySvc().CnvServices += ["LHCb::RawDataCnvSvc"]

# now required for HLT to work
ApplicationMgr().ExtSvc += ["HltANNSvc"]

# unpack packed Rec
from Configurables import DstConf
DstConf().Simulation = True
DstConf().EnableUnpack = True
