###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *
import GaudiKernel.SystemOfUnits as units

bunch = ["Prev2/", "Prev1/", "", "Next1/", "Next2/"]
CosmicReco = False

from Configurables import TrackSys, MagneticFieldSvc

## Set of standard fitting options
#importOptions( "$TRACKSYSROOT/options/Fitting.py" )

MagneticFieldSvc().ScaleFactor = 0
TrackSys().setSpecialDataOption("fieldOff", True)
TrackSys().ExpertTracking = ["noDrifttimes"]
TrackSys().SpecialData = ['fieldOff']

#PatSeeding for collisions events
from Configurables import (OTRawBankDecoder, Tf__OTHitCreator,
                           Tf__STHitCreator_Tf__IT_, PatTStationHitManager,
                           TrackEventFitter, PatSeeding, PatSeedingTool)
tsa_conf = {}

from TrackSys.Configuration import *
from Configurables import (
    Tf__Tsa__Seed, Tf__Tsa__SeedTrackCnv, Tf__Tsa__ITXSearch,
    Tf__Tsa__OTXSearch, Tf__Tsa__ITStereoSearch, Tf__Tsa__OTStereoSearch,
    Tf__Tsa__TStationHitManager, Tf__Tsa__SeedTrackCnvTool, Tf__Tsa__StubFind,
    Tf__Tsa__StubExtender, Tf__Tsa__SeedAddHits)
for x in bunch:
    b = x.replace('/', '')
    Tf__Tsa__SeedTrackCnv("TsaSeedTrackCnv" +
                          b).outputLocation = x + 'Rec/Track/Seed'
    ## Hit cleaning options
    tsaSeed = Tf__Tsa__Seed("TsaSeed" + b)
    tsa_conf["TsaSeed" + b] = tsaSeed
    tsaSeed.addTool(Tf__Tsa__TStationHitManager("TsaDataManager"))

    tsaSeed.PatTStationHitManager.OTHitCreator.OTRawBankDecoder.RawEventLocation = x + 'DAQ/RawEvent'
    Tf__Tsa__TStationHitManager("TsaDataManager" + b).CleanITHits = True
    Tf__Tsa__TStationHitManager("TsaDataManager" + b).CleanOTHits = True

    tsaSeed.addTool(Tf__Tsa__ITXSearch(), name="xSearchS0")
    tsaSeed.addTool(Tf__Tsa__ITXSearch(), name="xSearchS1")
    tsaSeed.addTool(Tf__Tsa__ITXSearch(), name="xSearchS2")
    tsaSeed.addTool(Tf__Tsa__OTXSearch(), name="xSearchS3")
    tsaSeed.addTool(Tf__Tsa__OTXSearch(), name="xSearchS4")

    tsaSeed.addTool(Tf__Tsa__ITStereoSearch(), name="stereoS0")
    tsaSeed.addTool(Tf__Tsa__ITStereoSearch(), name="stereoS1")
    tsaSeed.addTool(Tf__Tsa__ITStereoSearch(), name="stereoS2")
    tsaSeed.addTool(Tf__Tsa__OTStereoSearch(), name="stereoS3")
    tsaSeed.addTool(Tf__Tsa__OTStereoSearch(), name="stereoS4")

    tsaSeed.xSearchS0.sector = 0
    tsaSeed.xSearchS1.sector = 1
    tsaSeed.xSearchS2.sector = 2
    tsaSeed.xSearchS3.sector = 3
    tsaSeed.xSearchS4.sector = 4

    tsaSeed.stereoS0.sector = 0
    tsaSeed.stereoS1.sector = 1
    tsaSeed.stereoS2.sector = 2
    tsaSeed.stereoS3.sector = 3
    tsaSeed.stereoS4.sector = 4

    # Running over unused hits
    OnlyUnusedHits = False

    tsaSeed.xSearchS0.OnlyUnusedHits = OnlyUnusedHits
    tsaSeed.xSearchS1.OnlyUnusedHits = OnlyUnusedHits
    tsaSeed.xSearchS2.OnlyUnusedHits = OnlyUnusedHits
    tsaSeed.xSearchS3.OnlyUnusedHits = OnlyUnusedHits
    tsaSeed.xSearchS4.OnlyUnusedHits = OnlyUnusedHits

    tsaSeed.stereoS0.OnlyUnusedHits = OnlyUnusedHits
    tsaSeed.stereoS1.OnlyUnusedHits = OnlyUnusedHits
    tsaSeed.stereoS2.OnlyUnusedHits = OnlyUnusedHits
    tsaSeed.stereoS3.OnlyUnusedHits = OnlyUnusedHits
    tsaSeed.stereoS4.OnlyUnusedHits = OnlyUnusedHits

    tsaSeed.addTool(Tf__Tsa__SeedAddHits, name="SeedAddHits")
    tsaSeed.SeedAddHits.OnlyUnusedHits = OnlyUnusedHits

    Tf__Tsa__SeedTrackCnv("TsaSeedTrackCnv").addTool(
        Tf__Tsa__SeedTrackCnvTool("SeedTrackCnvTool"))
    Tf__Tsa__SeedTrackCnvTool("SeedTrackCnvTool").pFromCurvature = True

    tsaSeed.xSearchS3.sxCut = 0.40
    tsaSeed.xSearchS4.sxCut = 0.40
    tsaSeed.xSearchS3.collectPolicy = "Linear"
    tsaSeed.xSearchS4.collectPolicy = "Linear"
    tsaSeed.xSearchS3.sx2Cut = 0.330
    tsaSeed.xSearchS4.sx2Cut = 0.330

    tsaSeed.xSearchS0.dthCut = 0.08
    tsaSeed.xSearchS1.dthCut = 0.08
    tsaSeed.xSearchS2.dthCut = 0.08
    tsaSeed.xSearchS3.dthCut = 0.08
    tsaSeed.xSearchS4.dthCut = 0.08

    tsaSeed.xSearchS0.x0Cut = 400.
    tsaSeed.xSearchS1.x0Cut = 400.
    tsaSeed.xSearchS2.x0Cut = 400.
    tsaSeed.xSearchS3.x0Cut = 400.
    tsaSeed.xSearchS4.x0Cut = 400.

    tsaSeed.xSearchS0.sxCut = 0.10
    tsaSeed.xSearchS1.sxCut = 0.10
    tsaSeed.xSearchS2.sxCut = 0.10
    tsaSeed.xSearchS0.sx2Cut = 0.08
    tsaSeed.xSearchS1.sx2Cut = 0.08
    tsaSeed.xSearchS2.sx2Cut = 0.08

    ApplicationMgr().TopAlg += tsaSeed
