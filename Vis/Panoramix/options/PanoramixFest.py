###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *

#does not work yet use old opts
#ApplicationMgr().ExtSvc              += ["IncidentSvc","LHCb::NetworkEvtSelector/EventSelector"]
#ApplicationMgr().Runable             = "LHCb::EventRunable/Runable"
#Runable.MEPManager                   = "LHCb::MEPManager/MEPManager"
#MEPManager.Buffers                   = []
#EventSelector().Input                  = "$NODENAME/EvtServ"
#EventPersistencySvc().CnvServices      = ["LHCb::RawDataCnvSvc/RawDataCnvSvc"]

importOptions('$PANORAMIXROOT/options/PanoramixFest.opts')
allConfigurables[
    "EventSelector"].REQ1 = "EvType=2;TriggerMask=0xffffffff,0xffffffff,0xffffffff,0xffffffff;VetoMask=0,0,0,0;MaskType=ANY;UserType=VIP;Frequency=PERC;Perc=100.0"

from Configurables import OnXSvc
OnXSvc().File = "$PANORAMIXROOT/scripts/OnX/Panoramix_Online.onx"
OnXSvc().OutputToTerminal = False
