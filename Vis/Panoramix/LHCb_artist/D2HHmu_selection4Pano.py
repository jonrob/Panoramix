###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# to be run in Panoramix
# python $myPanoramix -f /media/Data/SEMILEPTONIC.DST/00012593_00000186_1.semileptonic.dst
# import D2HHmu_selection4Pano
# D2HHmu_selection4Pano.bestEvent()
# D2HHmu_selection4Pano.searchEvent()
#     produces D2HHmu_selection4Pano.result
# D2HHmu_selection4Pano.makeHIVs()
# however requires to have Event_tree widget and PR_Viewer widget and TES container selected with candidates
# the following is ugly, don't know a solution to make it automatic:
# Event_Tree, dump camera position enter values into LHCb_artist_X11_ev.cpp and compile
#

import PRplot, Panoramix
from DaVinciTools import Tools as DVTools
relPVFinder = Panoramix.getTool(DVTools.P2PVWithIPChi2, 'IRelatedPVFinder')
#
global result
result = {
    'ptr': None,
    'mode': '',
    'Dmass': '2.',
    'decaylength': '2.',
    '+/-': '2.',
    'lifetime': '2.',
    'Bdecaylength': '2.',
    'B+/-': '2.',
    'Dmasserr': 2.,
    'lifetimeerr': 2.,
    'charge': '+'
}


def makePrint(pname='D', t1=0.2, t2=0.88, t3=0.2, t4=0.1, d3=False):
    #
    print result
    runnr = evt['Rec/Header'].runNumber()
    evtnr = evt['Rec/Header'].evtNumber()
    if d3:
        PRplot.wprint(
            result['mode'] + '_' + str(runnr) + '_' + str(evtnr) + '_3d.jpg',
            format='JPEG',
            quality='100',
            info=False)
        PRplot.wprint(
            result['mode'] + '_' + str(runnr) + '_' + str(evtnr) + '_3d.gif',
            format='GIF')
        return
    proj = ui().parameterValue('Panoramix_Particle_input_azoom.value')
    if proj == 'stable':
        t1 = 0.01
        t2 = 0.78
    soPage = ui().currentPage()
    if result['ptr']:
        soRegion = result['ptr']
    else:
        soRegion = soPage.createRegion('SoTextRegion', t1, t2, t3, t4)
        result['ptr'] = soRegion
        soTextRegion = soRegion.cast_SoTextRegion()
        soTextRegion.verticalMargin.setValue(0)
    soTextRegion = soRegion.cast_SoTextRegion()
    Bname = 'B',
    if pname == 'D':
        txt = pname + '->' + result['mode'] + ' mu(' + result['charge'] + ')'
    else:
        bee = result['dchain']
        Bname = partsvc.find(bee.particleID()).name()
        dau = bee.daughters()[0]
        dname = partsvc.find(dau.particleID()).name()
        tmp = ''
        for gd in dau.daughters():
            tmp += partsvc.find(gd.particleID()).name()
        txt = Bname + '->' + dname + ' mu(' + result[
            'charge'] + ') nu X, ' + dname + '->' + tmp
    print txt
    soTextRegion.text.set1Value(0, txt)
    txt = pname + ' mass: [' + result['Dmass'] + '+/-' + result[
        'Dmasserr'] + '] GeV/c2'
    print txt
    soTextRegion.text.set1Value(1, txt)
    txt = pname + ' decaylength: [' + result['decaylength'] + '+/-' + result[
        '+/-'] + '] mm'
    print txt
    soTextRegion.text.set1Value(2, txt)
    txt = pname + ' lifetime: [' + result['lifetime'] + '+/-' + result[
        'lifetimeerr'] + '] ctau mm'
    print txt
    soTextRegion.text.set1Value(3, txt)
    txt = Bname + ' decaylength: [' + result['Bdecaylength'] + '+/-' + result[
        'B+/-'] + '] mm'
    print txt
    soTextRegion.text.set1Value(4, txt)
    soTextRegion.setPosition(t1, t2)
    soTextRegion.setSize(t3, t4)
    ui().synchronize()
    PRplot.wprint(
        result['mode'] + '_' + str(runnr) + '_' + str(evtnr) + '_' + proj +
        '.jpg',
        format='JPEG',
        quality='100',
        info=False)
    PRplot.wprint(
        result['mode'] + '_' + str(runnr) + '_' + str(evtnr) + '_' + proj +
        '.gif',
        format='GIF')


def bestEvent():
    while 1 > 0:
        appMgr.run(1)
        hdr = evt['Rec/Header']
        if not hdr: break
        runnr = hdr.runNumber()
        evtnr = hdr.evtNumber()
        #D0 delta ACP:
        if runnr in [93031] and evtnr in [523413416]: break
        # if runnr in [93031] and evtnr in [523413416,322135593, 327224831,254133968]: break
        #lambda_b:
        #if runnr in [93031] and evtnr in [250275156, 324194979, 328112743]: break
    print hdr
    return


def evtLoop():
    N = 100
    found = False
    for n in range(N):
        appMgr.run(1)
        if searchEvent():
            found = True
            break
    if not found: print 'scanned ', n, ' events'


debug = False
from panoramixmodule import *
import os, sys, time, math
import GaudiPython
from ROOT import Double

stream = 'Semileptonic'
dmodeDict = {
    '1Kpi': stream + '/Phys/b2D0MuXB2DMuNuXLine/Particles',
    '2KK': stream + '/Phys/b2D0MuXKKB2DMuNuXLine/Particles',
    '3pipi': stream + '/Phys/b2D0MuXpipiB2DMuNuXLine/Particles',
    '4K3pi': stream + '/Phys/b2D0MuXK3PiB2DMuNuXLine/Particles',
    '5K2pi': stream + '/Phys/b2DpMuXB2DMuNuXLine/Particles',
    '6Phipi': stream + '/Phys/b2DsMuXPhiPiB2DMuNuXLine/Particles',
    '7Lamc': stream + '/Phys/b2LcMuXB2DMuNuXLine/Particles'
}
selection_names = {}
candidates = dmodeDict.values()
for s in dmodeDict:
    selection_names[dmodeDict[s].split('/')[2]] = s[1:].replace('ws', '')
selection_lines = {}
for s in selection_names:
    selection_lines[selection_names[s]] = s

L0TOS_names = {
    'mu': ['L0MuonDecision'],
    'Phi': ['L0HadronDecision'],
    'Ds': ['L0HadronDecision']
}
Hlt1TOS_names = ['Hlt1TrackAllL0Decision', 'Hlt1TrackMuonDecision']
Hlt2TOS_names = [
    'Hlt2TopoMu3BodyBBDTDecision', 'Hlt2TopoMu2BodyBBDTDecision',
    'Hlt2SingleMuonDecision', 'Hlt2IncPhiDecision'
]

dmode = []
temp = dmodeDict.keys()
temp.sort()
for d in temp:
    dmode.append(d[1:])

masses = {}
for da in dmode:
    masses[da] = [1.866, 0.008]
    if not da.find('KK') < 0: masses[da] = [1.866, 0.0064]
    if not da.find('pipi') < 0: masses[da] = [1.866, 0.0088]
    if not da.find('2pi') < 0: masses[da] = [1.871, 0.0077]
    if not da.find('3pi') < 0: masses[da] = [1.866, 0.0074]
    if not da.find('Phi') < 0: masses[da] = [1.968, 0.0064]
    if not da.find('Lamc') < 0: masses[da] = [2.2865, 0.0056]


def dvs(v):  # dump vector content
    for iv in v:
        print iv,
    print


def dvsl0(v):  # special version for Hlt1L0 trigger names to strip Hlt1 off
    for iv in v:
        print iv[iv.find('L0'):],
    print


from GaudiPython import gbl
# dictionary for private data is genereated by createPrivateDict.py
rc = gbl.gSystem.Load(
    'tistosResult'
)  # Need to load the just created dictionary without autoloading
MyVectorString = gbl.tistosResult('<vector<std::string> >')
MyVectorDouble = gbl.tistosResult('<vector<double> >')
partCont = GaudiPython.makeClass(
    'KeyedContainer<LHCb::Particle,Containers::KeyedObjectManager<Containers::hashmap> >'
)
protoPartCont = GaudiPython.makeClass(
    'KeyedContainer<LHCb::ProtoParticle,Containers::KeyedObjectManager<Containers::hashmap> >'
)
trackCont = GaudiPython.makeClass(
    'KeyedContainer<LHCb::Track,Containers::KeyedObjectManager<Containers::hashmap> >'
)
smartPartRef = GaudiPython.makeClass('const SmartRef<LHCb::Particle>& ')
smartPartRefVec = GaudiPython.makeClass(
    'const SmartRefVector<LHCb::Particle>& ')
Particle = gbl.LHCb.Particle
ParticleID = gbl.LHCb.ParticleID
smVec = smartPartRefVec()


#
def add_priv_data(c, mPath):
    decisions = tisTosResults(c)
    lifetimes = charmLifetime(c)
    tescreate = {}
    for dt in decisions[0]:
        tescreate[dt] = MyVectorString()
    for d in decisions:
        for dt in d:
            conc = ''
            if d[dt]:
                for a in d[dt]:
                    conc += a + ':'
            if conc != '': conc = conc[:len(conc) - 1]
            tescreate[dt].push_back(conc)
    for dt in decisions[0]:
        objvec = tescreate[dt]
        GaudiPython.setOwnership(objvec, False)
        evt.registerObject(mPath + '/TisTosResults/' + dt, objvec)
    tescreate = {}
    for dt in lifetimes[0]:
        tescreate[dt] = MyVectorDouble()
    for d in lifetimes:
        for dt in d:
            tescreate[dt].push_back(d[dt])
    for dt in lifetimes[0]:
        objvec = tescreate[dt]
        GaudiPython.setOwnership(objvec, False)
        evt.registerObject(mPath + '/charmLifetime/' + dt, objvec)
    return True


def charmLifetime(c):
    lifetimes = []
    for bee in evt[c]:
        for daug in bee.daughters():
            if daug.particleID().abspid() != 13:
                cee = daug
                break
# decaylength of D
        eVxD = cee.endVertex()
        eVxDpos = eVxD.position()
        eVxDcov = eVxD.covMatrix()
        eVxBee = bee.endVertex()
        eVxBeepos = eVxBee.position()
        eVxBeecov = eVxBee.covMatrix()
        dz = eVxDpos.z() - eVxBeepos.z()
        dx = eVxDpos.x() - eVxBeepos.x()
        dy = eVxDpos.y() - eVxBeepos.y()
        dl = math.sqrt(dx * dx + dy * dy + dz * dz)
        if dz < 0: dl = -dl
        sigz2 = eVxBeecov.Col(2)[2] + eVxDcov.Col(2)[2]
        sigz = math.sqrt(abs(sigz2))
        if sigz2 < 0: sigz = -sigz
        lifetime = Double(-1.)
        error = Double(999.)
        chi2q = Double(-1.)
        rc = properTimeFitter.fit(eVxBee, cee, lifetime, error, chi2q)
        ctau = lifetime / units.s * 3E11
        cerr = error / units.s * 3E11
        lifetimes.append({
            'dl': dl,
            'sigz': sigz,
            'ctau': ctau,
            'cerr': cerr,
            'chi2': chi2q
        })
    return lifetimes


#
def dstarReconstruction(c):
    dstern = False
    dstarcont = c.replace('Particles', 'Dstar')
    if not evt['Phys/StdNoPIDsPions/Particles']:
        appMgr.algorithm(stdNoPIDsPions.name()).execute()
    for bee in evt[c]:
        tkeys = []
        for daug in bee.daughters():
            if daug.particleID().abspid() != 13:
                cee = daug
            else:
                tkeys.append(daug.proto().key())
        for daug in cee.daughters():
            tkeys.append(daug.proto().key())
        d0mom = cee.momentum()
        d0mass = cee.measuredMass()
        for i in range(evt['Phys/StdNoPIDsPions/Particles'].size()):
            apion = evt['Phys/StdNoPIDsPions/Particles'][i]
            pkey = apion.proto().key()
            # if pkey in tkeys: continue
            dstar = Particle(ParticleID(413))
            pionmom = apion.momentum()
            dstarmom = pionmom + d0mom
            dstar.setMomentum(dstarmom)
            dstarmass = dstarmom.mass()
            dstar.setMeasuredMass(dstarmass)
            delmas = dstarmass - d0mass
            if delmas / units.GeV < 0.2:
                dstern = True
                # register Dstar, pion, protopart, and track in TES
                theProto = apion.proto().clone()
                theTrack = theProto.track().clone()
                thePion = apion.clone()
                if not evt[dstarcont]:
                    cds = partCont()
                    csp = partCont()
                    ctr = trackCont()
                    cpr = protoPartCont()
                    for x in [cds, csp, cpr, ctr]:
                        GaudiPython.setOwnership(x, False)
                for x in [theProto, theTrack, thePion, dstar]:
                    GaudiPython.setOwnership(x, False)
                cds.add(dstar)
                csp.add(thePion)
                ctr.add(theTrack)
                cpr.add(theProto)
                smVec.clear()
                spion = smartPartRef(thePion)
                scee = smartPartRef(cee)
                GaudiPython.setOwnership(spion, False)
                GaudiPython.setOwnership(scee, False)
                smVec.push_back(spion)
                smVec.push_back(scee)
                GaudiPython.setOwnership(smVec, False)
                dstar.setDaughters(smVec)
                thePion.setProto(theProto)
                theProto.setTrack(theTrack)
    if dstern:
        evt.registerObject(dstarcont + '/Particle', cds)
        evt.registerObject(dstarcont + '/Slowpion', csp)
        evt.registerObject(dstarcont + '/ProtoPart', cpr)
        evt.registerObject(dstarcont + '/Track', ctr)
    return dstern


def tisTosResults(c):
    decisions = []
    for bee in evt[c]:
        sig = {'mu': None, 'Ds': None, 'Phi': None}
        l0tosres = {'mu': None, 'Ds': None, 'Phi': None}
        l0tisres = {'mu': None, 'Ds': None, 'Phi': None}
        for da in bee.daughters():
            daug = da
            if daug.particleID().abspid() == 13:
                sig['mu'] = daug
            if daug.particleID().abspid() == 411:
                sig['Ds'] = daug
                for gd in da.daughters():
                    gda = gd
                    if gda.particleID().abspid() == 333:
                        sig['Phi'] = gda
        for asign in sig:
            s = sig[asign]
            if not s: continue
            l0tistostool.setOfflineInput(s)
            # tistos L0 trigger via Hlt1L0 selections
            l0tistostool.setTriggerInput('L0.*Decision')
            l0 = l0tistostool.tisTosTobTrigger()
            l0tosres[asign] = l0tistostool.triggerSelectionNames(
                tistostool.kAnything, tistostool.kAnything,
                tistostool.kTrueRequired)
            l0tisres[asign] = l0tistostool.triggerSelectionNames(
                tistostool.kAnything, tistostool.kTrueRequired,
                tistostool.kAnything)
        tistostool.setOfflineInput(bee)
        tistostool.setTriggerInput('Hlt1.*Decision')
        hlt1tos = tistostool.triggerSelectionNames(tistostool.kTrueRequired,
                                                   tistostool.kAnything,
                                                   tistostool.kTrueRequired)
        hlt1tis = tistostool.triggerSelectionNames(tistostool.kTrueRequired,
                                                   tistostool.kTrueRequired,
                                                   tistostool.kAnything)
        tistostool.setTriggerInput('Hlt2.*Decision')
        hlt2tos = tistostool.triggerSelectionNames(tistostool.kTrueRequired,
                                                   tistostool.kAnything,
                                                   tistostool.kTrueRequired)
        hlt2tis = tistostool.triggerSelectionNames(tistostool.kTrueRequired,
                                                   tistostool.kTrueRequired,
                                                   tistostool.kAnything)
        # hlt summary
        if sig['Phi']:
            bl0tos = '%s' % (checkTOS(l0tosres['mu'], L0TOS_names['mu'])
                             or checkTOS(l0tosres['Phi'], L0TOS_names['Phi']))
            bl0tis = '%s' % (checkTIS(l0tisres['mu'], L0TOS_names['mu'])
                             and checkTIS(l0tisres['Phi'], L0TOS_names['Phi']))
        else:
            bl0tos = '%s' % (checkTOS(l0tosres['mu'], L0TOS_names['mu']))
            bl0tis = '%s' % (checkTIS(l0tisres['mu'], L0TOS_names['mu']))
        bhlt1tos = '%s' % (checkTOS(hlt1tos, Hlt1TOS_names))
        bhlt1tis = '%s' % (checkTIS(hlt1tis, Hlt1TOS_names))
        bhlt2tos = '%s' % (checkTOS(hlt2tos, Hlt2TOS_names))
        bhlt2tis = '%s' % (checkTIS(hlt2tis, Hlt2TOS_names))
        #
        trigsum = bl0tos[0] + bhlt1tos[0] + bhlt2tos[0] + bl0tis[0] + bhlt1tis[
            0] + bhlt2tis[0]
        decisions.append({
            'l0tosmu': l0tosres['mu'],
            'l0tismu': l0tisres['mu'],
            'l0tosphi': l0tosres['Phi'],
            'l0tisphi': l0tisres['Phi'],
            'hlt1tos': hlt1tos,
            'hlt1tis': hlt1tis,
            'hlt2tos': hlt2tos,
            'hlt2tis': hlt2tis,
            'trigsum': trigsum
        })
    return decisions


#
def checkTOS(li, l0n):
    flag = False
    for l in li:
        for n in l0n:
            if l == n: flag = True
    return flag


def checkTIS(li, l0n):
    flag = True
    if len(li) < 1: flag = False
    for l in li:
        for n in l0n:
            if l == n: flag = False
    return flag


#
# get TisTos tool
tistostool = tsvc.create('TriggerTisTos', interface='ITriggerTisTos')
l0tistostool = tsvc.create('L0TriggerTisTos', interface='ITriggerTisTos')
properTimeFitter = tsvc.create('PropertimeFitter', interface='ILifetimeFitter')
Vertexfitter = tsvc.create('LoKi::VertexFitter', interface='IVertexFit')
Particle = gbl.LHCb.Particle


# event loop
def searchEvent(reco=True):
    flag = False
    listOfWriters = []
    dstarReco = False
    for selection_name in selection_names.keys():
        c = stream + '/Phys/' + selection_name + '/Particles'
        if evt[c] and reco:
            if evt[c].size() > 0:
                if selection_names[selection_name] in [
                        'pipi', 'KK', 'Kpi', 'Lamc'
                ]:
                    dstarReco = dstarReconstruction(c)
                listOfWriters.append(c)
                # create tisTos results and add lifetime
                priv = c.replace('Particles', '')
                add_priv_data(c, priv)
# now do selection:
    xx = det['/dd/Conditions/Online/LHCb/Magnet/Measured']
    pol = xx.paramAsString('State')
    for dan in dmodeDict:
        c = dmodeDict[dan]
        if not evt[c]: continue
        da = dan[1:]
        RootInTes = ''
        for bee in evt[c]:
            N = {'1': 0, '-1': 0}
            sig = {'mu': None, 'D': None, 'K': None}
            trlist = []
            for daug in bee.daughters():
                if daug.particleID().abspid() == 13:
                    sig['mu'] = daug
                    trlist.append(daug)
                else:
                    sig['D'] = daug
                    sumPT = 0
                    for gda in daug.daughters():
                        abspid = gda.particleID().abspid()
                        # phi is the only other composite particle
                        if abspid != 333: trlist.append(gda)
                        if abspid == 321 or (da.find('Phi') > -1
                                             and abspid == 211):
                            sig['K'] = gda
                        if abspid == 333:
                            for ggda in gda.daughters():
                                trlist.append(ggda)
                                sumPT += ggda.pt()
                        else:
                            sumPT += gda.pt()
#
            if debug:
                keys = []
                for atr in trlist:
                    proto = atr.proto()
                    keys.append(proto.key())
                keys.sort()
                for k in range(len(keys) - 1):
                    if keys[k] == keys[k + 1]:
                        print 'BIG SHIT:', k
                        print bee
                        1 / 0
# sanity cuts, require all tracks in acceptance:
            accepted = True
            x = Double(-1)
            for atr in trlist:
                eta = atr.momentum().eta()
                if eta < 1.7 or eta > 5.2:
                    accepted = False
                theProto = atr.proto()
                if not theProto:
                    print 'how can this be no proto', atr
                    continue
                atrack = theProto.track()
                cloneDist = atrack.info(atrack.CloneDist, x)
                if cloneDist > 0 and evt[c].size() > 1: accepted = False
            if not accepted: continue
            # if sign of muon and kaon is different, either cabbibo suppressed or mixing
            if da.find('ws') > -1:
                if sig['K'].charge() * sig['mu'].charge() > 0 and da.find(
                        'Phi') < 0:
                    continue
                if sig['K'].charge() * sig['mu'].charge() < 0 and da.find(
                        'Phi') > -1:
                    continue
#
            tistoscont = c.replace('Particles', '') + '/TisTosResults/'
            bkey = bee.key()
            tistosResult = evt[tistoscont + 'trigsum'][bkey].split(':')
            if debug:
                print 'Tis tos results for', da
                rc = tisTosResults(c)
                print 'number of candidates', len(rc)
                checkTisTos = rc[0]
                print 'MicroDST', tistosResult
                print checkTisTos['trigsum']
                print '-----MicroDST', 'L0'
                print evt[tistoscont + 'l0tosmu'][0]
                print 'tistostool', 'L0'
                dvsl0(checkTisTos['l0tosmu'])
                print '-----MicroDST', 'hlt1'
                print evt[tistoscont + 'hlt1tos'][0]
                print 'tistostool', 'hlt1'
                dvs(checkTisTos['hlt1tos'])
                print '-----MicroDST', 'hlt2'
                print evt[tistoscont + 'hlt2tos'][0]
                print 'tistostool', 'hlt2'
                dvs(checkTisTos['hlt2tos'])
                evt.dump()
#
# a reminder: trigsum = bl0tos[0]+bhlt1tos[0]+bhlt2tos[0]+bl0tis[0]+bhlt1tis[0]+bhlt2tis[0]
#
            if da == 'Phipi':
                tos = tistosResult[0] == 'T' and tistosResult[
                    1] == 'T' and tistosResult[2] == 'T'
                bl0tos = tistosResult[0]
                bhlt1tos = tistosResult[1]
                bhlt2tos = tistosResult[2]
            else:
                l0tosmu = evt[tistoscont + 'l0tosmu'][bkey].split(':')
                hlt1tos = evt[tistoscont + 'hlt1tos'][bkey].split(':')
                hlt2tos = evt[tistoscont + 'hlt2tos'][bkey].split(':')
                bl0tos = checkTOS(l0tosmu, L0TOS_names['mu'])
                bhlt1tos = checkTOS(hlt1tos, Hlt1TOS_names)
                bhlt2tos = checkTOS(hlt2tos, Hlt2TOS_names)
                tos = bl0tos and bhlt1tos and bhlt2tos
                if debug:
                    print da, l0tosmu, bl0tos
                    print '    ', hlt1tos, bhlt1tos
                    print '    ', hlt2tos, bhlt2tos
#
            mD = sig['D'].measuredMass() / units.GeV
            mDerr = sig['D'].measuredMassErr() / units.GeV
            ptD = sig['D'].pt() / units.GeV
            ptmu = sig['mu'].pt() / units.GeV
            mB = bee.measuredMass() / units.GeV
            ch = str(sig['mu'].charge())
            # decaylength of D
            loca = c.replace('Particles', '') + '/charmLifetime/'
            key = bee.key()
            ctau = evt[loca + 'ctau'][key]
            cerr = evt[loca + 'cerr'][key]
            dl = evt[loca + 'dl'][key]
            sigz = evt[loca + 'sigz'][key]
            chi2q = evt[loca + 'chi2'][key]
            lifetime = {
                'dl': dl,
                'sigz': sigz,
                'ctau': ctau,
                'cerr': cerr,
                'chi2': chi2q
            }
            # fill my ntuple
            run = evt['Rec/Header'].runNumber()
            event = evt['Rec/Header'].evtNumber()
            polarity = pol
            dmode = dan
            muCharge = sig['mu'].charge()
            if sig['K']: DCharge = sig['K'].charge()
            else: DCharge = 0
            Dmasss = mD
            Dpt = ptD
            Deta = sig['D'].momentum().eta()
            Dlifetime = lifetime['ctau']
            Bmass = mB
            sumPt = sumPT
            muPt = sig['mu'].pt() / units.GeV
            mueta = sig['mu'].momentum().eta()
            nslow = 0
            dstarcont = c.replace('Particles', 'Dstar/Particle')
            pionC = 0
            delmas = -1.
            if evt[dstarcont]:
                for adstar in evt[dstarcont]:
                    pionC = 0
                    notRight = False
                    for apion in adstar.daughters():
                        if apion.particleID().abspid() == 211:
                            pionC = apion.charge()
                            slowpt = apion.pt() / units.GeV
                            sloweta = apion.momentum().eta()
                        elif apion.key() != sig['D'].key():
                            notRight = True
                    if notRight: continue
                    nslow += 1
                    dstarmass = adstar.measuredMass()
                    delmas = dstarmass / units.GeV - mD
                    slpt = slowpt
                    slCharge = pionC
                    #h['charmTuple'].Fill(run,event,polarity,dmode,muCharge,DCharge,Dmass,Dpt,
                    #                      Deta,Dlifetime,Bmass,sumPt,mupt,mueta,nslow,delmass,slpt,slCharge,bl0tos,bhlt1tos,bhlt2tos)

            if ptmu < 1.2: continue  # 1.2 stripping cut
            if mB < 2.5 or mB > 5.3: continue
            if mB > 5.1 and da != 'Lamc': continue  # 2.5 < stripping cut
            if mB > 5.0 and da != 'Phipi' and da != 'Lamc':
                continue  # tighter cut for Bd
            if ptD < 1.2: continue
            #
            if abs(mD - masses[da][0]) > 0.025: continue
            #
            print da, c
            relTable = c.replace('Particles', 'Particle2VertexRelations')
            print relTable
            rt = evt[relTable]
            relations = rt.relations()
            PV = relations[bee.key()].to()
            oVx = PV.position()
            eVx = bee.endVertex().position()
            bdecayl = math.sqrt((eVx.x() - oVx.x())**2 + (
                eVx.y() - oVx.y())**2 + (eVx.z() - oVx.z())**2)
            eVxBeecov = bee.covMatrix()
            result['mode'] = da
            result['dchain'] = bee
            result['Dmass'] = '%5.3F' % (mD)
            result['Dmasserr'] = '%5.3F' % (mDerr)
            result['decaylength'] = '%5.3F' % (lifetime['dl'] / units.mm)
            result['+/-'] = '%5.3F' % (lifetime['sigz'] / units.mm)
            result['lifetime'] = '%5.3F' % (lifetime['ctau'])
            result['lifetimeerr'] = '%5.3F' % (lifetime['cerr'])
            result['Bdecaylength'] = '%5.3F' % (bdecayl)
            result['B+/-'] = '%5.3F' % (math.sqrt(eVxBeecov(2, 2)) / units.mm)
            result['charge'] = ch
            print 'Dmass', mD, 'decaylength', lifetime[
                'dl'] / units.mm, '+/-', lifetime[
                    'sigz'] / units.mm, 'lifetime', lifetime['ctau']
            if delmas > 0.14 and delmas < 0.15:
                print 'Delmass', delmas, sig['mu'].charge(
                ), pionC  # "sig['mu'].charge()*pionC < 0 == correct else wrong sign"
            if da in ['pipi', 'KK'] and lifetime['dl'] / units.mm > 2:
                flag = True
            if da in ['Lamc'] and lifetime['dl'] / lifetime['cerr'] > 3:
                flag = True
            result['mutrack'] = sig['mu'].proto().track()
            d2hh = sig['D'].daughters()
            h1 = d2hh[0].proto().track()
            h2 = d2hh[1].proto().track()
            result['hhtracks'] = [h1, h2]
    return flag


#### Rest for making HUGE pictures
def displayMuonTrack(atrack):
    temp1 = session().parameterValue('modeling.userTrackRange')
    temp2 = session().parameterValue('modeling.trackEndz')
    temp3 = session().parameterValue('modeling.lineWidth')
    temp4 = session().parameterValue('modeling.trackStartz')
    rc = session().setParameter('modeling.userTrackRange', 'true')
    rc = session().setParameter('modeling.trackEndz', '22000.')
    rc = session().setParameter('modeling.trackStartz', '-200.')
    Style().setColor('magenta')
    Object_visualize(atrack)
    rc = session().setParameter('modeling.userTrackRange', temp1)
    rc = session().setParameter('modeling.trackEndz', temp2)
    rc = session().setParameter('modeling.lineWidth', temp3)
    rc = session().setParameter('modeling.trackStartz', temp4)


import subprocess


def makeArtist(hcam, znear, zfar, xpos, ypos, zpos, xorie, yorie, zorie,
               angle):
    afile = open(
        os.environ['HOME'] +
        '/LHCb_software/contrib/LHCb_artist/0.2/LHCb_artist/LHCb_artist_X11_ev.cpp'
    )
    newfile = open(
        os.environ['HOME'] +
        '/LHCb_software/contrib/LHCb_artist/0.2/LHCb_artist/LHCb_artist_X11_proj.cpp',
        'w')
    for aline in afile.readlines():
        aline = aline.replace('#hcam', hcam)
        aline = aline.replace('#znear', znear)
        aline = aline.replace('#zfar', zfar)
        aline = aline.replace('#xpos', xpos)
        aline = aline.replace('#ypos', ypos)
        aline = aline.replace('#zpos', zpos)
        aline = aline.replace('#xorie', xorie)
        aline = aline.replace('#yorie', yorie)
        aline = aline.replace('#zorie', zorie)
        aline = aline.replace('#angle', angle)
        newfile.write(aline)
    afile.close()
    newfile.close()
    myEnv = {
        "PATH": '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'
    }
    subprocess.Popen(
        './buildev',
        cwd=os.environ['HOME'] +
        '/LHCb_software/contrib/LHCb_artist/0.2/LHCb_artist',
        env=myEnv).wait()


#
import PR_Viewer, PRplot, Event_Tree, os


def makeHIVs(flagComp=False):
    hivFiles = []
    # remove other regions to make HIV files
    session().setParameter('modeling.lineWidth', '4.')
    session().setParameter("modeling.precision", '50')
    ui().setParameter('Panoramix_PRViewer_input_addinfo.value', 'False')
    page = ui().currentPage()
    page.deleteRegions()
    Page().titleVisible(False)
    Page().createDisplayRegions(1, 1, 0)
    PRplot.Magnet_view()
    session().setParameter('modeling.modeling', 'wireFrame')
    uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Ecal')
    uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Hcal')
    session().setParameter('modeling.modeling', 'solid')
    session().setParameter('modeling.lineWidth', '8.')
    session().setParameter('modeling.markerSize',
                           '20')  # somehow does not work for R clusters
    PR_Viewer.execute(version='single')
    session().setParameter('modeling.lineWidth', '10.')
    displayMuonTrack(result['mutrack'])
    Style().setColor('lightblue')
    Object_visualize(result['hhtracks'][0])
    Object_visualize(result['hhtracks'][1])
    ui().synchronize()
    tevt = "%05d" % (evt['DAQ/ODIN'].eventNumber())
    ttext = str(evt['DAQ/ODIN'].runNumber()) + '_' + tevt + '_0'
    PRplot.wprint(ttext, 'hiv')
    hivFiles.append(ttext + '.hiv')
    print ttext
    # remove other regions to make HIV files
    page = ui().currentPage()
    page.deleteRegions()
    Page().titleVisible(False)
    Page().createDisplayRegions(1, 1, 0)
    Region().clear()
    print 'event ', evt['DAQ/ODIN'].eventNumber()
    azoom = 'zx-proj'
    if flagComp:
        makeArtist('30.', '-10000.', '12000.', '4.2', '-2.5', '-30.1',
                   '-0.57735', '-0.57735', '-0.57735', '2.0943')
        nfile = os.environ[
            'HOME'] + '/LHCb_software/contrib/LHCb_artist/0.2/LHCb_artist/LHCb_artist_X11_proj'
        os.system('cp ' + nfile + ' ' + nfile.replace('proj', 'projzx'))
    ui().setParameter('Panoramix_Particle_input_azoom.value', azoom)
    #scolor    = ui().parameterValue('Panoramix_Particle_input_color.value')
    session().setParameter('modeling.lineWidth', '10.')
    ui().setParameter('Panoramix_Particle_input_text.value', 'no')
    ui().setParameter('Panoramix_Particle_input_action.value', 'visualize')
    ui().setParameter('Panoramix_Particle_input_precision.value', '50')
    ui().setParameter('Panoramix_Particle_input_otherPVs.value', 'yes')
    ui().setParameter('Panoramix_Particle_input_force.value', 'yes')
    Event_Tree.execute(artist=True)
    ui().executeScript('DLD', 'Panoramix layout_removeRulers')
    tevt = "%05d" % (evt['DAQ/ODIN'].eventNumber())
    ttext = str(evt['DAQ/ODIN'].runNumber()) + '_' + tevt + '_' + azoom
    PRplot.wprint(ttext, 'hiv')
    hivFiles.append(ttext + '.hiv')
    tsf = Page().currentRegion().cast_SoDisplayRegion().getTransform()
    tsf.scaleFactor.setValue(iv.SbVec3f(1., 1., 1.))
    PRplot.Magnet_view()
    session().setParameter('modeling.modeling', 'wireFrame')
    uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Ecal')
    uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Hcal')
    session().setParameter('modeling.modeling', 'solid')
    PRplot.disp_CaloMuon(False)
    displayMuonTrack(result['mutrack'])
    ui().synchronize()
    ttext = str(evt['DAQ/ODIN'].runNumber()) + '_' + tevt + '_3d'
    PRplot.wprint(ttext, 'hiv')
    hivFiles.append(ttext + '.hiv')
    if flagComp:
        makeArtist('19000.', '-12000.', '14500.', '-1387.99', '-597.965',
                   '10582.7', '-0.905421', '-0.134469', '0.402654', '3.82164')
        nfile = os.environ[
            'HOME'] + '/LHCb_software/contrib/LHCb_artist/0.2/LHCb_artist/LHCb_artist_X11_proj'
        os.system('cp ' + nfile + ' ' + nfile.replace('proj', 'proj3d'))


# DOES WORK ! add light to hiv files:
    for afile in hivFiles:
        if not afile.find('.hiv') > -1: continue
        tf = open(afile)
        tfl = tf.readlines()
        of = open('temp', 'w')
        k = 0
        while tfl[k].find("Separator") < 0:
            of.write(tfl[k])
            k += 1
        of.write(tfl[k])
        # works for top view
        of.write("  SoDirectionalLight {\n")
        of.write("  direction 5 0 -1 \n")
        of.write("  }\n")
        of.write("  SoDirectionalLight {\n")
        of.write("  direction -5 0 1  \n")
        of.write("  }\n")
        of.write("  SoDirectionalLight {\n")
        of.write("  direction 1 -5 0 \n")
        of.write("  }\n")
        of.write("  SoDirectionalLight {\n")
        of.write("  direction -1 5 0  \n")
        of.write("  }\n")
        of.write("  SoDirectionalLight {\n")
        of.write("  direction -1 0 5  \n")
        of.write("  }\n")
        of.write("  SoDirectionalLight {\n")
        of.write("  direction 0 -1 5  \n")
        of.write("  }\n")

        for n in range(k + 1, len(tfl)):
            of.write(tfl[n])
        of.close()
        rc = os.system("mv temp " + afile)
        if not afile.find('rz') < 0:
            os.system(
                "~/LHCb_software/contrib//LHCb_artist/0.2/LHCb_artist/LHCb_artist_X11_rz "
                + afile)
        elif not afile.find('projzx') < 0:
            os.system(
                "~/LHCb_software/contrib//LHCb_artist/0.2/LHCb_artist/LHCb_artist_X11_projzx "
                + afile)
        elif not afile.find('proj3d') < 0:
            os.system(
                "~/LHCb_software/contrib//LHCb_artist/0.2/LHCb_artist/LHCb_artist_X11_proj3d "
                + afile)
        else:
            os.system(
                "~/LHCb_software/contrib//LHCb_artist/0.2/LHCb_artist/LHCb_artist_X11 "
                + afile)
        rc = os.system("mv out.jpg " + afile.replace('.hiv', '_huge.jpg'))
        rc = os.system("cp " + afile.replace('.hiv', '_huge.jpg') +
                       ' /media/Work/Bolek')

        if not afile.find('proj3d') < 0:
            os.system(
                "~/LHCb_software/contrib//LHCb_artist/0.2/LHCb_artist/LHCb_artist_X11 "
                + afile)
            rc = os.system("mv out.jpg " +
                           afile.replace('3d.hiv', 'top_huge.jpg'))
            rc = os.system("cp " + afile.replace('3d.hiv', 'top_huge.jpg') +
                           ' /media/Work/Bolek')


def makeOnlyHugePlots():
    hivFiles = [
        '93031_523413416_3d.hiv', '93031_523413416_0.hiv',
        '93031_523413416_zx-proj.hiv'
    ]
    for afile in hivFiles:

        if not afile.find('rz') < 0:
            os.system(
                "~/LHCb_software/contrib//LHCb_artist/0.2/LHCb_artist/LHCb_artist_X11_ev \
                   -hcam=800. -xpos=300. -ypos=0. -zpos=100. -xorie=0. -yorie=1. -zorie=0. -angle=0. \
                   " + afile)
        elif not afile.find('zx') < 0:
            os.system(
                "~/LHCb_software/contrib//LHCb_artist/0.2/LHCb_artist/LHCb_artist_X11_ev \
                   -hcam=30. -znear=-10000. -zfar=12000. -xpos=4.2 -ypos=-2.5 -zpos=-30.1 -xorie=-0.57735 -yorie=-0.57735 -zorie=-0.57735 -angle=2.0943 \
                   " + afile)
        elif not afile.find('3d') < 0:
            os.system(
                "~/LHCb_software/contrib//LHCb_artist/0.2/LHCb_artist/LHCb_artist_X11_ev \
                   -hcam=19000. -znear=-12000. -zfar=14500. -xpos=1387.99 -ypos=-597.965 -zpos=10582.7 -xorie=-0.905421 -yorie=-0.134469 -zorie=0.402654 -angle=3.82164 \
                   " + afile)
        else:
            os.system(
                "~/LHCb_software/contrib//LHCb_artist/0.2/LHCb_artist/LHCb_artist_X11 "
                + afile)
        rc = os.system("mv out.jpg " + afile.replace('.hiv', '_huge.jpg'))
        rc = os.system("cp " + afile.replace('.hiv', '_huge.jpg') +
                       '  /media/Work/Bolek')

        if not afile.find('3d') < 0:
            os.system(
                "~/LHCb_software/contrib//LHCb_artist/0.2/LHCb_artist/LHCb_artist_X11_ev "
                + afile)
            rc = os.system("mv out.jpg " +
                           afile.replace('3d.hiv', 'top_huge.jpg'))
            rc = os.system("cp " + afile.replace('3d.hiv', 'top_huge.jpg') +
                           '  /media/Work/Bolek')
