###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#
#  Example script to manipulate a STL from Python.
#
#  Execute this script with :
#    OS> <setup Panoramix>
#    OS> python gb_test_stl.py
#

import gaudimodule

ss = gaudimodule.gbl.std.vector(str)()
ss.push_back('item 1')
ss.push_back('item 2')
print ss.size()
print ss[0]
print ss[1]
