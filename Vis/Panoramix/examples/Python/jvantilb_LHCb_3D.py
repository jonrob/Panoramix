###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import Panoramix

##################################################################
# Draw a nice 3D picture of LHCb                                 #
##################################################################
uiSvc = Panoramix.uiSvc()

region = Panoramix.Region()  # To work on the current region.
region.clear()
region.setBackgroundColor('white')

# Set the camera and background
camera = Panoramix.Camera()
camera.setHeight(18750)
camera.setPosition(-16464, 1921, 4115)
camera.setOrientation(0.0135, 0.99857, 0.05166, 4.333)

# Defaults
style = Panoramix.Style()
style.setSolid()
style.setTransparency(0)

# Use colors from color.xml
style.useVisSvc()

# Draw the beam pipe, Rich1 and Rich2
uiSvc.visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1')
uiSvc.visualize('/dd/Structure/LHCb/AfterMagnetRegion/Rich2')

# Use different colors than the standard colors from color.xml
style.dontUseVisSvc()

style.setRGB(0.5, 0.5, 0.5)
#uiSvc.visualize('/dd/Structure/LHCb/Pipe')

# Draw the Velo stations in blue
style.setColor('blue')
uiSvc.visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Velo')

# Draw TT in purple
style.setColor('magenta')
uiSvc.visualize('/dd/Structure/LHCb/BeforeMagnetRegion/TT/TTa')
uiSvc.visualize('/dd/Structure/LHCb/BeforeMagnetRegion/TT/TTb')
uiSvc.visualize('/dd/Structure/LHCb/AfterMagnetRegion/T/IT/Station1')
uiSvc.visualize('/dd/Structure/LHCb/AfterMagnetRegion/T/IT/Station2')
uiSvc.visualize('/dd/Structure/LHCb/AfterMagnetRegion/T/IT/Station3')

# Draw the magnet in red
style.setColor('red')
uiSvc.visualize('/dd/Structure/LHCb/MagnetRegion/Magnet')

# Draw OT1 to OT3 in cyan
style.setColor('cyan')
for i in range(1, 4):
    t = '/dd/Structure/LHCb/AfterMagnetRegion/T/OT/T%01d' % i
    uiSvc.visualize(t)

# Draw the Ecal in blue
style.setColor('blue')
uiSvc.visualize('/dd/Structure/LHCb/DownstreamRegion/Ecal')

# Draw the Hcal in dark blue
style.setRGB(0, 0, 0.7)
uiSvc.visualize('/dd/Structure/LHCb/DownstreamRegion/Hcal')

# Draw the Muon chambers in green
style.setColor('green')
uiSvc.visualize('/dd/Structure/LHCb/DownstreamRegion/Muon')

style.setColor('green')
style.useVisSvc()
