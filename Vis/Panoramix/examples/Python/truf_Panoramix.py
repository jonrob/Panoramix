###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from ROOT import TCanvas, TH1F, TH2F, TBrowser, gROOT, gSystem
import os, gaudimodule
gSystem.Load('libMathCore')

outputlevel = 3  ## DEBUG 2, INFO 3, WARNING 4, ERROR 5, FATAL 6
try:
    if os.environ['Panoramix_opts']:
        joboptions = os.environ['Panoramix_opts']
        print 'use opts ', joboptions
except:
    #joboptions = '$PANORAMIXROOT/options/tr_test_rec.opts'
    #joboptions = '$PANORAMIXROOT/options/tr_test_digi.opts'
    #joboptions = '$PANORAMIXROOT/options/tr_test_sim.opts'
    #joboptions = '$PANORAMIXROOT/options/gb_test_detector.opts'
    #joboptions = '$PANORAMIXROOT/options/gb_test_root.opts'
    #joboptions = '$PANORAMIXROOT/options/DetDescVis.opts'
    joboptions = '$PANORAMIXROOT/options/Panoramix.opts'
    print 'use default opts ', joboptions

appMgr = gaudimodule.AppMgr(outputlevel, joboptions)

#/////////////////////////////////////
#// Common Panoramix init :        ///
#/////////////////////////////////////

from panoramixmodule import *

# Give control to the GUI :
toui()
