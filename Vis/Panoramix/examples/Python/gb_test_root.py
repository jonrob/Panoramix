###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#
#  X11 : The X11 ROOT graphic behing launched in thread mode
# (XInitThread), if executing this script within Panoramix,
# then the OnX/Xt driver should have been also started in
# threded mode. This is done by having in job options :
#   OnXSvc.Threaded = true;
#
#  If having POOL around, someone have to create before it
# a graphical TApplication. This could be done by having :
#   ApplicationMgr.DLLs += {"RootSvc"};
#   ApplicationMgr.ExtSvc += {"RootSvc"};
# before the POOL options.
#

print 'gb_test_root : import the pain...'
import ROOT

ROOT.gROOT.Reset()

canvas = None
if ROOT.gPad == None:
    canvas = ROOT.TCanvas()
else:
    canvas = ROOT.gPad.GetCanvas()

histo = ROOT.TH1D("Random_gauss", "Random Gauss", 100, -5, 5)

import random
r = random.Random()
print 'gb_test_root : fill histogram...'
for I in range(0, 10000):
    histo.Fill(r.gauss(0, 1), 1)
del r

print 'gb_test_root : plot histogram...'
histo.Draw()

canvas.Modified()
canvas.Update()
