###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
setenv COIN_DEBUG_SOOFFSCREENRENDERER 1
setenv COIN_FORCE_TILED_OFFSCREENRENDERING 0
setenv COIN_OFFSCREENRENDERER_TILEWIDTH 6000
setenv COIN_OFFSCREENRENDERER_TILEHEIGHT 6000
