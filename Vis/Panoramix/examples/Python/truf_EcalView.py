###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from panoramixmodule import *
import time, Calo_Viewer

# Vertex view :
if not ui().findWidget('Ecal'):
    print 'create Ecal widget'
    ui().createComponent('Ecal', 'PageViewer', 'ViewerTabStack')
    ui().setCallback('Ecal', 'collect', 'DLD', 'OnX viewer_collect @this@')
    ui().setCallback('Ecal', 'popup', 'DLD',
                     'Panoramix Panoramix_viewer_popup')
    ui().setParameter(
        'Ecal.popupItems',
        'Current region\nNo highlighted\nTrack_Clusters\nTrack_Measurements\nTrack_MCParticle\nMCParticle_Track\nParticle_MCParticle\nParticle_Daughters\nMCParticle_Clusters\nCluster_MCParticles\nMCParticle_MCHits'
    )
    Page().setTitle('Ecal')
    Page().titleVisible(True)
    # Setup region (a page can have multiple drawing region) :
    Page().createDisplayRegions(1, 2, 0)
    Style().setColor('black')
    Style().setWireFrame()
    Style().setOpened()
    Style().dontUseVisSvc()
    # region for display of towers
    Page().setCurrentRegion(0)
    Viewer().setFrame()
    # Camera positionning :
    Camera().setPosition(500.9, 3000., -1000.)
    Camera().setHeight(5000.)
    Camera().setOrientation(-0.7, 0.7, 0.07, 2.9)
    # region for display of detector, xy proj
    Page().setCurrentRegion(1)
    Viewer().setFrame()
    # Camera positionning :
    Camera().setPosition(0., 0., 20000.)
    Camera().setHeight(7500.)
    Camera().setOrientation(0., 0., 1., 0.)
    Style().setColor('orange')
    Style().setOpened()
    uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Ecal/EcalInner')
    Style().setColor('red')
    uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Ecal/EcalOuter')
    Style().setSolid()
    Style().setColor('grey')
    uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/PipeDownstream')
    uiSvc().visualize(
        '/dd/Structure/LHCb/AfterMagnetRegion/Rich2/Rich2BeamPipe')
    uiSvc().visualize('/dd/Structure/LHCb/MagnetRegion/PipeInMagnet')
    Style().setSolid()
    Style().useVisSvc()

ui().setCurrentWidget(ui().findWidget('Ecal'))

# calo hits
ecolor = session().parameterValue('Panoramix_Calo_input_ecolor')
if ecolor == '': Calo_Viewer.defaults()
ecolor = session().parameterValue('Panoramix_Calo_input_ecolor')
Ecalenergy = session().parameterValue('Panoramix_Calo_input_Ecalenergy')

Style().dontUseVisSvc()
Style().setSolid()
if Ecalenergy != 'n':
    Style().setColor(hcolor)
    #  trigger decoding
    Ecal = evt['Raw/Ecal/Digits']
    Page().setCurrentRegion(0)
    Region().eraseEvent()
    data_collect(da(), 'EcalDigits', '(e>' + Ecalenergy + ')')
    data_visualize(da())
    ui().synchronize()
    Page().setCurrentRegion(1)
    Region().eraseEvent()
    data_collect(da(), 'EcalDigits', '(e>' + Ecalenergy + ')')
    data_visualize(da())
Style().setWireFrame()
Style().useVisSvc()
