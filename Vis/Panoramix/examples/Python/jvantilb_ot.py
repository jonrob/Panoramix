###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Panoramix import *

##################################################################
# Draw OT for tracking event display                             #
##################################################################

# Clear the page
page = ui.currentPage()
region = page.currentRegion()
region.clear()

# Set the camera and background
region_setCamera('height 5000', 'position 0 2000 9000',
                 'orientation -0.577 -0.577 -0.577 2.0943')
region.setParameter('color', 'white')

# Use different colors than the standard colors from color.xml
session.setParameter('modeling.useVisSvc', 'false')

# Draw OT
session.setParameter('modeling.modeling', 'wireFrame')
session.setParameter('modeling.color', '0 0.5 0')
uiSvc.visualize('/dd/Structure/LHCb/OT')
session.setParameter('modeling.modeling', 'solid')
session.setParameter('modeling.color', '0.9 0.9 0.9')
uiSvc.visualize('/dd/Structure/LHCb/Pipe')
