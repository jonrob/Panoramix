###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from ROOT import TMinuit, Long, Double, TH2F, TH1F, TCanvas, TText, gStyle, gROOT, TF1
from array import array
from PRplot import *
import GaudiPython

newcont = GaudiPython.makeClass(
    'KeyedContainer<LHCb::Track,Containers::KeyedObjectManager<Containers::hashmap> >'
)
Track = GaudiPython.gbl.LHCb.Track
LHCbID = GaudiPython.gbl.LHCb.LHCbID
State = GaudiPython.gbl.LHCb.State
# to force loading of dictionary for XYZTPoint
dummy = GaudiPython.gbl.MuonTrack()
XYZTPoint = GaudiPython.gbl.Gaudi.XYZTPoint
location = '/Event/Rec/Muon/Track'

muon = det['/dd/Structure/LHCb/DownstreamRegion/Muon']
m_cosmicTool = tsvc.create('MuonNNetRec', interface='IMuonNNetRec')

h_slopes = TH2F('h_slopes', 'tx / ty ', 50, -3.14, 3.14, 50, -3.14, 3.14)
res_hcal_x = TH1F('res_hcal_x', 'res_hcal_x', 100, -3000., 3000.)
res_hcal_y = TH1F('res_hcal_y', 'res_hcal_y', 100, -3000., 3000.)
res_ecal_x = TH1F('res_ecal_x', 'res_ecal_x', 100, -3000., 3000.)
res_ecal_y = TH1F('res_ecal_y', 'res_ecal_y', 100, -3000., 3000.)

del_hcal_x = TH1F('del_hcal_x', 'del_hcal_x', 100, -300., 300.)
del_hcal_y = TH1F('del_hcal_y', 'del_hcal_y', 100, -300., 300.)
del_ecal_x = TH1F('del_ecal_x', 'del_ecal_x', 100, -300., 300.)
del_ecal_y = TH1F('del_ecal_y', 'del_ecal_y', 100, -300., 300.)
ff = TF1('ff', '[0]+gaus(1)')


def store_track(track, hits=''):
    t = Track()
    GaudiPython.setOwnership(t, False)
    s1 = State()
    GaudiPython.setOwnership(s1, False)
    s2 = State()
    GaudiPython.setOwnership(s2, False)
    z1 = 7000.
    x1, y1 = trackxy(track, z1)
    z2 = 20000.
    x2, y2 = trackxy(track, z2)
    s1.setX(x1)
    s1.setY(y1)
    s1.setZ(z1)
    s1.setTx((x2 - x1) / (z2 - z1))
    s1.setTy((y2 - y1) / (z2 - z1))
    s1.setQOverP(0.00001)
    t.addToStates(s1)
    s2.setX(x2)
    s2.setY(y2)
    s2.setZ(z2)
    s2.setTx((x2 - x1) / (z2 - z1))
    s2.setTy((y2 - y1) / (z2 - z1))
    s2.setQOverP(0.00001)
    t.addToStates(s2)
    if hits != '':
        for hit in hits:
            tile = hit.tile()
            t.addToLhcbIDs(LHCbID(tile))
    if not evt[location]:
        cosmic_cont = newcont()
        GaudiPython.setOwnership(cosmic_cont, False)
        cosmic_cont.add(t)
        evt.registerObject(location, cosmic_cont)
    else:
        evt[location].add(t)


def store_track_direct(track):
    pos = XYZTPoint()
    t = Track()
    GaudiPython.setOwnership(t, False)
    s1 = State()
    GaudiPython.setOwnership(s1, False)
    s2 = State()
    GaudiPython.setOwnership(s2, False)
    z1 = 7000.
    track.extrap(z1, pos)
    x1, y1 = pos.x(), pos.y()
    z2 = 20000.
    track.extrap(z2, pos)
    x2, y2 = pos.x(), pos.y()
    s1.setX(x1)
    s1.setY(y1)
    s1.setZ(z1)
    s1.setTx(track.sx())
    s1.setTy(track.sy())
    s1.setQOverP(0.00001)
    t.addToStates(s1)
    s2.setX(x2)
    s2.setY(y2)
    s2.setZ(z2)
    s2.setTx(track.sx())
    s2.setTy(track.sy())
    s2.setQOverP(0.00001)
    t.addToStates(s2)
    for hit in track.getHits():
        tile = hit.tile()
        t.addToLhcbIDs(LHCbID(tile))
    if not evt[location]:
        cosmic_cont = newcont()
        GaudiPython.setOwnership(cosmic_cont, False)
        cosmic_cont.add(t)
        evt.registerObject(location, cosmic_cont)
    else:
        evt[location].add(t)


def fcn(npar, gin, f, par, iflag):
    #calculate chisquare
    chisq = 0
    global points, epoints
    # (x,y) = (tx,ty)*z + (px,py)
    for p in range(len(points)):
        point = points[p]
        epoint = epoints[p]
        deltax = point[0] - (point[2] * par[2] + par[0])
        deltay = point[1] - (point[2] * par[3] + par[1])
        edeltax = (epoint[0] * epoint[0])
        edeltax += (epoint[2] * par[2]) * (epoint[2] * par[2])
        edeltay = (epoint[1] * epoint[1])
        edeltay += (epoint[2] * par[3]) * (epoint[2] * par[3])
        if iflag == 3:
            print 'chi2', p, (
                deltax * deltax / edeltax + deltay * deltay / edeltay)
        chisq += (deltax * deltax / edeltax + deltay * deltay / edeltay)
    f[0] = chisq
    return


def outlier_removal(par):
    global points, epoints
    outlier = []
    eoutlier = []
    for p in range(len(points)):
        point = points[p]
        epoint = epoints[p]
        deltax = point[0] - (point[2] * par[2] + par[0])
        deltay = point[1] - (point[2] * par[3] + par[1])
        edeltax = (epoint[0] * epoint[0])
        edeltax += (epoint[2] * par[2]) * (epoint[2] * par[2])
        edeltay = (epoint[1] * epoint[1])
        edeltay += (epoint[2] * par[3]) * (epoint[2] * par[3])
        chi2 = (deltax * deltax / edeltax + deltay * deltay / edeltay)
        print 'chi2', p, chi2
        if chi2 > 100:
            outlier.append(point)
            eoutlier.append(epoint)
    print "remove ", len(outlier), " points"
    for o in outlier:
        points.remove(o)
    for o in eoutlier:
        epoints.remove(o)
    return


def outliermax_removal(par):
    global points, epoints
    chi2max = 10000
    outlier = []
    eoutlier = []
    sc = -1
    for p in range(len(points)):
        point = points[p]
        epoint = epoints[p]
        deltax = point[0] - (point[2] * par[2] + par[0])
        deltay = point[1] - (point[2] * par[3] + par[1])
        edeltax = (epoint[0] * epoint[0])
        edeltax += (epoint[2] * par[2]) * (epoint[2] * par[2])
        edeltay = (epoint[1] * epoint[1])
        edeltay += (epoint[2] * par[3]) * (epoint[2] * par[3])
        chi2 = (deltax * deltax / edeltax + deltay * deltay / edeltay)
        print 'chi2', p, chi2
        if chi2 > chi2max:
            chi2max = chi2
            eoutlier = epoint
            outlier = point
    if chi2max > 100:
        points.remove(outlier)
        epoints.remove(eoutlier)
        sc = 0
    return sc


def fillpoints():
    points = []
    epoints = []
    for coord in evt['Raw/Muon/Coords']:
        x = Double(0)
        y = Double(0)
        z = Double(0)
        deltax = Double(0)
        deltay = Double(0)
        deltaz = Double(0)
        tile = coord.key()
        muon.Tile2XYZ(tile, x, deltax, y, deltay, z, deltaz)
        points.append([x, y, z])
        epoints.append([deltax, deltay, deltaz])
    return points, epoints


def trackxy(track, z):
    x = track[0] + track[2] * z
    y = track[1] + track[3] * z
    return x, y


def drawline(track):
    LINES = 0
    XYZPoint = GaudiPython.gbl.Math.XYZPoint
    ps = GaudiPython.gbl.std.vector(XYZPoint)()
    x, y = trackxy(track, 7000.)
    po = XYZPoint(x, y, 7000.)
    ps.push_back(po)
    x, y = trackxy(track, 20000.)
    po = XYZPoint(x, y, 20000.)
    ps.push_back(po)
    Style().setColor('green')
    uiSvc().visualize(ps, LINES)


def runMinuit():
    global points, epoints
    gMinuit = TMinuit(4)
    # initialize TMinuit with a maximum of 4 params
    gMinuit.SetFCN(fcn)
    arglist = array('d', [500, 1.])
    vstart = array('d', [0., 0., 0., 0.])
    step = array('d', [1., 1., 1., 1.])
    ierflg = Long(0)
    # initial guess
    meanx, meany = 0., 0.
    for p in points:
        meanx += p[0]
        meany += p[1]
    vstart[0] = meanx / float(len(points) + 1)
    vstart[1] = meany / float(len(points) + 1)
    gMinuit.SetPrintLevel(-1)
    gMinuit.mnparm(0, "px", vstart[0], step[0], 0, 0, ierflg)
    gMinuit.mnparm(1, "py", vstart[1], step[1], 0, 0, ierflg)
    gMinuit.mnparm(2, "tx", vstart[2], step[2], 0, 0, ierflg)
    gMinuit.mnparm(3, "ty", vstart[3], step[3], 0, 0, ierflg)
    gMinuit.mnexcm("SIMPLEX", arglist, 4, ierflg)
    gMinuit.mnexcm("MIGRAD", arglist, 4, ierflg)
    #gMinuit.mnexcm("MINOS",arglist,4,ierflg)
    track = [Double(0), Double(0), Double(0), Double(0)]
    etrack = [Double(0), Double(0), Double(0), Double(0)]
    for i in range(4):
        gMinuit.GetParameter(i, track[i], etrack[i])
    return track, etrack


def select():
    odin = 1
    while odin:
        appMgr.run(1)
        s = [0, 0, 0, 0, 0]
        for coord in evt['Raw/Muon/Coords']:
            station = coord.digitTile()[0].station()
            s[station] += 1
        if s[4] > 0 and s[3] > 0 and s[2] > 0 and s[1] > 0: break
    odin = evt['DAQ/ODIN']
    if odin:
        print odin.runNumber(), odin.eventNumber(), s[4], s[3], s[2], s[1]
        Page().currentRegion().clear("dynamicScene")
        Style().setColor('green')
        data_collect(da(), 'MuonCoord', '')
        data_visualize(da())
        Style().setColor('blue')
        data_collect(da(), 'HcalDigits', '(e>150)')
        Style().setColor('red')
        data_collect(da(), 'EcalDigits', '(e>1000)')
    return odin


def xzview():
    # Camera positioning :
    Camera().setPosition(-9042.79, 0, 13507.5)
    Camera().setHeight(15000.)
    Camera().setOrientation(0, -1, 0, 1.57)
    Camera().setNearFar(8000., 11000.)


def xyview():
    # Camera positioning :
    Camera().setPosition(0, 0, 20000)
    Camera().setHeight(13000.)
    Camera().setOrientation(0, 0, 1, 0)
    Camera().setNearFar(0., 13000.)


# nice events:
# 21396 5064 1 2 2 1
# 21396 5844
#       6156
#       7313
#       8912


def setup():
    Magnet_view()
    session().setParameter('modeling.modeling', 'wireFrame')
    uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Ecal')
    uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Hcal')
    session().setParameter('modeling.modeling', 'solid')


def evtloop():
    global points, epoints
    plots = True
    w3d = False
    h_slopes.Reset()
    odin = 0
    # event loop
    while odin:
        odin = select()
        if not odin:
            print 'odin = None'
            break
        points, epoints = fillpoints()
        sc = 0
        while sc == 0:
            track, etrack = runMinuit()
            sc = outliermax_removal(track)
        if len(points) > 3:
            track, etrack = runMinuit()
            h_slopes.Fill(track[2], track[3])
            print 'track slopes', track[2], track[3]
            #   drawline(track)
            store_track(track)
            Object_visualize(evt[location][0])
            xyview()
            ui().synchronize()
            if plots:
                widget = ui().currentWidget()
                fname = 'cosmics_calo_muon_xy_' + str(odin.eventNumber())
                widget.write(fname + '.jpg', 'JPEG100')
                xzview()
                ui().synchronize()
                widget = ui().currentWidget()
                fname = 'cosmics_calo_muon_xz_' + str(odin.eventNumber())
                widget.write(fname + '.jpg', 'JPEG100')
                if w3d:
                    Region().write_wrl()
                    f = open('out.wrl')
                    a = f.readlines()
                    f.close()
                    x = len(a) - 1
                    while a[x].find('diffuseColor') < 0:
                        x -= 1
                    new = a[x].replace('diffuseColor', 'emissiveColor')
                    a.insert(x, new)
                    f = open(fname + '.wrl', 'w')
                    f.writelines(a)
                    f.close()


def correctwrl():
    import os
    c = os.listdir('')
    for fn in c:
        if fn.find('.wrl') != -1:
            f = open(fn)
            a = f.readlines()
            f.close()
            x = len(a) - 1
            while a[x].find('diffuseColor') < 0:
                x -= 1
            new = a[x].replace('diffuseColor', 'emissiveColor')
            a.insert(x, new)
            f = open(fn, 'w')
            f.writelines(a)
            f.close()


def test():
    global points, epoints
    for h in gROOT.GetList():
        h.Reset()
    odin = 0
    while odin:
        odin = select()
        points, epoints = fillpoints()
        sc = 0
        while sc == 0:
            track, etrack = runMinuit()
            sc = outliermax_removal(track)
        if len(points) > 3:
            track, etrack = runMinuit()
            h_slopes.Fill(track[2], track[3])
            print 'track slopes', track[2], track[3]
            store_track(track)
            Object_visualize(evt[location][0])
            cal_residual(track)
            ui().synchronize()


def searchOrPlot(sel):
    # main entry to muon cosmic track
    global points, epoints
    while 1 > 0:
        if not evt['DAQ/ODIN']: break
        if sel: appMgr.run(1)
        muonTracks = m_cosmicTool.tracks()
        if not sel or muonTracks.size() > 0: break
    if evt['DAQ/ODIN']:
        for tr in muonTracks:
            points = []
            epoints = []
            for hit in tr.getHits():
                tile = hit.tile()
                x = Double(0)
                y = Double(0)
                z = Double(0)
                deltax = Double(0)
                deltay = Double(0)
                deltaz = Double(0)
                muon.Tile2XYZ(tile, x, deltax, y, deltay, z, deltaz)
                points.append([x, y, z])
                epoints.append([deltax, deltay, deltaz])
            if len(points) > 2:
                track, etrack = runMinuit()
                #store_track(track,tr.getHits())
                store_track_direct(tr)


def cal_residual(track):
    Ecal = det['/dd/Structure/LHCb/DownstreamRegion/Ecal']
    Hcal = det['/dd/Structure/LHCb/DownstreamRegion/Hcal']
    for d in evt['/Event/Raw/Hcal/Digits']:
        if d.e() > 150:
            cid = d.cellID()
            x = Hcal.cellX(cid)
            y = Hcal.cellY(cid)
            z = Hcal.cellZ(cid)
            pl_mi = Hcal.plane(0.)
            newz = -(pl_mi.D() + pl_mi.A() * x + pl_mi.B() * y) / pl_mi.C()
            trax, tray = trackxy(track, newz)
            res_hcal_x.Fill(x - trax)
            res_hcal_y.Fill(y - tray)
            traxz, trayz = trackxy(track, z)
            del_hcal_x.Fill(trax - traxz)
            del_hcal_y.Fill(tray - trayz)
            print 'hcal', trax - traxz, tray - trayz
    for d in evt['/Event/Raw/Ecal/Digits']:
        if d.e() > 1000:
            cid = d.cellID()
            x = Ecal.cellX(cid)
            y = Ecal.cellY(cid)
            z = Ecal.cellZ(cid)
            pl_mi = Ecal.plane(0.)
            newz = -(pl_mi.D() + pl_mi.A() * x + pl_mi.B() * y) / pl_mi.C()
            trax, tray = trackxy(track, newz)
            res_ecal_x.Fill(x - trax)
            res_ecal_y.Fill(y - tray)
            traxz, trayz = trackxy(track, z)
            del_ecal_x.Fill(trax - traxz)
            del_ecal_y.Fill(tray - trayz)
            print 'ecal', trax - traxz, tray - trayz


def histo_setup():
    tc = TCanvas('tc', 'first muon calo alignment', 1024, 768)
    tc.Divide(2, 2)


def fit_residuals():
    gStyle.SetOptStat(0)
    gStyle.SetOptFit(0)
    text = TText()
    if not tc:
        tc = TCanvas('tc', 'first muon calo alignment', 1024, 768)
        tc.Divide(2, 2)
    tc.cd(1)
    res_ecal_x.Fit('gaus', '', '', -750., 750.)
    fit_ff(res_ecal_x)
    tc.cd(2)
    res_ecal_y.Fit('gaus', '', '', -750., 750.)
    fit_ff(res_ecal_y)
    tc.cd(3)
    res_hcal_x.Fit('gaus', '', '', -1000., 1000.)
    fit_ff(res_hcal_x)
    tc.cd(4)
    res_hcal_y.Fit('gaus', '', '', -1000., 1000.)
    fit_ff(res_hcal_y)
    print_res(res_ecal_x, tc, 1)
    print_res(res_ecal_y, tc, 2)
    print_res(res_hcal_x, tc, 3)
    print_res(res_hcal_y, tc, 4)


def fit_ff(h):
    for i in range(3):
        ff.SetParameter(i + 1, h.GetFunction('gaus').GetParameter(i))
    h.Fit('ff', '', '', -2000., 2000.)


def print_res(h, tc, i):
    mean = h.GetFunction('ff').GetParameter(2)
    err = h.GetFunction('ff').GetParError(2)
    print h.GetName(), mean, '+/-', err
    tc.cd(i)
    tres = '(%5.1F +/- %5.1F)mm' % (mean, err)
    x = h.GetBinCenter(10)
    y = h.GetMaximum() * 0.8
    text = TText(x, y, tres)
    print x, y, tres
    text.DrawClone()
