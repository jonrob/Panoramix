###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Prints properties of L0 and L1 triggers

from panoramixmodule import *

print ' '
print(68 * ('*'))
print '*', (64 * (' ')), '*'
print '* ///////////////////////////////// ', (29 * (' ')), '*'
print '* /Properties of the LHCb Triggers/ ', (29 * (' ')), '*'
print '* ///////////////////////////////// ', (29 * (' ')), '*'

#---Produce all variables that will be required

# L0 decision
L0dir = evt['Trig/L0/L0DUReport']
L0dec = 0
L0dtype = 'failed'
L0Et = []
L0mupt = []
L0pvdec = -1

if L0dir:
    L0dec = L0dir.decision()
    print '*   condition Name  condition Value ', 29 * (' '), '*'
    for k in range(L0dir.conditionsValueSummaries().size()):
        print '* %15s          %d  %s   *' % (
            L0dir.conditionName(k), L0dir.conditionValue(k), 34 * (' '))
    print '*', 64 * '=', '*'
    print '*  channel Name  channel decision ', 31 * (' '), '*'
    for k in range(L0dir.conditionsValueSummaries().size()):
        print '* %18s          %d  %s   *' % (
            L0dir.channelName(k), L0dir.channelDecision(k), 31 * (' '))

    if L0dec > 0:
        L0dtype = 'passed'

    L0cal = evt['Trig/L0/Calo']
    if L0cal:
        for obj in L0cal:
            L0Et.append(obj.et())
    L0mu = evt['Trig/L0/MuonCtrl']
    if L0mu:
        for obj in L0mu:
            L0mupt.append(obj.pt())

    L0pv = evt['Trig/L0/PuVeto']
    if L0pv:
        L0pvdec = L0pv.decision()

#####################################################

# L1 decision
L1scDec = 0
L1GenDec = 0
L1dtype = 'failed'
extra = ''

L1sc = evt['Trig/L1Score']
if L1sc:

    L1scDec = L1sc.decision()
    L1GenDec = L1sc.decisionGen()
    if (L1scDec) > 0:
        L1dtype = 'passed'
    if (L0dec) == 0:
        extra = 'but would have already failed L0'
    if L1scDec == 0:
        L1dtype = 'failed'
    if L1scDec > 0:
        L1dtype = 'passed'
    L1MuDec = L1sc.decisionMu()
    L1diMuDec = L1sc.decisionDiMu()
    L1diMuJPsiDec = L1sc.decisionDiMuJPsi()
    L1elecDec = L1sc.decisionElec()
    L1PhotDec = L1sc.decisionPhot()
    L1pat = L1sc.decisionPattern()
    L1pt1 = L1sc.pt1()
    L1pt2 = L1sc.pt2()
    L1ptMu = L1sc.ptMu()
    L1diMuM = L1sc.diMuMass()
    L1diMuIP = L1sc.diMuIP()
    L1etElec = L1sc.etElec()
    L1etPhot = L1sc.etPhot()

############################################################

#HLTGenericScore
HLTdec = 0
HLTGensc = evt['Trig/HltGenericScore']
if HLTGensc:
    HLTGenDec = HLTGensc.decisionGen()

    if (HLTGenDec) == 0:
        HLTGendtype = 'failed'

    if HLTGenDec > 0:
        HLTGendtype = 'passed'

    HLTdec = HLTGensc.decision()
    HLTMuDec = HLTGensc.decisionMu()
    HLTdiMuDec = HLTGensc.decisionDiMu()
    HLTdiMuJPsiDec = HLTGensc.decisionDiMuJPsi()
    HLTelecDec = HLTGensc.decisionElec()
    HLTPhotDec = HLTGensc.decisionPhot()

    HLTpat = HLTGensc.decisionPattern()
    HLTpt1 = HLTGensc.pt1()
    HLTpt2 = HLTGensc.pt2()
    HLTptMu = HLTGensc.ptMu()
    HLTdiMuM = HLTGensc.diMuMass()
    HLTdiMuIP = HLTGensc.diMuIP()
    HLTetElec = HLTGensc.etElec()
    HLTetPhot = HLTGensc.etPhot()

###############################################

# HLT Score

HL = evt['Trig/HltScore']

if HL:
    HLdec = HL.decision()
    if (HLdec) == 0:
        HLTdtype = 'failed'

    if HLdec > 0:
        HLTdtype = 'passed'

    HLGenDec = HL.decisionGen()
    HLExclu = HL.decisionExclusive()
    HLIncluB = HL.decisionInclusiveB()
    HLdiMuDec = HL.decisionDimuon()
    HLDStarDec = HL.decisionDstar()
    HLpat = HL.decisionPattern()

    HLnbB = HL.nbB()
    HLnbDimu = HL.nbDimu()
    HLnbDstar = HL.nbDstar()
    HLnbMuon = HL.nbMuon()
    HLnbPV = HL.nbPV()
    HLnbPhoton = HL.nbPhoton()
    HLnbPi0 = HL.nbPi0()
    HLnbTrack = HL.nbTrack()

##############################################

# Algorithm's used and properties
# does not yet work for DC06
DC06 = -1
numAlg = 0
algID = []
algchi2 = []
algMass = []
if DC06 > 0:
    AlgDir = appMgr.tool('Algorithm2ID')
    prop = AlgDir.properties()
    AlgName = prop.values()
    if HL:
        can = HL.candidates()
        numAlg = can.size()
        if numAlg > 0:
            for k in range(0, numAlg):
                new = can.at(k)
                algID.append(new.algorithmID())
                algchi2.append(new.chi2())
                algMass.append(new.mass())

###############################################
#---Start of main method----------//

print '*', (64 * (' ')), '*'
if L0dir == 0 and L1GenDec == 0 and HLTdec == 0:
    print '* There is no Trigger info present in this data set', (
        14 * (' ')), '*'

print '*', (64 * (' ')), '*'

L0Et.sort()
L0mupt.sort()
if L0dir:
    L0Et.reverse()
    L0mupt.reverse()
    print L0Et

    print '* L0 Data ', (55 * (' ')), '*'
    print '* ~~~~~~~ ', (55 * (' ')), '*'
    print '* The event has', L0dtype, 'L0', (40 * (' ')), '*'
    print '*', (64 * (' ')), '*'
    print '%-30s %-15s * ' % (
        '* The Level 0 decision is                      => ', L0dec)
    print '*', (64 * (' ')), '*'
    if len(L0Et) > 0:
        print '%-30s %-15s * ' % (
            '* The Level 0 highest calorimeter energy is     => ', L0Et[0])
    if len(L0mupt) > 0:
        print '%-30s %-15s * ' % (
            '* The Level 0 Muon pt                          => ', L0mupt[0])
    if L0pvdec > -1:
        print '%-30s %-15s * ' % (
            '* The Level 0 PuVeto decision is               => ', L0pvdec)

print '*', (64 * (' ')), '*'

if L1sc:
    print '* L1 Data ', (55 * (' ')), '*'
    print '* ~~~~~~~ ', (55 * (' ')), '*'
    print '%-15s %-6s %-2s %-40s * ' % ('* The event has', L1dtype, 'L1',
                                        extra)
    print '*', (64 * (' ')), '*'
    print '%-30s %-15s * ' % (
        '* The Level 1 decision is                      => ', L1scDec)
    print '*', (64 * (' ')), '*'
    print '%-30s %-15s * ' % (
        '* The Level 1 Generic decision is              => ', L1GenDec)
    print '%-30s %-15s * ' % (
        '* The Level 1 Muon decision is                 => ', L1MuDec)
    print '%-30s %-15s * ' % (
        '* The Level 1 diMuon decision is               => ', L1diMuDec)
    print '%-30s %-15s * ' % (
        '* The Level 1 diMuon J/Psi decision is         => ', L1diMuJPsiDec)
    print '%-30s %-15s * ' % (
        '* The Level 1 Electron decision is             => ', L1elecDec)
    print '%-30s %-15s * ' % (
        '* The Level 1 Photon decision is               => ', L1PhotDec)
    print '*', (64 * (' ')), '*'
    print '%-30s %-15s * ' % (
        '* The pt for particle 1 is                     => ', '%7.2F' % L1pt1)
    print '%-30s %-15s * ' % (
        '* The pt for particle 2 is                     => ', '%7.2F' % L1pt2)
    print '%-30s %-15s * ' % (
        '* The pt of the hardest muon is                => ', '%7.2F' % L1ptMu)
    print '%-30s %-15s * ' % (
        '* The dimuon mass is                           => ',
        '%7.2F' % L1diMuM)
    print '%-30s %-15s * ' % (
        '* The impact parameter of the dimuon is        => ',
        '%7.2F' % L1diMuIP)
    print '%-30s %-15s * ' % (
        '* The Et of the electron is                    => ',
        '%7.2F' % L1etElec)
    print '%-30s %-15s * ' % (
        '* The Et of the photon is                      => ',
        '%7.2F' % L1etPhot)
    print '*', (64 * (' ')), '*'

if HLTGensc:

    print '* HLTGeneric Data ', (47 * (' ')), '*'
    print '* ~~~~~~~~~~~~~~~ ', (47 * (' ')), '*'
    print '%-15s %-6s %-2s %-32s * ' % ('* The event has', HLTGendtype,
                                        'HLTGeneric', extra)
    print '*', (64 * (' ')), '*'
    #print '%-30s %-15s * ' % ('* The HLT decision is                          => ', HLTdec)
    #print '%-30s %-15s * ' % ('* The HLT decision pattern is                  => ', HLTpat)
    print '%-30s %-15s * ' % (
        '* The HLT Generic decision is                  => ', HLTGenDec)
    print '*', (64 * (' ')), '*'
    print '%-30s %-15s * ' % (
        '* The HLT Muon decision is                     => ', HLTMuDec)
    print '%-30s %-15s * ' % (
        '* The HLT diMuon decision is                   => ', HLTdiMuDec)
    print '%-30s %-15s * ' % (
        '* The HLT diMuon J/Psi decision is             => ', HLTdiMuJPsiDec)
    print '%-30s %-15s * ' % (
        '* The HLT Electron decision is                 => ', HLTelecDec)
    print '%-30s %-15s * ' % (
        '* The HLT Photon decision is                   => ', HLTPhotDec)
    print '*', (64 * (' ')), '*'
    print '%-30s %-15s * ' % (
        '* The pt for particle 1 is                     => ', '%7.2F' % HLTpt1)
    print '%-30s %-15s * ' % (
        '* The pt for particle 2 is                     => ', '%7.2F' % HLTpt2)
    print '%-30s %-15s * ' % (
        '* The pt of the hardest muon is                => ',
        '%7.2F' % HLTptMu)
    print '%-30s %-15s * ' % (
        '* The dimuon mass is                           => ',
        '%7.2F' % HLTdiMuM)
    print '%-30s %-15s * ' % (
        '* The impact parameter of the dimuon is        => ',
        '%7.2F' % HLTdiMuIP)
    print '%-30s %-15s * ' % (
        '* The Et of the electron is                    => ',
        '%7.2F' % HLTetElec)
    print '%-30s %-15s * ' % (
        '* The Et of the photon is                      => ',
        '%7.2F' % HLTetPhot)
    print '*', (64 * (' ')), '*'

if HL:

    print '* HLT Data ', (54 * (' ')), '*'
    print '* ~~~~~~~~ ', (54 * (' ')), '*'
    print '%-15s %-6s %-2s %-39s * ' % ('* The event has', HLTdtype, 'HLT',
                                        extra)
    print '*', (64 * (' ')), '*'
    print '%-30s %-15s * ' % (
        '* The HLT decision is                          => ', HLdec)
    print '*', (64 * (' ')), '*'
    #print '%-30s %-15s * ' % ('* The HLT decision pattern is                  => ', HLpat)
    #print '%-30s %-15s * ' % ('* The HLT Generic decision is                  => ', HLGenDec)
    print '%-30s %-15s * ' % (
        '* The HLT Exclusive decision is                => ', HLExclu)
    print '%-30s %-15s * ' % (
        '* The HLT Inclusive B decision is              => ', HLIncluB)
    print '%-30s %-15s * ' % (
        '* The HLT diMuon decision is                   => ', HLdiMuDec)
    print '%-30s %-15s * ' % (
        '* The HLT Dstar decision is                    => ', HLDStarDec)
    print '*', (64 * (' ')), '*'
    print '%-30s %-15s * ' % (
        '* The number of Bs is                          => ', HLnbB)
    print '%-30s %-15s * ' % (
        '* The number of DiMuons is                     => ', HLnbDimu)
    print '%-30s %-15s * ' % (
        '* The number of Dstars is                      => ', HLnbDstar)
    print '%-30s %-15s * ' % (
        '* The number of Primary Vertices is            => ', HLnbPV)
    print '%-30s %-15s * ' % (
        '* The number of reconstructed Photons is       => ', HLnbPhoton)
    print '%-30s %-15s * ' % (
        '* The number of reconstructed Pi0s is          => ', HLnbPi0)
    print '%-30s %-15s * ' % (
        '* The number of reconstructer tracks is        => ', HLnbTrack)
    print '*', (64 * (' ')), '*'

if numAlg > 0:
    print '* Exclusive Algorithm Information ', (31 * (' ')), '*'
    print '* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ', (31 * (' ')), '*'
    print '*', (64 * (' ')), '*'
    for k in range(0, numAlg):
        print '* %-64s %-0s ' % (AlgName[0][algID[k]], '*')
        print '%-30s %-15s * ' % (
            '* Algorithm ID number                          => ', algID[k])
        print '%-30s %-15s * ' % (
            '* Chi2                                         => ',
            '%0.5F' % algchi2[k])
        print '%-30s %-15s * ' % (
            '* Mass                                         => ',
            '%0.2F' % algMass[k])
        print '*', (64 * (' ')), '*'

print '*', (64 * (' ')), '*'
print(68 * ('*'))
