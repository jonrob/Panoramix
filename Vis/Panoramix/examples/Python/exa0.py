###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#
#  This script shows the lauching of Panoramix
# from the python shell, how to send visualization orders
# from python and how to give control to the GUI.
# From the GUI, control is returned back to the
# python shell by clicking in the 'File/Exit" of the
# GUI.
#
#
# Usage :
#   OS> <setup Panoramix>
#   OS> python -i exa0.py
# or :
#   OS> python
#   >>> import exa0
#
# WARNING :
#  This script is intended to be executed from the python prompt
# to create "from scratch" a Gaudi and a Panoramix session.
# It is not advised to execute it within the Panoramix GUI
# (from the GUI Python prompt or from a GUI Python callback).
#

#/////////////////////////////////////
#// Create the session :           ///
#/////////////////////////////////////

import Panoramix as pmx

joboptions = '$PANORAMIXROOT/options/Panoramix.opts'
#joboptions = '$PANORAMIXROOT/options/DetDescVis.opts'
#joboptions = '$PANORAMIXROOT/options/gb_test_detector.opts'
#joboptions = '$PANORAMIXROOT/options/gb_test_sim.opts'
#joboptions = '$PANORAMIXROOT/options/gb_test_all.opts'
pmx.start(joboptions)

#/////////////////////////////////////////////////////
#// Visualize somthing and give control to the GUI ///
#/////////////////////////////////////////////////////

# Outer tracker :
style = pmx.Style()
style.setColor('red')

uiSvc = pmx.uiSvc()
uiSvc.visualize('/dd/Structure/LHCb/AfterMagnetRegion/T/OT')

region = pmx.Region()

for index in range(0, 4):
    # Clear event part of the scene :
    region.eraseEvent()
    # Get an event :
    uiSvc.nextEvent()
    # Visualize MCParticles :
    style.setColor('yellow')
    uiSvc.visualize('/Event/MC/Particles')
    # Give control to the GUI :
    pmx.toui()
