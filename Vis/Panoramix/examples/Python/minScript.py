###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *
from Configurables import LHCbApp, DstConf
#import CommonParticles.StandardBasic
#import CommonParticles.StandardIntermediate
importOptions('$STDOPTS/DecodeRawEvent.py')
DstConf().EnableUnpack = ["Reconstruction", "Stripping"]
LHCbApp()
appConf = ApplicationMgr(OutputLevel=INFO, AppName='Phireco')
appConf.ExtSvc += ['DataOnDemandSvc']

import GaudiPython
appMgr = GaudiPython.AppMgr()
sel = appMgr.evtsel()
sel.open('/media/Data/Phi/gauss_1.dst')
#
evt = appMgr.evtsvc()
appMgr.run(1)
print evt['Rec/Vertex/Primary'].size()
print evt['Rec/Vertex/V0'].size()
print evt['Phys/StdKs2PiPiLL/Particles'].size()
appMgr.run(1)
print evt['Rec/Vertex/Primary'].size()
print evt['Rec/Vertex/V0'].size()
print evt['Phys/StdKs2PiPiLL/Particles'].size()


def printAlgMap():
    x = DataOnDemandSvc().AlgMap
    for a in x:
        print a, ':', x[a]
