###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#
# Example of script to work on picked objects.
#

import pmx
uiSvc = pmx.uiSvc()
from ROOT import TPython


# first way (bypassing the OnX accessor system).
# It gets directly the string deposited on the picked SoShape name.
# The string is assumed to be of the form "<class>/<address>".
def first_way():
    names = uiSvc.getHighlightedSoShapeNames()
    number = names.size()
    print 'Picked objects (first way) :'
    #print number
    for i in range(0, number):
        s = names[i]  # string stored on the SoShape name (with SoNode.setName)
        # s should be of the form '<class>/<address>'
        #print s
        l = s.split('/')
        if len(l) <= 1:
            print 'syntax error in SoShape name.'
        else:
            sclass = 'LHCb::' + l[0]
            p = uiSvc.topointer(l[1])
            obj = TPython.ObjectProxy_FromVoidPtr(p, sclass)
            print obj


# Second way that uses the OnX data accessor system.
def second_way():
    import OnX
    session = OnX.session()
    da = session.accessorManager()  # data accessor.
    da.collect('SceneGraph(@current@)', 'highlight==true')
    da.filter('name')
    handlerIterator = da.handlersIterator()
    print 'Picked objects (second way) :'
    while 1:
        handler = handlerIterator.handler()
        if handler == None: break
        accessor = handler.type()
        cls = 'LHCb::' + accessor.name()
        # print cls
        swig_p = handler.object()  # void* but wrapped by SWIG
        # print swig_p
        lcg_p = uiSvc.topointer(
            OnX.smanip.p2sx(swig_p))  # void* but wrapped by LCG
        #print lcg_p
        obj = TPython.ObjectProxy_FromVoidPtr(lcg_p, cls)
        print obj
        handlerIterator.next()
    handlerIterator.thisown = 1
    del handlerIterator


# third way (bypassing also the OnX accessor system).
# Similar to the first way but by returning a
# vector of ContainedObject*.
# This permits to avoid the usage of the TPython.ObjectProxy_FromVoidPtr.
# WARNING : it assumes that the address on the SoShape deposited string
# is a ContainedObject* !
def third_way():
    objs = uiSvc.getHighlightedContainedObject()
    #number = objs.size()
    print 'Picked objects (third way) :'
    #print number
    for obj in objs:
        print obj


first_way()
second_way()
third_way()
