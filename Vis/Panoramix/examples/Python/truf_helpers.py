###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Panoramix import *

from ROOT import Double
tsvc = appMgr.toolsvc()
distCalc = tsvc.create(
    'LoKi__TrgDistanceCalculator', interface='IDistanceCalculator')
imp = Double()
chi2 = Double()
rc = distCalc.distance(atrack, thePV, imp, chi2)
print imp, chi2


def ip(p):
    for pv in evt['Rec/Vertex/Primary']:
        rc = distCalc.distance(p, thePV, imp, chi2)
        print ip, chi2


def ipofparticles(arg):
    ip = 0.
    error = 0.
    selection = ui().parameterValue('mainTree.selection')
    location = selection.replace('\n', '/')
    if location.find('Phys') < 0:
        print 'no Phys found in path'
    else:
        temp = location.split('/')
        location = temp[1] + '/' + temp[2] + '/Particles'
        container = evt[location]
        if container:
            print 'ip [mm],  chi2'
            if arg == 'all':
                for p in container:
                    ip(p)
            else:
                p = evt[location][p]
                ip(p)
