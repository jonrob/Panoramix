###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#
# Test script for common SoPage manipulations.
#

import pmx

page = pmx.Page()
page.setTitle('gb_test_page')

page.createDisplayRegions(2, 3, 0)
page.setCurrentRegion(1)

page.createPlotterRegion(0.2, 0.1, 0.4, 0.4)

region = page.currentRegion()
page.connectCurrentRegion(3)

# Region :
region = pmx.Region()
region.setBackgroundColor('blue')

page.setCurrentRegion(2)
region = pmx.Region()
region.setBackgroundRGB(1, 1, 0)
region.eraseEvent()
region.clear()
