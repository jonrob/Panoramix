###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from panoramixmodule import *

if not ui().findWidget('Viewer_XY'):
    ui().createComponent('Viewer_XY', 'PageViewer', 'ViewerTabStack')
    ui().setCallback('Viewer_XY', 'collect', 'DLD',
                     'OnX viewer_collect @this@')
    ui().setCallback('Viewer_XY', 'popup', 'DLD',
                     'Panoramix Panoramix_viewer_popup')
    ui().setParameter(
        'Viewer_XY.popupItems',
        'Current region\nNo highlighted\nTrack_MCParticle\nMCParticle_Track\nParticle_MCParticle\nParticle_Daughters\nMCParticle_Clusters\nCluster_MCParticles'
    )
    ui().setParameter('Viewer_XY.viewing', '3D')

ui().setCurrentWidget(ui().findWidget('Viewer_XY'))
session().setParameter('modeling.color', 'forestgreen')
ui().executeScript('DLD', 'Panoramix layout_velo')

Page().setTitle('XY view')
Page().titleVisible(True)
