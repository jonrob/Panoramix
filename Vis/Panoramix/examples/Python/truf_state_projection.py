###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#
# change projection to look along the track trajectory
#
from panoramixmodule import *

key_of_track = 4

tracklocation = 'Rec/Track/Best'
tc = evt[tracklocation]

session().setParameter('Track.location', tracklocation)
session().setParameter('modeling.useExtrapolator', 'true')

page = OnX.session().ui().currentPage()

t = tc[key_of_track]
print 'momentum ', t.p(), t.pt()

one_state = t.states()[0]

Viewer().removeAutoClipping()
tx = one_state.tx()
ty = one_state.ty()
Camera().lookAt(tx, ty, -1.)

Object_visualize(t)
