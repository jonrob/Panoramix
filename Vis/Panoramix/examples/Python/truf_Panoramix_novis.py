###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import gaudimodule

# to get linker package working
from LinkerInstances.eventassoc import *

appMgr = gaudimodule.AppMgr(
    outputlevel=5, joboptions='$PANORAMIXROOT/options/Panoramix_novis.opts')
appMgr.config()

# for new track event model:
appMgr.loaddict('TrackEventDict')
appMgr.loaddict('TrEventDict')
appMgr.loaddict('TrFitEventDict')
#appMgr.loaddict('TrackPythonDict')

appMgr.initialize()

sel = appMgr.evtsel()
evt = appMgr.evtsvc()
his = appMgr.histsvc()
det = appMgr.detsvc()

# from PyLCGDict import *
gaudimodule.PyLCGDict.loadDict('SealDictDict')
std = gaudimodule.PyLCGDict.makeNamespace('std')

#/////////////////////////////////////////////////////////////////////////////
# sys procedures :
#/////////////////////////////////////////////////////////////////////////////


def sys_import(aModule):
    import sys
    if sys.modules.has_key(aModule):
        reload(sys.modules[aModule])
    else:
        exec ('import %s' % aModule)


def sys_dump_main():
    import sys
    print dir(sys.modules['__main__'])


def sys_exists(name):
    try:
        eval(name)
        return 1
    except NameError:
        return 0
