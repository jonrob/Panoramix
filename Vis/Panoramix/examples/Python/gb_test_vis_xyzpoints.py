###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#
# Example script to visualize a Gaudi::XYZPoint from Python.
#
# Execute this script from a Panoramix Python callback.
#

import PyCintex

import sys
if sys.platform == 'win32':
    PyCintex.gbl.gSystem.Load("MathRflx.dll")
else:
    PyCintex.gbl.gSystem.Load("libMathRflx.so")

XYZPoint = PyCintex.gbl.ROOT.Math.XYZPoint
#XYZVector = PyCintex.gbl.ROOT.Math.XYZVector

point = XYZPoint(0., 0., 0.)
point.SetX(3.)

print point.X()

import pmx

pmx.session().setParameter('modeling.color', 'red')
pmx.uiSvc().visualize(point)

# Representation flags :
LINES = 0  # default
POINTS = 1
SEGMENTS = 2
POLYGON = 3

# A square done with a std::vector<HepPoint3D> :
import GaudiPython
gbl = GaudiPython.gbl
size = 4000
points = gbl.std.vector(XYZPoint)()
point = XYZPoint()
point.SetX(0)
point.SetY(0)
points.push_back(point)
point.SetX(size)
point.SetY(0)
points.push_back(point)
point.SetX(size)
point.SetY(size)
points.push_back(point)
point.SetX(0)
point.SetY(size)
points.push_back(point)
point.SetX(0)
point.SetY(0)
points.push_back(point)
print points[1].x()
# Visualize the square with lines :
pmx.session().setParameter('modeling.color', 'green')
pmx.uiSvc().visualize(points, LINES)

# Visualize the square with markers :
pmx.session().setParameter('modeling.color', 'blue')
pmx.session().setParameter('modeling.markerStyle', 'cross')  #plus, cross, star
pmx.session().setParameter('modeling.markerSize', '9')  # 5,7,9
pmx.uiSvc().visualize(points, POINTS)

# Visualize segments :
points = gbl.std.vector(XYZPoint)()
point = XYZPoint()
point.SetX(2 * size + 0)
point.SetY(0)
points.push_back(point)
point.SetX(2 * size + size)
point.SetY(0)
points.push_back(point)
point.SetX(2 * size + size)
point.SetY(size)
points.push_back(point)
point.SetX(2 * size + 0)
point.SetY(size)
points.push_back(point)
point.SetX(2 * size + 0)
point.SetY(0)
points.push_back(point)
print points[1].x()
pmx.session().setParameter('modeling.color', 'magenta')
pmx.uiSvc().visualize(points, SEGMENTS)

# Visualize a polygon :
points = gbl.std.vector(XYZPoint)()
point = XYZPoint()
point.SetX(3 * size + 0)
point.SetY(0)
points.push_back(point)
point.SetX(3 * size + size)
point.SetY(0)
points.push_back(point)
point.SetX(3 * size + size)
point.SetY(size)
points.push_back(point)
point.SetX(3 * size + 0)
point.SetY(size)
points.push_back(point)
point.SetX(3 * size + 0)
point.SetY(0)
points.push_back(point)
print points[1].x()
pmx.session().setParameter('modeling.color', 'cyan')
pmx.uiSvc().visualize(points, POLYGON)
