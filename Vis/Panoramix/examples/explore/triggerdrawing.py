###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import ExploreDraw as D
import ExploreLib as L


def draw():
    ui = D.ui()
    trigsel = ui.parameterValue('sl_trigger.selection')
    cands = ui.parameterValue('sl_trigcands.selection')

    if cands == 'all' or cands == '':
        D.d_hlt(trigsel)

    else:
        seltype = D.HLTSUM.selectionType(trigsel)

        if seltype == 'Track':
            sum = D.HLTSUM.selectionTracks(trigsel)
            D.d_track(sum[int(cands)])

        elif seltype == 'Vertex':
            sum = list(D.HLTSUM.selectionVertices(trigsel))
            D.d_vertex(sum[int(cands)])

        elif seltype == 'Particle':
            sum = list(D.HLTSUM.selectionParticles(trigsel))
            D.d_part(sum[int(cands)])

        else:
            print 'd_hlt - Selection type Unknown'
            return


def drawSW():
    ui = D.ui()
    trigsel = ui.parameterValue('sl_trigger.selection')
    seeds = D.HLTSUM.selectionTracks(trigsel)
    cands = ui.parameterValue('sl_trigcands.selection')

    if cands == 'all' or cands == '':
        for seed in seeds:
            D.d_sw(seed)

    else:
        D.d_sw(seeds[int(cands)])


def drawSWIDs():
    ui = D.ui()
    trigsel = ui.parameterValue('sl_trigger.selection')
    seeds = D.HLTSUM.selectionTracks(trigsel)
    cands = ui.parameterValue('sl_trigcands.selection')

    if cands == 'all' or cands == '':
        for seed in seeds:
            D.d_swids(seed)

    else:
        D.d_swids(seeds[int(cands)])


def updateTriggers():
    ui = D.ui()
    allsels = D.HLTSUM.selections()
    activesel = ui.parameterValue('cb_trig.value')
    filtersels = L.findStrOnList(activesel, allsels)
    ui.setParameter('sl_trigger.items', L.StrList2Items(filtersels))


def updateCandidates():
    ui = D.ui()
    trigsel = ui.parameterValue('sl_trigger.selection')
    ncands = D.HLTSUM.selectionNCandidates(trigsel)
    candsstr = ['all']
    for i in range(ncands):
        candsstr.append(str(i))
    ui.setParameter('sl_trigcands.items', L.StrList2Items(candsstr))
