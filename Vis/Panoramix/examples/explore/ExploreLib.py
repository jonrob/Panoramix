###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import GaudiPython
from ROOT import Double

#import LoKiPhys.decorators as L


def findStrOnList(string, lista):
    res = []

    if string == 'all':
        return lista

    for l in lista:
        if l.find(string) > -1:
            res.append(l)
    return res


def StrList2Items(stringlist):
    res = ''
    for string in stringlist:
        res += string
        res += '\n'
    return res[:len(res) - 1]


def SelectionsList(selections):
    selstrs = []
    for sel in selections:
        if sel.found():
            selname = sel.location()
            selname = selname.split('/')
            selname = selname[3]
            selstrs.append(selname)
    return selstrs


def cloneObjects(objs):
    cloneobjs = []
    for obj in objs:
        cloneobjs.append(obj.clone())
    return cloneobjs


def recursiveDaughters(particle):
    #pdb.set_trace()

    try:
        daus = particle.daughters()

    except AttributeError:
        return []

    if daus and len(daus) > 0:
        #daus = cloneObjects(daus)
        for dau in daus:
            #daus += cloneObjects( recursiveDaughters(dau) )
            newdaus = recursiveDaughters(dau)
            for newdau in newdaus:
                if newdau not in daus:
                    daus.append(newdau)

        return daus

    else:
        return []


def recursiveTracks(particle):
    alldaus = recursiveDaughters(particle)
    tracks = []
    for dau in alldaus:
        track = dau.proto().track()
        if track:
            tracks.append(track)

    return tracks


#---------------------------------------------------


def ip(p, v):
    ip = Double(0)
    ipchi2 = Double(0)
    DISTC = GaudiPython.AppMgr().toolsvc().create(
        'LoKi::DistanceCalculator', interface='IDistanceCalculator')
    DISTC.distance(p, v, ip, ipchi2)
    return ip


def ipmin(p, vs):
    ipmin = 1e6
    for v in vs:
        ipval = ip(p, v)
        if ipval < ipmin:
            ipmin = ipval
    return ipmin


def findbestpv(bcand, pvs):
    bestpv = None
    minip = 1e6
    for pv in pvs:
        ip = ip(bcand, pv)
        if ip < minip:
            minip = ip
            bestpv = pv
    return bestpv


def filterParticles(partlocation, pvlocation, ptcut, ipcut):
    evt = GaudiPython.AppMgr().evtsvc()
    vertices = evt[pvlocation]
    particles = evt[partlocation]

    if ptcut != 0:
        ptfilter = lambda x: x.pt() > ptcut
        particles = filter(ptfilter, particles)

    if ipcut != 0:
        ipfilter = lambda x: ipmin(x, vertices) > ipcut
        particles = filter(ipfilter, particles)

    return particles
