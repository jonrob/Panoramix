###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from panoramixmodule import *
from PanoramixSys.Configuration import *
import OT_Viewer


def visualizeCosmicITTrack(flag):
    if PanoramixSys().getProp("Cosmics"):
        location = '/Event/Rec/Track/Seed'
        odin = 1
        found = False
        if flag:
            while odin:
                appMgr.run(1)
                odin = evt['DAQ/ODIN']
                if not odin: break
                # temporary hack, does not work by default
                for b in [
                        "Prev3", "Next3", "Prev2", "Next2", "Prev1", "Next1",
                        ""
                ]:
                    for a in appMgr.algorithms():
                        if a.find('PatSeeding') > -1:
                            if appMgr.algorithm(a).Enable == False:
                                appMgr.algorithm(a).execute()
                for b in [
                        "Prev5", "Next5", "Prev4", "Next4", "Prev3/", "Next3/",
                        "Prev2/", "Next2/", "Prev1/", "Next1/", ""
                ]:
                    location = '/Event/' + b + 'Rec/Track/Seed'
                    if evt[location]:
                        if evt[location].size() > 0:
                            for t in evt[location]:
                                for l in t.lhcbIDs():
                                    if l.isIT():
                                        found = True
                                        print 'track with IT hits found in slot', b
                                        break
                                if found: break
                if found: break
        if not odin:
            print 'No Odin bank, probably End of File'
        else:
            thisPage = ui().currentPage()
            curtitle = thisPage.title.getValues()[0]
            found = False
            for t in onlineViews:
                if curtitle == t:
                    found = True
                    break
            session().setParameter('modeling.userTrackRange', 'true')
            session().setParameter('modeling.trackStartz', '5000.')
            session().setParameter('modeling.trackEndz', '20000.')
            if found:
                x(onlineViews[t])
                Style().setRGB(0., 1., 0.2)
                for i in range(Page().fPage.getNumberOfRegions()):
                    Page().setCurrentRegion(i)
                    uiSvc().visualize(evt[location])
                    ui().synchronize()
            else:
                title = 'CosmicOTTrackView'
                if curtitle != title:
                    Page().setTitle(title)
                    OT_Viewer.setup()
                OT_Viewer.view3d(location)

    else:
        print "Panoramix not setup for IT cosmic tracks, missing option --Cosmics"
