###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from panoramixmodule import *
import Onlinemodule
import ROOT
from ExploreDraw import *


def createPage(title, nregions):
    page = ui().currentPage()
    page.deleteRegions()
    Page().setTitle(title)
    Page().titleVisible(True)
    if nregions == 5: Page().createDisplayRegions(2, 3, 0)
    if nregions == 4: Page().createDisplayRegions(2, 2, 0)
    if nregions == 3: Page().createDisplayRegions(3, 1, 0)
    if nregions == 2: Page().createDisplayRegions(2, 1, 0)
    if nregions == 1: Page().createDisplayRegions(1, 1, 0)
    ui().currentWidget().cast_ISoViewer().setHeadlight(1)
    Style().setWireFrame()


def clearRegions():
    for i in range(Page().fPage.getNumberOfRegions()):
        Page().setCurrentRegion(i)
        Page().currentRegion().clear("dynamicScene")


def setCamera(what, l0Dict):
    """
    Camera options:
      Camera().setPosition(x,y,z)
      Camera().setOrientation(x,y,z,phi)
      Camera().setHeight(height)
      Camera().setNearFar(near,far)
      Camera().lookAt(x,y,z)
      Camera().pointAt(x,y,z)
    """

    def caloFront():
        Camera().setHeight(9000.)
        Camera().setPosition(0., 0., 20000.)
        Camera().setOrientation(0., 0., 1., 0)
        Camera().setNearFar(-5000., 20000.)

    def caloTop():
        Camera().setPosition(32.624, 10.1058, 15560)
        Camera().setOrientation(-0.57632, -0.579403, -0.576323, 2.09132)
        Camera().setHeight(8563.89)
        Camera().setNearFar(-5000., 20000.)

    def caloRightside():
        Camera().setPosition(-0.008789, 0., 15298.7)
        Camera().setOrientation(0., -1., 0., 1.5708)
        Camera().setHeight(6950.58)
        Camera().setNearFar(-5000., 20000.)

    def muonFront():
        Camera().setPosition(0., 0., 9000)
        Camera().setOrientation(0., 0., 1., 0.)
        Camera().setHeight(10992.6)
        Camera().setNearFar(-11000., 30000.)

    def muonTop():
        Camera().setPosition(-130.22, 9.25, 15100)
        Camera().setOrientation(-0.57632, -0.579403, -0.576323, 2.09132)
        Camera().setHeight(13000)
        Camera().setNearFar(-5000., 20000.)

    def muonRightside():
        Camera().setPosition(0., 13., 15700)
        Camera().setOrientation(0., -1., 0., 1.5697)
        Camera().setHeight(10643.5)
        Camera().setNearFar(-5000., 20000.)

    if l0Dict['type'] == 'Obj':
        Page().setCurrentRegion(0)
        if what == 'l0_caloObj': caloTop()
        if what == 'l0_muonObj': muonTop()
        Page().setCurrentRegion(1)
        if what == 'l0_caloObj': caloRightside()
        if what == 'l0_muonObj': muonRightside()
    if l0Dict['type'] == 'L0DU':
        muonRightside()
    if l0Dict['type'] == 'Point':
        for i in range(Page().fPage.getNumberOfRegions()):
            Page().setCurrentRegion(i)
            if what == 'l0_caloPoint': caloFront()
            if what == 'l0_muonPoint': muonFront()


def caloType(caloType):
    '''
    Return a string with the name of  the type trigger of L0 Calo Candidate object.
    It's based on Event/L0DUBase.h (L0DUBase::Type).
    Type {Electron=0, Photon, Hadron,Pi0Local,Pi0Global,SumEt,SpdMult,
          HadronSlave1Out,HadronSlave2Out,HadronSlave1In,HadronSlave2In,
          SumEtSlave1Out,SumEtSlave2Out,SumEtSlave1In,SumEtSlave2In};
    '''
    if caloType == 0: return 'Electron'
    if caloType == 1: return 'Photon'
    if caloType == 2: return 'Hadron'
    if caloType == 3: return 'Pi0Local'
    if caloType == 4: return 'Pi0Global'
    if caloType == 5: return 'SumEt'
    if caloType == 6: return 'SpdMult'
    if caloType == 7: return 'HadronSlave1Out'
    if caloType == 8: return 'HadronSlave2Out'
    if caloType == 9: return 'HadronSlave1In'
    if caloType == 10: return 'HadronSlave2In'
    if caloType == 11: return 'SumEtSlave1Out'
    if caloType == 12: return 'SumEtSlave2Out'
    if caloType == 13: return 'SumEtSlave1In'
    if caloType == 14: return 'SumEtSlave2In'
    if caloType < 0 or caloType > 14:
        return '>>> ERROR: No calo type associated to: ' + str(caloType)


def viewCaloPoints(container, i):
    XYZPoint = GaudiPython.gbl.Math.XYZPoint
    for calocand in evt[container]:
        cellIDcand = calocand.id()
        if cellIDcand.calo() == i:
            x = calocand.position().x()
            y = calocand.position().y()
            z = calocand.position().z()
            if (abs(x) + abs(y) + abs(z) != 0):
                session().setParameter('modeling.markerStyle',
                                       'cross')  #plus, cross, star
                session().setParameter('modeling.markerSize', '9')  # 5,7,9
                ## Prs
                if cellIDcand.calo() == 0:
                    Page().setCurrentRegion(0)
                    session().setParameter('modeling.color', 'magenta')
                    ui().echo('    Cand. ' + str(calocand.index()) + ' : ' +
                              caloType(calocand.type()))
                ## Spd
                if cellIDcand.calo() == 1:
                    Page().setCurrentRegion(1)
                    session().setParameter('modeling.color', 'magenta')
                    ui().echo('    Cand. ' + str(calocand.index()) + ' : ' +
                              caloType(calocand.type()))
                ## Ecal
                if cellIDcand.calo() == 2:
                    Page().setCurrentRegion(2)
                    session().setParameter('modeling.color', 'red')
                    ui().echo('    Cand. ' + str(calocand.index()) + ' : ' +
                              caloType(calocand.type()))
                ## Hcal
                if cellIDcand.calo() == 3:
                    Page().setCurrentRegion(3)
                    session().setParameter('modeling.color', 'blue')
                    ui().echo('    Cand. ' + str(calocand.index()) + ' : ' +
                              caloType(calocand.type()))
                point = XYZPoint(x, y, z)
                uiSvc().visualize(point)


def viewMuonPoints(container):
    PosTool = appMgr.toolsvc().create(
        'MuonFastPosTool', interface='IMuonFastPosTool')
    XYZPoint = GaudiPython.gbl.Math.XYZPoint

    def Double(d):
        return ROOT.Double(d)

    x = Double(0.)
    y = Double(0.)
    z = Double(0.)
    deltax = Double(0.)
    deltay = Double(0.)
    deltaz = Double(0.)

    point = XYZPoint(0, 0, 0)
    muons = evt['Raw/Muon/Coords']
    for muoncand in evt[container]:
        for n in range(3):
            Page().setCurrentRegion(n)
            for tile in muoncand.muonTileIDs(n):
                sc = PosTool.calcTilePos(tile, x, deltax, y, deltay, z, deltaz)
                point = XYZPoint(x, y, z)
                session().setParameter('modeling.markerStyle',
                                       'cross')  #plus, cross, star
                session().setParameter('modeling.markerSize', '9')  # 5,7,9
                session().setParameter('modeling.color', 'green')
                uiSvc().visualize(point)
                m = muons(tile)
                if m:
                    session().setParameter('modeling.color', 'red')  #blue')
                    Object_visualize(m)


def L0DUChannels():
    class irange(object):
        def __init__(self, b, e):
            self.begin, self.end = b, e

        def __iter__(self):
            it = self.begin
            while it != self.end:
                yield it.__deref__()
                it.__postinc__(1)

    L0DUReport = evt['Trig/L0/L0DUReport']
    channels = L0DUReport.configuration().channels()
    print "-------------------------------------------------"
    print " "
    print "Channels Decision :  "
    for c in irange(channels.begin(), channels.end()):
        print '%15s: %2i ' % (c.first, L0DUReport.channelDecision(
            c.second.id()))
    print " "
    print "sumEt : " + str(L0DUReport.sumEt())
    print " "
    print "-------------------------------------------------"


def L0DUConfiguration():
    L0DUReport = evt['Trig/L0/L0DUReport']
    print "-------------------------------------------------"
    print L0DUReport.configuration().description()
    print "-------------------------------------------------"


def execute():
    what = session().parameterValue('modeling.l0Trig')
    #############################################################
    # L0 CALO Dictionary
    ##########
    # L0 Calo Candidate Variables:
    # type     < Type of trigger, from L0DUBase::Type
    # ID       < Calo Cell ID. To access MC information...
    # etCode   < integer version of Et.
    # et       < Transverse energy
    # position < Position
    # posTol   < Tolerance on x/y position = 1/2 cell size
    #############################################################
    if what == 'l0_caloObj':
        l0Dict = {
            'type':
            'Obj',
            'name':
            'L0Calo Top/Side View',
            'title':
            '    Top                  L0 Calo                Side',
            'nbRegions':
            2,
            'det': [
                '/dd/Structure/LHCb/DownstreamRegion/Prs',
                '/dd/Structure/LHCb/DownstreamRegion/Spd',
                '/dd/Structure/LHCb/DownstreamRegion/Ecal',
                '/dd/Structure/LHCb/DownstreamRegion/Hcal'
            ],
            'container':
            '/Event/Trig/L0/Calo'
        }
    if what == 'l0_caloPoint':
        l0Dict = {
            'type':
            'Point',
            'name':
            'L0Calo XY View',
            'title':
            'L0 Calo:          Top: Prs and SPD;       Bottom: ECA and and HCAL;',
            'nbRegions':
            4,
            'det': [
                '/dd/Structure/LHCb/DownstreamRegion/Prs',
                '/dd/Structure/LHCb/DownstreamRegion/Spd',
                '/dd/Structure/LHCb/DownstreamRegion/Ecal',
                '/dd/Structure/LHCb/DownstreamRegion/Hcal'
            ],
            'container':
            '/Event/Trig/L0/Calo'
        }
    #############################################################
    # L0 MUON Dictionary
    #############################################################
    if what == 'l0_muonObj':
        l0Dict = {
            'type':
            'Obj',
            'name':
            'L0Muon Top/Side View',
            'title':
            '    Top                       L0 Muon                     Side',
            'nbRegions':
            2,
            'det': [],
            'container':
            '/Event/Trig/L0/MuonCtrl'
        }
    if what == 'l0_muonPoint':
        l0Dict = {
            'type':
            'Point',
            'name':
            'L0Muon XY View',
            'title':
            'L0Muon:    M1                                    M2                                          M3',
            'nbRegions':
            3,
            'det': [],
            'container':
            '/Event/Trig/L0/MuonCtrl'
        }
    #############################################################
    # L0 DU Dictionary
    ##########
    # L0 DU Variables:
    # decision                     < (bool) Global decision
    # timingTriggerBit             < (bool) Timing Trigger Decision
    # forceBit                     < (bool)
    # tck                          < (long)
    # configuration                < (LHCb::L0DUConfig) L0DU algorithm configuration
    # channelsPreDecisionSummaries < (std::map<std::pair<int,unsigned int>,unsigned int>)
    # channelsDecisionSummaries    < (std::map<std::pair<int,unsigned int>,unsigned int>)
    # conditionsValueSummaries     < (std::map<std::pair<int,unsigned int>,unsigned int>)
    # sumEt                        < (int)  Get the sumEt value for a given BX
    # valid                        < (bool) Validity of the report
    #############################################################
    if what == 'l0_du_Channels' or what == 'l0_du_Config':
        l0Dict = {
            'type':
            'L0DU',
            'name':
            'L0DU View',
            'title':
            'L0 DU',
            'nbRegions':
            1,
            'det': [
                '/dd/Structure/LHCb/DownstreamRegion/Prs',
                '/dd/Structure/LHCb/DownstreamRegion/Spd'
            ],  ## Used ExploreDraw for Ecal,Hcal,M1-M5
            'container':
            'Trig/L0/L0DUReport',
            'all_L0': ['/Event/Trig/L0/Calo', '/Event/Trig/L0/MuonCtrl']
        }
    #############################################################

    ## Create the Page and Regions(for PointView)
    ui().echo('>>> ' + l0Dict['name'])
    createPage(l0Dict['title'], l0Dict['nbRegions'])

    ## ???
    onlineViews[l0Dict['title']] = 'L0_Viewer'

    ## Clear regions before showing new event
    clearRegions()

    ## Visualize the detector
    detector = l0Dict['det']
    for i in range(l0Dict['nbRegions']):
        ## Change the region if you use the PointView
        if l0Dict['nbRegions'] > 1: Page().setCurrentRegion(i)
        if what == 'l0_muonPoint':
            Onlinemodule.muonStationContours(i)
        if what == 'l0_caloPoint':
            uiSvc().visualize(detector[i])
        if what == 'l0_muonObj' or what == 'l0_du_Channels' or what == 'l0_du_Config':
            det_muonContours()
        if what == 'l0_caloObj' or what == 'l0_du_Channels' or what == 'l0_du_Config':
            uiSvc().visualize(detector[0])
            uiSvc().visualize(detector[1])
            det_ecal()
            det_hcal()

    ## Set camera
    setCamera(what, l0Dict)

    ## Overwrite default visualization attributes
    Style().dontUseVisSvc()

    ## Visualize
    for i in range(l0Dict['nbRegions']):
        session().setParameter('modeling.color', 'orange')
        if l0Dict['nbRegions'] > 1: Page().setCurrentRegion(i)
        if l0Dict['type'] == 'Obj':
            session().setParameter('modeling.modeling', 'solid')
            uiSvc().visualize(evt[l0Dict['container']])  # For Objects
        if what == 'l0_caloPoint':
            viewCaloPoints(l0Dict['container'], i)  # Calo(Point)
        if what == 'l0_muonPoint':
            viewMuonPoints(l0Dict['container'])  # Muon(Point)
        if what == 'l0_du_Channels' or what == 'l0_du_Config':
            l0Objs = l0Dict['all_L0']
            session().setParameter('modeling.modeling', 'solid')
            for i in range(0, len(l0Objs)):
                uiSvc().visualize(evt[l0Objs[i]])

    ## Use default visualization attributes
    Style().useVisSvc()

    if what == 'l0_du_Channels' or what == 'l0_du_Config':
        ### Only write the report on the white screen under the Page
        du = evt['Trig/L0/L0DUReport']
        #print '\n*************************************************************************'
        #print '\nL0DU Report                                                              '
        #print '\n*************                                                            '
        #print du
        #print '************************************************************************** \n'

        if what == 'l0_du_Channels': L0DUChannels()
        if what == 'l0_du_Config': L0DUConfiguration()


execute()

#########################################
###For Debug
# evt.dump()
# from gaudigadgets import *
# nodes(evt, True, 'Trig')
# event = evt['/Event/Trig/L0']
# doxygen(event)
# dir()
#########################################
