###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from panoramixmodule import *

# Dialog inputs :
eventnr = ui().parameterValue('Panoramix_JumpEvent_input_Eventnumber.value')
runnr = ui().parameterValue('Panoramix_JumpEvent_input_Runnumber.value')

if eventnr == "":
    print "No event number given. "
if eventnr != "":
    evtnr = int(eventnr)
    panojump(evtnr)
    if runnr != "":
        if evtRunNumber():
            enr, runnr = evtRunNumber()
            while runnr != int(runnr):
                panojump(evtnr)
                if not evtRunNumber(): break
                enr, runnr = evtRunNumber()
if evt['DAQ/ODIN']:
    print evt['DAQ/ODIN']
elif evt['Rec/Header']:
    print evt['Rec/Header']
