###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from panoramixmodule import *

locations = [
    '/Event/MC/Rich/CherenkovRings', '/Event/Prev/MC/Rich/CherenkovRings',
    '/Event/PrevPrev/MC/Rich/CherenkovRings',
    '/Event/Next/MC/Rich/CherenkovRings',
    '/Event/NextNext/MC/Rich/CherenkovRings',
    '/Event/LHCBackground/MC/Rich/CherenkovRings'
]


def Visualize_Rich_MCRings():

    #ui().echo('Visualising RICH CK rings in MC')
    #ui().echo(' -> Requires extended RICH data with addition MC information stored')

    Style().dontUseVisSvc()

    # Save current color
    save_color = session().parameterValue('modeling.color')

    # Draw in all regions
    for region in range(Page().fPage.getNumberOfRegions()):

        # Move to each region in turn
        Page().setCurrentRegion(region)

        # each data location
        for location in locations:

            # Draw the centre of the rings
            ui().session().setParameter('modeling.RichRecRingMode', 'center')
            Style().setColor('palegreen')
            uiSvc().visualize(location)
            # Draw parts of the rings in HPD acceptance
            ui().session().setParameter('modeling.RichRecRingMode', 'inside')
            Style().setColor('green')
            uiSvc().visualize(location)
            # Draw parts of the rings outside HPD acceptance
            ui().session().setParameter('modeling.RichRecRingMode', 'outside')
            Style().setColor('darkgreen')
            uiSvc().visualize(location)

    # reset back
    Style().useVisSvc()
    Style().setColor(save_color)


def Visualize_Selected_Rich_MCRings(da):

    #ui().echo('Visualising RICH CK rings in MC')
    #ui().echo(' -> Requires extended RICH data with addition MC information stored')

    # Save current color
    save_color = session().parameterValue('modeling.color')

    # Draw the centre of the rings
    ui().session().setParameter('modeling.RichRecRingMode', 'center')
    Style().setColor('palegreen')
    data_visualize(da)

    # Draw parts of the rings in HPD acceptance
    ui().session().setParameter('modeling.RichRecRingMode', 'inside')
    Style().setColor('green')
    data_visualize(da)

    # Draw parts of the rings outside HPD acceptance
    ui().session().setParameter('modeling.RichRecRingMode', 'outside')
    Style().setColor('darkgreen')
    data_visualize(da)

    # reset back
    Style().setColor(save_color)
