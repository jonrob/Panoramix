###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Onlinemodule import *
import panoramixmodule
import Calo_Viewer, PRplot
ot = det['/dd/Structure/LHCb/AfterMagnetRegion/T/OT']
it = det['/dd/Structure/LHCb/AfterMagnetRegion/T/IT']
XYZPoint = GaudiPython.gbl.ROOT.Math.XYZPoint


def createPage(title, nregions):
    page = ui().currentPage()
    page.deleteRegions()
    Page().setTitle(title)
    Page().titleVisible(True)
    Page().createDisplayRegions(2, 1, 0)
    ui().currentWidget().cast_ISoViewer().setHeadlight(1)
    Style().setWireFrame()
    Page().setCurrentRegion(0)
    uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Ecal')
    Page().setCurrentRegion(1)
    uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Ecal')
    Style().setSolid()


def execute():
    curtitle = ui().currentPage().title.getValues()[0]
    # setup top/side projections for downstream detectors
    title = 'Top         AfterMagnet  Downstream          Side'
    if curtitle != title or action == 'recreate':
        createPage(title, 2)
        onlineViews[title] = 'AfterMagnetDownstream'
    if action == 'visualize':
        ui().echo(' Visualize OT and Downstream detectors')
        # clear regions before showing new event
        for i in range(Page().fPage.getNumberOfRegions()):
            Page().setCurrentRegion(i)
            Page().currentRegion().clear("dynamicScene")
            if i == 0:
                # Camera positioning top:
                Camera().setPosition(0., 2000., 13000.)
                Camera().setHeight(15000.)
                Camera().setOrientation(-0.57735, -0.57735, -0.57735, 2.0943)
                Camera().setNearFar(1., 100000.)
            if i == 1:
                # Camera positioning side:
                Camera().setPosition(-10000., 0., 13500.)
                Camera().setHeight(13000.)
                Camera().setOrientation(0., -1., 0., 1.57)
                Camera().setNearFar(1., 100000.)

# muon hits
            Style().dontUseVisSvc()
            for k in range(5):
                muonStationContours(k)
                g = 1. - 0.5 * float(k) / 6.
                b = 0.5 * k / 6.
                Style().setRGB(0., g, b)
                data_collect(da(), 'MuonCoord', 'station==' + str(k))
                data_visualize(da())
            Style().useVisSvc()

            # calo hits
            Style().dontUseVisSvc()
            if hcalenergy != 'n':
                Style().setColor(hcolor)
                data_collect(da(), 'HcalDigits', '(e>' + hcalenergy + ')')
                data_visualize(da())
            if ecalenergy != 'n':
                Style().setColor(ecolor)
                data_collect(da(), 'EcalDigits', '(e>' + ecalenergy + ')')
                data_visualize(da())
            if prscalenergy != 'n':
                Style().setColor(pcolor)
                data_collect(da(), 'PrsDigits', '(e>' + prscalenergy + ')')
                data_visualize(da())
            if spdcalenergy != 'n':
                Style().setColor(scolor)
                data_collect(da(), 'SpdDigits', '(e>' + spdcalenergy + ')')
                data_visualize(da())
            Style().useVisSvc()
            ######
            # OT layers
            Style().dontUseVisSvc()
            for k in range(3):
                Style().setColor('red')
                data_collect(da(), 'OTTime',
                             'station==' + str(k + 1) + '&&layer==0')
                data_visualize(da())
                Style().setColor('magenta')
                data_collect(da(), 'OTTime',
                             'station==' + str(k + 1) + '&&layer==3')
                data_visualize(da())
                Style().setColor('cyan')
                data_collect(da(), 'OTTime',
                             'station==' + str(k + 1) + '&&layer==1')
                data_visualize(da())
                data_collect(da(), 'OTTime',
                             'station==' + str(k + 1) + '&&layer==2')
                data_visualize(da())
            Style().useVisSvc()
            if i == 0:
                for o in evt['Raw/OT/Times']:
                    channelx = o.channel()
                    modulex = ot.findModule(channelx)
                    name = modulex.name()
                    if name.find('X1layer') == -1 and name.find(
                            'X2layer') == -1:
                        continue
                    if name.find('X1layer') > -1: Style().setColor('red')
                    if name.find('X2layer') > -1: Style().setColor('magenta')
                    lhcbidx = GaudiPython.gbl.LHCb.LHCbID(channelx)
                    trajx = ot.trajectory(lhcbidx, 0.)
                    x = (trajx.beginPoint().x() + trajx.endPoint().x()) / 2.
                    y = (trajx.beginPoint().y() + trajx.endPoint().y()) / 2.
                    z = (trajx.beginPoint().z() + trajx.endPoint().z()) / 2.
                    aPoint = XYZPoint(x, y, z)
                    sc = session().setParameter('modeling.markerStyle',
                                                'cross')  #plus, cross, star
                    sc = session().setParameter('modeling.markerSize',
                                                '9')  # 5,7,9
                    sc = uiSvc().visualize(aPoint)

# IT layers
            Style().dontUseVisSvc()
            for k in range(3):
                Style().setColor('orange')
                data_collect(
                    da(), 'STCluster',
                    'ITorTT=="IT"&&station==' + str(k + 1) + '&&layer==1')
                data_visualize(da())
                data_collect(
                    da(), 'STCluster',
                    'ITorTT=="IT"&&station==' + str(k + 1) + '&&layer==4')
                Style().setColor('yellow')
                data_visualize(da())
                data_collect(
                    da(), 'STCluster',
                    'ITorTT=="IT"&&station==' + str(k + 1) + '&&layer==2')
                data_visualize(da())
                data_collect(
                    da(), 'STCluster',
                    'ITorTT=="IT"&&station==' + str(k + 1) + '&&layer==3')
                data_visualize(da())
            Style().useVisSvc()
            if i == 0:
                for ithit in evt['Raw/IT/Clusters']:
                    channel = ithit.channelID()
                    box = it.findBox(channel)
                    layer = box.findLayer(channel)
                    lhcbid = GaudiPython.gbl.LHCb.LHCbID(channel)
                    traj = it.trajectory(lhcbid, ithit.interStripFraction())
                    if layer.sinAngle() != 0:
                        Style().setColor('yellow')
                    else:
                        Style().setColor('orange')
                    x = (traj.beginPoint().x() + traj.endPoint().x()) / 2.
                    y = (traj.beginPoint().y() + traj.endPoint().y()) / 2.
                    z = (traj.beginPoint().z() + traj.endPoint().z()) / 2.
                    aPoint = XYZPoint(x, y, z)
                    sc = session().setParameter('modeling.markerStyle',
                                                'cross')  #plus, cross, star
                    sc = session().setParameter('modeling.markerSize',
                                                '9')  # 5,7,9
                    sc = uiSvc().visualize(aPoint)

            panoramixmodule.overlay_runeventnr()
            ui().synchronize()
            prefix = ''
            if os.environ.has_key('PANORAMIXPICTURES'):
                prefix = os.environ['PANORAMIXPICTURES'] + '/'
            PRplot.wprint(
                prefix + 'PanoramixEventDisplay_0.jpg',
                format='JPEG',
                quality='100',
                info=False)
            Page().setCurrentRegion(0)


# Dialog inputs :
action = ui().parameterValue('Panoramix_Global_input_action.value')

ecolor = session().parameterValue('Panoramix_Calo_input_ecolor')
if ecolor == '': Calo_Viewer.defaults()
ecolor = session().parameterValue('Panoramix_Calo_input_ecolor')
hcolor = session().parameterValue('Panoramix_Calo_input_hcolor')
scolor = session().parameterValue('Panoramix_Calo_input_scolor')
pcolor = session().parameterValue('Panoramix_Calo_input_pcolor')
ecalenergy = session().parameterValue('Panoramix_Calo_input_ecalenergy')
hcalenergy = session().parameterValue('Panoramix_Calo_input_hcalenergy')
spdcalenergy = session().parameterValue('Panoramix_Calo_input_spdcalenergy')
prscalenergy = session().parameterValue('Panoramix_Calo_input_prscalenergy')
scale = session().parameterValue('Panoramix_Calo_input_scale')
eoret = session().parameterValue('Panoramix_Calo_input_eoret')

if evt['DAQ/ODIN']:
    execute()
