###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import OnX, sys, os
if OnX.session().printer().enabled():
    print 'cannot exit to Python, output to OnX is enabled'
    print 'click File / output -> terminal'
else:
    try:
        if os.uname()[3].find('Ubuntu') > -1:
            sys.excepthook = sys.__excepthook__
    except:
        rc = 'OK'
    OnX.session().ui().exit()
