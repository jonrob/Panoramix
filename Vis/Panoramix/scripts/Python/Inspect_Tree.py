###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from panoramixmodule import *

selection = ui().parameterValue('mainTree.selection')
location = selection.replace('\n', '/')
container = evt[location.replace('Event/', '')]

nrobj_value = ui().parameterValue('Panoramix_InspectEvent_input_dump.value')
dox_value = ui().parameterValue('Panoramix_InspectEvent_input_dox.value')

nmax = 9999999
if nrobj_value != 'all':
    nmax = int(nrobj_value) - 1

dox = False
if dox_value == 'Yes': dox = True

cont = False
keyed_cont = False
if type(container).__name__.find('Keyed') > -1: keyed_cont = True
if type(container).__name__.find('Container') > -1: cont = True

if cont:
    n = 0
    for obj in container:
        key = n
        if keyed_cont: key = obj.key()
        print '----- key = ', key, ' --------'
        print obj
        n += 1
        if n > nmax: break
    if dox:
        obj_class = type(container).__name__.split(',')[0].split('<')[1]
        x = gaudimodule.makeClass(obj_class)()
        doxygen(x)
else:
    print container
    if dox: doxygen(container)
