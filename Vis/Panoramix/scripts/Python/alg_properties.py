###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from panoramixmodule import *


def print_properties(a):
    try:
        properties = gaudi.algorithm(a).properties()
    except:
        properties = []
    print(10 * '-')
    print('Properties of %s' % a)
    print(20 * '-')
    print('')
    for key in properties:
        ptype = gaudi.algorithm(a).getInterface().getProperty(key).type()
        if ptype == 'bool':
            if properties[key] == 1:
                print('%-35s : true ' % (key))
            else:
                print('%-35s : false ' % (key))
        elif type(properties[key].value()) != type(''):
            try:
                first = True
                for x in properties[key].value():
                    if first:
                        first = False
                        print('%-35s : %s ' % (key, x))
                    else:
                        print('%-35s , %s ' % (' ', x))
            except:
                print('%-35s : %s ' % (key, properties[key].value()))
        else:
            print('%-35s : %s ' % (key, properties[key].value()))


gaudi = gaudimodule.AppMgr()
a = ui().callbackValue()
print_properties(a)
