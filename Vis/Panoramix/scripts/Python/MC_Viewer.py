###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from panoramixmodule import *
import math
from ROOT import MakeNullPointer

azoom = 'yes'
MCParticle = gaudimodule.gbl.LHCb.MCParticle
part = MakeNullPointer(MCParticle)
MCDecayFinder = getTool('MCDecayFinder', 'IMCDecayFinder')
MCDebugTool = getTool('PrintMCDecayTreeTool', 'IPrintMCDecayTreeTool')
if not MCDecayFinder:
    MCDecayFinder = tsvc.create('MCDecayFinder', interface='IMCDecayFinder')
if not MCDebugTool:
    MCDebugTool = tsvc.create(
        'PrintMCDecayTreeTool', interface='IPrintMCDecayTreeTool')


def set_color(d, tscale):
    partname = 'Unknown'
    knownPart = partsvc.find(d.particleID())
    if knownPart: partname = knownPart.particle()

    tpos = 0.1
    Style().setColor('grey')
    if d.particleID().hasBottom():
        Style().setColor('white')
    elif d.particleID().hasCharm():
        Style().setColor('magenta')
    elif partname == 'KS0':
        Style().setColor('cyan')
        tpos = 2.
    elif partname == 'K+' or partname == 'K-':
        Style().setColor('red')
        tpos = 1.
    elif partname == 'pi+' or partname == 'pi-':
        Style().setColor('green')
        tpos = 0.5
    elif partname == 'mu+' or partname == 'mu-':
        Style().setColor('blue')
        tpos = 3.
    elif partname == 'e+' or partname == 'e-':
        Style().setColor('darkred')
        tpos = 3.
    elif partname == 'gamma':
        Style().setColor('yellow')
        tpos = 4.
    textpos = str(tscale * tpos)
    session().setParameter('modeling.posText', textpos)


def filter(d):
    e = d.momentum().E()
    ch = d.particleID().threeCharge()
    declength = 9.E19
    if d.endVertices().size() > 0:
        eVx = d.endVertices()[0].position()
        oVx = d.primaryVertex().position()
        declength = math.sqrt((eVx.x() - oVx.x()) * (eVx.x() - oVx.x()) +
                              (eVx.y() - oVx.y()) * (eVx.y() - oVx.y()) +
                              (eVx.z() - oVx.z()) * (eVx.z() - oVx.z()))
    partname = 'Unknown'
    foundObj = partsvc.find(d.particleID())
    if foundObj: partname = foundObj.particle()
    filter1 = (ch == 0 and kind == 'neutral') or (
        kind != 'neutral' and kind != 'charged') or (ch != 0
                                                     and kind == 'charged')
    filter2 = e > energy
    filter3 = declength > stable
    veto1 = (partname == 'KS0' or partname == 'KL0' or partname == 'Lambda')
    mothname = ' '
    if d.mother():
        foundObj = partsvc.find(d.mother().particleID())
        if foundObj: mothname = foundObj.particle()
    veto2 = (mothname == 'KS0' or mothname == 'KL0' or mothname == 'Lambda')
    filter4 = d.originVertex().position().z() < ovx or veto1 or veto2
    return filter1 and filter2 and filter3 and filter4


def dump(d):
    stable = False
    e = d.momentum().E()
    pt = d.pt()
    ch = d.particleID().threeCharge() / 3.
    if d.endVertices().size() == 0:
        stable = True
    else:
        eVx = d.endVertices()[0].position()
    oVx = d.originVertex().position()
    pid = d.particleID().pid()
    try:
        pidmk = d.mother().key()
        pidm = d.mother().particleID().pid()
        pidmn = partsvc.find(pidm).particle()
    except:
        pidm = -1
        pidmk = -1
        pidmn = ' '
    if not stable:
        declength = math.sqrt((eVx.x() - oVx.x()) * (eVx.x() - oVx.x()) +
                              (eVx.y() - oVx.y()) * (eVx.y() - oVx.y()) +
                              (eVx.z() - oVx.z()) * (eVx.z() - oVx.z()))
    else:
        declength = 99999.
    partname = partsvc.find(d.particleID()).particle()
    printout = '%4i' % (d.key()) + '%12s' % (partname) + '%9i' % (
        pid) + '%10.2f' % (e) + '%10.2f' % (pt) + '%5i' % (ch) + '%9.2f' % (
            d.virtualMass()) + '%10.2f' % (declength) + '%5i' % (
                d.endVertices().size()) + '%10i' % (pidmk) + '%12s' % (pidmn)
    return [d.originVertex().position().z(), printout]


# Dialog inputs :
container = ui().parameterValue('SoEvent_MCParticle_input_container.value')
stable = float(ui().parameterValue('SoEvent_MCParticle_input_stable.value'))
ovx = float(ui().parameterValue('SoEvent_MCParticle_input_ovx.value'))
kind = ui().parameterValue('SoEvent_MCParticle_input_kind.value')
decaystr = ui().parameterValue('SoEvent_MCParticle_input_decay.value')
energy = float(ui().parameterValue('SoEvent_MCParticle_input_energy.value'))
azoom = ui().parameterValue('SoEvent_MCParticle_input_azoom.value')
scolor = ui().parameterValue('SoEvent_MCParticle_input_color.value')
linewidth = ui().parameterValue('SoEvent_MCParticle_input_linewidth.value')
text = ui().parameterValue('SoEvent_MCParticle_input_text.value')
textsize = ui().parameterValue('SoEvent_MCParticle_input_textsize.value')
textpos = ui().parameterValue('SoEvent_MCParticle_input_textpos.value')
maxDepth = int(ui().parameterValue('SoEvent_MCParticle_input_maxdepth.value'))
action = ui().parameterValue('SoEvent_MCParticle_input_action.value')

mc = evt[container]
tscale = 1.
if textpos != 'medium':
    tscale = float(textpos)
session().setParameter('modeling.lineWidth', linewidth)
session().setParameter('modeling.sizeText', textsize)
session().setParameter('modeling.posText', textpos)

if text == 'yes':
    session().setParameter('modeling.showText', 'true')
else:
    session().setParameter('modeling.showText', 'false')

if scolor != 'default': Style().setColor(scolor)

if action == 'dump':
    print '%4s' % ('key'), '%12s' % ('Part.Name'), '%7s' % ('pid'), '%12s' % (
        'energy(MeV)'), '%7s' % ('pt(MeV)'), '%4s' % ('ch'), '%8s' % (
            'Mass(MeV)'), '%8s' % ('tau(mm)'), '%3s' % ('nr eVx'), '%14s' % (
                'mother')

sorted_list = []
if decaystr != '':
    decaylist = []
    MCDecayFinder.setDecay(decaystr)
    if MCDecayFinder.hasDecay(mc):
        #  print 'Decay found'
        while MCDecayFinder.findDecay(mc, part) > 0:
            decaylist.append(mc[part.key()])
        for decay in decaylist:
            daughters = gaudimodule.gbl.std.vector('const LHCb::MCParticle*')()
            MCDecayFinder.descendants(decay, daughters)
            dlist = []
            for d in daughters:
                if filter(d):
                    if action == 'visualize':
                        if scolor == 'default': set_color(d, tscale)
                        Object_visualize(d)
                    elif action == 'dump':
                        dlist.append(dump(d))
            dlist.sort()
            sorted_list.append(dlist)
    else:
        print 'no decay found for descriptor:',

else:
    for d in mc:
        if filter(d):
            if action == 'visualize':
                if scolor == 'default': set_color(d, tscale)
                Object_visualize(d)
            elif action == 'dump':
                sorted_list.append(dump(d))
    sorted_list.sort()

if action == 'dump':
    for t in sorted_list:
        for l in t:
            print l[1]

if action == 'printTree':
    for decay in decaylist:
        MCDebugTool.printTree(decay, maxDepth)

if action == 'visualize' and azoom != 'no' and len(decaylist) > 0:
    page = OnX.session().ui().currentPage()
    # try current region
    region = page.currentRegion()
    type = region.getTypeId().getName().getString()
    test = type != "SoRulerRegion" and type != "SoImageRegion" and type != "SoTextRegion"
    if not test:
        found = False
        for n in range(page.getNumberOfRegions()):
            region = page.getRegion(n)
            type = region.getTypeId().getName().getString()
            test = type != "SoRulerRegion" and type != "SoImageRegion" and type != "SoTextRegion"
            if test:
                found = True
                page.setCurrentRegion(n)
                break
        if not found: print "no display regions found. Don't know what to do."
    ui().executeScript('DLD', 'Panoramix layout_rulers')
    oVx = decaylist[0].originVertex().position()
    eVx = decaylist[0].endVertices()[0].position()
    height = math.sqrt((eVx.x() - oVx.x()) * (eVx.x() - oVx.x()) +
                       (eVx.y() - oVx.y()) * (eVx.y() - oVx.y())) * 5.
    if height < 0.001: height = 100.
    if azoom == 'xy-proj':
        Camera().setPosition(oVx.x(), oVx.y(), oVx.z())
        Camera().setHeight(height)
        Camera().setOrientation(0, 1, 0, 3.14)
        Camera().setNearFar(1., 5000.)
        tsf = region.cast_SoDisplayRegion().getTransform()
        tsf.scaleFactor.setValue(iv.SbVec3f(1., 1., 1.))
    if azoom == 'zx-proj':
        height = abs(eVx.z() - oVx.z()) * 5.
        position = '%6.2f' % (oVx.x()) + ' %6.2f' % (oVx.y()) + ' %6.2f' % (
            oVx.z() + height * 0.7)
        Camera().setPosition(oVx.x(), oVx.y(), oVx.z() + height * 0.7)
        Camera().setHeight(height)
        Camera().setOrientation(-0.57735, -0.57735, -0.57735, 2.0943)
        Camera().setNearFar(1., 5000.)
        tsf = region.cast_SoDisplayRegion().getTransform()
        tsf.scaleFactor.setValue(iv.SbVec3f(5., 5., 1.))
