###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from panoramixmodule import *

if evt['MC/Header']:
    print 'Next event: Run number:', evt['MC/Header'].runNumber(
    ), 'Event number:', evt['MC/Header'].evtNumber()
elif evt['Rec/Header']:
    print 'Next event: Run number:', evt['Rec/Header'].runNumber(
    ), 'Event number:', evt['Rec/Header'].evtNumber()
