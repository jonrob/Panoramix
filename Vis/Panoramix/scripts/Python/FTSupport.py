###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from panoramixmodule import *
import math


def makeFTClusterContainer():
    if evt['/Event/Raw/FT/Clusters']: return
    bank = evt['Raw/FT/RawClusters']
    newcont = gaudimodule.makeClass(
        'KeyedContainer<LHCb::FTCluster,Containers::KeyedObjectManager<Containers::hashmap> >'
    )
    FTcont = newcont()
    GaudiPython.setOwnership(FTcont, False)
    for ftc in bank:
        cid = ftc.channelID()
        fc = GaudiPython.gbl.LHCb.FTCluster(cid, ftc.fraction(), ftc.size(),
                                            ftc.charge())
        GaudiPython.setOwnership(fc, False)
        ly = cid.layer()
        s = int(math.modf(ly / 4.)[0] * 4)
        if s == 0 or s == 3: Style().setColor('blue')
        else: Style().setColor('orange')
        rc = FTcont.add(fc)
    evt.registerObject('/Event/Raw/FT/Clusters', FTcont)


def displayFTClusters():
    makeFTClusterContainer()
    for fc in evt['Raw/FT/Clusters']:
        cid = fc.key()
        ly = cid.layer()
        s = int(math.modf(ly / 4.)[0] * 4)
        if s == 0 or s == 3: Style().setColor('blue')
        else: Style().setColor('orange')
        Object_visualize(fc)


def displayUTClusters():
    for cl in evt['Raw/UT/Clusters']:
        s = cl.station()
        if s == 1: Style().setColor('green')
        else: Style().setColor('orange')
        Object_visualize(cl)
