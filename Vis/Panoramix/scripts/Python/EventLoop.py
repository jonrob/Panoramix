###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from panoramixmodule import *

# Dialog inputs :
number = ui().parameterValue('Panoramix_InputEventLoop_input_number.value')
selection = ui().parameterValue(
    'Panoramix_InputEventLoop_input_selection.value')

if selection != 'Yes':
    ui().executeScript('DLD', 'Panoramix Panoramix_InputEventLoop_ok')
else:
    phys_size = 0
    print 'loop until selected event apppears in Phys'
    # loop until more things appear below Phys
    while phys_size < 2:
        appMgr.run(1)
        if not evt['Phys']:
            print 'no Phys container, exit'
            break
        phys_size = evt.leaves(evt['Phys'].registry()).size()
    if phys_size > 1:
        Region().eraseEvent()
        ui().executeScript('Python', 'x(\'print_header\')')
