###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from panoramixmodule import *
from PanoramixSys.Configuration import *
import Velo_Viewer


def check_appMgr():
    flag = False
    for a in appMgr.algorithms():
        if a.find("PatVelo") > -1:
            flag = True
            break
    return flag


def visualizeHaloVeloTrack(flag):
    if not check_appMgr():
        print "not configured for Velo reconstruction"
        return
    else:
        location = '/Event/Rec/Track/Velo'
        odin = 1
        if flag:
            while odin:
                appMgr.run(1)
                odin = evt['DAQ/ODIN']
                if not odin: break
                if evt[location].size() > 0: break
        Page().currentRegion().clear("dynamicScene")
        if not odin:
            print 'No Odin bank, probably End of File'
            return
        thisPage = ui().currentPage()
        curtitle = thisPage.title.getValues()[0]
        found = False
        for t in onlineViews:
            if curtitle == t:
                found = True
                break
        session().setParameter('modeling.userTrackRange', 'true')
        session().setParameter('modeling.trackStartz', '-1000.')
        session().setParameter('modeling.trackEndz', '20000.')
        if found:
            x(onlineViews[t])
        else:
            Velo_Viewer.execute('rz', 'visualize')
        Style().setRGB(0., 1., 0.2)
        if thisPage.title.getValues()[0] == 'VELO RZ':
            session().setParameter('modeling.projection', '-ZR')
        for i in range(Page().fPage.getNumberOfRegions()):
            Page().setCurrentRegion(i)
            uiSvc().visualize(evt[location])
            ui().synchronize()
        session().setParameter('modeling.projection', '')
