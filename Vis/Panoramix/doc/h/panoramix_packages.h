/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/**

@page panoramix_packages The Vis packages

  The Panoramix interactive environment is the association of the Vis
 packages : OnXSvc, VisSvc, SoDet, SoEvent, SoStat, SoHepMC, SoCalo
 and Panoramix itself. These packages are available in the LHCb CVS repository.
 These packages contains C++ code but also various scripts file.

@section panoramix_packages_OnXSvc OnXSvc

  OnXSvc is the package that "Gaudi services" the OnX package (see
 http://www.lal.in2p3.fr/SI/OnX).

  OnXSvc implements the IUserInterfaceSvc abstract class. When installation
 is correctly done, someone can load the OnXSvc from a job
 option file. The XML OnX top file describing the GUI could then be
 specified in the job options as long as the desired GUI toolkit. For example :

@verbatim
ApplicationMgr.DLLs = {
 "DetDesc"
,"OnXSvc"
,...
};
ApplicationMgr.ExtSvc = {
 "XmlCnvSvc"
,"OnXSvc"
,"SoConversionSvc"
,...
};
// Fundamental in order to have a responding GUI :
ApplicationMgr.Runable = "OnXSvc";
// GUI toolkit :
#ifdef WIN32
OnXSvc.Toolkit = "Win";
#else
OnXSvc.Toolkit = "Xt";
#endif
//OnXSvc.Toolkit = "Gtk";
//OnXSvc.Toolkit = "Qt";
// XML file describing the GUI :
OnXSvc.File = "$PANORAMIXROOT/scripts/OnX/Panoramix.onx";
// If nothing given, the ONXFILE environment variable is searched.
@endverbatim

  Note that OnXSvc is over GaudiKernel and does not depend directly
 of LHCb related packages. (This information is for people that may think to
 use OnX for doing the interactivity and visualization of other
 Gaudi based experiments).

@section panoramix_packages_VisSvc VisSvc

  VisSvc permits to customize data visualization attributes with XML.

@section panoramix_packages_SoDet SoDet package

  The SoDet package contains the "So converters" to visualize the geometry
 with Inventor ("So" is more or less the namespace of Inventor).

  To permit a fast startup in case of "targeted visualization",
 the display could be customized, through the Panoramix.opts,
 to load only the SoDet library and not the event, statistics, etc...
 libraries of the other So packages. This is true for the other packages ;
 someone wanted to display Gaudi histograms can customize to load only
 the SoStat package.

@section panoramix_packages_SoEvent SoEvent package

  The SoEvent package contains the event data So converters : MC,
 TrStoredTrack, [Velo, IT, OT]Cluster, etc....

  Note that we do not compell people to deposit all their "So" converters
 in the SoEvent package. Some people may wish to
 have data converters related to a given subdetector in a package attached
 to this subdetector. The SoCalo package should be seen as a template
 to create detector related packages.

@section panoramix_packages_SoCalo SoCalo package

 The SoCalo package contains So converters for calorimeters data.

@section panoramix_packages_SoStat SoStat package

 The SoStat contains converters to display Gaudi histograms.
 It uses the HEPVis SoPlotter to visualize histograms.

@section panoramix_packages_SoHepMC SoHepMC package

 The SoHepMC package contains converters to display HepMC related data.

@section panoramix_packages_Panoramix Panoramix

  The Panoramix package contains scripts for the Panoramix environment
 and the main of the Panoramix program. The Panoramix program is a
 GaudiMain that loads the $PANORAMIXROOT/options/Panoramix.opts file.

  The scripts directory contains general scripts needed to start the display.
 It has subdirectories (Python, OnX, XML, sh, DOS) ready to receive scripting
 files for various scripting system used by the environment. For example,
 scripts/OnX contains the Panoramix.onx which is the "head" of the default GUI
 description.

  The options directory contains the Panoramix.opts generic job
 option file.

  The examples directory contains scripts to demonstrate the capabilites
 of the system (and also to test an installation !).

*/
