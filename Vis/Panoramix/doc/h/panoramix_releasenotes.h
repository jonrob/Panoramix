/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/**

@page panoramix_releasenotes v6r0 (July 2003)

  In a general way, the GUI is more complete. Exist now a toolbar,
 tooltips, a tab stack of viewers, a customizable popup menu on the viewer
 in picking mode. A click in a leaf of the tree widget displays the
 selected item.

  Had been introduced an elastic rectangle on the viewer.
 It is used by doing "ctrl+shift+ptr move" to select an area in the viewer.
 In viewing mode it permits to zoom. Hiting the backspace key returns
 to previous position. In picking mode, it permits to collect objects in the
 selected area.

  The GL2PS software had been introduced. It permits
 to produce vector PostScript for 2D and 2D scenes.

  In Vis/SoEvent the SoMuonCoord converter had been introduced to visualize
 muon data.

  It had been proved (R.Beneyton) that Panoramix can work with
 DaVinci. In Vis/SoEvent had been introduced the SoParticle and SoVertex
 converters to visualize DaVinci data. A dedicated dialog had been
 introduced in the "Event/Phys" menu.

  At the technical point of view, Vis/VisSvc had been ported on the
 Xerces-2 software and Panoramix in general had been ported
 on Linux with the g++-3.2 compiler and on Windows(XP) with
 the Visual .Net compiler (VC7). On Windows, the dropdown of the
 comboboxes in dialogs had been restored. On this GUI, someone can
 also use now the Shift/Tab to back pass from one item to another in
 the dialogs.

  By using GAUDI_v12, the GaudiPython module is again available
 within Panoramix.

  For the lxplus installation, the software had been linked with
 the OpenGL coming with RedHat-7.3. This OpenGL is a client/server one,
 then think to upgrade your X server in order to have the OpenGL extensions.
(You see if you have the OpenGL extension by issuing the xdpyinfo command
 on lxplus ; GLX must be in the list of extensions)

@section panoramix_releasenotes_v5r0 v5r0 (November 2002)

  Vis/SoEvent : more data event converters had been added. Someone can
 now visualize the instances of the classes : VeloCluster, ITCluster,
 OTCluster, TrState, TrStoredTrack, VeolClusterOnStoredTrack,
 ITClusterOnStoredTrack, OTClusterOnStoredTrack

  Vis/SoEvent : the introduction in OnX/v12r0 of an easy way to build
 dialog boxes had permit to introduce the "Rec dialog". With it we can
 visualize selectivly some subsets of the TrStoredTracks, like the velo,
 the seed ones, etc... We can visualize some associations like
 the measurements of some selected tracks. We can visualize the MCParticle
 associated to some selected tracks.

  Vis/Panoramix : the www directory content had been moved under the
 doc directory. The web pages are now handled with Doxygen. Text had been
 put in some .h files organized in way that permits to produce a PostScript
 users guide along with the .html files for the web pages.
 The manual is under doc/ps and the web pages under doc/html.

  Vis/Panoramix : the scripts/Python/runWin.py python scripted
 had been introduced (thanks to Pere) to be able to start on Windows
 in a way that permits to bypass the "PATH too long" problem.

  Vis/SoCalo : this package had been updated and comes now with the release
 of Panoramix. It permits to visualize the CaloCluster and CaloDigit instances.

  Vis/VisSvc : this package if now in place. It permits to set some predefined
 visualization attributes for the detector (thanks to Sebastien).

  OnX/v12 : introduction of the dialog boxes.

  CoinGL/v1r1 : introduction of the transparency. Some usage of it could
 be done through the VisSvc or with the Scene/Graphical object editor"
 dialog box.

@section panoramix_releasenotes_v4r0 v4r0

 28/03/2002 : SoLHCb / HepMC : if vtx beg-end distance is null ; visualize
              the GenParticle with a pickable box of size proportional
              to energy.

 28/03/2002 : HepMC visualization : correct the bad treatment of minus sign
              of the Lib / collect system. Then the Lib/v2r0p1 should be
              used in order that the "HepMC/Incoming from highlighted"
              button works.

 22/03/2002 : HepMC visualization : In SoLHCb, the SoMcEventCollection
              had been added to visualize HepMC data.
              The Panoramix/options/Panoramix.opts file had been changed
              to easily setup the HepMC options. The GenDst.root file,
              containing some HepMC events, had been added in
                 Panoramix/data to test things.

 18/03/2002 : Correct SoFree/source/SoShape.cxx in order to avoid a bug
              in Mesa/GLU tessel system appearing when manipulating Rich2
              geometry. SoFree v3r3p1 had been then released and installed
              on /afs/cern.ch/sw/contrib/

 23/01/2002 : (Sebastien Ponce) Removed usage of DDDB variable in the job
              option files.

 21/01/2002 : (Sebastien Ponce) Added support for the VisualizationSvc in
              the option files.

@section panoramix_releasenotes_v3r0 v3r0

 19/12/2001 : (Sebastien Ponce) Remove all depth related items in menus
              since the load on demand of volumes is now implemented

 14/12/2001 : (Sebastien Ponce) Add more common values to the Scene
              pulldown menu. Erase all (and caches) when changing the depth.

 20/11/2001 : (Guy Barrand) Handle detector depth tree representation.
              When giving a /dd element to vidualize ; only
              a given number of sublevels are put in the Inventor scene
              graph. By default the "depth" is one. Note that a level
              in the /dd tree having no representation (no volume) is
              not counted. In particular, if depth is one and someone asks
              to draw an element without volume, the sub hierarchy is
              traversed to find the first level with something to represent.
              From scripting, the depth could be set with set setDepth
              macro. Some common value could be set through the Scene
              pulldown menu.

 20/11/2001 : (Guy Barrand) Have scrollbars for the tree widget.

<P> 23/11/2001 : new version corresponding to a new Gaudi realease

@section panoramix_releasenotes_v2r0 v2r0

<P> 20/11/2001 : (Sebastien Ponce) Make use of the new GaudiKernel.
                 Implements polycones.

<P> 06/11/2001 : (Sebastien Ponce) Adds new button to clear the screen and
                 the caches. This allows to reload geometry in case the XML
                 files changed on disk.

<P> 18/10/2001 : (Sebastien Ponce) new script to launch Panoramix in scripts.
                 The syntax is :
                      panoramix [-noevent]
                 The -noevent flag allows to run without event related
                 facilities, which avoids to wait for SICB at launch time

@section panoramix_releasenotes_v1r0 v1r0

 05/10/2001 : ViewPoint is removed. Things are transfered into
              the Panoramix package. Suppression of the "user" directory
              logic.

@section panoramix_releasenotes_v1 v1

 27/09/2001 : GaudiLab becomes Panoramix.

*/
