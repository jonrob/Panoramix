/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/**

@page panoramix_installation Packages

  To install from source, we assume that the :
 - Gaudi suite of packages is installed,
 - the LHCb suite is installed
 - the OnX suite of the OpenScientist distribution is installed.

  The Lab suite of OpenScientist is not mendatory.
 Refer to respective documentation for
 installation of all these packages. The web site of OpenScientist
 is http://www.lal.in2p3.fr/OpenScientist.

  Panoramix itself contains a group of eight packages under
 the "Vis" directory in the LHCb repository : VisSvc, OnXSvc,
 SoDet, SoEvent, SoStat, SoHepMC, SoCalo, Panoramix.
 See the "Packages" section for a description of these
 packages. The reconstruction order of these packages is given below :

@verbatim
    VisSvc : it needs Gaudi, LHCb suites.
    OnXSvc : it needs VisSvc and OnX suite.
    SoDet, SoEvent, SoStat, SoHepMC, SoCalo : they need OnXSvc
    Panoramix : it needs OnXSvc and optionnaly SoDet, SoEvent,
                SoStat, SoHepMC, SoCalo.
@endverbatim
 The So packages are plugins to Panoramix. By customizing the
 Panoramix/&lt;version&gt;/options/Panoramix.opts file you can connect
 or disconnect them at run time. Edit also the
 Panormix/&lt;version&gt;/cmt/requirements file if you do not want to use
 some of them.

  The Lab analysis suite of OpenScientist is also connectable/disconnectable
 at the Panoramix/cmt/requirements file level. It must be understood
 that these packages are not needed now to display Gaudi histograms.
 This could be done by using the SoStat package that uses the
 HEPVis library (coming with the OnX suite).

  The VisSvc, OnXSvc, So packages contain libraries only.
 There is no program (.exe) there. For the Panoramix package,
 no library have to be reconstructed. In principle, a standard
 GaudiMain program can be used to start Panoramix. For conveniency,
 the Panoramix package comes with a Panoramix.exe program which is
 a standard GaudiMain that loads the $PANORAMIX/options/Panoramix.opts
 file.

@section panoramix_installation_packages Reconstruction with CMT

  Most of things are handled by CMT.
 Then if needed, edit the cmt/requirements files of the packages to adapt to
 your environment.For OnX and Lab related packages, the relationships
 to packages not handled by CMT (like Python, freetype,...)
 are concentrated in the little Interfaces/&lt;external package&gt; coming with
 the OpenScientist release. Note that you do NOT have to customize ALL of them.
 Only the ones related to the choosen GUI and scripting drivers are needed.

  After unpacking, follow the CMT guidelines to install.
 On UNIX (with csh shell flavour) :

@verbatim
     csh> source <path>/CMT/<version>/mgr/setup.csh
     csh> setenv CMTPATH ...
     csh> setenv CMTSITE ....
     csh> setenv SITEROOT ....
     csh> cd <package>/<version>/cmt
     csh> cmt show uses
       to see which packages are needed.
     csh> <edit> requirements
     csh> cmt config
     csh> source setup.csh
     csh> gmake
  If needed :
     csh> gmake <group>
@endverbatim
  On Windows (from a DOS prompt) :

@verbatim
     DOS> call <path>\CMT\<version>\mgr\setup.bat
     DOS> set CMTPATH=...
     DOS> set CMTSITE=...
     DOS> set SITEROOT ....
     DOS> cd <package>\<version>\cmt
     DOS> cmt show uses
       to see which packages are needed.
     DOS> <edit> requirements
     DOS> cmt config
     DOS> call setup.bat
     DOS> nmake /f nmake
  If needed :
     DOS> nmake /f nmake <group>
@endverbatim

  The installation of the OnX package is a touchy part.
 It is here that the connection of the graphic libraries (OpenGL, Inventor),
 the GUI library and the scripting is done. Not all GUI toolkits
 and interpreter "drivers" are needed for the LHCb display.
 For UNIXes and Linux, it is advised to use Open Motif as GUI toolkit
 and the Win32 library on Windows. The scripting language is today
 Python. In the OnX/&lt;version&gt;/cmt/requirements file we have used
 the CMT notion of "group" to handle drivers reconstruction.
 For OnX, by default, the "gmake" ("nmake /f nmake" on Windows)
 reconstructs only the "core" libraries (things over C++ and Inventor). Other
 drivers should be explicitly reconstructed through a :

@verbatim
     UNIX> gmake <group>
     DOS> nmake /f nmake <group>
@endverbatim

  Then for Panoramix, the reconstruction of OnX needs only for UNIXes :

@verbatim
     UNIX> gmake
     UNIX> gmake Xm
     UNIX> gmake Python
@endverbatim
  and for Windows :

@verbatim
     DOS> nmake /f nmake
     DOS> nmake /f nmake Win32
     DOS> nmake /f nmake Python
@endverbatim

  Note that, at Orsay, Panoramix is regulary tested also on Linux with gtk+
 and Qt (two GUI toolkits known by OnX).

*/
