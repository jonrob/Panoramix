/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/**

@page panoramix_download Open Scientist binary installation

  The available tarballs for various platforms are listed below.
 These tarballs are run time kits (RTK). There are not intended
 for developments. In particular there is no include files (.h) in them.
 If you want to develop, do an installation from source
 by following the Installation section.

  Then, the available tarballs are :
 - Redhat-6.1-gxx-2.95.2.
 - Windows.

  According the platform, retrieve at  ftp://ftp.lal.in2p3.fr/pub/LHCb
 one of the file :
@verbatim
     Panoramix-<version>-<platform>.zip
@endverbatim

  Due to firewall protections on both side (your machine and LAL ftp server
 machine) this ftp web access may fail.

  If so, tar files can also be retrieved with ftp (or sftp) command line
 program by doing :
@verbatim
     UNIX> ftp ftp.lal.in2p3.fr
     anonymous
     user name
     ftp> cd pub/LHCb/<version>
     ftp> bin
    (ftp> passive)
     ftp> get Panoramix-<version>-<platform>.zip
     ftp> quit
@endverbatim

  A default event file comes with the tarballs :
@verbatim
     Panoramix/<version>/data/v254r1.dst
@endverbatim
 The Panoramix/&lt;version&gt;/options/Panoramix.opts DATAFILE property
 is set to read this file.

  Common installation problems are:
 - on Windows, you don't have VisualC++ or the access (PATH, etc...)
   to VisualC++ is badly done.
 - on Linux, you try to install a binary on the wrong platform ;
   for example you try to install the RedHat-6.x binary on a RedHat-7.x.

@section panoramix_download_Linux Installation Linux

  Unpack the .zip file with :
@verbatim
     UNIX> cd <path>
     UNIX> unzip Panoramix-<version>-<platform>.zip
    (UNIX> rm Panoramix-<version>-<platform>.zip)
@endverbatim

 Then you can start the program with :
@verbatim
     UNIX> cd Panoramix/<version>/cmt
     UNIX> ./install
      csh> source setup.csh
    (  sh> . ./setup.sh)
    ( csh> setenv DISPLAY <your X11 display> )
  ( or sh> DISPLAY=<your X11 display>;export DISPLAY )
     UNXI> ../<platform>/Panoramix.exe
@endverbatim

@section panoramix_download_Windows Installation Windows

  Use some interactive facility (WinZip, PowerArchiver,
 PowerZip) to decompress and extract the files.

 To start Panoramix, get a DOS comman window and type :
@verbatim
     DOS> cd <path>
         where the Panoramix-<version>.zip file had been unpacked.
     DOS> cd Panoramix\<version>\cmt
     DOS> call INSTALL.bat
     DOS> call setup.bat
     DOS> ..\<platform>\Panoramix.exe
@endverbatim

*/
