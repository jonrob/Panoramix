/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/**

@page panoramix_support Mailing list

  Send comments, corrections, questions, and suggestions
 to lhcb-soft-talk@cern.ch.

@section panoramix_support_team Support in labs and dedicated help

  More dedicated help could be found directly close to :
 - Guy Barrand (barrand@lal.in2p3.fr) for the visualization, GUI and
   scripting machinery, for some "representation" code and for support at LAL
   and IN2P3.
 - Sebastien Ponce (Sebastien.Ponce@cern.ch) for the generic XML
   detector visualization and support person at CERN.
 - Pere Mato (Pere.Mato@cern.ch) for Gaudi questions and Windows support at
   CERN.
 - Ivan Belyaev (Ivan.Belyaev@cern.ch) for calorimeter detector
   and data visualization and support at ITEP.

*/
