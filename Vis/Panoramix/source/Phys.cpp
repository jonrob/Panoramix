/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//
//  All functions here should be OnX callbacks, that is to say
// functions with signature :
//   extern "C" {
//     void callback_without_arguments(IUI&);
//     void callback_with_arguments(IUI&,const std::vector<std::string>&);
//   }
//

#include <OnX/Interfaces/IUI.h>

#include <Lib/Interfaces/IPrinter.h>
#include <Lib/Interfaces/ISession.h>
#include <Lib/Manager.h>
#include <Lib/Out.h>
#include <Lib/smanip.h>
#include <Lib/sout.h>

#include <GaudiKernel/GaudiException.h>
#include <GaudiKernel/IRegistry.h>
#include <GaudiKernel/IService.h>
#include <GaudiKernel/ISvcLocator.h>
#include <GaudiKernel/SmartDataPtr.h>
#include <GaudiKernel/SmartIF.h>

#include <OnXSvc/ISvcLocatorManager.h>

#include "Helpers.h"

//////////////////////////////////////////////////////////////////////////////
inline bool find_particles( IDataProviderSvc* aDataSvc, SmartIF<IDataManagerSvc>& aDataManagerSvc,
                            SmartDataPtr<DataObject>& aDataObject, IPrinter& aPrinter,
                            std::vector<std::string>& aItems )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  std::vector<IRegistry*> children;
  aDataManagerSvc->objectLeaves( aDataObject->registry(), children );
  std::vector<IRegistry*>::iterator it;
  for ( it = children.begin(); it != children.end(); it++ ) {
    std::string path = ( *it )->identifier();

    std::string name = path;
    {
      std::string::size_type pos = path.find_last_of( "/" );
      if ( pos != std::string::npos ) name = path.substr( pos + 1, path.size() - pos );
    }

    if ( name == "Particles" ) aItems.push_back( path );

    try {
      // aLog << MSG::INFO << (*it)->identifier() << endmsg;
      SmartDataPtr<DataObject> obj( aDataSvc, ( *it ) );
      if ( obj ) { // To compell loading of things in the sub directory.
        if ( !find_particles( aDataSvc, aDataManagerSvc, obj, aPrinter, aItems ) ) {
          aItems.clear();
          return false;
        }
      } else {
        Lib::Out out( aPrinter );
        out << "find_particles :"
            << " Unable to load " << Lib::sout( path ) << Lib::endl;
        // return false;
      }
    } catch ( GaudiException& e ) {
      Lib::Out out( aPrinter );
      out << "find_particles :"
          << " Unable to load " << Lib::sout( path ) << Lib::endl;
      aItems.clear();
      return false;
    }
  }
  return true;
}

//////////////////////////////////////////////////////////////////////////////
bool Phys_find_particles( ISession& aSession, std::vector<std::string>& aItems )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  aItems.clear();

  IDataProviderSvc* dataSvc = find_eventDataSvc( aSession );
  if ( !dataSvc ) return false;
  SmartIF<IDataManagerSvc> dataManagerSvc( dataSvc );
  if ( dataManagerSvc == 0 ) {
    Lib::Out out( aSession.printer() );
    out << "Phys_find_particles :"
        << "Unable to get DataManager interface " << Lib::endl;
    return false;
  }

  SmartDataPtr<DataObject> dataObject( dataSvc, "/Event/Phys" );
  if ( dataObject == 0 ) {
    Lib::Out out( aSession.printer() );
    out << "Phys_find_particles :"
        << "Can't get /Event." << Lib::endl;
    return false;
  }

  // log << MSG::INFO << aPath << endmsg;

  return find_particles( dataSvc, dataManagerSvc, dataObject, aSession.printer(), aItems );
}

extern "C" {

//////////////////////////////////////////////////////////////////////////////
void InputParticle_update_locations( IUI& aUI )
//////////////////////////////////////////////////////////////////////////////
// Called by the create callback of SoEvent/scripts/OnX/InputParticle.onx.
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  std::vector<std::string> items;
  if ( !Phys_find_particles( aUI.session(), items ) ) {
    items.clear();
    items.push_back( "/Event/Phys/PreLoadParticles/Particles" );
  }

  std::string sitems = Lib::smanip::tostring( items, "\n" );

  aUI.setParameter( "SoEvent_InputParticle_input_location.items", sitems );
  Lib::smanip::replace( sitems, "Particles", "Vertices" );
  aUI.setParameter( "SoEvent_InputVertex_input_location.items", sitems );
}

} // extern "C"
