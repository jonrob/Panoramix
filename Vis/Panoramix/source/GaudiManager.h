/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef Panoramix_GaudiManager_h
#define Panoramix_GaudiManager_h

#include <Lib/Interfaces/IManager.h>
class ISession;

#include <string>

class IAppMgrUI;

class GaudiManager : public IManager {
public: // IManager
  virtual std::string name() const;
  virtual void*       cast( const std::string& ) const;

public:
  GaudiManager( ISession&, const std::string& );
  ~GaudiManager();

private:
  std::string fName;
  ISession&   fSession;
  IAppMgrUI*  fAppMgrUI;
};

#endif
