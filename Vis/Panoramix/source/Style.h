/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef Panoramix_Style_h
#define Panoramix_Style_h

#include <Lib/Interfaces/ISession.h>

// C++ Pending of the Panoramix.py Python Style class.
// To help converting a .py to C++ if needed.

namespace Panoramix {

  class Style {
  public:
    Style( ISession& aSession );

  public:
    void useVisSvc();
    void dontUseVisSvc();
    // Color :
    void setColor( const std::string& aColor );
    void setRGB( double aR, double aG, double aB );
    void setTransparency( double aValue );
    // Line :
    void setLineWidth( int aValue );
    // Marker :
    void setMarkerSize( int aValue );
    void setMarkerStyle( const std::string& aValue );
    // Text :
    void setTextSize( int aValue );
    void showText();
    void hideText();
    // Geometry :
    void setWireFrame();
    void setSolid();
    void setOpened();
    void setClosed();

  private:
    ISession& fSession;
  };

} // namespace Panoramix

#endif
