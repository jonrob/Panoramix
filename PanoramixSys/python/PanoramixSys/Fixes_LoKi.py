#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
## @file Benedr/Fixes_LoKi.py
#  The helper Python module for Bender application
#
#  This file is a part of
#  <a href="http://cern.ch/lhcb-comp/Analysis/Bender/index.html">Bender project</a>
#  <b>``Python-based Interactive Environment for Smart and Friendly
#   Physics Analysis''</b>
#
#  The package has been designed with the kind help from
#  Pere MATO and Andrey TSAREGORODTSEV.
#  And it is based on the
#  <a href="http://cern.ch/lhcb-comp/Analysis/LoKi/index.html">LoKi project:</a>
#  ``C++ ToolKit for Smart and Friendly Physics Analysis''
#
#  @date   2004-07-11
#  @author Vanya BELYAEV ibelyaev@physics.syr.edu
#
#                    $Revision: 129470 $
#  Last modification $Date: 2011-09-26 12:40:16 +0200 (Mon, 26 Sep 2011) $
#                 by $Author: ibelyaev $
# =============================================================================
"""
The helper module for Bender application.
It applies some last-moment (version-dependent) fixes

This file is a part of BENDER project:
``Python-based Interactive Environment for Smart and Friendly Physics Analysis''

The project has been designed with the kind help from
Pere MATO and Andrey TSAREGORODTSEV.

And it is based on the
LoKi project: ``C++ ToolKit for Smart and Friendly Physics Analysis''

"""
# =============================================================================
__author__ = 'Vanya BELYAEV ibelyaev@physics.syr.edu'
__date__ = "2004-07-11"
__version__ = '$Revision: 129470 $'
__all__ = ()
# =============================================================================

# =============================================================================
## decorate the ranges
# =============================================================================
from LoKiCore.basic import std, Gaudi, LHCb, cpp
import LoKiCore.decorators as LCD

HepMC = cpp.HepMC
_iter_ = LCD._iter_1_
_slice_ = LCD._slice_

_rb = Gaudi.RangeBase_
if not hasattr(_rb, '__iter__') or _rb.__iter__ != _iter_:
    _rb.__iter__ = _iter_
if not hasattr(_rb, '__getslice__') or _rb.__getslice__ != _slice_:
    _rb.__getslice__ = _slice_

import LoKiPhys.Phys
import LoKiMC.MC
import LoKiGen.HepMC

for _t in (
        LHCb.Particle,
        #LHCb.VertexBase  ,
        LHCb.MCParticle,
        LHCb.MCVertex,
        HepMC.GenVertex,
        HepMC.GenParticle):

    _r = _t.Range

    if not hasattr(_r, '__iter__') or _r.__iter__ != _iter_:
        _r.__iter__ = _iter_
    if not hasattr(_r, '__getslice__') or _r.__getslice__ != _slice_:
        _r.__getslice__ = _slice_

for t in (Gaudi.Range_(std.vector('const LHCb::Particle*')),
          Gaudi.Range_(std.vector('const LHCb::VertexBase*')),
          Gaudi.NamedRange_(std.vector('const LHCb::VertexBase*')),
          Gaudi.Range_(std.vector('const LHCb::MCParticle*')),
          Gaudi.Range_(std.vector('const LHCb::MCVertex*')),
          Gaudi.Range_(std.vector('const HepMC::GenParticle*')),
          Gaudi.Range_(std.vector('const HepMC::GenVertex*'))):

    _r = t
    if not hasattr(_r, '__iter__') or _r.__iter__ != _iter_:
        _r.__iter__ = _iter_
    if not hasattr(_r, '__getslice__') or _r.__getslice__ != _slice_:
        _r.__getslice__ = _slice_

# =============================================================================
if __name__ == '__main__':
    print '*' * 120
    print __doc__
    print ' Author  : %s ' % __author__
    print ' Version : %s ' % __version__
    print ' Date    : %s ' % __date__
    print '*' * 120

# =============================================================================
# The END
# =============================================================================
