#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
## @file Benedr/Fixes_Gaudi.py
#  The helper Python module for Bender application
#
#  This file is a part of
#  <a href="http://cern.ch/lhcb-comp/Analysis/Bender/index.html">Bender project</a>
#  <b>``Python-based Interactive Environment for Smart and Friendly
#   Physics Analysis''</b>
#
#  The package has been designed with the kind help from
#  Pere MATO and Andrey TSAREGORODTSEV.
#  And it is based on the
#  <a href="http://cern.ch/lhcb-comp/Analysis/LoKi/index.html">LoKi project:</a>
#  ``C++ ToolKit for Smart and Friendly Physics Analysis''
#  @date   2011-10-21
#  @author Vanya BELYAEV Ivan.Belyaev@cern.ch
#
#                    $Revision: 129470 $
#  Last modification $Date: 2011-09-26 12:40:16 +0200 (Mon, 26 Sep 2011) $
#                 by $Author: ibelyaev $
# =============================================================================
"""
The helper module for Bender application.
It applies some last-moment (version-dependent) fixes

This file is a part of BENDER project:
``Python-based Interactive Environment for Smart and Friendly Physics Analysis''

The project has been designed with the kind help from
Pere MATO and Andrey TSAREGORODTSEV.

And it is based on the
LoKi project: ``C++ ToolKit for Smart and Friendly Physics Analysis''
"""
# =============================================================================
__author__ = 'Vanya BELYAEV ibelyaev@physics.syr.edu'
__date__ = "2011-10-21"
__version__ = '$Revision: 129470 $'
__all__ = ()
# =============================================================================
# copied by T.Ruf to be used for GaudiPython and Panoramix

## some straneglines... Reflex feature ?
from GaudiPython.Bindings import gbl as cpp
import GaudiPython.Bindings

_EvtSel = GaudiPython.Bindings.iEventSelector

if not hasattr(_EvtSel, '_openNew_'):

    def _openNew_(self,
                  stream,
                  typ='ROOT',
                  opt='READ',
                  sel=None,
                  fun=None,
                  collection=None,
                  castor=True):
        """
        Open the file(s) :

        >>> evtSel.open ( 'file.dst' )                   ## open one file
        >>> evtSel.open ( [ 'file1.dst' , 'file2.dst'] ) ## list of files
        >>> evtSel.open ( '/castor/.../file.dst' )       ## open castor file
        >>> evtSel.open ( 'file.raw' )                   ## open RAW file
        >>> evtSel.open ( 'file.mdf' )                   ## open MDF file

        """

        if typ == 'ROOT' and (
                'RootCnvSvc' not in self.g.ExtSvc
                and 'Gaudi::RootCnvSvc/RootCnvSvc' not in self.g.ExtSvc):
            self.g.ExtSvc += ['Gaudi::RootCnvSvc/RootCnvSvc']
            rsvc = self.g.service('RootCnvSvc')
            eps = self.g.service('EventPersistencySvc')
            eps.CnvServices += ['RootCnvSvc']
            ##
            rsvc.CacheBranches = []
            rsvc.VetoBranches = ['*']

        if isinstance(stream, tuple): stream = list(stream)
        if isinstance(stream, str): stream = [stream]

        from PanoramixSys.DataUtils import extendfile2

        files = []
        for f in stream:
            ff = extendfile2(f, castor)
            if sel: ff += " SEL=\'%s\'" % sel
            if fun: ff += " FUN=\'%s\'" % fun
            if collection: ff += " COLLECTION=\'%s\'" % collection
            files += [ff]

        self.Input = files

        ## reinitialize the service
        state_s = self._isvc.FSMState()

        from GaudiPython.Bindings import gbl as cpp
        ## we need here 'INIITALIZED' state
        if cpp.Gaudi.StateMachine.OFFLINE == state_s:
            self.configure()
            self.sysInitialize()
        elif cpp.Gaudi.StateMachine.CONFIGURED == state_s:
            self.sysInitialize()
        elif cpp.Gaudi.StateMachine.RUNNING == state_s:
            self._isvc.sysStop()

        state_s = self._isvc.FSMState()
        if cpp.Gaudi.StateMachine.INITIALIZED != state_s:
            print 'Invalid State of EventSelector ', state_s

        sc = self._isvc.sysReinitialize()
        return self._isvc.sysStart()

    _EvtSel._openNew_ = _openNew_
    _EvtSel.open = _openNew_

## Decorate iDataSvc with proper dir/ls methods '
import AnalysisPython.Dir

## decorate the algorhtms with the proper dumpHistos methods
import GaudiPython.Bindings
_iAlgorithm = GaudiPython.Bindings.iAlgorithm  ##    Algorithm interface
_iAlgTool = GaudiPython.Bindings.iAlgTool  ##         Tool interface
for _t in (_iAlgorithm, _iAlgTool):
    if not hasattr(_t, 'dumpHistos'):
        ## dump all histogram from the given component
        def _dumpHistos(cmp, *args):
            """
            ASCII-Dump for 1D-histograms and 1D-profiles

            >>> cmp = ...  # get the component
            >>> cmp.dumpHistos ()

            """
            _histos = cmp.Histos()
            for _k in _histos:
                _h = _histos[_k]
                if hasattr(_h, 'dump'): print _h.dump(*args)

        _t.dumpHistos = _dumpHistos

if not hasattr(_iAlgorithm, 'isEnabled'):
    _iAlgorithm.isEnabled = lambda self : self.__call_interface_method__("_ialg","isEnabled")

if not hasattr(_iAlgorithm, 'setEnabled'):
    ## enable/disbale
    def __set_Enabled_(self, value):
        self.Enable = True if value else False
        return self.Enable

    _iAlgorithm.setEnabled = __set_Enabled_

# =============================================================================
## decorate stat-entity
_SE = cpp.StatEntity
_iadd_old_ = _SE.__iadd__


def _iadd_new_(s, v):
    if isinstance(v, (int, long)): v = float(v)
    return _iadd_old_(s, v)


_SE.__iadd__ = _iadd_new_
_SE.__str__ = _SE.toString

_AppMgr = GaudiPython.Bindings.AppMgr
if not hasattr(_AppMgr, '_new_topAlg_'):

    ## get the list of top-level algorithms
    def __top_Algs_(self):
        """

        Get the list of top-level algorithms as ``algorithms''

        """
        _algs = self.TopAlg

        def _alg_name_(_n):

            _p = _n.rfind('/')
            if 0 > _p: return _n
            return _n[_p:]

        def _pyAlg(_n):
            for _a in self.pyalgorithms:
                if _n == _a.name(): return _a
            return None

        algs = []
        for _a in _algs:
            # get the proper name
            _n = _alg_name_(_a)
            # check if it is pyalgorithm:
            _pa = _pyAlg(_n)
            if _pa:
                algs += [_pa]
            else:
                _alg = self.algorithm(_a, True)
                algs += [_alg]

        return algs

    _AppMgr._new_topAlg_ = __top_Algs_
    _AppMgr.topAlgs = __top_Algs_

##
if not hasattr(_AppMgr, '_new_allAlg_'):

    ## get the list of all algorithms
    def __all_Algs_(self):
        """

        Get the list of all algorithms as ``algorithms''

        """
        _algs = self.algorithms()

        algs = []
        for _a in _algs:
            algs += [self.algorithm(_a)]
        return algs

    _AppMgr._new_allAlg_ = __all_Algs_
    _AppMgr.allAlgs = __all_Algs_

##
if not hasattr(_AppMgr, '_disable_Tops_'):

    ## get the list of all algorithms
    def __disable_Top_(self):
        """

        Disable all top-level algorithms

        """
        _tops = self.topAlgs()
        _disabled = []
        for _a in _tops:
            if _a.isEnabled():
                _a.setEnable(False)
                _disabled += [_a]

        return _disabled

    _AppMgr._disable_Tops_ = __disable_Top_
    _AppMgr.disableTopAlgs = __disable_Top_

##
if not hasattr(_AppMgr, '_disable_All_'):

    ## get the list of all algorithms
    def __disable_All_(self):
        """
        Disable all algorithms
        """
        _tops = self.allAlgs()
        _disabled = []
        for _a in _tops:
            if _a.isEnabled():
                _a.setEnable(False)
                _disabled += [_a]

        return _disabled

    _AppMgr._disable_All_ = __disable_All_
    _AppMgr.disableAllAlgs = __disable_All_

# =============================================================================
if __name__ == '__main__':
    print '*' * 120
    print __doc__
    print ' Author  : %s ' % __author__
    print ' Version : %s ' % __version__
    print ' Date    : %s ' % __date__
    print '*' * 120

# =============================================================================
# The END
# =============================================================================
