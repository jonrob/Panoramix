###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# - Try to find OpenScientist
# Defines:
#
#  OPENSCIENTIST_FOUND
#  OPENSCIENTIST_ROOT
#  OPENSCIENTIST_INCLUDE_DIRS (not cached)
#  OPENSCIENTIST_LIBRARIES (not cached)
#  OPENSCIENTIST_LIBRARY_DIRS (not cached)
#  OPENSCIENTIST_PYTHON_PATH (not cached)
#  OPENSCIENTIST_BINARY_PATH (not cached)
#
#  Allowed components:
#   - OnX
#   - Vis
#   - OnXXt

if(NOT OPENSCIENTIST_ROOT)
  include(BinaryTagUtils OPTIONAL RESULT_VARIABLE BinaryTagUtils_FOUND)
  if(BinaryTagUtils_FOUND STREQUAL "NOTFOUND")
    set(_compatible_tags ${BINARY_TAG})
  else()
    compatible_binary_tags(_compatible_tags)
  endif()

  set(_osc_path_suffixes $ENV{OPENSCIENTIST_ROOT})
  foreach(_vers 17.0.2.qt5)
    foreach(_tag ${_compatible_tags})
      list(APPEND _osc_path_suffixes osc_vis/${_vers}/${_tag})
    endforeach()
  endforeach()
  if(_osc_path_suffixes)
    list(INSERT _osc_path_suffixes 0 PATH_SUFFIXES)
  endif()

  find_path(OPENSCIENTIST_ROOT Resources/OnX/include ${_osc_path_suffixes})
endif()

set(OPENSCIENTIST_INCLUDE_DIRS)
set(OPENSCIENTIST_LIBRARIES)
set(OPENSCIENTIST_LIBRARY_DIRS)
set(OPENSCIENTIST_PYTHON_PATH)
set(OPENSCIENTIST_BINARY_PATH)

foreach(_comp ${OpenScientist_FIND_COMPONENTS})
  #message(STATUS "Required component ${_comp}")
  if(_comp STREQUAL OnX)
    list(APPEND OPENSCIENTIST_REQUIRED_LIBS
         OnXCore LibXML ourex_expat LibZip LibUtils)
  elseif(_comp STREQUAL Vis)
    list(APPEND OPENSCIENTIST_REQUIRED_LIBS
         ourex_HEPVis ourex_freetype ourex_CoinGL LibUtils)
    list(APPEND OPENSCIENTIST_EXTRA_LIBS -lGLU -lGL)
    # -lGLU -lGL -ldl -L/usr/X11R6/lib -lX11
  elseif(_comp STREQUAL OnXXt)
    list(APPEND OPENSCIENTIST_REQUIRED_LIBS
         OnXxTCore CoinxT)
  else()
    message(FATAL_ERROR "Unknown component: ${_comp}")
  endif()
endforeach()

# Look for libraries
foreach(_lib ${OPENSCIENTIST_REQUIRED_LIBS})
  find_library(OPENSCIENTIST_${_lib}_LIB ${_lib}
               HINTS ${OPENSCIENTIST_ROOT}/lib)
  if(OPENSCIENTIST_${_lib}_LIB)
    list(APPEND OPENSCIENTIST_LIBRARIES
         ${OPENSCIENTIST_${_lib}_LIB})
    get_filename_component(_lib_dir ${OPENSCIENTIST_${_lib}_LIB} PATH)
    list(APPEND OPENSCIENTIST_LIBRARY_DIRS ${_lib_dir})
  endif()
endforeach()
if(OPENSCIENTIST_LIBRARY_DIRS)
  list(REMOVE_DUPLICATES OPENSCIENTIST_LIBRARY_DIRS)
endif()

list(APPEND OPENSCIENTIST_LIBRARIES ${OPENSCIENTIST_EXTRA_LIBS})


# handle the QUIETLY and REQUIRED arguments and set OPENSCIENTIST_FOUND to TRUE if
# all listed variables are TRUE
include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(OpenScientist DEFAULT_MSG
                                  OPENSCIENTIST_ROOT OPENSCIENTIST_LIBRARIES)

if(OPENSCIENTIST_ROOT)

  foreach(_dir CoinGL HEPVis expat Lib OnX)
    list(APPEND OPENSCIENTIST_INCLUDE_DIRS
        ${OPENSCIENTIST_ROOT}/Resources/${_dir}/include)
  endforeach()
  foreach(_dir Slash inlib exlib backcomp)
    list(APPEND OPENSCIENTIST_INCLUDE_DIRS
        ${OPENSCIENTIST_ROOT}/Resources/${_dir})
  endforeach()

  set(OPENSCIENTIST_BINARY_PATH ${OPENSCIENTIST_ROOT}/bin)

  set(OPENSCIENTIST_PYTHON_PATH
      ${OPENSCIENTIST_ROOT}/Resources/CoinPython/scripts
      ${OPENSCIENTIST_ROOT}/Resources/HEPVis/scripts/Python
      ${OPENSCIENTIST_ROOT}/Resources/OnX/scripts/Python
      ${OPENSCIENTIST_BINARY_PATH}
      )

endif()

mark_as_advanced(OPENSCIENTIST_FOUND)
