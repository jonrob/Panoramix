###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
variables:
  TARGET_BRANCH: master

check-copyright:
  image: gitlab-registry.cern.ch/ci-tools/ci-worker:cc7
  script:
    - curl -o lb-check-copyright "https://gitlab.cern.ch/lhcb-core/LbDevTools/raw/master/LbDevTools/SourceTools.py?inline=false"
    - python lb-check-copyright origin/${TARGET_BRANCH}

check-formatting:
  image: gitlab-registry.cern.ch/lhcb-docker/style-checker
  script:
    - if [ ! -e .clang-format ] ; then
    -   curl -o .clang-format "https://gitlab.cern.ch/lhcb-core/LbDevTools/raw/master/LbDevTools/data/default.clang-format?inline=false"
    -   echo '.clang-format' >> .gitignore
    -   git add .gitignore
    - fi
    - curl -o lb-format "https://gitlab.cern.ch/lhcb-core/LbDevTools/raw/master/LbDevTools/SourceTools.py?inline=false"
    - python lb-format --format-patch apply-formatting.patch origin/${TARGET_BRANCH}
  artifacts:
    paths:
      - apply-formatting.patch
    when: on_failure
    expire_in: 1 week

check-python-syntax:
  image: gitlab-registry.cern.ch/ci-tools/ci-worker:cc7
  script:
    - errors=$(find . -name \*.py \! -exec python -m py_compile \{} \; -print 2>/dev/null)
    - if [ -n "$errors" ] ; then
    -   echo "Found errors in Python files:" > ${CI_JOB_NAME}-report.txt
    -   for f in $errors ; do
    -     echo -e "\n--------------------------------------------------------------------------------" >> ${CI_JOB_NAME}-report.txt
    -     echo -e ">>> $f" >> ${CI_JOB_NAME}-report.txt
    -     echo -e "--------------------------------------------------------------------------------" >> ${CI_JOB_NAME}-report.txt
    -     python -m py_compile $f >> ${CI_JOB_NAME}-report.txt 2>&1 || true
    -     echo "" >> ${CI_JOB_NAME}-report.txt
    -   done
    -   cat ${CI_JOB_NAME}-report.txt ; echo -e "\nErrors in $(grep -c '^>>> ' ${CI_JOB_NAME}-report.txt ) Python files"
    -   exit 1
    - fi
  artifacts:
    paths:
      - ${CI_JOB_NAME}-report.txt
    when: on_failure
    expire_in: 1 week
